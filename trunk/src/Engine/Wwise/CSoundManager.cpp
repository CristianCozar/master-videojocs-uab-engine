#pragma once

#include "Utils\Logger\Logger.h"
#include "CSoundManager.h"
#include "Debug\CustomAssert.h"

#include <AK\SoundEngine\Common\AkSoundEngine.h>
#include <AK\SoundEngine\Common\AkTypes.h>
#include <AK\IBytes.h>
#include <AK\SoundEngine\Common\AkMemoryMgr.h>
#include <AK\SoundEngine\Common\AkModule.h>
#include <AK\SoundEngine\Common\IAkStreamMgr.h>
#include <AK\Tools\Common\AkPlatformFuncs.h>
#include <AK\MusicEngine\Common\AkMusicEngine.h>

#include <SoundEngine\Win32\AkFilePackageLowLevelIOBlocking.h>
#include <SoundEngine\Win32\AkSoundEngineDLL.h>

#include "Debug\imgui.h"

#pragma comment(lib, "AkSoundEngineDLL.lib")

namespace engine
{
	CSoundManager::CSoundManager(std::string path)
	{
		m_LastGameObjectID = 1;
		m_Path = path;
	}

	CSoundManager::~CSoundManager()
	{
		Terminate();
	}

	bool CSoundManager::Reload()
	{
		Clean();

		bool l_IsOk = true;
		l_IsOk = LoadSoundBanksXML();
		l_IsOk &= LoadSpeakersXML();

		return l_IsOk;
	}

	bool CSoundManager::Init()
	{
		AkMemSettings memSettings;
		memSettings.uMaxNumPools = 20;

		AkStreamMgrSettings stmSettings;
		AK::StreamMgr::GetDefaultSettings(stmSettings);

		AkDeviceSettings deviceSettings;
		AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

		AkInitSettings l_InitSettings;
		AkPlatformInitSettings l_platInitSettings;
		AK::SoundEngine::GetDefaultInitSettings(l_InitSettings);
		AK::SoundEngine::GetDefaultPlatformInitSettings(l_platInitSettings);

		l_InitSettings.uDefaultPoolSize = 512 * 1024;
		l_InitSettings.uMaxNumPaths = 16;
		l_InitSettings.uMaxNumTransitions = 128;

		l_platInitSettings.uLEngineDefaultPoolSize = 512 * 1024;

		AkMusicSettings musicInit;
		AK::MusicEngine::GetDefaultInitSettings(musicInit);

		AKRESULT eResult = AK::SOUNDENGINE_DLL::Init(&memSettings, &stmSettings, &deviceSettings, &l_InitSettings, &l_platInitSettings, &musicInit);

		if (eResult != AK_Success)
		{
			AK::SOUNDENGINE_DLL::Term();
			LOG_ERROR_APPLICATION("Error initializing CSoundManager");

			return false;
		}

		m_DefaultSpeakerID = GenerateObjectID();
		AK::SoundEngine::RegisterGameObj(m_DefaultSpeakerID);
	}

	void CSoundManager::RegisterSpeaker(const CSceneNode* _speaker)
	{
		Assert(m_GameObjectSpeakers.find(_speaker) == m_GameObjectSpeakers.end(), "El Speaker ya existe.");

		AkGameObjectID id = GenerateObjectID();
		m_GameObjectSpeakers[_speaker] = id;

		Vect3f l_Position = _speaker->GetPosition();
		float l_Yaw = _speaker->GetYaw();
		float l_Pitch = _speaker->GetPitch();

		Vect3f l_Oritentation(cos(l_Yaw) * cos(l_Pitch), sin(l_Pitch), sin(l_Yaw) * cos(l_Pitch));

		AkSoundPosition l_SoundPosition = {};

		l_SoundPosition.Position.X = l_Position.x;
		l_SoundPosition.Position.Y = l_Position.y;
		l_SoundPosition.Position.Z = l_Position.z;

		l_SoundPosition.Orientation.X = l_Oritentation.x;
		l_SoundPosition.Orientation.Y = l_Oritentation.y;
		l_SoundPosition.Orientation.Z = l_Oritentation.z;

		AK::SoundEngine::RegisterGameObj(id);
		AK::SoundEngine::SetPosition(id, l_SoundPosition);
	}

	void CSoundManager::UnregisterSpeaker(const CSceneNode* _speaker)
	{
		auto it = m_GameObjectSpeakers.find(_speaker);
		if (it != m_GameObjectSpeakers.end())
		{
			AK::SoundEngine::UnregisterGameObj(it->second);

			m_FreeObjectIDs.push_back(it->second);

			m_GameObjectSpeakers.erase(it);
		}else{
			Assert(false, "El Speaker que se quiere borrar no existe");
		}
	}

	bool CSoundManager::Load(const std::string &soundbanks_filename, const std::string& speakers_filename)
	{
		m_SoundBanksFilename = soundbanks_filename;
		m_SpeakersFilename = speakers_filename;

		bool l_IsOk = true;
		l_IsOk = LoadSoundBanksXML();
		l_IsOk &= LoadSpeakersXML();

		return l_IsOk;
	}

	bool CSoundManager::InitBanks()
	{
		AkOSChar* path;
		AKRESULT retValue;
		CONVERT_CHAR_TO_OSCHAR(m_Path.c_str(), path);
		retValue = AK::SOUNDENGINE_DLL::SetBasePath(path);
		retValue = AK::StreamMgr::SetCurrentLanguage(L"English(US)");

		AkBankID bankID;
		retValue = AK::SoundEngine::LoadBank("Init.bnk", AK_DEFAULT_POOL_ID, bankID);
		if (retValue != AK_Success)
		{
			return false;
		}

		return true;
	}

	bool CSoundManager::LoadSoundBank(const std::string &bank)
	{
		AkBankID bankID;
		AKRESULT retValue;
		
		retValue = AK::SoundEngine::LoadBank(bank.c_str(), AK_DEFAULT_POOL_ID, bankID);
		if (retValue != AK_Success)
		{
			LOG_ERROR_APPLICATION("Error initializing bank");
			return false;
		}
		
		return true;
	}

	bool CSoundManager::UnloadSoundBank(const std::string& bank)
	{
		AkBankID bankID;
		AKRESULT retValue;

		retValue = AK::SoundEngine::UnloadBank(bank.c_str(), nullptr);
		if (retValue != AK_Success)
		{
			return false;
		}

		return true;
	}


	bool CSoundManager::LoadSoundBanksXML()
	{
		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((m_SoundBanksFilename).c_str());

		if (base::xml::SucceedLoad(error)) {
			tinyxml2::XMLElement *l_SoundBanksInfo = document.FirstChildElement("SoundBanksInfo");
			if (l_SoundBanksInfo)
			{
				tinyxml2::XMLElement *l_SoundBanks = l_SoundBanksInfo->FirstChildElement("SoundBanks");
				if (l_SoundBanks) {
					for (tinyxml2::XMLElement *l_SoundBank = l_SoundBanks->FirstChildElement("SoundBank"); l_SoundBank != nullptr; l_SoundBank = l_SoundBank->NextSiblingElement("SoundBank"))
					{
						tinyxml2::XMLElement* name = l_SoundBank->FirstChildElement("Path");
						std::string l_Name = name->FirstChild()->Value();
						//std::string l_Name = l_SoundBank->GetAttribute<std::string>("name", "");
						if (l_Name != "Init.bnk")
						{
							LoadSoundBank(l_Name);
						}
					}
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Sound Manager (SoundBank) XML '%s' does not exist", m_SoundBanksFilename.c_str());
		}

		return true;
	}

	bool CSoundManager::LoadSpeakersXML()
	{
		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((m_SpeakersFilename).c_str());

		if (base::xml::SucceedLoad(error)) {
			tinyxml2::XMLElement *l_Speakers = document.FirstChildElement("Speakers");
			if (l_Speakers) {
				for (tinyxml2::XMLElement *l_Speaker = l_Speakers->FirstChildElement("Speaker"); l_Speaker != nullptr; l_Speaker = l_Speaker->NextSiblingElement("Speaker"))
				{
					std::string l_Name = l_Speaker->GetAttribute<std::string>("name", "");
					Vect3f l_Position = l_Speaker->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
					Vect3f l_Orientation = l_Speaker->GetAttribute<Vect3f>("orientation", Vect3f(0.0f, 0.0f, 0.0f));

					AkSoundPosition l_SoundPosition = {};

					l_SoundPosition.Position.X = l_Position.x;
					l_SoundPosition.Position.Y = l_Position.y;
					l_SoundPosition.Position.Z = l_Position.z;

					l_SoundPosition.Orientation.X = l_Orientation.x;
					l_SoundPosition.Orientation.Y = l_Orientation.y;
					l_SoundPosition.Orientation.Z = l_Orientation.z;

					AkGameObjectID id = GenerateObjectID();
					m_NamedSpeakers[l_Name] = id;
					AK::SoundEngine::RegisterGameObj(id);
					AK::SoundEngine::SetPosition(id, l_SoundPosition);
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Sound Manager (Speakers) XML '%s' does not exist", m_SpeakersFilename.c_str());
		}

		return true;
	}

	void CSoundManager::Terminate()
	{
		AK::SoundEngine::ClearBanks();
		AK::SoundEngine::UnregisterAllGameObj();

		AK::SOUNDENGINE_DLL::Term();
	}

	void CSoundManager::Clean()
	{
		AK::SoundEngine::ClearBanks();

		for (auto it : m_NamedSpeakers)
		{
			AK::SoundEngine::UnregisterGameObj(it.second);
			m_FreeObjectIDs.push_back(it.second);
		}
		m_NamedSpeakers.clear();
	}


	void CSoundManager::Update(const CCameraController * camera)
	{
		AkSoundPosition l_SoundPosition = {};

		for (auto it : m_GameObjectSpeakers)
		{
			Vect3f l_Position = it.first->GetPosition();
			float l_Yaw = it.first->GetYaw();
			float l_Pitch = it.first->GetPitch();

			Vect3f l_Orientation(cos(l_Yaw) * cos(l_Pitch), sin(l_Pitch), sin(l_Yaw) * cos(l_Pitch));

			l_SoundPosition.Position.X = l_Position.x;
			l_SoundPosition.Position.Y = l_Position.y;
			l_SoundPosition.Position.Z = l_Position.z;

			l_SoundPosition.Orientation.X = l_Orientation.x;
			l_SoundPosition.Orientation.Y = l_Orientation.y;
			l_SoundPosition.Orientation.Z = l_Orientation.z;

			AK::SoundEngine::SetPosition(it.second, l_SoundPosition);
		}

		SetListenerPosition(camera);

		AK::SOUNDENGINE_DLL::Tick();
	}

	void CSoundManager::SetListenerPosition(const CCameraController * camera)
	{
		Vect3f l_Position = camera->GetPosition();
		Vect3f l_Orientation = camera->GetFront();
		Vect3f l_VectorUp = camera->GetUp();

		AkListenerPosition l_ListenerPosition = {};

		l_ListenerPosition.Position.X = l_Position.x;
		l_ListenerPosition.Position.Y = l_Position.y;
		l_ListenerPosition.Position.Z = l_Position.z;

		l_ListenerPosition.OrientationFront.X = l_Orientation.x;
		l_ListenerPosition.OrientationFront.Y = l_Orientation.y;
		l_ListenerPosition.OrientationFront.Z = l_Orientation.z;

		l_ListenerPosition.OrientationTop.X = l_VectorUp.x;
		l_ListenerPosition.OrientationTop.Y = l_VectorUp.y;
		l_ListenerPosition.OrientationTop.Z = l_VectorUp.z;

		AK::SoundEngine::SetListenerPosition(l_ListenerPosition);
	}
		
	void CSoundManager::PlayEvent(const SoundEvent& _Event, const AkGameObjectID& id)
	{
		AK::SoundEngine::PostEvent(_Event.eventName.c_str(), id);
	}

	void CSoundManager::PlayEvent(const SoundEvent& _event)
	{
		PlayEvent(_event, m_DefaultSpeakerID);
	}

	void CSoundManager::PlayEvent(const SoundEvent& _event, const std::string& _speaker)
	{
		auto it = m_NamedSpeakers.find(_speaker);
		if (it != m_NamedSpeakers.end())
		{
			PlayEvent(_event, it->second);
		}
		else{
			Assert(false, "El speaker no existe", _speaker.c_str());
		}
	}

	void CSoundManager::PlayEvent(const SoundEvent& _event, const CSceneNode* _speaker)
	{
		auto it = m_GameObjectSpeakers.find(_speaker);
		if (it != m_GameObjectSpeakers.end())
		{
			PlayEvent(_event, it->second);
		}
		else{
			Assert(false, "El speaker no existe", _speaker->GetName().c_str());
		}
	}


	void CSoundManager::SetSwitch(const SoundSwitchValue& switchValue, const AkGameObjectID& id)
	{
		AKRESULT res = AK::SoundEngine::SetSwitch(switchValue.switchName.c_str(), switchValue.switchValue.c_str(), id);
		Assert(res, "Error al setter el Switch", switchValue.switchName.c_str(), switchValue.switchValue.c_str(), id)
	}

	void CSoundManager::SetSwitch(const SoundSwitchValue& switchValue)
	{
		SetSwitch(switchValue, m_DefaultSpeakerID);
	}

	void  CSoundManager::SetSwitch(const SoundSwitchValue& switchValue, const std::string& _speaker)
	{
		auto it = m_NamedSpeakers.find(_speaker);
		if (it != m_NamedSpeakers.end())
		{
			SetSwitch(switchValue, it->second);
		}
		else{
			Assert(false, "El switch no existe", _speaker.c_str());
		}
	}

	void  CSoundManager::SetSwitch(const SoundSwitchValue& switchValue, const CSceneNode* _speaker)
	{
		auto it = m_GameObjectSpeakers.find(_speaker);
		if (it != m_GameObjectSpeakers.end())
		{
			SetSwitch(switchValue, it->second);
		}
		else{
			Assert(false, "El switch no existe", _speaker->GetName().c_str());
		}
	}


	void CSoundManager::SetRTPCValue(const SoundRTPC& rtpc, float value, const AkGameObjectID& id)
	{
		AKRESULT res = AK::SoundEngine::SetRTPCValue(rtpc.RTPCName.c_str(), (AkRtpcValue) value, id);
		Assert(res, "Error al setter el RTPC", rtpc.RTPCName.c_str(), value, id);
	}

	void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value)
	{
		SetRTPCValue(_rtpc, value, m_DefaultSpeakerID);
	}

	void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value, const std::string& _speaker)
	{
		auto it = m_NamedSpeakers.find(_speaker);
		if (it != m_NamedSpeakers.end())
		{
			SetRTPCValue(_rtpc, value, it->second);
		}
		else{
			Assert(false, "El RTPC no existe", _speaker.c_str());
		}
	}

	void CSoundManager::SetRTPCValue(const SoundRTPC &_rtpc, float value, const CSceneNode* _speaker)
	{
		auto it = m_GameObjectSpeakers.find(_speaker);
		if (it != m_GameObjectSpeakers.end())
		{
			SetRTPCValue(_rtpc, value, it->second);
		}
		else{
			Assert(false, "El RTPC no existe", _speaker->GetName().c_str());
		}
	}


	void CSoundManager::BroadcastRTPCValue(const SoundRTPC& rtpc, float value)
	{
		AKRESULT res = AK::SoundEngine::SetRTPCValue(rtpc.RTPCName.c_str(), (AkRtpcValue)value);
		Assert(res, "Error al hacer BroadCast del RTPC ", rtpc.RTPCName.c_str(), value);
	}


	void CSoundManager::BroadCastState(const SoundStateValue & state)
	{
		AKRESULT res = AK::SoundEngine::SetState(state.stateName.c_str(), state.ValueName.c_str());
	}

	void CSoundManager::RenderDebugGUI()
	{
		ImGui::Begin("Sound Manager");
		ImGui::Text("Sound Manager tests");
		static int footstepOption = 0;
		ImGui::RadioButton("Dirt", &footstepOption, 0); ImGui::SameLine();
		ImGui::RadioButton("Forest", &footstepOption, 1);
		SoundSwitchValue ssv;
		ssv.switchName = "SwitchTest";

		switch (footstepOption) {
		case 0:
			ssv.switchValue = "Dirt";
			break;
		case 1:
			ssv.switchValue = "Forest";
			break;
		}
		SetSwitch(ssv);

		if (ImGui::Button("Sound event")) {
			SoundEvent se;
			se.eventName = "MoveEvent";
			PlayEvent(se);
		}

		SoundRTPC srtpc;
		srtpc.RTPCName = "MusicPitch";
		static float pitch = 50.0f;
		float lastPitch = pitch;
		ImGui::SliderFloat("Pitch", &pitch, 0.0f, 100.0f);
		if (lastPitch != pitch)
		{
			SetRTPCValue(srtpc, pitch);
		}
		//
		static int musicOption = 0;
		int lastMusicOption = musicOption;
		ImGui::RadioButton("Music ON (state)", &musicOption, 0); ImGui::SameLine();
		ImGui::RadioButton("Music OFF (state)", &musicOption, 1);

		if (lastMusicOption != musicOption)
		{
			SoundStateValue ssv;
			ssv.stateName = "StateTest";
			switch (musicOption)
			{
			case 0:
				ssv.ValueName = "Music_On";
				break;
			case 1:
				ssv.ValueName = "Music_Off";
				break;
			}
			BroadCastState(ssv);
		}
		//
		ImGui::End();
	}
}