#pragma once

#include <string>
#include <unordered_map>
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"
#include "Camera\CameraController.h"
#include "Scenes\SceneNode.h"
#include <AK\SoundEngine\Common\AkTypes.h>

namespace engine
{
	struct SoundEvent 
	{
		std::string eventName;
	};

	struct SoundSwitchValue
	{
		std::string switchName;
		std::string switchValue;
	};

	struct SoundRTPC
	{
		std::string RTPCName;
	};

	struct SoundStateValue
	{
		std::string stateName;
		std::string ValueName;
	};


	class CSoundManager 
	{
	public:
		CSoundManager(std::string path);
		~CSoundManager();
		bool Init();
		void Update(const CCameraController * camera);
		bool Load(const std::string &soundbanks_filename, const std::string &speakers_filename) ;
		bool LoadSoundBanksXML() ;
		bool LoadSpeakersXML() ;
		bool Reload() ;

		bool InitBanks();
		bool LoadSoundBank(const std::string &banks);
		bool UnloadSoundBank(const std::string &bank);

		void RegisterSpeaker(const CSceneNode* _speaker);
		void UnregisterSpeaker(const CSceneNode* _speaker);

		void PlayEvent(const SoundEvent &_event, const AkGameObjectID& id);
		void PlayEvent(const SoundEvent &_event);
		void PlayEvent(const SoundEvent &_event, const std::string& _speaker);
		void PlayEvent(const SoundEvent &_event, const CSceneNode* _speaker);

		void SetSwitch(const SoundSwitchValue& switchValue, const AkGameObjectID& id);
		void SetSwitch(const SoundSwitchValue& switchValue);
		void SetSwitch(const SoundSwitchValue& switchValue, const std::string& _speaker);
		void SetSwitch(const SoundSwitchValue& switchValue, const CSceneNode* _speaker);
		
		void BroadcastRTPCValue(const SoundRTPC &_rtpc, float value);
		void SetRTPCValue(const SoundRTPC &_rtpc, float value, const AkGameObjectID& id);
		void SetRTPCValue(const SoundRTPC &_rtpc, float value);
		void SetRTPCValue(const SoundRTPC &_rtpc, float value, const std::string& _speaker);
		void SetRTPCValue(const SoundRTPC &_rtpc, float value, const CSceneNode* _speaker);
		
		void BroadCastState(const SoundStateValue &_state);
		
		void SetListenerPosition(const CCameraController * camera);
		void Terminate();
		void Clean();

		void RenderDebugGUI();

	protected:	

		std::string m_Path;
		std::string m_SoundBanksFilename;
		std::string m_SpeakersFilename;

		AkGameObjectID m_LastGameObjectID; // ID > 0
		std::vector<AkGameObjectID> m_FreeObjectIDs;

		AkGameObjectID m_DefaultSpeakerID;
		std::unordered_map<std::string, AkGameObjectID> m_NamedSpeakers;
		std::unordered_map<const CSceneNode*, AkGameObjectID> m_GameObjectSpeakers;

		AkGameObjectID CSoundManager::GenerateObjectID()
		{
			AkGameObjectID result;
			if (m_FreeObjectIDs.size() > 0)
			{
				result = m_FreeObjectIDs.back();
				m_FreeObjectIDs.pop_back();
			}else{
				result = ++m_LastGameObjectID;
			}

			return result;
		}
	};
}