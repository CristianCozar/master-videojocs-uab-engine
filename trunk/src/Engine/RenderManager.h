#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3dcsx.h>

#include "Math\Vector4.h"
#include "Math\Vector3.h"
#include "Math\Matrix44.h"
#include "Math\Color.h"
#include "Utils\MemLeaks\Releaser.h"
#include "Camera\Frustum.h"

#define MAX_RENDER_TARGETS 8


namespace engine {
	class CRenderManager
	{
	public:
		struct CDebugVertex
		{
			Vect4f Position;
			CColor Color;
		};

		static const int DebugVertexBufferSize = 0x10000;
		Vect4f m_BackgroundColor;

		CRenderManager();
			
		~CRenderManager();
		bool Init(HWND hWnd, int width, int height);
		bool InitStencil(int width, int height);
		void BeginRender();
		void Clear(bool renderTarget, bool depthStencil, const CColor& backgroundColor);
		void EndRender();
		void SetSolidRenderState(const D3D11_FILL_MODE& aFillMode = D3D11_FILL_SOLID);
		bool SetSolidRenderState(const D3D11_FILL_MODE& aFillMode, const D3D11_CULL_MODE& aCullMode, bool aClockwise);
		bool CreateDebugShader();
		bool CreateDebugVertexBuffer();
		void CreateDebugObjects();
		void DrawTriangle();
		void DrawAxis(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color);
		void DrawGrid(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color);
		void DrawCube(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color);
		void DrawSphere(float Radius, const float rotation, const CColor Color, const Vect3f position = Vect3f(0.0f, 0.0f, 0.0f));
		void DrawPyramid(float pScale, const float rotationX, const float rotationY, const CColor Color, const Vect3f position = Vect3f(0.0f, 0.0f, 0.0f));
		void DrawArrow(float pScale, const float rotationX, const float rotationY, const CColor Color, const Vect3f position = Vect3f(0.0f, 0.0f, 0.0f));
		void DrawDebug(const CDebugVertex* points, int vertices, D3D11_PRIMITIVE_TOPOLOGY topology);
		void DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint);

		ID3D11Device* GetDevice() const;
		ID3D11DeviceContext* GetDeviceContext() const;
		IDXGISwapChain* GetSwapChain();
		void Resize(int Width, int Height);

		void SetModelMatrix(const Mat44f &Model);
		Mat44f GetModelMatrix();
		void SetViewMatrix(const Mat44f &View);
		void SetViewMatrix(const Vect3f &vPos, const Vect3f &vTarget, const Vect3f &vUp);
		Mat44f GetViewMatrix();
		Mat44f GetProjectionMatrix();
		void SetProjectionMatrix(const Mat44f &Projection);
		void SetProjectionMatrix(float fovy, float aspect, float zn, float zf);
		void SetProjectionMatrix(float fovy, int aspectWidth, int aspectHeight, float zn, float zf);
		void SetViewProjectionMatrix(const Mat44f &View, const Mat44f &Projection);
		Mat44f GetViewProjectionMatrix();
		void UpdateFrustum();
		CFrustum GetFrustum();
		void SetViewport(const Vect2u& aPosition, const Vect2u& aSize);		
		D3D11_VIEWPORT GetViewport();
		int GetCurrentHeight();
		int GetCurrentWidth();
		float GetCurrentAspect();
		void ResetViewport();
		void UnsetRenderTargets();
		void SetRenderTargets(int    aNumViews, ID3D11RenderTargetView **aRenderTargetViews,ID3D11DepthStencilView *aDepthStencilViews);

	private:
		// Elementos b�sicos de DirectX
		releaser_ptr<ID3D11Device>				m_Device;
		releaser_ptr<ID3D11DeviceContext>		m_DeviceContext;
		releaser_ptr<IDXGISwapChain>			m_SwapChain;
		releaser_ptr<ID3D11RenderTargetView>	m_RenderTargetView;
		releaser_ptr<ID3D11Texture2D>			m_DepthStencil;
		releaser_ptr<ID3D11DepthStencilView>	m_DepthStencilView;
		releaser_ptr<ID3D11RasterizerState>		m_SolidRenderState;
		releaser_ptr<ID3D11Debug>				m_D3D11Debug;

		// Shader debug
		releaser_ptr<ID3D11Buffer>			m_DebugVertexBuffer;
		int									m_DebugVertexBufferCurrentIndex;
		releaser_ptr<ID3D11VertexShader>	m_DebugVertexShader;
		releaser_ptr<ID3D11InputLayout>		m_DebugVertexLayout;
		releaser_ptr<ID3D11PixelShader>		m_DebugPixelShader;

		// Objetos debug
		const CDebugVertex		*m_TriangleRenderableVertexs;
		const CDebugVertex		*m_AxisRenderableVertexs;
		const CDebugVertex		*m_GridRenderableVertexs;
		const CDebugVertex		*m_CubeRenderableVertexs;
		const CDebugVertex		*m_SphereRenderableVertexs;

		const CDebugVertex		*m_PyramidRenderableVertexs;
		const CDebugVertex		*m_ArrowRenderableVertexs;
		int m_NumVerticesAxis, m_NumVerticesGrid, m_NumVerticesCube, m_NumVerticesSphere, m_NumVerticesTriangle;
		int m_NumVerticesPyramid, m_NumVerticesArrow;

		// Matrices
		Mat44f					m_ModelMatrix, m_ViewMatrix, m_ProjectionMatrix;
		Mat44f					m_ViewProjectionMatrix, m_ModelViewProjectionMatrix;

		// Frustum
		CFrustum m_Frustum;

		//ViewPort
		D3D11_VIEWPORT m_Viewport;
		ID3D11RenderTargetView*  m_CurrentRenderTargetViews[MAX_RENDER_TARGETS];
		ID3D11DepthStencilView* m_currentDepthStencil;
		//releaser_ptr<ID3D11RenderTargetView> m_CurrentRenderTargetViews[5];
		int m_NumViews;
		int m_currentWidth;
		int m_currentHeight;
		float m_currentAspect;

		void Destroy();
	};
}