#pragma once
#include "PointLight.h"
#include "ConstantBufferManager.h"

namespace engine {

	CPointLight::CPointLight(const CXMLElement* aElement) : CLight(aElement, CLight::ELightType::ePoint) {

	}
	CPointLight::~CPointLight(){

	}

	void CPointLight::SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex)
	{
		CLight::SetLightConstants(aPerLightDesc, aIndex);
	}

	void CPointLight::SetShadowMap(CRenderManager& lRM)
	{
	
	}
}