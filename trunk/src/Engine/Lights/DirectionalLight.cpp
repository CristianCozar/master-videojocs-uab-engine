#pragma once
#include "DirectionalLight.h"
#include "RenderManager.h"
#include "ConstantBufferManager.h"
#include "Engine.h"

namespace engine {
	CDirectionalLight::CDirectionalLight(const CXMLElement* aElement) : CLight(aElement, CLight::ELightType::eDirectional)
	{
		m_OrthoShadowMapSize = aElement->GetAttribute<Vect2f>("ortho_shadow_map", Vect2f());
	}

	CDirectionalLight::~CDirectionalLight() {

	}

	void CDirectionalLight::SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex)
	{
		CLight::SetLightConstants(aPerLightDesc, aIndex);
	}

	void CDirectionalLight::SetShadowMap(CRenderManager& lRM)
	{
		m_ShadowView.SetIdentity();
		m_ShadowView.SetFromLookAt(m_Position, m_Position + GetRight(),GetUp()); //Vector Up correcto?
		unsigned int l_ShadowMapWidth = m_pShadowMap->GetSize().x;
		unsigned int l_ShadowMapHeight = m_pShadowMap->GetSize().y;

		m_ShadowProjection.SetFromOrtho(m_OrthoShadowMapSize.x, m_OrthoShadowMapSize.y, 0.1f, m_RangeAttenuation.y);
		//m_ShadowProjection.SetFromOrtho(m_OrthoShadowMapSize.x, m_OrthoShadowMapSize.y, 0.1f, 100000.0f);
		CConstantBufferManager& l_CBM = CEngine::GetInstance().GetConstantBufferManager();

		lRM.SetViewProjectionMatrix(m_ShadowView, m_ShadowProjection);

		l_CBM.mFrameDesc.m_View = m_ShadowView;
		l_CBM.mFrameDesc.m_Projection = m_ShadowProjection;
		l_CBM.mFrameDesc.m_ViewProjection = lRM.GetViewProjectionMatrix();
		l_CBM.mFrameDesc.m_ViewInverse = l_CBM.mFrameDesc.m_View.GetInverted();
		l_CBM.mFrameDesc.m_InverseProjection = l_CBM.mFrameDesc.m_Projection.GetInverted();

		l_CBM.BindVSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_Frame);
		l_CBM.BindPSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_FramePS);

		ID3D11RenderTargetView* l_RenderTargetViews[1];
		l_RenderTargetViews[0] = m_pShadowMap->GetRenderTargetView();

		D3D11_VIEWPORT m_viewport;
		m_viewport.Width = (float)l_ShadowMapWidth;
		m_viewport.Height = (float)l_ShadowMapHeight;
		m_viewport.MinDepth = 0.0f;
		m_viewport.MaxDepth = 1.0f;
		m_viewport.TopLeftX = 0.0f;
		m_viewport.TopLeftY = 0.0f;

		lRM.GetDeviceContext()->RSSetViewports(1, &m_viewport);
		lRM.SetRenderTargets(1, l_RenderTargetViews, m_pShadowMap->GetDepthStencilView());

	}
}