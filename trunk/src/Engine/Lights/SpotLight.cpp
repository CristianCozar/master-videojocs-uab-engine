#pragma once
#include "SpotLight.h"

#include "ConstantBufferManager.h"
#include "Debug\imgui.h"

namespace engine {
	CSpotLight::CSpotLight(const CXMLElement* aElement) : CLight(aElement, CLight::ELightType::eSpot) {
		SetAngle(aElement->GetAttribute<float>("angle", 0.0f));
		SetFallOff(aElement->GetAttribute<float>("fall_off", 0.0f));
	}

	CSpotLight::CSpotLight() {
		m_Angle = 0.0f;
		m_FallOff = 0.0f;
	}
	CSpotLight::~CSpotLight(){

	}

	void CSpotLight::RenderDebugGUI() {
		CLight::RenderDebugGUI();
		if (ImGui::CollapsingHeader("Special Attributes")) {
			ImGui::SliderFloat("Angle", &m_Angle, 0.0f, 180.0f);
			ImGui::SliderFloat("Falloff", &m_FallOff, 0.0f, 180.0f);
		}

	}

	void CSpotLight::ResetDebugGUI() {
		m_Angle = 45.0f;
		m_FallOff = 60.0f;
		CLight::ResetDebugGUI();
	}

	void CSpotLight::SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex)
	{
		CLight::SetLightConstants(aPerLightDesc, aIndex);
		aPerLightDesc->m_LightAngle[aIndex] = m_Angle;
		aPerLightDesc->m_LightFallOffAngle[aIndex] = m_FallOff;
	}

	void CSpotLight::SetShadowMap(CRenderManager& lRM)
	{
		if (m_pShadowMap == NULL)
			return;

		m_ShadowView.SetIdentity();
		m_ShadowView.SetFromLookAt(m_Position, m_Position + GetForward(), GetUp());
		unsigned int l_ShadowMapWidth = m_pShadowMap->GetSize().x;
		unsigned int l_ShadowMapHeight = m_pShadowMap->GetSize().y;

		m_ShadowProjection.SetIdentity();
		m_ShadowProjection.SetFromPerspective(m_FallOff, l_ShadowMapWidth / (float)l_ShadowMapHeight, 0.1f, m_RangeAttenuation.y);
		
		CConstantBufferManager& l_CBM = CEngine::GetInstance().GetConstantBufferManager();

		lRM.SetViewProjectionMatrix(m_ShadowView, m_ShadowProjection);

		l_CBM.mFrameDesc.m_View = m_ShadowView;
		l_CBM.mFrameDesc.m_Projection = m_ShadowProjection;
		l_CBM.mFrameDesc.m_ViewProjection = lRM.GetViewProjectionMatrix();
		l_CBM.mFrameDesc.m_ViewInverse = l_CBM.mFrameDesc.m_View.GetInverted();
		l_CBM.mFrameDesc.m_InverseProjection = l_CBM.mFrameDesc.m_Projection.GetInverted();

		l_CBM.BindVSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_Frame);
		l_CBM.BindPSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_FramePS);

		ID3D11RenderTargetView* l_RenderTargetViews[1];
		l_RenderTargetViews[0] = m_pShadowMap->GetRenderTargetView();

		D3D11_VIEWPORT m_viewport;
		m_viewport.Width = (float)l_ShadowMapWidth;
		m_viewport.Height = (float)l_ShadowMapHeight;
		m_viewport.MinDepth = 0.0f;
		m_viewport.MaxDepth = 1.0f;
		m_viewport.TopLeftX = 0.0f;
		m_viewport.TopLeftY = 0.0f;
		lRM.GetDeviceContext()->RSSetViewports(1, &m_viewport);
		lRM.SetRenderTargets(1, l_RenderTargetViews, m_pShadowMap->GetDepthStencilView());		
	}
}