#pragma once

#include "Scenes\SceneNode.h"
#include "Utils\Defines.h"
#include "Math\Color.h"
#include "RenderPipeline\DynamicTexture.h"
#include "Scenes\Layer.h"

#include "Utils\EnumToString.h"

namespace engine {
	struct PerLightsDesc;
	class CLight : public CSceneNode
	{
	public:
		enum ELightType
		{
			ePoint = 0,
			eDirectional,
			eSpot
		};
	public:
		CLight();
		CLight(ELightType aLightType);
		CLight(const CXMLElement* aElement, ELightType aLightType);
		virtual ~CLight();
		GET_SET(ELightType, Type);
		GET_SET(float, Intensity);
		GET_SET(float, SpecularIntensity);
		GET_SET_REF(CColor, Color);
		GET_SET_REF(Vect2f, RangeAttenuation);
		GET_SET(bool, GenerateShadowMap);
		GET_SET(Mat44f, ShadowView);
		GET_SET(Mat44f, ShadowProjection);
		GET_PTR(CDynamicTexture, ShadowMap);
		GET_PTR(CTexture, ShadowMask);
		GET_REF(std::vector<CLayer*>, ShadowLayers);
		bool CLight::Render(engine::CRenderManager& aRendermanager);
		virtual void SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex);
		void RenderDebugGUI();
		void ResetDebugGUI();
		virtual void SetShadowMap(CRenderManager& lRM) = 0;

	protected:
		float m_ShadowBias;
		float m_ShadowStrength;
		ELightType m_Type;
		float m_Intensity;
		float m_SpecularIntensity;
		CColor m_Color;
		Vect2f m_RangeAttenuation;
		bool m_GenerateShadowMap;
		Mat44f m_ShadowView, m_ShadowProjection;
		CDynamicTexture* m_pShadowMap;
		std::vector<CLayer*> m_ShadowLayers;
		CTexture* m_pShadowMask;
	};
}

Begin_Enum_String(engine::CLight::ELightType)
{
	Enum_String_Id(engine::CLight::ePoint, "point");
	Enum_String_Id(engine::CLight::eSpot, "spot");
	Enum_String_Id(engine::CLight::eDirectional, "directional");
}
End_Enum_String;