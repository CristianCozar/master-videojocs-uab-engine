#pragma once

#include "LightManager.h"
#include "Utils\Logger\Logger.h"


#include "XML\XML.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "Debug\imgui.h"
#include "Paths.h"
#include "ConstantBufferManager.h"


namespace engine {
	CLightManager::CLightManager()
	{
		m_AmbientLightColor = CColor(0.5f, 0.5f, 0.5f, 1.0f);
	}

	CLightManager::~CLightManager()
	{

	}

	bool CLightManager::Load()
	{
		return Load(FILENAME_DEFAULT_LIGHTS);
	}

	bool CLightManager::Load(const std::string& aFileName)
	{
		LOG_INFO_APPLICATION("Loading lights from '%s'", aFileName.c_str());
		m_LevelLightsFilename = aFileName;
		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((aFileName).c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading lights");
			CXMLElement *lights = document.FirstChildElement("lights"); //Lights
			if (lights) {
				for (CXMLElement *light = lights->FirstChildElement("light"); light != nullptr; light = light->NextSiblingElement("light")) {//Light
					if (!Exist(light->Attribute("name"))) {
						CLight *light_to_save = nullptr;
						CLight::ELightType ltype;
						if (EnumString<CLight::ELightType>::ToEnum(ltype, light->GetAttribute<std::string>("type", ""))) {
							switch (ltype) {
							case CLight::ELightType::eDirectional:
								light_to_save = new CDirectionalLight(light);
								break;
							case CLight::ELightType::ePoint:
								light_to_save = new CPointLight(light);
								break;
							case CLight::ELightType::eSpot:
								light_to_save = new CSpotLight(light);
								break;
							}
							LOG_INFO_APPLICATION("Light '%s' added", light->Attribute("name"));
							base::utils::CTemplatedMapVector<CLight>::Add(light->Attribute("name"), light_to_save);
						} else {
							Assert(false, "El tipo de luz %s no existe.", light->Attribute("type"));
						}
					}
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Lights XML '%s' does not exist", aFileName.c_str());
		}

		return base::xml::SucceedLoad(error);
		return false;
	}

	bool CLightManager::ReLoad()
	{
		CLightManager::Destroy();
		return Load() || Load(m_LevelLightsFilename);
	}

	bool CLightManager::ReLoad(const std::string &aFilename)
	{
		CLightManager::Destroy();
		return Load() || Load(aFilename);
	}

	void CLightManager::Destroy()
	{
		CTemplatedMapVector::Destroy();
	}

	void CLightManager::SetLightConstants(const int& aIndex, CLight *aLight)
	{
		CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();
		if (aLight == nullptr || !aLight->IsActive()) {
			l_CBM.mLightDesc.m_LightEnabled[aIndex] = 0.0f;
		}
		else {
			l_CBM.mLightDesc.m_LightEnabled[aIndex] = 1.0f;
			aLight->SetLightConstants(&l_CBM.mLightDesc, aIndex);
		}
	}

	void CLightManager::SetLightsConstants()
	{
		CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();
		l_CBM.mLightDesc = {};
		l_CBM.mLightDesc.m_LightAmbient = m_AmbientLightColor;
		for (unsigned int i = 0; i < 4; ++i)
		{
			if (i < GetCount()) {
				SetLightConstants(i, GetIndex(i)); // Temporal
			}
			else {
				SetLightConstants(i, nullptr); // Temporal
			}
		}
	}

	void CLightManager::UpdateConstants()
	{
		CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();
		l_CBM.BindVSBuffer(CEngine::GetInstance().GetRenderManager().GetDeviceContext(), CConstantBufferManager::ConstanBufferVS::CB_LightVS);
		l_CBM.BindPSBuffer(CEngine::GetInstance().GetRenderManager().GetDeviceContext(), CConstantBufferManager::ConstanBufferPS::CB_LightPS);
	}

}