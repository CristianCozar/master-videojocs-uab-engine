#pragma once

#include "Light.h"

namespace engine {
	class CSpotLight : public CLight
	{
	public:
		CSpotLight();
		CSpotLight(const CXMLElement* aElement);
		virtual ~CSpotLight();
		void SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex);
		GET_SET(float, Angle);
		GET_SET(float, FallOff);
		void RenderDebugGUI();
		void ResetDebugGUI();
		virtual void SetShadowMap(CRenderManager& lRM);

	private:
		float m_Angle;
		float m_FallOff;
	};
}