#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Math\Vector4.h"
#include "Utils\Defines.h"

#include "Light.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "SpotLight.h"

namespace engine {
	class CLightManager : public base::utils::CTemplatedMapVector<CLight>
	{
	public:
		Vect4f m_AmbientLightColor;
		CLightManager();
		~CLightManager();
		bool Load();
		bool Load(const std::string& aFileName);
		bool ReLoad();
		bool ReLoad(const std::string& aFileName);
		void Destroy();
		void SetLightConstants(const int& aIndex, CLight *aLight);
		void SetLightsConstants();
		void UpdateConstants();
		GET_SET(Vect4f, AmbientLightColor)
	private:
		
		std::string m_LevelLightsFilename;
	};
}