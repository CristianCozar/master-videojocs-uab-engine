#pragma once
#include "Light.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"

#include "Materials\TextureManager.h"
#include "ConstantBufferManager.h"
#include "Math\Vector4.h"
#include "Math\Vector3.h"
#include "Scenes\Layer.h"
#include "Scenes\SceneManager.h"

#include "Debug\imgui.h"
#include "Debug\CustomAssert.h"


namespace engine {

	CLight::CLight() :
		m_Type(CLight::ELightType::eDirectional),
		m_Intensity(0.0f),
		m_SpecularIntensity(1.0f),
		m_Color(CColor()),
		m_RangeAttenuation(Vect2f())
	{

	}

	CLight::CLight(const CXMLElement* aElement, ELightType aLightType) : CSceneNode(aElement, "Light")
	{
		SetName(aElement->GetAttribute<std::string>("name", ""));
		SetColor(aElement->GetAttribute<CColor>("color", CColor()));
		//SetForward(aElement->GetAttribute<Vect3f>("forward", Vect3f()));
		SetIntensity(aElement->GetAttribute<float>("intensity", 1.0f));
		SetRangeAttenuation(aElement->GetAttribute<Vect2f>("attenuation_range", Vect2f()));
		//SetPosition(aElement->GetAttribute<Vect3f>("position", Vect3f()));	
		//SetScale(aElement->GetAttribute<float>("scale", 0.0f));
		SetSpecularIntensity(aElement->GetAttribute<float>("specular_exponent", 1.0f));
		CLight::ELightType ltype;
		EnumString<CLight::ELightType>::ToEnum(ltype, aElement->GetAttribute<std::string>("type", ""));
		SetType(ltype);
		SetVisible(aElement->GetAttribute<bool>("visible", true));
		SetGenerateShadowMap(aElement->GetAttribute<bool>("generate_shadow_map", false));
		if (m_GenerateShadowMap)
		{
			CRenderManager &l_RM = CEngine::GetInstance().GetRenderManager();
			D3D11_VIEWPORT lVP = l_RM.GetViewport();

			int SizeX = lVP.Width;
			int SizeY = lVP.Height;
			float l_TexWidth = aElement->GetAttribute<float>("shadow_map_width", 0);
			if (l_TexWidth > SizeX)
			{
				l_TexWidth = SizeX;
			}

			float l_TexHeight = aElement->GetAttribute<float>("shadow_map_height", 0);
			if (l_TexHeight > SizeY)
			{
				l_TexHeight = SizeY;
			}

			m_pShadowMap = new CDynamicTexture(l_TexWidth, l_TexHeight, m_Name, false, CDynamicTexture::TFormatType::R32_FLOAT);
			m_ShadowBias = aElement->GetAttribute<float>("shadow_bias", 0.0037f);
			m_ShadowStrength = aElement->GetAttribute<float>("shadow_strength", 1.0f);
			CTextureManager& l_TM = CEngine::GetInstance().GetTextureManager();
			l_TM.AddTexture(m_pShadowMap->GetName(), m_pShadowMap);
			//m_pShadowMap = (CDynamicTexture*) l_TM.GetTexture(m_pShadowMap->GetName());
			const std::string l_mask = aElement->GetAttribute<std::string>("shadow_texture_mask", "");
			if (l_mask != "")
				m_pShadowMask = l_TM.GetTexture(l_mask);
			/*CSceneManager& l_SM = CEngine::GetInstance().GetSceneManager();
			CScene* scene = nullptr;
			for (int i = 0; i < l_SM.GetCount(); ++i)
			{
			if (l_SM[i]->IsActive())
			{
			scene = l_SM[i];
			break;
			}
			}

			if (scene != nullptr)
			{*/
			for (const CXMLElement *layer = aElement->FirstChildElement("layer"); layer != nullptr; layer = layer->NextSiblingElement("layer"))
			{
				CLayer* l_layer = new CLayer(layer->GetAttribute<std::string>("layer", ""));
				/*	l_layer = scene->Get(layer->GetAttribute<std::string>("layer", ""));
					if (l_layer != nullptr)*/
				m_ShadowLayers.push_back(l_layer);
			}
		}
		//}
	}

	CLight::CLight(ELightType aLightType) {
		m_Type = aLightType;
		m_Intensity = 0.0f;
		m_SpecularIntensity = 0.0f;
		m_Color = CColor();
		m_RangeAttenuation = Vect2f();
	}

	CLight::~CLight() {

	}


	bool CLight::Render(engine::CRenderManager& aRendermanager) {
		switch (m_Type)
		{
			case ELightType::eDirectional:
				aRendermanager.DrawArrow(1.0f, GetPitch(), GetYaw(), m_Color, GetPosition());
				break;
			case ELightType::ePoint:
				aRendermanager.DrawSphere(0.1f, 0.0f, m_Color, GetPosition());
				break;
			case ELightType::eSpot:
				aRendermanager.DrawPyramid(1.0f, GetPitch(), GetYaw(), m_Color, GetPosition());
				break;
		}
		
		return false;
	}

	void CLight::SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex)
	{
		Assert(aIndex >= 0 && aIndex < MAX_LIGHTS_BY_SHADER ,"El valor debe estar entre 0 y %d de luces por shader.", MAX_LIGHTS_BY_SHADER);
		aPerLightDesc->m_LightType[aIndex] = (float)m_Type;
		aPerLightDesc->m_LightIntensity[aIndex] = m_Intensity;
		aPerLightDesc->m_LightColor[aIndex] = m_Color;
		aPerLightDesc->m_LightAttenuationStartRange[aIndex] = m_RangeAttenuation.x;
		aPerLightDesc->m_LightAttenuationEndRange[aIndex] = m_RangeAttenuation.y;
		aPerLightDesc->m_LightPosition[aIndex] = Vect4f(GetPosition());
		aPerLightDesc->m_LightDirection[aIndex] = Vect4f(GetForward(), 0.0f);
		//TO DO ShadowMap Constants!!
		aPerLightDesc->m_UseShadowMap[aIndex] = m_GenerateShadowMap == true ? 1.0f : 0.0f;
		if (m_GenerateShadowMap)
		{
			CRenderManager &l_RM = CEngine::GetInstance().GetRenderManager();

			aPerLightDesc->m_UseShadowMask[aIndex] = m_pShadowMask != NULL ? 1.0f : 0.0f;
			aPerLightDesc->m_LightView[aIndex] = m_ShadowView;
			aPerLightDesc->m_LightProjection[aIndex] = m_ShadowProjection;
			aPerLightDesc->m_ShadowMapBias[aIndex] = m_ShadowBias;
			aPerLightDesc->m_ShadowMapStrength[aIndex] = m_ShadowStrength;
			m_pShadowMap->Bind(UAB_ID_SHADOW_MAP + aIndex*2, l_RM.GetDeviceContext());
			if (m_pShadowMask != NULL)
				m_pShadowMask->Bind(UAB_ID_SHADOW_MAP + 1 + aIndex*2, l_RM.GetDeviceContext());
		}
	}


	void CLight::RenderDebugGUI() {
		CSceneNode::RenderDebugGUI();
		if(ImGui::CollapsingHeader("Common Attributes")) {
			ImGui::LabelText("Name", m_Name.c_str());
			ImGui::LabelText("Type", EnumString<CLight::ELightType>::ToStr(m_Type).c_str());
			ImGui::DragFloat("Intensity", &m_Intensity, 0.0f, 0.0f, 30.0f);
			ImGui::DragFloat("ShadowBias", &m_ShadowBias, 0.0f, 0.0f, 100.0f);
			ImGui::DragFloat("ShadowStrength", &m_ShadowStrength, 0.0f, 0.0f, 100.0f);
			ImGui::DragFloat("Specular Intensity", &m_SpecularIntensity, 0.0f, 1.0f, 80.0f);
			ImGui::DragFloat3("Color", &m_Color.x, 0.0f, 0.0f, 1.0f);
			ImGui::DragFloat2("Range Attenuation", &m_RangeAttenuation.x, 0.0f, 0.0f, 1000.0f);
		}
	}

	void CLight::ResetDebugGUI() {
		m_Intensity = 1.0f;
		m_SpecularIntensity = 5.0f;
		m_Color = CColor(1.0f, 1.0f, 1.0f);
		m_RangeAttenuation = Vect2f(0.0f, 1000.0f);
		CSceneNode::ResetDebugGUI();
	}
}