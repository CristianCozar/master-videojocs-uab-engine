#pragma once
#include "Light.h"

namespace engine {
	class CDirectionalLight : public CLight
	{
	public:
		CDirectionalLight();
		CDirectionalLight(const CXMLElement * aElement);
		virtual ~CDirectionalLight();
		void SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex);
		virtual void SetShadowMap(CRenderManager& lRM);
	protected:
		Vect2f m_OrthoShadowMapSize;
	};
}