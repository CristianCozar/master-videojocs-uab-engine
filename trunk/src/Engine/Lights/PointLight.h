#pragma once

#include "Light.h"

namespace engine{
	class CPointLight: public CLight
	{
	public:
		CPointLight();
		CPointLight(const CXMLElement *aElement);
		virtual ~CPointLight();
		void SetLightConstants(PerLightsDesc *aPerLightDesc, const int &aIndex) override;
		virtual void SetShadowMap(CRenderManager& lRM);

	};
}