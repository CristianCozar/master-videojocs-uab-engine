#include "GUIManager.h"

#include "Debug\CustomAssert.h"
#include "RenderManager.h"
#include "Input\InputManager.h"
#include "Input\ActionManager.h"
#include "Math\MathUtils.h"
#include "Engine.h"
#include "Paths.h"
#include "Utils\Logger\Logger.h"
#include "XML\XML.h"
#include "Materials\MaterialManager.h"
#include "Mesh\GeometryMacros.h"
#include "Scripting\ScriptManager.h"

namespace engine
{

	CGUIManager::CGUIManager() :
		m_ActiveItem(""),
		m_HotItem(""),
		m_InputUpToDate(false),
		m_MouseX(0),
		m_MouseY(0),
		m_MouseWentPressed(false),
		m_MouseWentReleased(false),
		m_Filename(""),
		m_SpriteMaps(base::utils::CTemplatedMapVector<SpriteMapInfo>()),
		m_Sprites(base::utils::CTemplatedMap<SpriteInfo>()),
		m_ButtonInfos(base::utils::CTemplatedMap<ButtonInfo>()),
		m_SliderInfos(base::utils::CTemplatedMap<SliderInfo>()),
		m_BarInfos(base::utils::CTemplatedMap<BarInfo>()),
		m_ZeldaInfos(base::utils::CTemplatedMap<ZeldaInfo>()),
		m_ImageInfos(base::utils::CTemplatedMap<ImageInfo>()),
		m_Commands(std::vector<GUICommand>())
	{
		CRenderManager& l_RM = CEngine::GetInstance().GetRenderManager();

		CVertexBuffer< VertexTypes::GUIVertex>* lVB = new CVertexBuffer< VertexTypes::GUIVertex >(l_RM, m_CurrentBufferData, MAX_VERTICES_PER_CALL, true);
		m_Geometry = new CGeometryTriangleList<VertexTypes::GUIVertex>(lVB);
	}

	CGUIManager::~CGUIManager()
	{
		m_SpriteMaps.Destroy();
		m_Sprites.Destroy();
		m_ButtonInfos.Destroy();
		m_SliderInfos.Destroy();
		m_ZeldaInfos.Destroy();
		m_BarInfos.Destroy();
		m_ImageInfos.Destroy();
		m_Commands.clear();
		delete m_Geometry;
	}

	bool CGUIManager::Load(const std::string& aFilename)
	{
		//return true;
					
		m_Filename = aFilename;
		std::string l_Path = PATH_BASE + aFilename;

		LOG_INFO_APPLICATION("Loading GUI from '%s'", l_Path.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile(l_Path.c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading GUI");
			tinyxml2::XMLElement *elements = document.FirstChildElement("gui_elements");
			if (elements) {
				for (tinyxml2::XMLElement *spritemap = elements->FirstChildElement("gui_spritemap"); spritemap != nullptr; spritemap = spritemap->NextSiblingElement("gui_spritemap")) { //spritemap
					SpriteMapInfo *l_SpriteMap = new SpriteMapInfo();
					l_SpriteMap->w = spritemap->GetAttribute<float>("width", 0.0f);
					l_SpriteMap->h = spritemap->GetAttribute<float>("height", 0.0f);
					l_SpriteMap->material = spritemap->GetAttribute<std::string>("material", "");
					std::string l_SpriteMapName = spritemap->GetAttribute<std::string>("name", "");
					for (tinyxml2::XMLElement *sprite = spritemap->FirstChildElement("sprite"); sprite != nullptr; sprite = sprite->NextSiblingElement("sprite")) {
						SpriteInfo *l_Sprite = new SpriteInfo();
						l_Sprite->spriteMap = l_SpriteMap;
						float x = sprite->GetAttribute<float>("x", 0.0f);
						float y = sprite->GetAttribute<float>("y", 0.0f);
						float w = sprite->GetAttribute<float>("w", 0.0f);
						float h = sprite->GetAttribute<float>("h", 0.0f);
						std::string l_SpriteName = sprite->GetAttribute<std::string>("name", "");
						l_Sprite->u1 = x;
						l_Sprite->u2 = x + w;
						l_Sprite->v1 = y;
						l_Sprite->v2 = y + h;
						m_Sprites.Add(l_SpriteName, l_Sprite);
					}
					m_SpriteMaps.Add(l_SpriteMapName, l_SpriteMap);
				}
				for (tinyxml2::XMLElement *button = elements->FirstChildElement("button"); button != nullptr; button = button->NextSiblingElement("button")) {
					ButtonInfo *l_Button = new ButtonInfo();
					l_Button->highlight = m_Sprites.Get(button->GetAttribute<std::string>("highlight", ""));
					l_Button->normal = m_Sprites.Get(button->GetAttribute<std::string>("normal", ""));
					l_Button->pressed = m_Sprites.Get(button->GetAttribute<std::string>("pressed", ""));
					std::string l_ButtonName = button->GetAttribute<std::string>("name", "");
					m_ButtonInfos.Add(l_ButtonName, l_Button);
				}
				for (tinyxml2::XMLElement *slider = elements->FirstChildElement("slider"); slider != nullptr; slider = slider->NextSiblingElement("slider")) {
					SliderInfo *l_Slider = new SliderInfo();
					l_Slider->base = m_Sprites.Get(slider->GetAttribute<std::string>("base", ""));
					l_Slider->handle = m_Sprites.Get(slider->GetAttribute<std::string>("handle", ""));
					l_Slider->pressed_handle = m_Sprites.Get(slider->GetAttribute<std::string>("pressed_handle", ""));
					l_Slider->top = m_Sprites.Get(slider->GetAttribute<std::string>("top", ""));

					l_Slider->handleRelativeWidth = (l_Slider->handle->u2 - l_Slider->handle->u1) / (l_Slider->base->u2 - l_Slider->base->u1);
					l_Slider->handleRelativeHeight = (l_Slider->handle->v2 - l_Slider->handle->v1) / (l_Slider->base->v2 - l_Slider->base->v1);

					std::string l_SliderName = slider->GetAttribute<std::string>("name", "");
					m_SliderInfos.Add(l_SliderName, l_Slider);
				}
				for (tinyxml2::XMLElement *bar = elements->FirstChildElement("bar"); bar != nullptr; bar = bar->NextSiblingElement("bar")) {
					BarInfo *l_Bar = new BarInfo();
					l_Bar->base = m_Sprites.Get(bar->GetAttribute<std::string>("base", ""));
					l_Bar->top = m_Sprites.Get(bar->GetAttribute<std::string>("top", ""));
				
					std::string l_BarName = bar->GetAttribute<std::string>("name", "");
					m_BarInfos.Add(l_BarName, l_Bar);
				}
				for (CXMLElement* img = elements->FirstChildElement("image"); img != nullptr; img = img->NextSiblingElement("image")) {
					ImageInfo *l_Image = new ImageInfo();
					l_Image->img = m_Sprites.Get(img->GetAttribute<std::string>("image", ""));

					std::string l_ImageName = img->GetAttribute<std::string>("name", "");
					m_ImageInfos.Add(l_ImageName, l_Image);
				}
				for (tinyxml2::XMLElement *zelda = elements->FirstChildElement("zelda"); zelda != nullptr; zelda = zelda->NextSiblingElement("zelda")) {
					ZeldaInfo *l_Zelda = new ZeldaInfo();
					l_Zelda->full = m_Sprites.Get(zelda->GetAttribute<std::string>("full", ""));
					l_Zelda->half = m_Sprites.Get(zelda->GetAttribute<std::string>("half", ""));
					l_Zelda->empty = m_Sprites.Get(zelda->GetAttribute<std::string>("empty", ""));

					std::string l_ZeldaName = zelda->GetAttribute<std::string>("name", "");
					m_ZeldaInfos.Add(l_ZeldaName, l_Zelda);
				}
				for (tinyxml2::XMLElement *textbox = elements->FirstChildElement("textbox"); textbox != nullptr; textbox = textbox->NextSiblingElement("textbox")) {
					
					std::string aText = textbox->GetAttribute<std::string>("text", "Text Placeholder");
					TextBoxInfo *l_TextBox = new TextBoxInfo();
					l_TextBox->text = aText;

					std::string l_TextBoxName = textbox->GetAttribute<std::string>("name", "");
					m_TextBoxInfos.Add(l_TextBoxName, l_TextBox);
				}

				
				for (tinyxml2::XMLElement *font = elements->FirstChildElement("font"); font != nullptr; font = font->NextSiblingElement("font")) {
					std::string path = PATH_BASE + (font->GetAttribute<std::string>("path", ""));
					CXMLDocument fontDocument;
					tinyxml2::XMLError fontError = fontDocument.LoadFile(path.c_str());

					CXMLElement *fontElement = fontDocument.FirstChildElement("font");
					CXMLElement *info = fontElement->FirstChildElement("info");				
					CXMLElement *common = fontElement->FirstChildElement("common");

					std::string fontName = info->GetAttribute<std::string>("face", "");
					int16 l_lineHeightPerFont = common->GetAttribute<int16>("lineHeight", 0);
					std::pair<std::string, int16> l_pair (fontName, l_lineHeightPerFont);
					m_LineHeightPerFont.insert(l_pair);

					int16 l_BasePerFont = common->GetAttribute<int16>("base", 0);
					l_pair = std::pair<std::string, int16>(fontName, l_BasePerFont);
					m_BasePerFont.insert(l_pair);

					CXMLElement *pages = fontElement->FirstChildElement("pages");
					for (CXMLElement *page = pages->FirstChildElement("page"); page != nullptr; page = page->NextSiblingElement("page")) {
						std::string l_texture = page->GetAttribute<std::string>("file", "");
						SpriteInfo *l_spriteInfo = m_Sprites.Get(l_texture);
						m_TexturePerFont[fontName].push_back(l_spriteInfo);
					}

					std::unordered_map<wchar_t, FontChar> l_fontChars;
					CXMLElement *characters = fontElement->FirstChildElement("chars");
					for (CXMLElement *character = characters->FirstChildElement("char"); character != nullptr; character = character->NextSiblingElement("char")) {
						wchar_t l_unicodeID = character->GetAttribute<wchar_t>("id", 0);

						uint16 l_x = character->GetAttribute<uint16>("x", 0);
						uint16 l_y = character->GetAttribute<uint16>("y", 0);
						uint16 l_width = character->GetAttribute<uint16>("width", 0);
						uint16 l_height = character->GetAttribute<uint16>("height", 0);

						int16 xoffset = character->GetAttribute<int16>("xoffset", 0);
						int16 yoffset = character->GetAttribute<int16>("yoffset", 0);
						int16 xadvance = character->GetAttribute<int16>("xadvance", 0);

						uint8 l_page = character->GetAttribute<uint8>("page", 0);
						uint8 l_chnl = character->GetAttribute<uint8>("chnl", 0);
						
						FontChar *l_fontChar = new FontChar(l_x, l_y, l_width, l_height, xoffset, yoffset, xadvance, l_page, l_chnl);
						std::pair<wchar_t, FontChar> l_pair2(l_unicodeID, *l_fontChar);
						l_fontChars.insert(l_pair2);
					}
					std::pair<std::string, std::unordered_map<wchar_t, FontChar> > l_pair3(fontName, l_fontChars);
					m_CharactersPerFont.insert(l_pair3);
					
					CXMLElement *kernings = fontElement->FirstChildElement("kernings");
					for (CXMLElement *kerning = kernings->FirstChildElement("kerning"); kerning != nullptr; kerning = kerning->NextSiblingElement("kerning")) {
						std::unordered_map<wchar_t, FontChar> &l_characterMap = m_CharactersPerFont[fontName];

						wchar_t l_firstUnicodeID = kerning->GetAttribute<wchar_t>("first", -1);
						wchar_t l_secondUnicodeID = kerning->GetAttribute<wchar_t>("second", -1);
						int l_amount = kerning->GetAttribute<int>("amount", 0);
						

						std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>> &l_kerningFontMap = m_KerningsPerFont[fontName];
						std::unordered_map<wchar_t, int> &l_kerningCharacterMap = l_kerningFontMap[l_firstUnicodeID];
						
						std::pair<wchar_t, int> pair4(l_secondUnicodeID, l_amount);
						l_kerningCharacterMap.insert(pair4);

					}
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Scenes XML '%s' does not exist", aFilename.c_str());
		}

		return base::xml::SucceedLoad(error);
	}

	void CGUIManager::SetActive(const std::string& aID)
	{
		m_ActiveItem = aID;
		//m_SelectedItem = aID;
	}

	void CGUIManager::SetNotActive(const std::string& aID)
	{
		Assert(aID == m_ActiveItem, "El objeto con la ID %s no est� activo, pero se est� intentando desactivar.", aID);
		m_ActiveItem = "";
	}

	void CGUIManager::SetHot(const std::string& aID)
	{
		if (m_ActiveItem == "" || m_ActiveItem == aID)
		{
			m_HotItem = aID;
		}
	}

	void CGUIManager::SetNotHot(const std::string& aID)
	{
		if (m_HotItem == aID)
		{
			m_HotItem = "";
		}
	}

	void CGUIManager::SetSelected(const std::string& aID) {
		m_SelectedItem = aID;
	}

	void CGUIManager::SetNotSelected(const std::string& aID) {
		if (m_SelectedItem == aID) {
			m_SelectedItem = "";
		}
	}

	void CGUIManager::Update(float elapsedTime)
	{

		m_animationTimer += elapsedTime;
		

		if (!m_InputUpToDate)
		{
			CInputManager l_IM = CEngine::GetInstance().GetActionManager().GetInputManager();
			m_MouseX = l_IM.GetMouseX();
			m_MouseY = l_IM.GetMouseY();
			m_MouseWentPressed = l_IM.LeftMouseButtonBecomesPressed();
			m_MouseWentReleased = l_IM.LeftMouseButtonBecomesReleased();
			m_InputUpToDate = true;
		}
	}

	void CGUIManager::PostUpdate()
		{
		if (m_MouseWentReleased)
		{
			m_ActiveItem = "";
		}
	}

	void CGUIManager::RadixSort()
	{
		int l_TotalSpriteMaps = m_SpriteMaps.GetCount();
		std::map<SpriteMapInfo*, int> l_SpriteMapCount = std::map<SpriteMapInfo*, int>();
		std::map<SpriteMapInfo*, int> l_SpriteMapIndex = std::map<SpriteMapInfo*, int>();
		for (int i = 0; i < m_Commands.size(); ++i)
		{
			SpriteMapInfo* l_SpriteMap = m_Commands[i].sprite->spriteMap;
			if (l_SpriteMapCount.find(l_SpriteMap) != l_SpriteMapCount.end())
			{
				int temp = l_SpriteMapCount[l_SpriteMap];
				temp++;
				l_SpriteMapCount[l_SpriteMap] = temp;
			}
			else {
				l_SpriteMapCount.insert(std::pair<SpriteMapInfo*, int>(l_SpriteMap, 1));
			}
		}
		int l_Offset = 0;
		for (int i = 0; i < m_SpriteMaps.GetCount(); ++i)
		{
			SpriteMapInfo* l_SpriteMap = m_SpriteMaps[i];
			l_SpriteMapIndex.insert(std::pair<SpriteMapInfo*, int>(l_SpriteMap, l_Offset));
			if (l_SpriteMapCount.find(l_SpriteMap) != l_SpriteMapCount.end())
			{
				l_Offset += l_SpriteMapCount[l_SpriteMap];
			}
		}

		std::vector<GUICommand> l_OrderedCommands = std::vector<GUICommand>();
		std::vector<bool> l_IndexUsed = std::vector<bool>();
		l_OrderedCommands.resize(m_Commands.size());
		l_IndexUsed.resize(m_Commands.size());
		std::vector<GUICommand>::iterator it = l_OrderedCommands.begin();
		for (int i = 0; i < m_Commands.size(); ++i)
		{
			GUICommand l_Command = m_Commands[i];
			int l_Index = l_SpriteMapIndex[l_Command.sprite->spriteMap];
			while (l_IndexUsed[l_Index] == true)
			{
				l_Index++;
			}
			l_OrderedCommands[l_Index] = l_Command;
			l_IndexUsed[l_Index] = true;
		}
		m_Commands = l_OrderedCommands;
	}
	
	void CGUIManager::Render(CRenderManager& aRM)
	{
		
		int currentVertex = 0;
		SpriteMapInfo *l_CurrentSpriteMap = nullptr;
		CMaterialManager &l_MaterialManager = CEngine::GetInstance().GetMaterialManager();

		RadixSort();

		for (int i = 0; i < m_Commands.size(); ++i) // Esto cambiar� cuando se use el Radix
		{
	
			GUICommand l_Command = m_Commands[i];
			l_Command.x1 = mathUtils::Clamp<int>(l_Command.x1, -1000, l_Command.x2);
			l_Command.y1 = mathUtils::Clamp<int>(l_Command.y1, -1000, l_Command.y2);
			Assert(l_Command.x1 <= l_Command.x2, "Un comando tiene un tama�o X de 0 o negativo.");
			Assert(l_Command.y1 <= l_Command.y2, "Un comando tiene un tama�o Y de 0 o negativo.");

			SpriteInfo *l_CommandSprite = l_Command.sprite;
			SpriteMapInfo *l_CommandSpriteMap = l_CommandSprite->spriteMap;

			if (l_CurrentSpriteMap != l_CommandSpriteMap || currentVertex == MAX_VERTICES_PER_CALL)
			{
				if (currentVertex > 0)
				{
					LOG_WARNING_APPLICATION_IF(currentVertex == MAX_VERTICES_PER_CALL, "Superado el l�mite de v�rtices en GUI.");

					CMaterial *l_Material = l_MaterialManager.Get(l_CurrentSpriteMap->material);
					if (l_Material != nullptr)
					{
						l_Material->Apply();

						m_Geometry->m_VertexBuffer->UpdateVertexs(m_CurrentBufferData, currentVertex);

						m_Geometry->Render(aRM.GetDeviceContext(), currentVertex);
					}

				}

				currentVertex = 0;
				l_CurrentSpriteMap = l_CommandSpriteMap;
			}
			int screenHeight = aRM.GetCurrentHeight();
			int screenWidth = aRM.GetCurrentWidth();
			float x1 = (l_Command.x1 / ((float)screenWidth * 0.5f)) -1.0f;
			float x2 = (l_Command.x2 / ((float)screenWidth * 0.5f)) -1.0f;
			float y1 = 1.0f - (l_Command.y1 / ((float)screenHeight * 0.5f));
			float y2 = 1.0f - (l_Command.y2 / ((float)screenHeight * 0.5f));

			float u1 = l_CommandSprite->u1 * (1.0f - l_Command.u1) + l_CommandSprite->u2 * l_Command.u1;
			
			float u2 = l_CommandSprite->u1 * (1.0f - l_Command.u2) + l_CommandSprite->u2 * l_Command.u2;
			
			float v1 = l_CommandSprite->v1 * (1.0f - l_Command.v1) + l_CommandSprite->v2 * l_Command.v1;
			
			float v2 = l_CommandSprite->v1 * (1.0f - l_Command.v2) + l_CommandSprite->v2 * l_Command.v2;
			

			u1 /= l_CommandSpriteMap->w;
			u2 /= l_CommandSpriteMap->w;
			v1 /= l_CommandSpriteMap->h;
			v2 /= l_CommandSpriteMap->h;

			m_CurrentBufferData[currentVertex++] = { Vect3f(x1, y2, 0.0f), l_Command.color, Vect2f(u1, v2) };
			m_CurrentBufferData[currentVertex++] = { Vect3f(x2, y2, 0.0f), l_Command.color, Vect2f(u2, v2) };
			m_CurrentBufferData[currentVertex++] = { Vect3f(x1, y1, 0.0f), l_Command.color, Vect2f(u1, v1) };

			m_CurrentBufferData[currentVertex++] = { Vect3f(x1, y1, 0.0f), l_Command.color, Vect2f(u1, v1) };
			m_CurrentBufferData[currentVertex++] = { Vect3f(x2, y2, 0.0f), l_Command.color, Vect2f(u2, v2) };
			m_CurrentBufferData[currentVertex++] = { Vect3f(x2, y1, 0.0f), l_Command.color, Vect2f(u2, v1) };
		}
		if (currentVertex > 0 )
		{
			CMaterial *l_Material = l_MaterialManager.Get(l_CurrentSpriteMap->material);
			if (l_Material != nullptr)
			{
				l_Material->Apply();

				m_Geometry->m_VertexBuffer->UpdateVertexs(m_CurrentBufferData, currentVertex);

				m_Geometry->Render(aRM.GetDeviceContext(), currentVertex);
			}

		}


		m_InputUpToDate = false;
		m_Commands.clear();
	}

	bool CGUIManager::DoButton(const std::string& aGUID, const std::string& aButtonID, const GUIPosition& position)
	{
		bool l_Result = false;
		ButtonInfo *l_Info = m_ButtonInfos.Get(aButtonID);
		SpriteInfo *l_Sprite = l_Info->normal;
		if (m_ActiveItem == aGUID)
		{
			l_Sprite = l_Info->pressed;
			if (m_MouseWentReleased)
			{
				if (m_HotItem == aGUID)
				{
					l_Result = true;
				}
				SetNotActive(aGUID);
				l_Sprite = l_Info->normal;
			}
		}
		else if (m_HotItem == aGUID)
		{
			l_Sprite = l_Info->highlight;
			if (m_MouseWentPressed)
			{
				SetActive(aGUID);
				l_Sprite = l_Info->pressed;
			}
		}

		if (IsMouseInside((float)m_MouseX, (float)m_MouseY, position._x, position._y, position._width, position._height))
		{
			SetHot(aGUID);
		}
		else
		{
			SetNotHot(aGUID);
		}

		
		GUICommand l_Command = {
			l_Sprite,
			position._x, position._y, position._x + position._width, position._y + position._height,
			0, 0, 1, 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_Command);

		return l_Result;
	}

	void CGUIManager::FillCommandQueueWithText(const std::string& font, const std::string& text, const Vect4f sizeLimits, Vect2f coord, Vect4f *textSizes, GUIAnchor anchor, const CColor& _color) {
		int numCommands = FillCommandQueueWithText(font, text, sizeLimits, _color, textSizes);

		Vect2f adjustment = coord;
		//adjustment.y -= (textSizes.y + textSizes.w) * 0.5f;
		//TODO
		if ((int)anchor & (int)GUIAnchor::TOP) {
		}
		else if ((int)anchor & (int)GUIAnchor::MID) {
			//adjustment.y -= (textSizes.y + textSizes.w) * 0.5f;
		}
		else if ((int)anchor & (int)GUIAnchor::BOTTOM) {
			//adjustment.y -= (textSizes.y + textSizes.w) * 0.5f;
		}

		if ((int)anchor & (int)GUIAnchor::LEFT) {
	
		}
		else if ((int)anchor & (int)GUIAnchor::CENTER) {
		
		}
		else if ((int)anchor & (int)GUIAnchor::RIGHT) {

		}
		else {
			assert(false);
		}

		for (int i = m_Commands.size() - numCommands; i < m_Commands.size(); ++i) {
			m_Commands[i].x1 += adjustment.x;
			m_Commands[i].x2 += adjustment.x;
			m_Commands[i].y1 += adjustment.y;
			m_Commands[i].y2 += adjustment.y;
		}
	}

	int CGUIManager::FillCommandQueueWithText(const std::string& font, const std::string& text, Vect4f sizeLimits, const CColor& _color, Vect4f *textBox_ ) {
		Vect4f dummy;

		if (textBox_ == nullptr) textBox_ = &dummy;

		*textBox_ = Vect4f(0.0f, 0.0f, 0.0f, 0.0f);

		/*
		assert(m_LineHeightPerFont.find(font) != m_LineHeightPerFont.end());
		assert(m_BasePerFont.find(font) != m_BasePerFont.end());
		assert(m_CharactersPerFont.find(font) != m_CharactersPerFont.end());
		assert(m_KerningsPerFont.find(font) != m_KerningsPerFont.end());
		assert(m_TexturePerFont.find(font) != m_TexturePerFont.end());*/

		int lineHeight = m_LineHeightPerFont[font];
		int base = m_BasePerFont[font];
		const std::unordered_map<wchar_t, FontChar> &l_CharacterMap = m_CharactersPerFont[font];
		const std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>> &l_Kernings = m_KerningsPerFont[font];
		const std::vector<SpriteInfo*> &l_TextureArray = m_TexturePerFont[font];

		wchar_t last = 0;
		int cursorX = 0, cursorY = lineHeight;
		float spriteWidth = l_TextureArray[0]->spriteMap->w;
		float spriteHeight = l_TextureArray[0]->spriteMap->h;
		int addedCommands = 0;
		int index = 1;
		std::string initialText = text;

		for (char c : initialText) {
			index++;
			if (c == '\n') {
				cursorY += lineHeight;
				cursorX = 0;
				last = 0;
			}
			else {
				auto it = l_CharacterMap.find((wchar_t)c);
				if (it != l_CharacterMap.end()) {
					const FontChar &fontChar = it->second;

					auto it1 = l_Kernings.find(last);

					if (it1 != l_Kernings.end()) {
						auto it2 = it1->second.find(c);
						if (it2 != it1->second.end()) {
							int kerning = it2->second;
							cursorX += kerning;
						}
					}

					GUICommand command = {};

					command.sprite = l_TextureArray[fontChar.page];
	
					command.x1 = cursorX + fontChar.xoffset;
					command.x2 = command.x1 + fontChar.width;
					command.y1 = cursorY - base + fontChar.yoffset;
					command.y2 = (command.y1 + fontChar.height);

					command.u1 = (float)fontChar.x / spriteWidth;
					command.u2 = (float)(fontChar.x + fontChar.width) / spriteWidth;
					command.v1 = (float)fontChar.y / spriteHeight;
					command.v2 = (float)(fontChar.y + fontChar.height) / spriteHeight;
					
					command.color = _color;

					m_Commands.push_back(command);
					++addedCommands;
					last = c;
					cursorX += fontChar.xadvance;

					if (c != '_') {
						if (command.x1 < textBox_->x) textBox_->x = command.x1;
						if (command.y1 < textBox_->y) textBox_->y = command.y1;
						if (command.x2 > textBox_->z) textBox_->z = command.x2;
						if (command.y2 > textBox_->w) textBox_->w = command.y2;
					}

		
				
				}
			}
		}
		return addedCommands;
	}

	SliderResult CGUIManager::DoSlider(const std::string& aGUID, const std::string& aSliderID, const GUIPosition& position, float aMinValue, float aMaxValue, float aCurrentValue)
	{
		SliderResult l_Result;
		bool l_RealResult = false;
		SliderInfo *l_Info = m_SliderInfos.Get(aSliderID);

		if (m_ActiveItem == aGUID)
		{
			if (m_MouseWentReleased)
			{
				if (m_HotItem == aGUID)
				{
					l_RealResult = true;
				}
				SetNotActive(aGUID);
			}
		}
		else if (m_HotItem == aGUID)
		{
			if (m_MouseWentPressed)
			{
				SetActive(aGUID);
			}
		}

		float factor = (float)(m_MouseX - position._x) / ((float)position._width);
		factor = mathUtils::Clamp<float>(factor, 0.0f, 1.0f);
		l_Result.temp = aMinValue + (aMaxValue - aMinValue) * factor;

		if (l_RealResult)
		{
			l_Result.real = l_Result.temp;
		}
		else if (m_ActiveItem == aGUID)
		{
			l_Result.real = aCurrentValue;
		}
		else
		{
			l_Result.temp = aCurrentValue;
			l_Result.real = aCurrentValue;
		}

		float l_HandlePosition = position._x + position._width * (l_Result.temp - aMinValue) / (aMaxValue - aMinValue);
		float l_RealHandleWidth = l_Info->handleRelativeWidth * position._width;
		float l_RealHandleHeight = l_Info->handleRelativeHeight * position._height;

		int l_RealHandleX = (int) (l_HandlePosition - l_RealHandleWidth * 0.5f);
		int l_RealHandleY = (int) (position._y + position._height * 0.5f - l_RealHandleHeight * 0.5f);


		if (IsMouseInside((float)m_MouseX, (float)m_MouseY, position._x, position._y, position._width, position._height))
		{
			SetHot(aGUID);
		}
		else if (IsMouseInside((float)m_MouseX, (float)m_MouseY, l_RealHandleX, l_RealHandleY, l_RealHandleWidth, l_RealHandleHeight))
		{
			SetHot(aGUID);
		}
		else
		{
			SetNotHot(aGUID);
		}

		// Base
		GUICommand l_CommandBase = {
			l_Info->base,
			position._x, position._y, position._x + position._width, position._y + position._height,
			0, 0, 1, 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_CommandBase);

		// Top
		GUICommand l_CommandTop = {
			l_Info->top,
			position._x, position._y, l_HandlePosition, position._y + position._height,
			0, 0, (l_Result.temp - aMinValue) / (aMaxValue - aMinValue), 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_CommandTop);

		// Handle
		GUICommand l_CommandHandle = {
			(m_ActiveItem == aGUID && m_HotItem == aGUID) ? l_Info->pressed_handle : l_Info->handle,
			l_RealHandleX, l_RealHandleY, l_RealHandleX + l_RealHandleWidth, l_RealHandleY + l_RealHandleHeight,
			0, 0, 1, 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_CommandHandle);

		return l_Result;
	}

	void CGUIManager::DoImage(const std::string& aGUID, const std::string& aImageID, const GUIPosition& position)
	{
		ImageInfo *l_Info = m_ImageInfos.Get(aImageID);

		// Image
		GUICommand l_Command = {
			l_Info->img,
			position._x, position._y, position._x + position._width, position._y + position._height,
			0, 0, 1, 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_Command);
	}

	void CGUIManager::DoBar(const std::string& aGUID, const std::string& aSliderID, const GUIPosition& position, float aMinValue, float aMaxValue, float aCurrentValue)
	{
		BarInfo *l_Info = m_BarInfos.Get(aSliderID);
		float l_HandlePosition = position._x + position._width * (aCurrentValue - aMinValue) / (aMaxValue - aMinValue);

		// Base
		GUICommand l_CommandBase = {
			l_Info->base,
			position._x, position._y, position._x + position._width, position._y + position._height,
			0, 0, 1, 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_CommandBase);

		// Top
		GUICommand l_CommandTop = {
			l_Info->top,
			position._x, position._y, l_HandlePosition, position._y + position._height,
			0, 0, (aCurrentValue - aMinValue) / (aMaxValue - aMinValue), 1,
			CColor(1, 1, 1, 1)
		};
		m_Commands.push_back(l_CommandTop);
	}

	void CGUIManager::DoZeldaBar(const std::string& aGUID, const std::string& aZeldaID, const GUIPosition& position, int aMaxValue, int aCurrentValue)
	{
		ZeldaInfo *l_Info = m_ZeldaInfos.Get(aZeldaID);
		SpriteInfo *l_SpriteInfo;
		Assert(aMaxValue % 2 == 0, "Max value debe ser m�ltiplo de dos, ya que cuenta mitades!");

		int l_TotalHearts = aMaxValue / 2;
		int l_FullHearts = aCurrentValue / 2;
		int l_HalfHearts = aCurrentValue % 2;
		int l_EmptyHearts = l_TotalHearts - l_FullHearts - l_HalfHearts;
		float l_OffsetX = 0.0f;
		float l_SpriteWidth = 0.0f;

		l_SpriteInfo = l_Info->full;
		l_SpriteWidth = l_SpriteInfo->u2 - l_SpriteInfo->u1;
		for (int i = 0; i < l_FullHearts; ++i)
		{
			float l_PosX = position._x + l_OffsetX;
			GUICommand l_HeartCommand = {
				l_SpriteInfo,
				l_PosX, position._y, l_PosX + l_SpriteWidth, position._y + position._height,
				0, 0, 1, 1,
				CColor(1, 1, 1, 1)
			};
			m_Commands.push_back(l_HeartCommand);
			l_OffsetX += l_SpriteWidth;
		}
		l_SpriteInfo = l_Info->half;
		l_SpriteWidth = l_SpriteInfo->u2 - l_SpriteInfo->u1;
		for (int i = 0; i < l_HalfHearts; ++i)
		{
			float l_PosX = position._x + l_OffsetX;
			GUICommand l_HeartCommand = {
				l_SpriteInfo,
				l_PosX, position._y, l_PosX + l_SpriteWidth, position._y + position._height,
				0, 0, 1, 1,
				CColor(1, 1, 1, 1)
			};
			m_Commands.push_back(l_HeartCommand);
			l_OffsetX += l_SpriteWidth;
		}
		l_SpriteInfo = l_Info->empty;
		l_SpriteWidth = l_SpriteInfo->u2 - l_SpriteInfo->u1;
		for (int i = 0; i < l_EmptyHearts; ++i)
		{
			float l_PosX = position._x + l_OffsetX;
			GUICommand l_HeartCommand = {
				l_SpriteInfo,
				l_PosX, position._y, l_PosX + l_SpriteWidth, position._y + position._height,
				0, 0, 1, 1,
				CColor(1, 1, 1, 1)
			};
			m_Commands.push_back(l_HeartCommand);
			l_OffsetX += l_SpriteWidth;
		}
	}

	std::string CGUIManager::DoConsole(const std::string& aGUID, const std::string& aConsoleID, GUIPosition& position, Vect4f sizeLimits) {
		TextBoxInfo *l_textboxInfo = m_TextBoxInfos.Get(aConsoleID);
		l_textboxInfo->padding = Vect4f(20.0f, 20.0f, 20.0f, 20.0f);
		l_textboxInfo->position = Vect2f(position._x, position._y);

		std::string l_text = DoTextBox(aGUID, aConsoleID, position, sizeLimits, false);
		
		position._width = l_textboxInfo->size.x + l_textboxInfo->padding.x + l_textboxInfo->padding.z;
		position._height = l_textboxInfo->size.y + l_textboxInfo->padding.y + l_textboxInfo->padding.w;
		position._x -= l_textboxInfo->padding.x;
		position._y -= l_textboxInfo->padding.y;

		DoButton(aGUID, aConsoleID, position);

		return l_text;
	}

	std::string CGUIManager::DoTextBox(const std::string& aGUID, const std::string& aTextBoxID, const GUIPosition& position, Vect4f sizeLimits, bool readOnly) {
		TextBoxInfo *l_textboxInfo = m_TextBoxInfos.Get(aTextBoxID);
		std::string activeText = l_textboxInfo->text;
		std::string displayText = activeText;

		if (!readOnly) {

			//if (IsMouseInside((float)m_MouseX, (float)m_MouseY, position._x, position._y, position._width, position._height))
			if (IsMouseInside((float)m_MouseX, (float)m_MouseY, position._x, position._y, l_textboxInfo->size.x, l_textboxInfo->size.y))
			{
				if (m_MouseWentReleased) {
					SetActive(aGUID);
					SetSelected(aGUID);
				}
				SetHot(aGUID);

			}
			else
			{
				SetNotHot(aGUID);

				if (m_MouseWentReleased) {
					SetNotSelected(aGUID);
				}
			}

			if (aGUID == m_SelectedItem) {

				CActionManager *im = &engine::CEngine::GetInstance().GetActionManager();

				char lastChar = im->ConsumeLastChar();


				bool shiftKeyDown = (GetKeyState(VK_SHIFT) & 0x8000);
				bool enterKeyDown = (GetKeyState(VK_RETURN) & 0x8000);

				if (shiftKeyDown &&  im->KeyBecomesPressed('\r')) { //Si las 2 teclas estan pulsadas, el enter esta pulsado y este frame es el cambio de estado.
					activeText += '\n';
				}
				else if (lastChar >= 0x20 && lastChar < 255) { //Del 32 (espacio) hasta el 255
					activeText += lastChar;
				}
				else if (lastChar == '\r') { //Carriage return (Enter)
					engine::CEngine::GetInstance().GetScriptManager().RunCode(displayText);
					activeText.clear();
				}
				else if (lastChar == '\b') { //Backspace
					activeText = displayText.substr(0, displayText.length() - 1);
				}

				float l_currentTimer = std::fmod(m_animationTimer, 1.0f);
				if (l_currentTimer > 1.0f) {
					displayText = activeText;
				}
				else if (l_currentTimer > 0.5f) {
					displayText = activeText + "_";
				}
				else {
					displayText = activeText;
				}

			}
			l_textboxInfo->text = activeText;
		}
		Vect4f textSizes;

		FillCommandQueueWithText("Arial", displayText, sizeLimits, Vect2f(position._x, position._y), &textSizes, position._anchor);
	
		//Resize de ventana para acomodarla a los tama�os m�nimos y m�ximos
		Vect2f l_newTextSize= Vect2f(textSizes.z - textSizes.x, textSizes.w - textSizes.y);
	
		if (l_newTextSize.x > sizeLimits.z) {
			l_newTextSize.x = sizeLimits.z;
		}
		else if (l_newTextSize.x < sizeLimits.x) {
			l_newTextSize.x = sizeLimits.x;
		}

		if (l_newTextSize.y > sizeLimits.w) {
			l_newTextSize.y = sizeLimits.w;
		}
		else if (l_newTextSize.y < sizeLimits.y) {
			l_newTextSize.y = sizeLimits.y;
		}
		
		l_textboxInfo->size.x = l_newTextSize.x;
		l_textboxInfo->size.y = l_newTextSize.y;

		return activeText;
	}

	bool CGUIManager::IsMouseInside(float x, float y, float px, float py, float w, float h)
	{
		if (x >= px && x <= px + w)
		{
			if (y >= py && y <= py + h)
			{
				return true;
			}
		}
		return false;
	}

	GUIPosition::GUIPosition(float x, float y, float w, float h, GUIAnchor anchor, GUICoordType anchorCoordType, GUICoordType sizeCoordType)
	{
		CRenderManager &l_RM = CEngine::GetInstance().GetRenderManager();
		int screenWidth = l_RM.GetCurrentWidth();
		int screenHeight = l_RM.GetCurrentHeight();
		float unitPixelSizeX = 1.0f;
		float unitPixelSizeY = 1.0f;
		switch (sizeCoordType)
		{
		case GUICoordType::GUI_ABSOLUTE:
			_width = w;
			_height = h;
			break;
		case GUICoordType::GUI_RELATIVE:
			_width = w * (float)screenWidth;
			_height = h * (float)screenHeight;
			break;
		case GUICoordType::GUI_RELATIVE_WIDTH:
			_width = w * (float)screenWidth;
			_height = h * (float)screenWidth;
			break;
		case GUICoordType::GUI_RELATIVE_HEIGHT:
			_width = w * (float)screenHeight;
			_height = h * (float)screenHeight;
			break;
		default:
			Assert(false, "Tipo de coordenadas para tama�o no definido");
			break;
		}
		switch (anchorCoordType)
		{
		case GUICoordType::GUI_ABSOLUTE:
			if (x < 0.0f)
			{
				x = screenWidth + x;
			}
			if (y < 0.0f)
			{
				y = screenHeight + y;
			}
			break;
		case GUICoordType::GUI_RELATIVE:
			unitPixelSizeX = screenWidth;
			unitPixelSizeY = screenHeight;
			if (x < 0.0f)
			{
				x = 1.0f + x;
			}
			if (y < 0.0f)
			{
				y = 1.0f + y;
			}
			break;
		case GUICoordType::GUI_RELATIVE_WIDTH:
			unitPixelSizeX = screenWidth;
			if (x < 0.0f)
			{
				x = 1.0f + x;
			}
			if (y < 0.0f)
			{
				y = screenHeight + y;
			}
			break;
		case GUICoordType::GUI_RELATIVE_HEIGHT:
			unitPixelSizeY = screenHeight;
			if (x < 0.0f)
			{
				x = screenWidth + x;
			}
			if (y < 0.0f)
			{
				y = 1.0f + y;
			}
			break;
		default:
			Assert(false, "Tipo de coordenadas no definido");
			break;
		}

		if ((int)anchor & (int)GUIAnchor::LEFT)
		{
			_x = x * unitPixelSizeX;
		}
		else if ((int)anchor & (int)GUIAnchor::CENTER)
		{
			_x = x * unitPixelSizeX - _width * 0.5f;
		}
		else if ((int)anchor & (int)GUIAnchor::RIGHT)
		{
			_x = x * unitPixelSizeX - _width;
		}
		else {
			Assert(false, "Qu� narices ha pasado con los anchors aqu�?");
		}

		if ((int)anchor & (int)GUIAnchor::TOP)
		{
			_y = y * unitPixelSizeY;
		}
		else if ((int)anchor & (int)GUIAnchor::MID)
		{
			_y = y * unitPixelSizeY - _height * 0.5f;
		}
		else if ((int)anchor & (int)GUIAnchor::BOTTOM)
		{
			_y = y * unitPixelSizeY - _height;
		}
		else {
			Assert(false, "Qu� narices ha pasado con los anchors aqu�?");
		}
	}
}

