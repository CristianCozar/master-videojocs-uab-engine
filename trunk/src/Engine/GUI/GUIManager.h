#pragma once

#define MAX_VERTICES_PER_CALL 1026

#include <string>
#include "Utils\TemplatedMap.h"
#include "Utils\TemplatedMapVector.h"
#include "Math\Color.h"
#include "Mesh\TemplatedGeometry.h"
#include "LayoutUtility.h"
#include "Utils\EnumToString.h"
#include <iostream>
#include <unordered_map>

namespace engine
{
	enum GUICoordType
	{
		GUI_ABSOLUTE,
		GUI_RELATIVE,
		GUI_RELATIVE_WIDTH,
		GUI_RELATIVE_HEIGHT
	};

	enum GUIAnchor
	{
		TOP = 0x1,
		MID = 0x2,
		BOTTOM = 0x4,

		LEFT = 0x10,
		CENTER = 0x20,
		RIGHT = 0x40,

		TOP_LEFT =		TOP | LEFT,
		TOP_CENTER =	TOP | CENTER,
		TOP_RIGHT =		TOP | RIGHT,
		MID_LEFT =		MID | LEFT,
		MID_CENTER =	MID | CENTER,
		MID_RIGHT =		MID | RIGHT,
		BOTTOM_LEFT =	BOTTOM | LEFT,
		BOTTOM_CENTER =	BOTTOM | CENTER,
		BOTTOM_RIGHT =	BOTTOM | RIGHT
	};

	class GUIPosition
	{
	public:
		float _x, _y, _width, _height;
		GUIAnchor _anchor;
		GUICoordType _anchorCoordType, _sizeCoordType;
		GUIPosition::GUIPosition(float x, float y, float w, float h, GUIAnchor anchor = GUIAnchor::TOP_LEFT, GUICoordType anchorCoordType = GUICoordType::GUI_ABSOLUTE, GUICoordType sizeCoordType = GUICoordType::GUI_ABSOLUTE);
	};

	struct SpriteMapInfo
	{
		std::string material;
		int w, h;
	};

	struct SpriteInfo
	{
		SpriteMapInfo* spriteMap;
		float u1, v1, u2, v2;
	};

	struct GUICommand
	{
		SpriteInfo *sprite;
		int x1, y1, x2, y2;
		float u1, v1, u2, v2;
		CColor color;
	};

	struct SliderResult
	{
		float real;
		float temp;
	};

	struct ButtonInfo
	{
		SpriteInfo *normal, *highlight, *pressed;
	};

	struct SliderInfo
	{
		SpriteInfo *base, *top, *handle, *pressed_handle;
		float handleRelativeWidth, handleRelativeHeight;
	};

	struct BarInfo
	{
		SpriteInfo *base, *top;
	};

	struct ImageInfo
	{
		SpriteInfo *img;
	};

	struct ZeldaInfo
	{
		SpriteInfo *full, *half, *empty;
	};

	struct TextBoxInfo
	{
		std::string text;
		Vect2f position;
		Vect2f size; //x -> current width, y -> current height
		Vect4f sizeLimits; //x -> minWidth, y -> minHeight, z -> maxWidth, w -> maxHeight
		Vect4f padding;

		TextBoxInfo() {
			position = Vect2f(0.0f, 0.0f);
			size = Vect2f(0.0f, 0.0f);
			sizeLimits = Vect4f(0.0f, 0.0f, 0.0f, 0.0f);
			padding = Vect4f(0.0f, 0.0f, 0.0f, 0.0f);
		}
	};

	struct FontChar {
		uint16 x, y, width, height;
		int16 xoffset, yoffset, xadvance;
		uint8 page, chnl;

		FontChar();
		FontChar(uint16 ax, uint16 ay, uint16 awidth, uint16 aheight, int16 axoffset, int16  ayoffset, int16 axadvance, uint8 apage, uint8 achnl) {
			x = ax, y = ay, width = awidth, height = aheight;
			xoffset = axoffset, yoffset = ayoffset, xadvance = axadvance;
			page = apage, chnl = achnl;
		}
	};

	class CRenderManager;
	class CGUIManager
	{

		public:
			CGUIManager();
			~CGUIManager();
			void SetActive(const std::string& aID);
			void SetNotActive(const std::string& aID);
			void SetHot(const std::string& aID);
			void SetNotHot(const std::string& aID);
			void SetSelected(const std::string& aID);
			void SetNotSelected(const std::string& aID);
			bool Load(const std::string& aFilename);
			void Update(float elapsedTime);
			void PostUpdate();
			void Render(CRenderManager& aRM);
			bool DoButton(const std::string& aGUID, const std::string& aButtonID, const GUIPosition& position);
			void DoImage(const std::string& aGUID, const std::string& aImageID, const GUIPosition& position);
			void FillCommandQueueWithText(const std::string& font, const std::string& text, const Vect4f sizeLimits,  Vect2f coord, Vect4f *textSizes, GUIAnchor anchor = GUIAnchor::BOTTOM_LEFT, const CColor& _color = CColor(1, 1, 1, 1));
			int FillCommandQueueWithText(const std::string& font, const std::string& text, const Vect4f sizeLimits, const CColor& _color = CColor(1.0f, 1.0f, 1.0f, 1.0f), Vect4f *textBox_ = nullptr);
			SliderResult DoSlider(const std::string& aGUID, const std::string& aSliderID, const GUIPosition& position, float aMinValue, float aMaxValue, float aCurrentValue);
			void DoBar(const std::string& aGUID, const std::string& aBarID, const GUIPosition& position, float aMinValue, float aMaxValue, float aCurrentValue);
			void DoZeldaBar(const std::string& aGUID, const std::string& aBarID, const GUIPosition& position, int aMaxValue, int aCurrentValue);
			std::string DoTextBox(const std::string& aGUID, const std::string& aButtonID, const GUIPosition& position, Vect4f textboxSize, bool readOnly);
			std::string DoConsole(const std::string& aGUID, const std::string& aButtonID, GUIPosition& position, Vect4f textboxSize);
	private:
			bool IsMouseInside(float x, float y, float px, float py, float w, float h);
			void RadixSort();
			std::string m_ActiveItem;
			std::string m_HotItem;
			std::string m_SelectedItem;
			float m_animationTimer;
			bool m_InputUpToDate;
			int m_MouseX;
			int m_MouseY;
			bool m_MouseWentPressed;
			bool m_MouseWentReleased;
			base::utils::CTemplatedMapVector<SpriteMapInfo> m_SpriteMaps;
			base::utils::CTemplatedMap<SpriteInfo> m_Sprites;
			base::utils::CTemplatedMap<ButtonInfo> m_ButtonInfos;
			base::utils::CTemplatedMap<SliderInfo> m_SliderInfos;
			base::utils::CTemplatedMap<BarInfo> m_BarInfos;
			base::utils::CTemplatedMap<ZeldaInfo> m_ZeldaInfos;
			base::utils::CTemplatedMap<TextBoxInfo> m_TextBoxInfos;
			base::utils::CTemplatedMap<ImageInfo> m_ImageInfos;
			
			std::unordered_map <std::string, int16 > m_LineHeightPerFont;
			std::unordered_map <std::string, int16 > m_BasePerFont;
			std::unordered_map <std::string, std::unordered_map<wchar_t, FontChar> > m_CharactersPerFont;
			std::unordered_map <std::string, std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>>> m_KerningsPerFont;
			std::unordered_map <std::string, std::vector<SpriteInfo*>> m_TexturePerFont;

			std::vector<GUICommand> m_Commands;
			std::string m_Filename;
			VertexTypes::GUIVertex m_CurrentBufferData[MAX_VERTICES_PER_CALL];
			CTemplatedGeometry<VertexTypes::GUIVertex>* m_Geometry;
	};
}

Begin_Enum_String(engine::GUIAnchor)
{
	Enum_String_Id(engine::GUIAnchor::BOTTOM, "BOTTOM");
	Enum_String_Id(engine::GUIAnchor::TOP, "TOP");
	Enum_String_Id(engine::GUIAnchor::CENTER, "CENTER");
	Enum_String_Id(engine::GUIAnchor::LEFT, "LEFT");
	Enum_String_Id(engine::GUIAnchor::RIGHT, "RIGHT");
	Enum_String_Id(engine::GUIAnchor::MID, "MID");

	Enum_String_Id(engine::GUIAnchor::BOTTOM_CENTER, "BOTTOM_CENTER");
	Enum_String_Id(engine::GUIAnchor::BOTTOM_LEFT, "BOTTOM_LEFT");
	Enum_String_Id(engine::GUIAnchor::BOTTOM_RIGHT, "BOTTOM_RIGHT");

	Enum_String_Id(engine::GUIAnchor::MID_CENTER, "MID_CENTER");
	Enum_String_Id(engine::GUIAnchor::MID_LEFT, "MID_LEFT");
	Enum_String_Id(engine::GUIAnchor::MID_RIGHT, "MID_RIGHT");

	Enum_String_Id(engine::GUIAnchor::TOP_CENTER, "TOP_CENTER");
	Enum_String_Id(engine::GUIAnchor::TOP_LEFT, "TOP_LEFT");
	Enum_String_Id(engine::GUIAnchor::TOP_RIGHT, "TOP_RIGHT");
}
End_Enum_String;

Begin_Enum_String(engine::GUICoordType)
{
	Enum_String_Id(engine::GUICoordType::GUI_ABSOLUTE, "GUI_ABSOLUTE");
	Enum_String_Id(engine::GUICoordType::GUI_RELATIVE, "GUI_RELATIVE");
	Enum_String_Id(engine::GUICoordType::GUI_RELATIVE_HEIGHT, "GUI_RELATIVE_HEIGHT");
	Enum_String_Id(engine::GUICoordType::GUI_RELATIVE_WIDTH, "GUI_RELATIVE_WIDTH");
}
End_Enum_String;
