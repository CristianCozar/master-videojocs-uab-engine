#include "Utils\lua_utils.h"
#include "Scripting\ScriptManager.h"
#include "Math\Transform.h"

#include "Engine.h"
#include "Scenes\SceneManager.h"
#include "Scenes\SceneNode.h"

using namespace engine;
namespace lua {

	CTransform* GetTransform(const std::string& scene, const std::string& layer, const std::string& name)
	{
		CSceneManager &l_manager = CEngine::GetInstance().GetSceneManager();
		CScene* l_scene = l_manager.Get(scene);
		CLayer* l_layer = l_scene->Get(layer);
		CTransform *node = (CTransform*)l_layer->Get(name);
		//CSceneNode *node = CEngine::GetInstance().GetSceneManager().Get(scene)->Get(layer)->Get(name);
		return node;
	}

	template <> void BindClass<CTransform>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "get_transform", &lua::GetTransform);
		module(aLua)
			[
				class_<CTransform>("transform")
				.def(constructor<>())
				.def_readwrite("position", &CTransform::m_Position)
				.def_readwrite("scale", &CTransform::m_Scale)
				.def_readwrite("yaw", &CTransform::m_Yaw)
				.def_readwrite("pitch",&CTransform::m_Pitch)
				.def_readwrite("roll",&CTransform::m_Roll)
				.property("forward", &CTransform::GetForward, &CTransform::SetForward)
				.property("right", &CTransform::GetRight, &CTransform::SetRight)
				.property("up", &CTransform::GetUp, &CTransform::SetUp)
				// TODO: El resto de parametros
			];
	}
}