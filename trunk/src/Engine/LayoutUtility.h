#pragma once

#include <string>

#include "Mesh/VertexsTypes_HelperMacros.h"
#include "RenderManager.h"


namespace VertexTypes
{
	enum EFlags
	{
		ePosition = 0x0001,
		eColor = 0x0002,
		eNormal = 0x0004,
		eTangent = 0x0008,
		eBinormal = 0x0010,
		eUV = 0x0020,
		eUV2 = 0x0040,
		ePosition4 = 0x0080,
		eDummy = 0x0100,
		eWeight = 0x0200,
		eIndices = 0x0400,
		eBump = eNormal | eTangent | eBinormal,

	};

	struct PositionNormal
	{
		POSITION; NORMAL;
		GET_VERTEX_FLAGS(ePosition | eNormal);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_NORMAL(12),
		}
		END_INPUT_LAYOUT
	};

	struct PositionNormalUV
	{
		POSITION; NORMAL; UV;
		GET_VERTEX_FLAGS(ePosition | eNormal | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_NORMAL(12),
			LAYOUT_UV(24),
		}
		END_INPUT_LAYOUT
	};

	struct PositionNormalUVColor
	{
		POSITION; NORMAL; UV; COLOR;
		GET_VERTEX_FLAGS(ePosition | eNormal | eUV | eColor);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_NORMAL(12),
			LAYOUT_UV(24),
			LAYOUT_COLOR(32),
		}
		END_INPUT_LAYOUT
	};

	struct PositionNormalUVUV2
	{
		POSITION; NORMAL; UV; UV2;
		GET_VERTEX_FLAGS(ePosition | eNormal | eUV | eUV2);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_NORMAL(12),
			LAYOUT_UV(24),
			LAYOUT_UV2(32),
		}
		END_INPUT_LAYOUT
	};

	struct PositionBump
	{
		POSITION; BUMP;
		GET_VERTEX_FLAGS(ePosition | eBump);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_BUMP(12),
		}
		END_INPUT_LAYOUT
	};

	struct PositionBumpUV
	{
		POSITION; BUMP; UV;
		GET_VERTEX_FLAGS(ePosition | eBump | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_BUMP(12),
			LAYOUT_UV(56),

		}
		END_INPUT_LAYOUT
	};

	struct PositionBumpUVUV2
	{
		POSITION; BUMP; UV; UV2;
		GET_VERTEX_FLAGS(ePosition | eBump | eUV | eUV2);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_BUMP(12),
			LAYOUT_UV(56),
			LAYOUT_UV2(64),

		}
		END_INPUT_LAYOUT
	};

	struct PositionUV
	{
		POSITION; UV;
		GET_VERTEX_FLAGS(ePosition | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_UV(12),
		}
		END_INPUT_LAYOUT
	};

	struct PositionWeightIndicesNormalUV
	{
		POSITION; WEIGHT; INDICES; NORMAL; UV;
		GET_VERTEX_FLAGS(ePosition | eWeight | eIndices | eNormal | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_WEIGHT(12),
			LAYOUT_INDICES(28),
			LAYOUT_NORMAL(44),
			LAYOUT_UV(56),
		}
		END_INPUT_LAYOUT
	};

	struct PositionWeightIndicesBumpUV
	{
		POSITION; WEIGHT; INDICES; BUMP; UV;
		GET_VERTEX_FLAGS(ePosition | eWeight | eIndices | eBump | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_WEIGHT(12),
			LAYOUT_INDICES(28),
			LAYOUT_BUMP(44),
			LAYOUT_UV(88),
		}
		END_INPUT_LAYOUT
	};

	struct ParticleVertex
	{
		POSITION; COLOR; UV; UV2;
		GET_VERTEX_FLAGS(ePosition | eColor | eUV | eUV2);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_COLOR(12),
			LAYOUT_UV(28),
			LAYOUT_UV2(36),
		}
		END_INPUT_LAYOUT
	};

	struct GUIVertex
	{
		POSITION; COLOR; UV;
		GET_VERTEX_FLAGS(ePosition | eColor | eUV);
		BEGIN_INPUT_LAYOUT
		{
			LAYOUT_POSITION(0),
			LAYOUT_COLOR(12),
			LAYOUT_UV(28),
		}
		END_INPUT_LAYOUT
	};


	uint32 GetVertexSize(uint32 aVertexFlags);

	bool CreateInputLayout(engine::CRenderManager& aRenderManager, uint32 aVertexFlags, ID3DBlob* aBlob, ID3D11InputLayout ** aVertexLayout);

	uint32 GetFlagsFromString(const std::string & aString);


}


