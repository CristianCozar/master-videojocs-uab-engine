#pragma once

#include "CameraController.h"

namespace engine
{

	class CTPSCameraController : public CCameraController
	{
	public:
		float yawSpeed, pitchSpeed;
		float zoomSpeed, frontSpeed, sideSpeed;
		Vect3f objectPosition;

		CTPSCameraController(Vect3f center = Vect3f(0.0f, 0.0f, 0.0f), Vect3f offset = Vect3f(-3.0f, 1.0f, -1.0f), float maxPitch = 1.5f, float minPitch = -1.5f, float maxZoom = 20.0f, float noZoom = 1.0f);
		virtual ~CTPSCameraController();

		virtual void Update(float ElapsedTime) override;
		virtual void RenderDebugGUI() override;
		virtual void HandleActions(CActionManager *actionManager) override;
		virtual void Render(CRenderManager *renderManager) override;

	private:
		float yaw, pitch, zoom;

		float maxPitch, minPitch;
		float maxZoom, noZoom;
		Vect3f offset;
	};

}