#include "Math\Matrix44.h"
#include "Mesh\AxisAlignedBoundingBox.h"
#include "Mesh\BoundingSphere.h"

namespace engine
{
	class CFrustum
	{
	public:
		CFrustum() {}
		virtual ~CFrustum() {}
		void Update(const Mat44f &ViewProj);
		bool IsVisible(const CBoundingSphere &aBoundingSphere, const Vect3f &aPos, const Vect3f &aScale) const;
		bool IsVisible(const CAxisAlignedBoundingBox &aAABB, const Vect3f &aPos) const;
	private:
		float m_Proj[16];
		float m_Modl[16];
		float m_Clip[16];
		float m_Frustum[6][4];
	};
}