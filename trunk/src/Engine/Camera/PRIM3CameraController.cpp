#include "PRIM3CameraController.h"
#include "Debug\imgui.h"
#include "Utils\lua_utils.h"
#include "Engine.h"
#include "Scripting\ScriptManager.h"

namespace engine
{

	CPRIM3CameraController::CPRIM3CameraController(Vect3f center, float maxPitch, float minPitch, float maxZoom, float minZoom) :
		m_MaxPitch(maxPitch), m_MinPitch(minPitch), m_MaxZoom(maxZoom), m_NoZoom(minZoom)
	{
		m_Pitch = 0.3f;
		m_Yaw = 1.5f;
		m_Zoom = 10.0f;
		m_Position = center;
		m_Type = CC_Prim3;

		m_YawSpeed = 0.0f;
		m_PitchSpeed = 0.0f;
		m_ZoomSpeed = 0.0f;
		m_FrontSpeed = 0.0f;
		m_SideSpeed = 0.0f;
		m_VerticalSpeed = 0.0f;
	}

	CPRIM3CameraController::~CPRIM3CameraController()
	{

	}

	void CPRIM3CameraController::Update(float ElapsedTime)
	{
		bool lOk = GLOBAL_LUA_FUNCTION("prim3_camera_update", bool, ElapsedTime);


		/*
		m_Yaw += m_YawSpeed * ElapsedTime;
		m_Pitch += m_PitchSpeed * ElapsedTime;
		m_Zoom -= m_ZoomSpeed * ElapsedTime;

		if (m_Pitch > m_MaxPitch)
			m_Pitch = m_MaxPitch;
		if (m_Pitch < m_MinPitch)
			m_Pitch = m_MinPitch;

		if (m_Zoom > m_MaxZoom)
			m_Zoom = m_MaxZoom;
		if (m_Zoom < m_NoZoom)
			m_Zoom = m_NoZoom;

		if (m_Yaw > mathUtils::PiTimes<float>())
			m_Yaw -= mathUtils::PiTimes<float>(2.0f);
		if (m_Yaw < -mathUtils::PiTimes<float>())
			m_Yaw += mathUtils::PiTimes<float>(2.0f);

		float deltaX = (cos(m_Yaw) * cos(m_Pitch) * m_FrontSpeed) + (sin(m_Yaw) * m_SideSpeed);
		float deltaY = sin(-m_Pitch) * m_FrontSpeed;
		float deltaZ = (sin(m_Yaw) * cos(m_Pitch) * m_FrontSpeed) + (-cos(m_Yaw) * m_SideSpeed);
		deltaX += cos(m_Yaw) * sin(m_Pitch) * m_VerticalSpeed;
		deltaY += cos(m_Pitch) * m_VerticalSpeed;
		deltaZ += sin(m_Yaw) * sin(m_Pitch) * m_VerticalSpeed;

		m_Front.z = sin(m_Yaw) * cos(m_Pitch);
		m_Front.y = -sin(m_Pitch);
		m_Front.x = cos(m_Yaw) * cos(m_Pitch);

		m_Position.x += deltaX;
		m_Position.y += deltaY;
		m_Position.z += deltaZ;

		m_YawSpeed = 0.0f;
		m_PitchSpeed = 0.0f;
		m_ZoomSpeed = 0.0f;
		m_FrontSpeed = 0.0f;
		m_SideSpeed = 0.0f;
		m_VerticalSpeed = 0.0f;*/
	}

	void CPRIM3CameraController::RenderDebugGUI()
	{
		ImGui::Text("Prim3 Camera");
		if (ImGui::CollapsingHeader("Mouse / Keyboard controls"))
		{
			ImGui::Text("Right click and drag to rotate");
			ImGui::Text("WASD / Directional arrows to move");
		}
		if (ImGui::CollapsingHeader("Gamepad controls"))
		{
			ImGui::Text("Right stick to rotate");
			ImGui::Text("Left stick to move");
		}
		ImGui::Separator();
		ImGui::SliderFloat("YAW", &m_Yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
		ImGui::SliderFloat("PITCH", &m_Pitch, m_MinPitch, m_MaxPitch);
		ImGui::SliderFloat("ZOOM", &m_Zoom, m_NoZoom, m_MaxZoom);
		ImGui::Separator();
		ImGui::SliderFloat("X", &m_Position.x, -20.0f, 20.0f);
		ImGui::SliderFloat("Y", &m_Position.y, -20.0f, 20.0f);
		ImGui::SliderFloat("Z", &m_Position.z, -20.0f, 20.0f);
		ImGui::Separator();
		ImGui::SliderFloat("Xf", &m_Front.x, -1.0f, 1.0f);
		ImGui::SliderFloat("Yf", &m_Front.y, -1.0f, 1.0f);
		ImGui::SliderFloat("Zf", &m_Front.z, -1.0f, 1.0f);
	}

	void CPRIM3CameraController::HandleActions(CActionManager *actionManager)
	{
		/*
		if (actionManager->Get("ROTATE")->active)
		{
			if (actionManager->Get("YAW")->active)
			{
				m_YawSpeed = actionManager->Get("YAW")->value;
			}
			if (actionManager->Get("PITCH")->active)
			{
				m_PitchSpeed = actionManager->Get("PITCH")->value;
			}
		}

		if (actionManager->Get("FRONT")->active)
		{
			m_FrontSpeed = actionManager->Get("FRONT")->value;
		}
		if (actionManager->Get("SIDE")->active)
		{
			m_SideSpeed = actionManager->Get("SIDE")->value;
		}
		if (actionManager->Get("VERTICAL")->active)
		{
			m_VerticalSpeed = actionManager->Get("VERTICAL")->value;
		}*/
	}
}