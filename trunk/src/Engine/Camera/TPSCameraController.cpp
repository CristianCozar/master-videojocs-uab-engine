#include "TPSCameraController.h"
#include "Debug\imgui.h"

namespace engine
{

	CTPSCameraController::CTPSCameraController(Vect3f center, Vect3f offset, float maxPitch, float minPitch, float maxZoom, float minZoom) :
		maxPitch(maxPitch), minPitch(minPitch), maxZoom(maxZoom), noZoom(minZoom), offset(offset)
	{
		pitch = 0.0f;
		yaw = 0.0f;
		zoom = 10.0f;
		objectPosition = center;
		m_Position = center + offset;
		m_Type = CC_TPS;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		zoomSpeed = 0.0f;
		frontSpeed = 0.0f;
		sideSpeed = 0.0f;
	}

	CTPSCameraController::~CTPSCameraController()
	{

	}

	void CTPSCameraController::Update(float ElapsedTime)
	{
		yaw += yawSpeed * ElapsedTime;
		pitch += pitchSpeed * ElapsedTime;
		zoom -= zoomSpeed * ElapsedTime;

		if (pitch > maxPitch)
			pitch = maxPitch;
		if (pitch < minPitch)
			pitch = minPitch;

		if (zoom > maxZoom)
			zoom = maxZoom;
		if (zoom < noZoom)
			zoom = noZoom;

		if (yaw > mathUtils::PiTimes<float>())
			yaw -= mathUtils::PiTimes<float>(2.0f);
		if (yaw < -mathUtils::PiTimes<float>())
			yaw += mathUtils::PiTimes<float>(2.0f);

		float deltaX = (cos(yaw) * cos(pitch) * frontSpeed) + (sin(yaw) * sideSpeed);
		float deltaY = sin(-pitch) * frontSpeed;
		float deltaZ = (sin(yaw) * cos(pitch) * frontSpeed) + (-cos(yaw) * sideSpeed);

		m_Front.z = sin(yaw) * cos(pitch);
		m_Front.y = -sin(pitch);
		m_Front.x = cos(yaw) * cos(pitch);

		objectPosition.x += deltaX;
		objectPosition.y += deltaY;
		objectPosition.z += deltaZ;

		/*m_Position.x = objectPosition.x + offset.x * m_Front.x;
		m_Position.y = objectPosition.y + offset.y;
		m_Position.z = objectPosition.z + offset.z * m_Front.z;*/
		m_Position = objectPosition + offset.x * m_Front + offset.z * (m_Front ^ m_Up) + offset.y * m_Up;
		//m_Position = objectPosition + m_Front * zoom;

		//m_Position = objectPosition + offset;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		zoomSpeed = 0.0f;
		frontSpeed = 0.0f;
		sideSpeed = 0.0f;
	}

	void CTPSCameraController::RenderDebugGUI()
	{
		ImGui::Text("TPS Camera");
		if (ImGui::CollapsingHeader("Mouse / Keyboard controls"))
		{
			ImGui::Text("Right click and drag to rotate");
			ImGui::Text("WASD / Directional arrows to move");
		}
		if (ImGui::CollapsingHeader("Gamepad controls"))
		{
			ImGui::Text("Right stick to rotate");
			ImGui::Text("Left stick to move");
		}
		ImGui::Separator();
		ImGui::SliderFloat("YAW", &yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
		ImGui::SliderFloat("PITCH", &pitch, minPitch, maxPitch);
		ImGui::Separator();
		ImGui::SliderFloat("ObjectX", &objectPosition.x, -20.0f, 20.0f);
		ImGui::SliderFloat("ObjectY", &objectPosition.y, -20.0f, 20.0f);
		ImGui::SliderFloat("ObjectZ", &objectPosition.z, -20.0f, 20.0f);
		ImGui::Separator();
		ImGui::SliderFloat("OffsetX", &offset.x, -20.0f, 20.0f);
		ImGui::SliderFloat("OffsetY", &offset.y, -20.0f, 20.0f);
		ImGui::SliderFloat("OffsetZ", &offset.z, -20.0f, 20.0f);
		ImGui::Separator();
		ImGui::SliderFloat("X", &m_Position.x, -20.0f, 20.0f);
		ImGui::SliderFloat("Y", &m_Position.y, -20.0f, 20.0f);
		ImGui::SliderFloat("Z", &m_Position.z, -20.0f, 20.0f);
	}

	void CTPSCameraController::Render(CRenderManager *renderManager)
	{
		renderManager->DrawSphere(1.0f, 0.0f, CColor(1.0f, 1.0f, 1.0f), objectPosition);
	}

	void CTPSCameraController::HandleActions(CActionManager *actionManager)
	{
		if (actionManager->Get("ROTATE")->active)
		{
			if (actionManager->Get("YAW")->active)
			{
				yawSpeed = actionManager->Get("YAW")->value;
			}
			if (actionManager->Get("PITCH")->active)
			{
				pitchSpeed = actionManager->Get("PITCH")->value;
			}
		}

		if (actionManager->Get("FRONT")->active)
		{
			frontSpeed = actionManager->Get("FRONT")->value;
		}
		if (actionManager->Get("SIDE")->active)
		{
			sideSpeed = actionManager->Get("SIDE")->value;
		}
	}

}