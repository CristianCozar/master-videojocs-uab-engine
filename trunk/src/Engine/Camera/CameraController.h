#pragma once

#include "RenderManager.h"
#include "Input\ActionManager.h"


namespace engine
{
	class CCameraController
	{
	public:
		enum CameraType
		{
			CC_Base = 0,
			CC_FPS,
			CC_TPS,
			CC_Spherical,
			CC_Prim3,
		};

		CCameraController() :m_Position(0, 0, 0), m_Front(0, 0, 1), m_Up(0, 1, 0), m_Type(CC_Base) {}
		virtual ~CCameraController() {}
		virtual void Update(float ElapsedTime){};
		virtual void RenderDebugGUI() { ; };
		virtual void HandleActions(CActionManager *actionManager) { ; };
		virtual void Render(CRenderManager *renderManager) { ; };

		void SetToRenderManager(CRenderManager &_RenderManager) const
		{
			_RenderManager.SetViewMatrix(m_Position, m_Position + m_Front, m_Up);
			_RenderManager.SetProjectionMatrix(0.8f, (float)_RenderManager.GetCurrentWidth(), (float)_RenderManager.GetCurrentHeight(), 0.1f, 10000.0f);
			_RenderManager.UpdateFrustum();
		}
		GET_SET(Vect3f, Position);
		GET_SET(Vect3f, Front);
		GET_SET(Vect3f, Up);
		GET_SET(CameraType, Type);

	protected:
		Vect3f m_Position;
		Vect3f m_Front;
		Vect3f m_Up;
		CFrustum m_Frustum;
		CameraType m_Type;
	};
}