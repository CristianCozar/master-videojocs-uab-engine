#include "SphericalCameraController.h"
#include "Debug\imgui.h"

namespace engine
{

	CSphericalCameraController::CSphericalCameraController(Vect3f center, float maxPitch, float minPitch, float maxZoom, float minZoom) :
		maxPitch(maxPitch), minPitch(minPitch), maxZoom(maxZoom), minZoom(minZoom), center(center)
	{
		pitch = 0.0f;
		yaw = 0.0f;
		roll = 0.0f;
		zoom = 10.0f;
		m_Type = CC_Spherical;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		rollSpeed = 0.0f;
		zoomSpeed = 0.0f;;
	}

	CSphericalCameraController::~CSphericalCameraController()
	{

	}

	void CSphericalCameraController::Update(float ElapsedTime)
	{
		yaw += yawSpeed * ElapsedTime;
		pitch += pitchSpeed * ElapsedTime;
		roll += rollSpeed * ElapsedTime;
		zoom -= zoomSpeed * ElapsedTime;

		if (pitch > maxPitch)
			pitch = maxPitch;
		if (pitch < minPitch)
			pitch = minPitch;

		if (zoom > maxZoom)
			zoom = maxZoom;
		if (zoom < minZoom)
			zoom = minZoom;

		if (yaw > mathUtils::PiTimes<float>())
			yaw -= mathUtils::PiTimes<float>(2.0f);
		if (yaw < -mathUtils::PiTimes<float>())
			yaw += mathUtils::PiTimes<float>(2.0f);

		if (roll > mathUtils::PiTimes<float>())
			roll -= mathUtils::PiTimes<float>(2.0f);
		if (roll < -mathUtils::PiTimes<float>())
			roll += mathUtils::PiTimes<float>(2.0f);

		m_Front.x = sin(yaw) * cos(-pitch);
		m_Front.y = -sin(-pitch);
		m_Front.z = -cos(yaw) * cos(-pitch);

		m_Up.x = sin(roll);
		m_Up.y = cos(roll);

		m_Position = center - m_Front * zoom;

		yawSpeed = 0.0f;
		pitchSpeed = 0.0f;
		rollSpeed = 0.0f;
		zoomSpeed = 0.0f;
	}

	void CSphericalCameraController::RenderDebugGUI()
	{
		ImGui::Text("Spherical Camera");
		if (ImGui::CollapsingHeader("Mouse / Keyboard controls"))
		{
			ImGui::Text("Right click and drag to rotate");
			ImGui::Text("Mouse wheel to zoom");
			ImGui::Text("Q & E to roll");
		}
		if (ImGui::CollapsingHeader("Gamepad controls"))
		{
			ImGui::Text("Right stick to rotate");
			ImGui::Text("LT & RT to zoom");
			ImGui::Text("LB & RB to roll");
		}
		ImGui::Separator();
		ImGui::SliderFloat("YAW", &yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
		ImGui::SliderFloat("PITCH", &pitch, minPitch, maxPitch);
		ImGui::SliderFloat("ROLL", &roll, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
		ImGui::SliderFloat("ZOOM", &zoom, minZoom, maxZoom);
		ImGui::Separator();
		ImGui::SliderFloat("UPX", &m_Up.x, -50.0f, 50.0f);
		ImGui::SliderFloat("UPY", &m_Up.y, -50.0f, 50.0f);
	}

	void CSphericalCameraController::HandleActions(CActionManager *actionManager)
	{
		if (actionManager->Get("ROTATE")->active)
		{
			if (actionManager->Get("YAW")->active)
			{
				yawSpeed = actionManager->Get("YAW")->value;
			}
			if (actionManager->Get("PITCH")->active)
			{
				pitchSpeed = actionManager->Get("PITCH")->value;
			}
		}

		if (actionManager->Get("ROLL")->active)
		{
			rollSpeed = actionManager->Get("ROLL")->value;
		}
		
		if (actionManager->Get("ZOOM")->active)
		{
			zoomSpeed = actionManager->Get("ZOOM")->value;
		}
	}

}