#pragma once

#include "CameraController.h"
#include "Utils\Defines.h"

namespace engine
{

	class CPRIM3CameraController : public CCameraController
	{
	public:
		float m_YawSpeed, m_PitchSpeed;
		float m_ZoomSpeed, m_FrontSpeed, m_SideSpeed, m_VerticalSpeed;

		CPRIM3CameraController(Vect3f center = Vect3f(0.0f, 0.0f, 0.0f), float maxPitch = 1.5f, float minPitch = -1.5f, float maxZoom = 20.0f, float noZoom = 1.0f);
		virtual ~CPRIM3CameraController();

		virtual void Update(float ElapsedTime) override;
		virtual void RenderDebugGUI() override;
		virtual void HandleActions(CActionManager *actionManager) override;
		GET_SET(float, YawSpeed);
		GET_SET(float, PitchSpeed);
		GET_SET(float, ZoomSpeed);
		GET_SET(float, FrontSpeed);
		GET_SET(float, SideSpeed);
		GET_SET(float, VerticalSpeed);

		GET_SET(float, Yaw);
		GET_SET(float, Pitch);
		GET_SET(float, Zoom);
		GET_SET(float, MaxPitch);
		GET_SET(float, MinPitch);
		GET_SET(float, MaxZoom);
		GET_SET(float, NoZoom);

	private:
		float m_Yaw, m_Pitch, m_Zoom;

		float m_MaxPitch, m_MinPitch;
		float m_MaxZoom, m_NoZoom;
	};

}