#pragma once

#include "CameraController.h"

namespace engine
{

	class CSphericalCameraController : public CCameraController
	{
	public:
		Vect3f center;
		float yawSpeed, pitchSpeed, rollSpeed;
		float zoomSpeed;

		CSphericalCameraController(Vect3f center = Vect3f(0.0f, 0.0f, 0.0f), float maxPitch = 1.5f, float minPitch = -1.5f, float maxZoom = 20.0f, float minZoom = 1.0f);
		virtual ~CSphericalCameraController();

		virtual void Update(float ElapsedTime) override;
		virtual void RenderDebugGUI() override;
		virtual void HandleActions(CActionManager *actionManager) override;

	private:
		float yaw, pitch, roll, zoom;

		float maxPitch, minPitch;
		float maxZoom, minZoom;
	};

}