#include "ScriptableObject.h"

#include "XML\XML.h"
#include "Debug\CustomAssert.h"
#include "Utils\StringUtils.h"
#include "Utils\lua_utils.h"


namespace engine
{
	CScriptableObject::CScriptableObject(const CXMLElement* aElement) :
		CScript(),
		m_HasStart(false),
		m_HasUpdate(false),
		m_HasDestroy(false)
	{
		Init();
		mFilename = aElement->GetAttribute<std::string>("filename", "NOFILE");
		Assert(mFilename != "NOFILE", "Falta el path del script.");
		std::string l_Name = aElement->GetAttribute<std::string>("name", "NONAME");
		Load(mFilename);
		if (l_Name == "NONAME")
			m_Name = base::utils::GetFilename(mFilename);
		BindClassesAndLibraries(aElement);

		for (const CXMLElement *function = aElement->FirstChildElement("has_function"); function != nullptr; function = function->NextSiblingElement("has_function")) {
			if (function->GetAttribute<std::string>("function", "") == "start")
				m_HasStart = function->BoolAttribute("value");
			else if (function->GetAttribute<std::string>("function", "") == "update")
				m_HasUpdate = function->BoolAttribute("value");
			else if (function->GetAttribute<std::string>("function", "") == "ondestroy")
				m_HasDestroy = function->BoolAttribute("value");
		}

		Start();
	}

	CScriptableObject::CScriptableObject(const CXMLElement* aElement, lua_State *aLua) :
		CScript(aLua),
		m_HasStart(true),
		m_HasUpdate(true),
		m_HasDestroy(true)
	{
		mFilename = aElement->GetAttribute<std::string>("filename", "NOFILE");
		Assert(mFilename != "NOFILE", "Falta el path del script.");
		std::string l_Name = aElement->GetAttribute<std::string>("name", "NONAME");
		Load(mFilename);
		if (l_Name == "NONAME")
			m_Name = base::utils::GetFilename(mFilename);
	}

	CScriptableObject::CScriptableObject(lua_State *aLua) :
		CScript(aLua),
		m_HasStart(true),
		m_HasUpdate(true),
		m_HasDestroy(true)
	{
		mFilename = "data/scripts/gameObject.lua";
		std::string l_Name = "gameObject";
		Load(mFilename);
	}

	bool CScriptableObject::Reload()
	{
		bool lOk = false;
		Release();
		Init();
		lOk = Load(mFilename);
		return lOk;
	}

	CScriptableObject::~CScriptableObject()
	{
		if (m_HasDestroy)
		{
			LUA_FUNCTION(mLS, "ondestroy", void);
		}
	}

	void CScriptableObject::Update(const float &dt)
	{
		if (m_HasUpdate)
		{
			LUA_FUNCTION(mLS, "update", void, dt);
		}
	}

	void CScriptableObject::Start()
	{
		if (m_HasStart)
		{
			LUA_FUNCTION(mLS, "start", void);
		}
	}

	void CScriptableObject::BindClassesAndLibraries(const CXMLElement* aElement)
	{
		// Lua libraries
		const CXMLElement *lLuaLibraries = aElement->FirstChildElement("lua_libraries");
		// Custom libraries
		const CXMLElement *lLibraries = aElement->FirstChildElement("libraries");
		// Classes
		const CXMLElement *lClasses = aElement->FirstChildElement("classes");
#define BindLibraryByName(Name, Enum) else if (name == ##Name) \
		lua::BindLibrary<lua::##Enum>(mLS)

		for (const CXMLElement *library = lLibraries->FirstChildElement("library"); library != nullptr; library = library->NextSiblingElement("library")) {
			std::string name = library->GetAttribute<std::string>("name", "");
			if (name == "") {}
			BindLibraryByName("input", Input);
			BindLibraryByName("engine", Engine);
			BindLibraryByName("logger", Logger);
			BindLibraryByName("gui", GUI);
			BindLibraryByName("node", Node);
			BindLibraryByName("material", Material);
			// ... add more
		}
#undef BindLibraryByName
#define BindClassByName(Name, Class) else if (name == ##Name) \
	lua::BindClass<##Class>(mLS)
		for (const CXMLElement *bclass = lClasses->FirstChildElement("class"); bclass != nullptr; bclass = bclass->NextSiblingElement("class")) {
			std::string name = bclass->GetAttribute<std::string>("name", "");
			if (name == "") {}
			BindClassByName("fpscameracontroller", CFPSCameraController);
			BindClassByName("vect3f", Vect3f);
			BindClassByName("transform", CTransform);
			// ... add more
		}
#undef BindClassByName

	}
}