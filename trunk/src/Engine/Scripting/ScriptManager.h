#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Utils\lua_utils.h"

struct lua_State;
namespace engine
{
	void BindAllLibraries(lua_State* aState);
	void BindAllClasses(lua_State* aState);
	class CScript;
	class CScriptableObject;
	class CScriptManager
	{
	public:
		CScriptManager();
		~CScriptManager();
		bool LoadFromPath(const std::string &aPath);
		bool RunCode(const std::string &aCode);
		bool LoadFromFile(const std::string &aFile);
		bool AddScript(CScriptableObject* aScript);
		bool Reload();
		bool ReloadStandalone();
		void RenderDebugGUI();
		void Release();
		void BindAllLibraries();
		void BindAllClasses();
		lua_State* GetState();

		void Update(const float &dt);
		void Start();
	private:
		lua_State* m_State;
		std::string m_Path;
		std::string m_Filename;
		typedef base::utils::CTemplatedMapVector<CScript> ScriptMap;
		typedef base::utils::CTemplatedMapVector<CScriptableObject> ScriptableObjectsMap;
		ScriptMap m_Globals;
		ScriptableObjectsMap m_Scripts;
		void DestroyGlobals();
		void DestroyScripts();
		void Init();
	};
}