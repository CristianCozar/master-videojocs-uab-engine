#pragma once

#include "Utils\Name.h"

struct lua_State;
namespace engine
{
	class CScript : public CName
	{
		public:
			CScript();
			CScript::CScript(lua_State* aState);
			virtual ~CScript();
			void Release();
			bool Load(const std::string& aFilename);
			bool LoadWithoutRun(const std::string& aFilename);
			bool RunLoaded();
			bool operator()(const std::string& aCode);
			bool RunCode(const std::string& aCode);
			virtual bool Reload();
			lua_State* GetState();
			void SetState(lua_State *aState);
			void RenderDebugGUI();
		protected:
			bool m_Global;
			lua_State* mLS;
			std::string mFilename;
			void Init();
	};
}