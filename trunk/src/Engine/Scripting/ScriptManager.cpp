#include "ScriptManager.h"

#include "Script.h"
#include "ScriptableObject.h"
#include "Utils\FileUtils.h"
#include "Utils\Logger\Logger.h"
#include "Debug\imgui.h"

#include "Utils\Types.h"
#include "Camera\FPSCameraController.h"
#include "XML\XML.h"
#include "Engine.h"
#include "Scenes\SceneManager.h"

namespace engine
{
	CScriptManager::CScriptManager() :
		m_Path(std::string())
	{
		Init();
	}

	CScriptManager::~CScriptManager()
	{
		DestroyGlobals();
		DestroyScripts();
		Release();
	}

	void CScriptManager::Init()
	{
		// Init of the lua state and the bindings
		m_State = luaL_newstate();
		luaL_openlibs(m_State);
		luabind::open(m_State);
		lua_atpanic(m_State, lua::atpanic);
		lua_register(m_State, "_ALERT", lua::atpanic);

		lua::OpenLibraries(m_State);
		BindAllLibraries();
		BindAllClasses();
	}

	void CScriptManager::BindAllLibraries()
	{
		engine::BindAllLibraries(m_State);
	}

	void CScriptManager::BindAllClasses()
	{
		engine::BindAllClasses(m_State);
	}

	void BindAllLibraries(lua_State* aState)
	{
		lua::BindLibrary<lua::EngineLibrary::Engine>(aState);
		lua::BindLibrary<lua::EngineLibrary::Input>(aState);
		lua::BindLibrary<lua::EngineLibrary::Logger>(aState);
		lua::BindLibrary<lua::EngineLibrary::GUI>(aState);
		lua::BindLibrary<lua::Node>(aState);
		lua::BindLibrary<lua::PhysX>(aState);
		lua::BindLibrary<lua::Cinematics>(aState);
		lua::BindLibrary<lua::Material>(aState);
	}

	void BindAllClasses(lua_State* aState)
	{
		lua::BindClass<Vect3f>(aState);
		lua::BindClass<CFPSCameraController>(aState);
		lua::BindClass<CTransform>(aState);
	}

	lua_State *CScriptManager::GetState()
	{
		return m_State;
	}

	bool CScriptManager::LoadFromPath(const std::string &aPath)
	{
		m_Path = aPath;
		std::vector<std::string> l_Files;
		base::utils::GetFilesFromPath(aPath, "lua", l_Files);

		CScript* l_Script = new CScript(m_State);
		if (l_Script->LoadWithoutRun("data/baseScripts/gameObjectEngine.lua"))
		{
			l_Script->RunLoaded();
			m_Globals.Add("GameObjectEngine", l_Script);
		}
		l_Script = new CScript(m_State);
		if (l_Script->LoadWithoutRun("data/baseScripts/gameObject.lua"))
		{
			l_Script->RunLoaded();
			m_Globals.Add("GameObject", l_Script);
		}

		for (unsigned int i = 0; i < l_Files.size(); ++i)
		{
			std::string l_FilePath = aPath + l_Files[i];
			l_Script = new CScript(m_State);
			if (l_Script->LoadWithoutRun(l_FilePath))
			{
				l_Script->RunLoaded();
				m_Globals.Add(l_Files[i], l_Script);
			}
		}

		return true;
	}

	bool CScriptManager::RunCode(const std::string &aCode)
	{
		return luaL_dostring(m_State, aCode.c_str()) == 0;
	}

	bool CScriptManager::LoadFromFile(const std::string &aFile)
	{
		bool lOk = false;
		m_Filename = aFile;
		CXMLDocument lDocument;
		tinyxml2::XMLError error = lDocument.LoadFile(aFile.c_str());
		if (base::xml::SucceedLoad(error))
		{
			lOk = true;
			CXMLElement *lScripts = lDocument.FirstChildElement("scripts");

			for (const CXMLElement *script = lScripts->FirstChildElement("script_object"); script != nullptr; script = script->NextSiblingElement("script_object")) {
				CScriptableObject* l_ScriptableObject = new CScriptableObject(script);
				AddScript(l_ScriptableObject);
			}
		}
		return lOk;
	}

	bool CScriptManager::AddScript(CScriptableObject* aScript)
	{
		return m_Scripts.Add(aScript->GetName(), aScript);
	}

	bool CScriptManager::Reload()
	{
		DestroyGlobals();
		Release();
		Init();
		bool l_Ok = LoadFromPath(m_Path);
		engine::CEngine::GetInstance().GetSceneManager().SetScriptTransforms();
		return l_Ok;
	}

	bool CScriptManager::ReloadStandalone()
	{
		DestroyScripts();
		return LoadFromFile(m_Filename);
	}

	void CScriptManager::DestroyGlobals()
	{
		m_Globals.Destroy();
	}

	void CScriptManager::DestroyScripts()
	{
		m_Scripts.Destroy();
	}

	void CScriptManager::RenderDebugGUI()
	{
		ImGui::Indent();
		ImGui::PushID("globalscripts");
		if (ImGui::CollapsingHeader("Global Scripts"))
		{
			if (ImGui::Button("Reload Globals"))
			{
				Reload();
			}
			for (unsigned int i = 0; i < m_Globals.GetCount(); ++i)
			{
				m_Globals.GetIndex(i)->RenderDebugGUI();
			}
		}
		ImGui::PopID();

		ImGui::PushID("standalonescripts");
		if (ImGui::CollapsingHeader("Standalone Scripts"))
		{
			if (ImGui::Button("Reload Standalone"))
			{
				ReloadStandalone();
			}
			for (unsigned int j = 0; j < m_Scripts.GetCount(); ++j)
			{
				m_Scripts.GetIndex(j)->RenderDebugGUI();
			}
		}
		ImGui::PopID();
		ImGui::Unindent();
	}

	void CScriptManager::Release()
	{
		lua_close(m_State);
		//delete m_State;
	}

	void CScriptManager::Update(const float &dt)
	{
		LUA_FUNCTION(m_State, "update", void, dt);

		for (unsigned int i = 0; i < m_Scripts.GetCount(); ++i)
		{
			m_Scripts[i]->Update(dt);
		}
	}

	void CScriptManager::Start()
	{
		LUA_FUNCTION(m_State, "start_global", void);
	}
}