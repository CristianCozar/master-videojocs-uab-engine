#include "Script.h"

#include "Utils\lua_utils.h"
#include "Utils\Logger\Logger.h"
#include "Debug\CustomAssert.h"
#include "Utils\StringUtils.h"

#include "Debug\imgui.h"

namespace engine
{
	CScript::CScript() : m_Global(false)
	{
		Init();
	}

	CScript::CScript(lua_State* aState) : mLS(aState), m_Global(true)
	{
		Assert(mLS != nullptr, "Un script global no tiene el state.");
	}

	CScript::~CScript()
	{
		Release();
	}

	void CScript::Release()
	{
		if (!m_Global)
		{
			lua_close(mLS);
			//delete mLS;
		}
	}

	bool CScript::Load(const std::string& aFilename)
	{
		if (LoadWithoutRun(aFilename))
			return RunLoaded();
		else
			return false;
	}

	bool CScript::LoadWithoutRun(const std::string& aFilename)
	{
		mFilename = aFilename;
		SetName(base::utils::GetFilename(aFilename, true));
		int status = luaL_loadfile(mLS, aFilename.c_str());
		auto const lua_ok = LUA_OK;
		if (status != lua_ok)
		{
			if (status == LUA_ERRSYNTAX)
				LOG_ERROR_APPLICATION("[LUA] Sintax Error: %s", lua_tostring(mLS, -1));

			else if (status == LUA_ERRFILE)
				LOG_ERROR_APPLICATION("[LUA] File Error: %s", lua_tostring(mLS, -1));

			else if (status == LUA_ERRMEM)
				LOG_ERROR_APPLICATION("[LUA] Memory Error: %s", lua_tostring(mLS, -1));

			return false;
		}
		return true;
	}

	bool CScript::RunLoaded()
	{
		int status = lua_pcall(mLS, 0, LUA_MULTRET, 0);
		auto const lua_ok = LUA_OK;
		if (status != lua_ok)
		{
			std::string err = lua_tostring(mLS, -1);
			LOG_ERROR_APPLICATION("[LUA] Error executing code %s", err.c_str());
			return false;
		}
		return true;
	}

	bool CScript::operator()(const std::string& aCode)
	{
		return RunCode(aCode);
	}

	bool CScript::RunCode(const std::string& aCode)
	{
		return luaL_dostring(mLS, aCode.c_str()) == 0;
	}

	bool CScript::Reload()
	{
		Release();
		if (!m_Global)
			Init();
		return Load(mFilename);
	}

	lua_State* CScript::GetState()
	{
		return mLS;
	}

	void CScript::Init()
	{
		// Init of the lua state and the bindings
		mLS = luaL_newstate();
		luaL_openlibs(mLS);
		luabind::open(mLS);
		lua_atpanic(mLS, lua::atpanic);
		lua_register(mLS, "_ALERT", lua::atpanic);

		lua::OpenLibraries(mLS);
	}

	void CScript::RenderDebugGUI()
	{
		ImGui::PushID(GetName().c_str());
		ImGui::Indent();
		if (ImGui::CollapsingHeader(GetName().c_str()))
		{
			/*ImGui::Indent();
			if (ImGui::Button("Reload"))
			{
				Reload();
			}
			ImGui::Unindent();*/
		}
		ImGui::Unindent();
		ImGui::PopID();
	}
}