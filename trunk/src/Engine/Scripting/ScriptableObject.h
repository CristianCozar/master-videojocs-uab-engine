#pragma once

#include "Script.h"

namespace engine
{
	class CScriptableObject : public CScript
	{
	public:
		CScriptableObject(const CXMLElement* aElement);
		CScriptableObject(const CXMLElement* aElement, lua_State *aLua);
		CScriptableObject(lua_State *aLua); // Utilizado para crear el componente base "gameObject"
		virtual ~CScriptableObject();
		void Update(const float &dt);
		void Start();
		bool Reload() override;
	private:
		void BindClassesAndLibraries(const CXMLElement* aElement);

		bool m_HasStart;
		bool m_HasUpdate;
		bool m_HasDestroy;
	};
}