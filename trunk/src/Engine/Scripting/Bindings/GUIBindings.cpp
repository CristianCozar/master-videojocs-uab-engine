#include "GUI\GUIManager.h"
#include "Engine.h"
#include "Scripting\ScriptManager.h"
#include "Utils\EnumToString.h"

#include "RenderManager.h"

namespace lua
{
	bool DoButton(const std::string &aGUID, const std::string &aButtonID, engine::GUIPosition aPosition)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		return l_GUI.DoButton(aGUID, aButtonID, aPosition);
	}

	engine::SliderResult DoSlider(const std::string &aGUID, const std::string &aSliderID, engine::GUIPosition aPosition, float aMin, float aMax, float aCurrent)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		return l_GUI.DoSlider(aGUID, aSliderID, aPosition, aMin, aMax, aCurrent);
	}

	void DoBar(const std::string &aGUID, const std::string &aSliderID, engine::GUIPosition aPosition, float aMin, float aMax, float aCurrent)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		l_GUI.DoBar(aGUID, aSliderID, aPosition, aMin, aMax, aCurrent);
	}

	void DoImage(const std::string &aGUID, const std::string &aImageID, engine::GUIPosition aPosition)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		l_GUI.DoImage(aGUID, aImageID, aPosition);
	}

	void DoZeldaBar(const std::string &aGUID, const std::string &aSliderID, engine::GUIPosition aPosition, int aMax, int aCurrent)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		l_GUI.DoZeldaBar(aGUID, aSliderID, aPosition, aMax, aCurrent);
	}

	void DoTextBox(const std::string& aGUID, const std::string& aTextBoxID, engine::GUIPosition aPosition, Vect4f aTextboxSize, bool aReadOnly)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		l_GUI.DoTextBox(aGUID, aTextBoxID, aPosition, aTextboxSize, aReadOnly);
	}


	void DoConsole(const std::string& aGUID, const std::string& aTextBoxID, engine::GUIPosition aPosition, Vect4f aTextboxSize)
	{
		engine::CGUIManager &l_GUI = engine::CEngine::GetInstance().GetGUIManager();
		l_GUI.DoConsole(aGUID, aTextBoxID, aPosition, aTextboxSize);
	}


	engine::GUIPosition GenerateGUIPosition(float x, float y, float w, float h, std::string anchorS, std::string anchorCoordTypeS, std::string sizeCoordTypeS)
	{
		engine::GUIAnchor anchor;
		engine::GUICoordType anchorCoordType, sizeCoordType;
		if (!EnumString<engine::GUIAnchor>::ToEnum(anchor, anchorS))
		{
			anchor = engine::GUIAnchor::TOP_LEFT;
		}
		if (!EnumString<engine::GUICoordType>::ToEnum(anchorCoordType, anchorCoordTypeS))
		{
			anchorCoordType = engine::GUICoordType::GUI_ABSOLUTE;
		}
		if (!EnumString<engine::GUICoordType>::ToEnum(sizeCoordType, sizeCoordTypeS))
		{
			sizeCoordType = engine::GUICoordType::GUI_ABSOLUTE;
		}
		return engine::GUIPosition(x, y, w, h, anchor, anchorCoordType, sizeCoordType);
	}

	Vect4f GenerateGUISizeLimits(float minWidth, float minHeight, float maxWidth, float maxHeight, std::string sizeCoordTypeS)
	{ 
		Vect4f size = Vect4f(minWidth, minHeight, maxWidth, maxHeight);
		engine::GUICoordType sizeCoordType;
		engine::CRenderManager &l_RM = engine::CEngine::GetInstance().GetRenderManager();

		int screenWidth = l_RM.GetCurrentWidth();
		int screenHeight = l_RM.GetCurrentHeight();
		float unitPixelSizeX = 1.0f;
		float unitPixelSizeY = 1.0f;
	
		if (!EnumString<engine::GUICoordType>::ToEnum(sizeCoordType, sizeCoordTypeS))
		{
			sizeCoordType = engine::GUICoordType::GUI_ABSOLUTE;
		}

		switch (sizeCoordType)
		{
		case engine::GUICoordType::GUI_ABSOLUTE:
			//No hay que hacer nada
			break;
		case engine::GUICoordType::GUI_RELATIVE:
			size.x = minWidth * (float)screenWidth;
			size.y = minHeight * (float)screenHeight;
			size.z = maxWidth * (float)screenWidth;
			size.w = maxHeight * (float)screenHeight;
			break;
		case engine::GUICoordType::GUI_RELATIVE_WIDTH:
			//TODO
			break;
		case engine::GUICoordType::GUI_RELATIVE_HEIGHT:
			//TODO
			break;
		default:
			Assert(false, "Tipo de coordenadas para tama�o no definido");
			break;
		}
		return size;
	}


	template <> void BindLibrary<GUI>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<engine::SliderResult>("slider_result")
				.def(constructor<>())
				.def_readwrite("real", &engine::SliderResult::real)
				.def_readwrite("temp", &engine::SliderResult::temp)
			];
		module(aLua)
			[
				class_<engine::GUIPosition>("gui_position")
				.def_readonly("x", &engine::GUIPosition::_x)
				.def_readonly("y", &engine::GUIPosition::_y)
				.def_readonly("width", &engine::GUIPosition::_width)
				.def_readonly("height", &engine::GUIPosition::_height)
			];
		
		REGISTER_LUA_FUNCTION(aLua, "gui_button", &DoButton);
		REGISTER_LUA_FUNCTION(aLua, "gui_slider", &DoSlider);
		REGISTER_LUA_FUNCTION(aLua, "gui_position", &GenerateGUIPosition);
		REGISTER_LUA_FUNCTION(aLua, "gui_sizeLimits", &GenerateGUISizeLimits);
		REGISTER_LUA_FUNCTION(aLua, "gui_bar", &DoBar);
		REGISTER_LUA_FUNCTION(aLua, "gui_image", &DoImage);
		REGISTER_LUA_FUNCTION(aLua, "gui_zelda", &DoZeldaBar);
		REGISTER_LUA_FUNCTION(aLua, "gui_textBox", &DoTextBox);
		REGISTER_LUA_FUNCTION(aLua, "gui_console", &DoConsole);

	}
}