#include "Engine.h"

#include "Input\ActionManager.h"
#include "RenderManager.h"
#include "Materials\MaterialManager.h"
#include "Camera\FPSCameraController.h"
#include "Camera\PRIM3CameraController.h"
#include "Shaders\ShaderManager.h"
#include "Debug\imgui.h"
#include "Debug\imgui_impl_dx11.h"
#include "Materials\TextureManager.h"
#include "Shaders\EffectManager.h"
#include "Shaders\TechniquePoolManager.h"
#include "Mesh\MeshManager.h"
#include "ConstantBufferManager.h"
#include "Scenes\SceneManager.h"
#include "Cinematics\CinematicManager.h"
#include "Lights\LightManager.h"
#include "AnimatedModel\AnimatedModelManager.h"
#include "Paths.h"
#include "Utils\lua_utils.h"
#include "RenderPipeline\RenderPipeline.h"
#include "Physx\PhysXManager.h"
#include "Particles\ParticleManager.h"
#include "GUI\GUIManager.h"
#include "Wwise\CSoundManager.h"
#include "Scripting\ScriptManager.h"

namespace lua
{

	void InitActionManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CActionManager *l_ActionManager = new engine::CActionManager();
		l_ActionManager->InitInputManager(engine.GethWnd());
		l_ActionManager->LoadActions("actions.xml");
		engine.SetActionManager(l_ActionManager);
	}

	void InitRenderManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CRenderManager *l_RenderManager = new engine::CRenderManager();
		l_RenderManager->Init(engine.GethWnd(), engine.GetWidth(), engine.GetHeight());
		l_RenderManager->SetModelMatrix(Mat44f().SetIdentity());
		l_RenderManager->SetProjectionMatrix(0.8f, (float)engine.GetWidth(), (float)engine.GetHeight(), 0.1f, 10000.0f);
		l_RenderManager->SetViewMatrix(Vect3f(5.0f, 2.0f, 3.5f), Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 1.0f, 0.0f));
		engine.SetRenderManager(l_RenderManager);
	}

	void InitTextureManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CTextureManager *l_TextureManager = new engine::CTextureManager();
		engine.SetTextureManager(l_TextureManager);
	}

	void InitShaderManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CShaderManager *l_ShaderManager = new engine::CShaderManager();
		l_ShaderManager->Load("shaders.xml");
		engine.SetShaderManager(l_ShaderManager);
	}

	void InitEffectManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CEffectManager *l_EffectManager = new engine::CEffectManager();
		l_EffectManager->Load("effects.xml");
		engine.SetEffectManager(l_EffectManager);
	}

	void InitConstantBufferManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CConstantBufferManager *l_ConstantBufferManager = new engine::CConstantBufferManager();
		engine.SetConstantBufferManager(l_ConstantBufferManager);
	}

	void InitMeshManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CMeshManager *l_MeshManager = new engine::CMeshManager();
		engine.SetMeshManager(l_MeshManager);
	}

	void InitTechniquePoolManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CTechniquePoolManager *l_TechniquePoolManager = new engine::CTechniquePoolManager();
		l_TechniquePoolManager->Load(FILENAME_TECHNIQUE_POOLS);
		engine.SetTechniquePoolManager(l_TechniquePoolManager);
	}

	void InitAnimatedModelManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CAnimatedModelManager *l_animatedModelManager = new engine::CAnimatedModelManager();
		l_animatedModelManager->Load("animated_models.xml");
		engine.SetAnimatedModelManager(l_animatedModelManager);
	}

	void InitMaterialManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CMaterialManager *l_MaterialManager = new engine::CMaterialManager();
		l_MaterialManager->Load();
		engine.SetMaterialManager(l_MaterialManager);
	}

	void InitLightsManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CLightManager *l_LightManager = new engine::CLightManager();
		l_LightManager->Load();
		engine.SetLightManager(l_LightManager);
	}

	void InitSceneManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CSceneManager *l_SceneManager = new engine::CSceneManager();
		engine.SetSceneManager(l_SceneManager);
		l_SceneManager->Load(FILENAME_SCENES);
	}

	void InitCamera()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		//engine::CSphericalCameraController* cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 100.0f, 5.0f);
		//engine.SetCameraController(cc);

		//engine::CCameraController *cc = new engine::CFPSCameraController(Vect3f(-75.0f, 77.0f, -123.0f), 1.5f, -1.5f, 20.0f, 1.0f);
		//engine.SetCameraController(cc);

		engine::CPRIM3CameraController* cc = new engine::CPRIM3CameraController(Vect3f(10.0f, 10.0f, 10.0f), 1.5f, -1.5f, 20.0f, 1.0f);
		engine.SetCameraController(cc);

		GLOBAL_LUA_FUNCTION("prim3_camera_start", void);

	}

	void InitCinematicManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CCinematicManager *l_CinematicManager = new engine::CCinematicManager();
		l_CinematicManager->Load("data/CINEMATICS.xml");
		engine.SetCinematicManager(l_CinematicManager);
	}

	void InitParticleManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CParticleManager *l_ParticleManager = new engine::CParticleManager();
		l_ParticleManager->Load("data/Particles.xml");
		engine.SetParticleManager(l_ParticleManager);
	}

	void InitRenderPipeline()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CRenderPipeline *l_RenderPipeline = new engine::CRenderPipeline();
		l_RenderPipeline->Load(FILENAME_RENDER_PIPELINE);
		engine.SetRenderPipeline(l_RenderPipeline);
	}

	void InitImGui()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();
		engine::CRenderManager& lRM = engine.GetRenderManager();;

		ImGui_ImplDX11_Init(engine.GethWnd(), lRM.GetDevice(), lRM.GetDeviceContext());
	}

	void InitPhysX()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CPhysXManager* l_PhysXManager = engine::CPhysXManager::CreatePhysXManager();
		l_PhysXManager->RegisterMaterial("Default", 0.1f, 0.1f, 0.5f); // TEMP
		engine.SetPhysXManager(l_PhysXManager);
	}

	void InitGUIManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CGUIManager* l_GUIManager = new engine::CGUIManager();
		l_GUIManager->Load("GUI.xml");
		engine.SetGUIManager(l_GUIManager);
	}

	void InitSoundManager()
	{
		engine::CEngine& engine = engine::CEngine::GetInstance();

		engine::CSoundManager* l_SoundManager = new engine::CSoundManager("data/SoundBanks/");
		l_SoundManager->Init();
		l_SoundManager->InitBanks();
		l_SoundManager->Load("data/SoundbanksInfo.xml", "data/Speakers.xml");
		engine.SetSoundManager(l_SoundManager);
	}

	template <> void BindLibrary<Engine>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "init_action_manager", &InitActionManager);
		REGISTER_LUA_FUNCTION(aLua, "init_render_manager", &InitRenderManager);
		REGISTER_LUA_FUNCTION(aLua, "init_texture_manager", &InitTextureManager);
		REGISTER_LUA_FUNCTION(aLua, "init_shader_manager", &InitShaderManager);
		REGISTER_LUA_FUNCTION(aLua, "init_effect_manager", &InitEffectManager);
		REGISTER_LUA_FUNCTION(aLua, "init_constantbuffer_manager", &InitConstantBufferManager);
		REGISTER_LUA_FUNCTION(aLua, "init_mesh_manager", &InitMeshManager);
		REGISTER_LUA_FUNCTION(aLua, "init_technique_pool_manager", &InitTechniquePoolManager);
		REGISTER_LUA_FUNCTION(aLua, "init_animated_model_manager", &InitAnimatedModelManager);
		REGISTER_LUA_FUNCTION(aLua, "init_material_manager", &InitMaterialManager);
		REGISTER_LUA_FUNCTION(aLua, "init_light_manager", &InitLightsManager);
		REGISTER_LUA_FUNCTION(aLua, "init_scene_manager", &InitSceneManager);
		REGISTER_LUA_FUNCTION(aLua, "init_camera", &InitCamera);
		REGISTER_LUA_FUNCTION(aLua, "init_cinematic_manager", &InitCinematicManager);
		REGISTER_LUA_FUNCTION(aLua, "init_render_pipeline", &InitRenderPipeline);
		REGISTER_LUA_FUNCTION(aLua, "init_imgui", &InitImGui);
		REGISTER_LUA_FUNCTION(aLua, "init_physx", &InitPhysX);
		REGISTER_LUA_FUNCTION(aLua, "init_particle_manager", &InitParticleManager);
		REGISTER_LUA_FUNCTION(aLua, "init_gui_manager", &InitGUIManager);
		REGISTER_LUA_FUNCTION(aLua, "init_sound_manager", &InitSoundManager);
	}

}