#include "Camera\CameraController.h"
#include "Camera\FPSCameraController.h"
#include "Camera\PRIM3CameraController.h"
#include "Scripting\ScriptManager.h"
#include "Math\Vector3.h"
#include "Utils\lua_utils.h"
#include "Engine.h"

using namespace luabind;
using namespace engine;

namespace lua {

	template <> void BindClass<CCameraController>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<CCameraController>("CameraController")
				.def(constructor<>())
				.def("Update", &CCameraController::Update)
				.def("GetPosition", &CCameraController::GetPosition)
				.def("SetPosition", &CCameraController::SetPosition)
				.def("GetFront", &CCameraController::GetFront)
				.def("SetFront", &CCameraController::SetFront)
				.def("GetUp", &CCameraController::GetUp)
				.def("SetUp", &CCameraController::SetUp)
			];
	}


	CFPSCameraController* GetCamera()
	{
		engine::CCameraController *cc = &CEngine::GetInstance().GetCameraController();
		return (CFPSCameraController*)cc;
		//return *static_cast<CFPSCameraController*>(cc);
		//return *cc;
	}

	CPRIM3CameraController* GetPRIM3Camera()
	{
		engine::CCameraController *cc = &CEngine::GetInstance().GetCameraController();
		return (CPRIM3CameraController*)cc;
		//return *static_cast<CFPSCameraController*>(cc);
		//return *cc;
	}


	template <> void BindClass<CFPSCameraController>(lua_State *aLua)
	{

		REGISTER_LUA_FUNCTION(aLua, "get_camera", &GetCamera);
		REGISTER_LUA_FUNCTION(aLua, "get_prim3_camera", &GetPRIM3Camera);

		module(aLua)
		[

			class_<CCameraController>("CCameraController"),
			class_<CFPSCameraController, CCameraController>("CFPSCameraController")
			//.def_readwrite("pos", &CCameraController::m_Position)
			//.def("GetPosition", &CCameraController::GetPosition)
			//.def("SetPosition", &CCameraController::SetPosition)
		//	.def("GetFront", &CCameraController::GetFront)
		//	.def("SetFront", &CCameraController::SetFront)
		//	.def("GetUp", &CCameraController::GetUp)
		//	.def("SetUp", &CCameraController::SetUp)
	
				.def(constructor<>())
				.def(constructor<Vect3f, float, float, float, float >())
				.def("Update", &CFPSCameraController::Update)
				.property("m_Yaw", &CFPSCameraController::GetYaw, &CFPSCameraController::SetYaw)
				.property("m_Pitch", &CFPSCameraController::GetPitch, &CFPSCameraController::SetPitch)
				.property("m_Zoom", &CFPSCameraController::GetZoom, &CFPSCameraController::SetZoom)
				.property("m_MaxPitch", &CFPSCameraController::GetMaxPitch, &CFPSCameraController::SetMaxPitch)
				.property("m_MinPitch", &CFPSCameraController::GetMinPitch, &CFPSCameraController::SetMinPitch)
				.property("m_MaxZoom", &CFPSCameraController::GetMaxZoom, &CFPSCameraController::SetMaxZoom)
				.property("m_NoZoom", &CFPSCameraController::GetNoZoom, &CFPSCameraController::SetNoZoom)
				.def_readwrite("m_YawSpeed", &CFPSCameraController::m_YawSpeed)
				.def_readwrite("m_PitchSpeed", &CFPSCameraController::m_PitchSpeed)
				.def_readwrite("m_ZoomSpeed", &CFPSCameraController::m_ZoomSpeed)
				.def_readwrite("m_FrontSpeed", &CFPSCameraController::m_FrontSpeed)
				.def_readwrite("m_SideSpeed", &CFPSCameraController::m_SideSpeed)
				.def_readwrite("m_VerticalSpeed", &CFPSCameraController::m_VerticalSpeed)
				.property("m_Position", &CFPSCameraController::GetPosition, &CFPSCameraController::SetPosition)
				.property("m_Front", &CFPSCameraController::GetFront, &CFPSCameraController::SetFront)
				.property("m_Up", &CFPSCameraController::GetUp, &CFPSCameraController::SetUp)
				//GET_SET_LUA(CFPSCameraController, YawSpeed)
				//GET_SET_LUA(CFPSCameraController, PitchSpeed)
			//	GET_SET_LUA(CFPSCameraController, ZoomSpeed)
			//	GET_SET_LUA(CFPSCameraController, FrontSpeed)
			//	GET_SET_LUA(CFPSCameraController, SideSpeed)
			//	GET_SET_LUA(CFPSCameraController, VerticalSpeed)
			//	GET_SET_LUA(CFPSCameraController, Yaw)
			//	GET_SET_LUA(CFPSCameraController, Pitch)
			//	GET_SET_LUA(CFPSCameraController, Zoom)
			//	GET_SET_LUA(CFPSCameraController, MaxZoom)
			//	GET_SET_LUA(CFPSCameraController, NoZoom)
			];

		module(aLua)
			[
				class_<CPRIM3CameraController, CCameraController>("CPRIM3CameraController")
				.def(constructor<>())
				.def(constructor<Vect3f, float, float, float, float >())
				.def("Update", &CPRIM3CameraController::Update)
				.property("m_Yaw", &CPRIM3CameraController::GetYaw, &CPRIM3CameraController::SetYaw)
				.property("m_Pitch", &CPRIM3CameraController::GetPitch, &CPRIM3CameraController::SetPitch)
				.property("m_Zoom", &CPRIM3CameraController::GetZoom, &CPRIM3CameraController::SetZoom)
				.property("m_MaxPitch", &CPRIM3CameraController::GetMaxPitch, &CPRIM3CameraController::SetMaxPitch)
				.property("m_MinPitch", &CPRIM3CameraController::GetMinPitch, &CPRIM3CameraController::SetMinPitch)
				.property("m_MaxZoom", &CPRIM3CameraController::GetMaxZoom, &CPRIM3CameraController::SetMaxZoom)
				.property("m_NoZoom", &CPRIM3CameraController::GetNoZoom, &CPRIM3CameraController::SetNoZoom)
				.def_readwrite("m_YawSpeed", &CPRIM3CameraController::m_YawSpeed)
				.def_readwrite("m_PitchSpeed", &CPRIM3CameraController::m_PitchSpeed)
				.def_readwrite("m_ZoomSpeed", &CPRIM3CameraController::m_ZoomSpeed)
				.def_readwrite("m_FrontSpeed", &CPRIM3CameraController::m_FrontSpeed)
				.def_readwrite("m_SideSpeed", &CPRIM3CameraController::m_SideSpeed)
				.def_readwrite("m_VerticalSpeed", &CPRIM3CameraController::m_VerticalSpeed)
				.property("m_Position", &CPRIM3CameraController::GetPosition, &CPRIM3CameraController::SetPosition)
				.property("m_Front", &CPRIM3CameraController::GetFront, &CPRIM3CameraController::SetFront)
				.property("m_Up", &CPRIM3CameraController::GetUp, &CPRIM3CameraController::SetUp)
			];
		

	}
}



  