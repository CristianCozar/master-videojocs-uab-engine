#include "Engine.h"

#include "Input\ActionManager.h"
#include "RenderManager.h"
#include "Materials\MaterialManager.h"
#include "Camera\FPSCameraController.h"
#include "Shaders\ShaderManager.h"
#include "Debug\imgui.h"
#include "Debug\imgui_impl_dx11.h"
#include "Materials\TextureManager.h"
#include "Shaders\EffectManager.h"
#include "Shaders\TechniquePoolManager.h"
#include "Mesh\MeshManager.h"
#include "ConstantBufferManager.h"
#include "Scenes\SceneManager.h"
#include "Cinematics\CinematicManager.h"
#include "Lights\LightManager.h"
#include "AnimatedModel\AnimatedModelManager.h"
#include "Paths.h"
#include "Utils\lua_utils.h"
#include "RenderPipeline\RenderPipeline.h"
#include "Physx\PhysXManager.h"
#include "Physx\PhysXManagerImplementation.h"
#include "Particles\ParticleManager.h"
#include "GUI\GUIManager.h"
#include "Wwise\CSoundManager.h"

namespace lua
{	
	void RegisterMaterial(const std::string &name, float staticFriction, float dynamicFriction, float restitution)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.RegisterMaterial(name, staticFriction, dynamicFriction, restitution);
	}

	void AddStaticBox(const std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createStaticBox(name, sizeX, sizeY, sizeZ, position, orientation, materialName);
	}

	void AddStaticPlane(const std::string& name, float x, float y, float z, float  dist, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createStaticPlane(name, x, y, z, dist, materialName);
	}

	void AddDynamicBox(const std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createDynamicBox(name, sizeX, sizeY, sizeZ, position, orientation, density, materialName);
	}

	void AddDynamicSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createDynamicSphere(name, radius, position, orientation, density, materialName);
	}

	void AddStaticSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createStaticSphere(name, radius, position, orientation, materialName);
	}

	void AddDynamicConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createDynamicConvexMesh(name, vertices, position, orientation, density, materialName);
	}

	void AddStaticConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createStaticConvexMesh(name, vertices, position, orientation, materialName);
	}

	void AddStaticTriangleMesh(const std::string& name, std::vector<Vect3f> vertices, std::vector<Vect3w> indices, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.createStaticTriangleMesh(name, vertices, indices, position, orientation, materialName);
	}

	void AddTriggerSphere(const std::string& name, float radius, const Vect3f& position, const Quatf& orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.AddTriggerSphere(name, radius, position, orientation, materialName);
	}

	void AddTriggerBox(const std::string& name, float sizeX, float sizeY, float sizeZ, const Vect3f& position, const Quatf& orientation, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.AddTriggerBox(name, sizeX, sizeY, sizeZ, position, orientation, materialName);
	}

	bool Raycast(const Vect3f& origin, const Vect3f& end, engine::RaycastData* result_) {
		//engine::RaycastData result_ = engine::RaycastData();
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		return l_PhysXManager.Raycast(origin, end, result_);
	}

	void SetActorPostion(const std::string& name, Vect3f pos)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.SetActorPosition(name, pos);
	}

	void SetActorRotation(const std::string& name, Vect3f rotation)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		rotation.x = rotation.x * DEG2RAD;
		rotation.y = rotation.y * DEG2RAD;
		rotation.z = rotation.z * DEG2RAD;
		Quatf q;
		q.QuatFromEuler(Vect3f(rotation.z, rotation.x, rotation.y));
		l_PhysXManager.SetActorOrientation(name, q);
	}

	Vect3f GetActorRotation(const std::string& name)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		Vect3f rot;
		Quatf quat = l_PhysXManager.GetActorOrientation(name);
		rot = quat.EulerFromQuat();
		rot.x = rot.x * RAD2DEG;
		rot.y = rot.y * RAD2DEG;
		rot.z = rot.z * RAD2DEG;
		float tempX = rot.x;
		float tempY = rot.y;
		float tempZ = rot.z;
		rot.x = tempZ;
		rot.y = tempX;
		rot.z = tempY;
		
		

		return rot;
	}

	void DeleteActor(const std::string& name)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.DeleteActorRequest(name);	
	}

	/*
	void AddActor(const std::string& name, Vect3f pos, Vect3f size)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		l_PhysXManager.AddStaticBoxRequest(name, pos, size);
	}*/

	void AddCharacterController(const std::string& name, float height, float radius, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		engine::CPhysXManager& l_PhysXManager = engine::CEngine::GetInstance().GetPhysXManager();
		((engine::CPhysXManagerImplementation*)&l_PhysXManager)->AddCharacterController(name, height, radius, position, orientation, materialName, density, name);
	}

	template <> void BindLibrary<PhysX>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "register_material", &RegisterMaterial);
		REGISTER_LUA_FUNCTION(aLua, "create_static_box", &AddStaticBox);
		REGISTER_LUA_FUNCTION(aLua, "create_static_plane", &AddStaticPlane);
		REGISTER_LUA_FUNCTION(aLua, "create_dynamic_box", &AddDynamicBox);
		REGISTER_LUA_FUNCTION(aLua, "create_dynamic_sphere", &AddDynamicSphere);
		REGISTER_LUA_FUNCTION(aLua, "create_static_sphere", &AddStaticSphere);
		REGISTER_LUA_FUNCTION(aLua, "create_dynamic_convex_mesh", &AddDynamicConvexMesh);
		REGISTER_LUA_FUNCTION(aLua, "create_static_convex_mesh", &AddStaticConvexMesh);
		REGISTER_LUA_FUNCTION(aLua, "create_static_triangle_mesh", &AddStaticTriangleMesh);
		REGISTER_LUA_FUNCTION(aLua, "create_trigger_box", &AddTriggerBox);
		REGISTER_LUA_FUNCTION(aLua, "set_actor_position", &SetActorPostion);
		REGISTER_LUA_FUNCTION(aLua, "set_actor_rotation", &SetActorRotation);
		REGISTER_LUA_FUNCTION(aLua, "get_actor_rotation", &GetActorRotation);

		module(aLua)
			[
				class_<engine::RaycastData>("raycast_data")
				.def(constructor<>())
				.def_readwrite("position", &engine::RaycastData::position)
				.def_readwrite("normal", &engine::RaycastData::normal)
				.def_readwrite("distance", &engine::RaycastData::distance)
				.def_readwrite("actor", &engine::RaycastData::actor)
			];
		REGISTER_LUA_FUNCTION(aLua, "raycast", &Raycast);
		REGISTER_LUA_FUNCTION(aLua, "delete_actor", &DeleteActor);
		REGISTER_LUA_FUNCTION(aLua, "create_char_controller", &AddCharacterController);
		REGISTER_LUA_FUNCTION(aLua, "add_char_controller", &AddCharacterController);
	}
}