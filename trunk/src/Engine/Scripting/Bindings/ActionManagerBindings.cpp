#include "Input\ActionManager.h"
#include "Input\InputManager.h"
#include "Engine.h"
#include "Scripting\ScriptManager.h"
#include "Debug\imgui.h"

#include "Debug\CustomAssert.h"

namespace lua
{
    bool GetAction(const char* aAction)
    {
        bool lActioned = false;
        if (!ImGui::GetIO().WantCaptureMouse)
        {
			Assert(engine::CEngine::GetInstance().GetActionManager().Exist(aAction), "La acci�n %s no existe", aAction);
			if (engine::CEngine::GetInstance().GetActionManager().Exist(aAction)) 
			{
				return  engine::CEngine::GetInstance().GetActionManager().Get(aAction)->active;
			}
			else {
				return  false;
			}


        }
        return lActioned;
    }

    float GetActionAmount(const std::string& aAction)
    {
        float lValue = 0.0;
        if (!ImGui::GetIO().WantCaptureMouse)
        {
			Assert(engine::CEngine::GetInstance().GetActionManager().Exist(aAction), "La acci�n %s no existe", aAction);
			lValue = engine::CEngine::GetInstance().GetActionManager().Get(aAction)->value;
        }
        return lValue;
    }

    int GetMouseMovementX()
    {
		return engine::CEngine::GetInstance().GetActionManager().GetInputManager().GetMouseMovementX();
    }
    
    int GetMouseMovementY()
    {
		return engine::CEngine::GetInstance().GetActionManager().GetInputManager().GetMouseMovementY();
    }

    template <> void BindLibrary<Input>(lua_State *aLua)
    {
        REGISTER_LUA_FUNCTION(aLua, "get_action", &GetAction );
        REGISTER_LUA_FUNCTION(aLua, "get_action_amount", &GetActionAmount);
        REGISTER_LUA_FUNCTION(aLua, "x_mouse_movement", &GetMouseMovementX);
        REGISTER_LUA_FUNCTION(aLua, "y_mouse_movement", &GetMouseMovementY);
    }
}