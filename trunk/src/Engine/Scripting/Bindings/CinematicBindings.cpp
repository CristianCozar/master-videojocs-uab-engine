#include "Engine.h"

#include "Input\ActionManager.h"
#include "RenderManager.h"
#include "Materials\MaterialManager.h"
#include "Camera\FPSCameraController.h"
#include "Shaders\ShaderManager.h"
#include "Debug\imgui.h"
#include "Debug\imgui_impl_dx11.h"
#include "Materials\TextureManager.h"
#include "Shaders\EffectManager.h"
#include "Shaders\TechniquePoolManager.h"
#include "Mesh\MeshManager.h"
#include "ConstantBufferManager.h"
#include "Scenes\SceneManager.h"
#include "Cinematics\CinematicManager.h"
#include "Lights\LightManager.h"
#include "AnimatedModel\AnimatedModelManager.h"
#include "Paths.h"
#include "Utils\lua_utils.h"
#include "RenderPipeline\RenderPipeline.h"
#include "Physx\PhysXManager.h"
#include "Particles\ParticleManager.h"
#include "GUI\GUIManager.h"
#include "Wwise\CSoundManager.h"
#include "Cinematics\CinematicManager.h"

namespace lua
{	
	void PlayCinematic(const std::string &name)
	{
		engine::CCinematicManager& l_CinematicManager = engine::CEngine::GetInstance().GetCinematicManager();
		l_CinematicManager.Play(name);
	}

	template <> void BindLibrary<Cinematics>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "play_cinematic", &PlayCinematic);
	}
}