#include "Engine.h"
#include "Scripting\ScriptManager.h"
#include "Debug\imgui.h"
#include "Math\Color.h"

#include "Debug\CustomAssert.h"
#include "Materials\MaterialManager.h"
#include "Materials\TemplatedMaterialParameter.h"


#define SET_MATERIAL_PARAMETER(type) {\
	auto parameters = l_Mat->GetParameters();\
engine::CMaterialParameter* l_Param = nullptr;\
for (int i = 0; i < parameters.size(); i++)\
{\
	if (parameters[i]->GetName() == aAttributeName)\
		l_Param = parameters[i];\
}\
if (l_Param == nullptr) { \
		return; \
}\
engine::CTemplatedMaterialParameter<type>* l_Temp = (engine::CTemplatedMaterialParameter<type>*)l_Param; \
l_Temp->SetValue(value); \
}\

namespace lua
{
	engine::CMaterial *GetMaterial(const std::string &aName)
	{
		return engine::CEngine::GetInstance().GetMaterialManager().Get(aName);
	}

	void ChangeMaterialFloat(const std::string &aMaterialName, const std::string &aAttributeName, float value)
	{
		engine::CMaterial *l_Mat = GetMaterial(aMaterialName);
		if (l_Mat != nullptr)
		{
			SET_MATERIAL_PARAMETER(float);
		}
	}

	void ChangeMaterialVec2(const std::string &aMaterialName, const std::string &aAttributeName, Vect2f value)
	{
		engine::CMaterial *l_Mat = GetMaterial(aMaterialName);
		if (l_Mat != nullptr)
		{
			SET_MATERIAL_PARAMETER(Vect2f);
		}
	}

	void ChangeMaterialVec3(const std::string &aMaterialName, const std::string &aAttributeName, Vect3f value)
	{
		engine::CMaterial *l_Mat = GetMaterial(aMaterialName);
		if (l_Mat != nullptr)
		{
			SET_MATERIAL_PARAMETER(Vect3f);
		}
	}

	void ChangeMaterialVec4(const std::string &aMaterialName, const std::string &aAttributeName, Vect4f value)
	{
		engine::CMaterial *l_Mat = GetMaterial(aMaterialName);
		if (l_Mat != nullptr)
		{
			SET_MATERIAL_PARAMETER(Vect4f);
		}
	}

	void ChangeMaterialColor(const std::string &aMaterialName, const std::string &aAttributeName, CColor value)
	{
		engine::CMaterial *l_Mat = GetMaterial(aMaterialName);
		if (l_Mat != nullptr)
		{
			SET_MATERIAL_PARAMETER(CColor);
		}
	}

	template <> void BindLibrary<Material>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "change_material_float", &ChangeMaterialFloat);
		REGISTER_LUA_FUNCTION(aLua, "change_material_vec2", &ChangeMaterialVec2);
		REGISTER_LUA_FUNCTION(aLua, "change_material_vec3", &ChangeMaterialVec3);
		REGISTER_LUA_FUNCTION(aLua, "change_material_vec4", &ChangeMaterialVec4);
		REGISTER_LUA_FUNCTION(aLua, "change_material_color", &ChangeMaterialColor);
	}
}