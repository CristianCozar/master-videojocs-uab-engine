#include "Engine.h"
#include "Scenes\SceneManager.h"
#include "Scripting\ScriptManager.h"
#include "Scenes\Layer.h"
#include "Scenes\SceneNode.h"
#include "Scenes\SceneAnimatedModel.h"

#include "Paths.h"
#include "Materials\MaterialManager.h"
#include "Lights\LightManager.h"
#include "Materials\TextureManager.h"
#include "Physx\PhysXManager.h"

namespace lua
{
	engine::CSceneNode* GetNodeFromName(const std::string& aName)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					engine::CSceneNode *l_Node = l_Layer->Get(aName);
					if (l_Node)
					{
						return l_Node;
					}
				}
			}
		}
		return nullptr;
	}

	void SetParent(const std::string& aName, const std::string& aChildrenName)
	{
		engine::CSceneNode *l_Parent = GetNodeFromName(aName);
		engine::CSceneNode *l_Child = GetNodeFromName(aChildrenName);
		if (l_Parent != nullptr && l_Child != nullptr)
		{
			l_Child->SetParent(l_Parent);

		}
	}
	
	void DetachFromParent(const std::string& aName)
	{
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != nullptr)
		{
			l_Node->DetachFromParent();
		}
	}

	void DetachChildren(const std::string& aName, const std::string& aChildrenName)
	{
		engine::CSceneNode *l_Parent = GetNodeFromName(aName);
		engine::CSceneNode *l_Child = GetNodeFromName(aChildrenName);
		if (l_Parent != nullptr && l_Child != nullptr)
		{
			l_Parent->DetachChildren(l_Child);
		}
	}

	void AddChildren(const std::string& aName, const std::string& aChildrenName)
	{
		engine::CSceneNode *l_Parent = GetNodeFromName(aName);
		engine::CSceneNode *l_Child = GetNodeFromName(aChildrenName);
		if (l_Parent != nullptr && l_Child != nullptr)
		{
			l_Parent->AddChildren(l_Child);
		}
	}

	void SetActive(const std::string& aName, bool aActive)
	{
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != nullptr)
		{
			l_Node->SetActive(aActive);
		}
	}

	bool GetActive(const std::string& aName)
	{
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != nullptr)
		{
			return l_Node->IsActive();
		}
		return false;
	}

	void SetVisible(const std::string& aName, bool aVisible)
	{
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != nullptr)
		{
			l_Node->SetVisible(aVisible);
		}
	}

	bool GetVisible(const std::string& aName)
	{
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != nullptr)
		{
			return l_Node->IsVisible();
		}
		return false;
	}

	std::string GetScene(const std::string& aName)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					engine::CSceneNode *l_Node = l_Layer->Get(aName);
					if (l_Node)
					{
						return l_Scene->GetName();
					}
				}
			}
		}
		return std::string();
	}

	std::string GetLayer(const std::string& aName)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					engine::CSceneNode *l_Node = l_Layer->Get(aName);
					if (l_Node)
					{
						return l_Layer->GetName();
					}
				}
			}
		}
		return std::string();
	}

	engine::CLayer* GetLayerReference(const std::string& aName)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					engine::CSceneNode *l_Node = l_Layer->Get(aName);
					if (l_Node)
					{
						return l_Layer;
					}
				}
			}
		}
		return nullptr;
	}

	std::string Instance(const std::string& aInstantiable, const std::string& aName, const std::string& aScene, const std::string& aLayer)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		engine::CScene *l_Scene = l_SceneManager.Get(aScene);
		engine::CLayer *l_Layer = l_Scene->Get(aLayer);
		return l_Layer->AddFromNode(aInstantiable, aName);
	}

	std::string InstanceInSame(const std::string& aInstantiable, const std::string& aName, const std::string& aOther)
	{
		return Instance(aInstantiable, aName, GetScene(aOther), GetLayer(aOther));
	}

	std::string InstanceInLayer(const std::string& aInstantiable, const std::string& aName, const std::string& aOther, const std::string& aLayer)
	{
		return Instance(aInstantiable, aName, GetScene(aOther), aLayer);
	}

	void DeleteNode(const std::string& aName)
	{
		engine::CLayer* l_Layer = GetLayerReference(aName);

		engine::CSceneNode *l_Node = GetNodeFromName(aName);

		if (l_Node != NULL)
		{
			if (l_Node->m_Parent != nullptr)
			{
				l_Node->DetachFromParent();
			}
			//GLOBAL_LUA_FUNCTION("ondestroy", void, aName);

			l_Layer->Remove(aName);
		}
		//delete l_Node;
	}

	void SetPosition(const std::string& aName, const Vect3f aPosition) {
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		l_Node->SetPosition(aPosition);
	}

	void MoveNode(const std::string& aName, const Vect3f aMovement) {
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		if (l_Node != NULL)
		{
			l_Node->Move(aMovement);
		}
	}

	void RotateNode(const std::string& aName, const Vect3f aRotation) {
		engine::CSceneNode *l_Node = GetNodeFromName(aName);
		l_Node->Rotate(aRotation);
	}

	void DeleteAllNodesInLayer(const std::string& aScene, const std::string& aLayer)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		engine::CScene *l_Scene = l_SceneManager.Get(aScene);
		engine::CLayer *l_Layer = l_Scene->Get(aLayer);
		
		for (int i = 0; i < l_Layer->GetCount(); ++i)
		{
			engine::CSceneNode* l_Node = l_Layer->GetIndex(i);
			if (l_Node->m_Parent == nullptr)
			{
				//GLOBAL_LUA_FUNCTION("ondestroy", void, l_Node->GetName());
			delete l_Layer->GetIndex(i);
		}
		}

		l_Layer->Clear();
	}

	// API ANIMACIONES //


	engine::CSceneAnimatedModel* GetAnimatedNodeFromName(const std::string& aName)
	{
		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					engine::CSceneNode *l_Node = l_Layer->Get(aName);
					if (l_Node)
					{
						return (engine::CSceneAnimatedModel*)l_Node;
					}
				}
			}
		}
		return nullptr;
	}

	void ExecuteAction(const std::string& aName, int Id, float DelayIn, float DelayOut, float WeightTarget)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		l_AnimModel->ForceAction(Id, DelayIn, DelayOut, WeightTarget);
	}

	void ExecuteActionByName(const std::string& aName, const std::string& animName, float DelayIn, float DelayOut, float WeightTarget)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		int id = l_AnimModel->GetAnimationId(animName);
		if (id != -1)
			l_AnimModel->ForceAction(id, DelayIn, DelayOut, WeightTarget);
	}

	void QueueAction(const std::string& aName, int Id, float DelayIn, float DelayOut, float WeightTarget)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		l_AnimModel->QueueAction(Id, DelayIn, DelayOut, WeightTarget);
	}

	void QueueActionByName(const std::string& aName, const std::string& animName, float DelayIn, float DelayOut, float WeightTarget)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		int id = l_AnimModel->GetAnimationId(animName);
		if (id != -1)
			l_AnimModel->QueueAction(id, DelayIn, DelayOut, WeightTarget);
	}

	void ClearQueue(const std::string& aName)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		l_AnimModel->ClearQueue();
	}


	void BlendCycle(const std::string& aName, int Id, float Weight, float DelayIn)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		l_AnimModel->BlendCycle(Id, Weight, DelayIn);
	}

	void BlendCycleByName(const std::string& aName, const std::string& animName, float Weight, float DelayIn)
	{
		if (animName == "salt_ability_move")
		{
			int x;
			x = 0;
		}
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		if (l_AnimModel != NULL)
		{
			int id = l_AnimModel->GetAnimationId(animName);
			if (id != -1)
				l_AnimModel->BlendCycle(id, Weight, DelayIn);
		}
	}

	void ClearCycle(const std::string& aName, int Id, float DelayOut)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		l_AnimModel->ClearCycle(Id, DelayOut);
	}

	void ClearCycleByName(const std::string& aName, const std::string& animName, float DelayOut)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		if (l_AnimModel != NULL)
		{
			int id = l_AnimModel->GetAnimationId(animName);
			if (id != -1)
				l_AnimModel->ClearCycle(id, DelayOut);
		}
	}

	bool IsCycleAnimationActive(const std::string& aName, int Id)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		return l_AnimModel->IsCycleAnimationActive(Id);
	}

	bool IsCycleAnimationActiveByName(const std::string& aName, const std::string& animName)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		int id = l_AnimModel->GetAnimationId(animName);
		if (id != -1)
			return l_AnimModel->IsCycleAnimationActive(id);
		else return false;
	}

	bool IsActionAnimationActive(const std::string& aName, int Id)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		return l_AnimModel->IsActionAnimationActive(Id);
	}

	bool IsActionAnimationActiveByName(const std::string& aName, const std::string& animName)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		int id = l_AnimModel->GetAnimationId(animName);
		if (id != -1)
			return l_AnimModel->IsActionAnimationActive(id);
		else return false;
	}

	bool IsAnyActionAnimationActive(const std::string& aName)
	{
		engine::CSceneAnimatedModel* l_AnimModel = GetAnimatedNodeFromName(aName);
		if (l_AnimModel != NULL)
		{
			return l_AnimModel->IsAnyActionAnimationActive();
		}
		else{
			return false;
		}
	}

	void UnloadScene(const std::string& aScene)
	{
		engine::CEngine::GetInstance().GetSceneManager().Remove(aScene);
		engine::CEngine::GetInstance().GetLightManager().Clear();
		//engine::CEngine::GetInstance().GetPhysXManager().DeleteRequestedActors();
		engine::CEngine::GetInstance().GetPhysXManager().m_RequestDeleteAllActors = true;
	}

	void UnloadCurrentScene()
	{
		std::string l_Scene = "";
		engine::CSceneManager &l_SM = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SM.GetCount(); ++i)
		{
			if (l_SM.GetIndex(i)->IsActive())
				l_Scene = l_SM.GetIndex(i)->GetName();
		}
		if (l_Scene != "")
			UnloadScene(l_Scene);
	}

	void LoadScene(const std::string& aName)
	{
		engine::CScene *sceneObject = new engine::CScene(aName);

		sceneObject->SetActive(true);
		std::string l_NewSceneFolder = std::string(aName);
		std::string l_SceneFolderPath = PATH_BASE + l_NewSceneFolder;
		std::string l_NewScenePath = PATH_BASE + l_NewSceneFolder + FILENAME_SCENE;

		std::string l_materilsFile = PATH_BASE + l_NewSceneFolder + FILENAME_MATERIALS;
		engine::CMaterialManager& lmM = engine::CEngine::GetInstance().GetMaterialManager();
		lmM.Load(l_materilsFile);

		std::string l_LightsFile = PATH_BASE + l_NewSceneFolder + FILENAME_LIGHTS;
		engine::CLightManager& llM = engine::CEngine::GetInstance().GetLightManager();
		//llM.ReLoad(l_LightsFile);
		llM.Load();
		llM.Load(l_LightsFile);

		engine::CEngine::GetInstance().GetSceneManager().Add(sceneObject->GetName(), sceneObject);
		sceneObject->Load(l_NewScenePath, l_SceneFolderPath);
	}

	template <> void BindLibrary<Node>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "set_parent", &SetParent);
		REGISTER_LUA_FUNCTION(aLua, "detach_from_parent", &DetachFromParent);
		REGISTER_LUA_FUNCTION(aLua, "detach_children", &DetachChildren);
		REGISTER_LUA_FUNCTION(aLua, "add_children", &AddChildren);
		REGISTER_LUA_FUNCTION(aLua, "set_active", &SetActive);
		REGISTER_LUA_FUNCTION(aLua, "get_active", &GetActive);
		REGISTER_LUA_FUNCTION(aLua, "set_visible", &SetVisible);
		REGISTER_LUA_FUNCTION(aLua, "get_visible", &GetVisible);
		REGISTER_LUA_FUNCTION(aLua, "get_scene", &GetScene);
		REGISTER_LUA_FUNCTION(aLua, "get_layer", &GetLayer);
		REGISTER_LUA_FUNCTION(aLua, "instance_in_same", &InstanceInSame);
		REGISTER_LUA_FUNCTION(aLua, "instance_in_layer", &InstanceInLayer);
		REGISTER_LUA_FUNCTION(aLua, "instance", &Instance);
		REGISTER_LUA_FUNCTION(aLua, "delete_node", &DeleteNode);
		REGISTER_LUA_FUNCTION(aLua, "delete_all_nodes", &DeleteAllNodesInLayer);
		REGISTER_LUA_FUNCTION(aLua, "set_position", &SetPosition);
		REGISTER_LUA_FUNCTION(aLua, "move_node", &MoveNode);
		REGISTER_LUA_FUNCTION(aLua, "rotate_node", &RotateNode);

		// API ANIMACIONES
		REGISTER_LUA_FUNCTION(aLua, "execute_action", &ExecuteAction);
		REGISTER_LUA_FUNCTION(aLua, "execute_action_by_name", &ExecuteActionByName);
		REGISTER_LUA_FUNCTION(aLua, "queue_action", &QueueAction);
		REGISTER_LUA_FUNCTION(aLua, "queue_action_by_name", &QueueActionByName);
		REGISTER_LUA_FUNCTION(aLua, "clear_queue", &ClearQueue);
		REGISTER_LUA_FUNCTION(aLua, "blend_cycle", &BlendCycle);
		REGISTER_LUA_FUNCTION(aLua, "blend_cycle_by_name", &BlendCycleByName);
		REGISTER_LUA_FUNCTION(aLua, "clear_cycle", &ClearCycle);
		REGISTER_LUA_FUNCTION(aLua, "clear_cycle_by_name", &ClearCycleByName);
		REGISTER_LUA_FUNCTION(aLua, "is_cycle_animation_active", &IsCycleAnimationActive);
		REGISTER_LUA_FUNCTION(aLua, "is_cycle_animation_active_by_name", &IsCycleAnimationActiveByName);
		REGISTER_LUA_FUNCTION(aLua, "is_action_animation_active", &IsActionAnimationActive);
		REGISTER_LUA_FUNCTION(aLua, "is_action_animation_active_by_name", &IsActionAnimationActiveByName);
		REGISTER_LUA_FUNCTION(aLua, "is_any_action_animation_active", &IsAnyActionAnimationActive);
		// FIN API ANIMACIONES

		REGISTER_LUA_FUNCTION(aLua, "unload_scene", &UnloadScene);
		REGISTER_LUA_FUNCTION(aLua, "unload_current_scene", &UnloadCurrentScene);
		REGISTER_LUA_FUNCTION(aLua, "load_scene", &LoadScene);
		
	}
}