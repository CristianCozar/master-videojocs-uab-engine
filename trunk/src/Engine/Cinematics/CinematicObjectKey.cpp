#include "CinematicObjectKey.h"



namespace engine
{
	CCinematicObjectKey::CCinematicObjectKey()
	{
		m_Rotation = Vect3f(0.0f, 0.0f, 0.0f);
		m_Scale = Vect3f(1.0f, 1.0f, 1.0f);
	}

	CCinematicObjectKey::~CCinematicObjectKey()
	{

	}


	bool CCinematicObjectKey::Load(const CXMLElement* aElement)
	{
		m_Time = aElement->GetAttribute<float>("time", -1.0);
		m_Position = aElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
		m_Rotation = aElement->GetAttribute<Vect3f>("rotation", Vect3f(0.0f, 0.0f, 0.0f));
		m_Scale = aElement->GetAttribute<Vect3f>("scale", Vect3f(0.0f, 0.0f, 0.0f));

		return true;
	}
}