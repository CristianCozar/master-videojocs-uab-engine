#pragma once


#include "Utils\Defines.h"
#include "XML\XML.h"
#include "Math\Vector3.h"


namespace engine
{
	class CCinematicKey
	{
	public:
		CCinematicKey() : m_Time(0.0f), m_Position(0.0f, 0.0f, 0.0f) {};
		virtual ~CCinematicKey() {};
		virtual bool Load(const CXMLElement* aElement) = 0;
		GET_SET_REF(float, Time);
		GET_SET_REF(Vect3f, Position);

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicKey);

	protected:
		float m_Time;
		Vect3f m_Position;

	};
}