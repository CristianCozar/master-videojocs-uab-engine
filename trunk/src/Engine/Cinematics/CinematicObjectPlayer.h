#pragma once


#include <string>
#include "CinematicPlayer.h"
#include "Scenes\SceneNode.h"

namespace engine
{
	class CCinematicObjectPlayer : public CCinematicPlayer
	{
	public:
		CCinematicObjectPlayer(const std::string& aName);
		virtual ~CCinematicObjectPlayer();
		virtual bool Load(const CXMLElement* aElement);

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicObjectPlayer);
		CSceneNode* m_SceneNode;

	protected:
		virtual void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B);
	};
}