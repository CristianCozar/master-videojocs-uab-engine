#include "CinematicPlayer.h"
#include "Animator\Animator.h"

#include "Utils\CheckedDelete.h"

namespace engine
{
	CCinematicPlayer::CCinematicPlayer()
	{
		m_CurrentKey = 0;
		m_NextKey = 0;
	}

	CCinematicPlayer::~CCinematicPlayer()
	{
		base::utils::CheckedDelete(mKeys);
		mKeys.clear();
	}

	void CCinematicPlayer::PlayForward(float currentTime)
	{
		float l_CurrentP;
		GetCurrentKeyForward(currentTime);

		// Calculate the % of the animation, in order to interpolate key by key
		if (m_CurrentKey != m_NextKey)
		{
			l_CurrentP = (currentTime - mKeys[m_CurrentKey]->GetTime()) /
				(mKeys[m_NextKey]->GetTime() - mKeys[m_CurrentKey]->GetTime());
		}
		else{
			l_CurrentP = mKeys[mKeys.size()-1]->GetTime();
		}

		Apply(l_CurrentP, mKeys[m_CurrentKey], mKeys[m_NextKey]);

	}


	void CCinematicPlayer::PlayBackward(float currentTime)
	{
		float l_CurrentP;
		GetCurrentKeyBackward(currentTime);

		// Calculate the % of the animation, in order to interpolate key by key

		if (m_CurrentKey != m_NextKey)
		{
			l_CurrentP = (currentTime - mKeys[m_CurrentKey]->GetTime()) /
				(mKeys[m_NextKey]->GetTime() - mKeys[m_CurrentKey]->GetTime());
		}
		else{
			l_CurrentP = mKeys[0]->GetTime();
		}

		Apply(l_CurrentP, mKeys[m_CurrentKey], mKeys[m_NextKey]);
	}


	void CCinematicPlayer::GetCurrentKeyForward(float currentTime)
	{
		if (currentTime > mKeys[mKeys.size() - 1]->GetTime())
			currentTime = mKeys[mKeys.size() - 1]->GetTime();
		for (size_t i = 0; i < mKeys.size(); ++i)
		{
			if (mKeys[i]->GetTime() >= currentTime)
			{
				m_CurrentKey = i;
				if (i == mKeys.size() - 1)
				{
					m_NextKey = i;
				}
				else{
					m_NextKey = i+1;
				}
				break;
			}
		}
	}

	void CCinematicPlayer::GetCurrentKeyBackward(float currentTime)
	{
		if (currentTime <= 0.0f)
			currentTime = 0.0f;
		for (size_t i = mKeys.size()-1; i >= 0; --i)
		{
			if (mKeys[i]->GetTime() <= currentTime)
			{
				m_CurrentKey = i;
				if (i == 0)
				{
					m_NextKey = i;
				}
				else{
					m_NextKey = i - 1;
				}
				break;
			}
		}
	}
}