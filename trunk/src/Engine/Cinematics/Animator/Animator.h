#pragma once

// Author: Alejandro V�zquez

#include "Utils\Types.h"
#include "Math\MathTypes.h"

namespace engine
{
	template < typename T >
	class CAnimator
	{
	public:
		CAnimator();
		virtual ~CAnimator() {}

		void Init(const T& aInitValue, const T& aEndValue, float aTotalTime, ETypeFunction type);
		void SetDegree(uint32 aDegree);
		bool Update(float aElapsedTime, T& aValue);
		bool IsFinish();
	private:
		ETypeFunction   mFunction;
		T               mInitValue;
		T               mEndValue;
		float           mTotalTime;
		float           mElapsedTime;
		uint32          mDegree;
	};

	template<typename T>
	inline CAnimator<T>::CAnimator()
	{
	}

	template<typename T>
	inline void CAnimator<T>::Init(const T& aInitValue, const T& aEndValue, float aTotalTime, ETypeFunction aType)
	{
		mInitValue = aInitValue;
		mEndValue = aEndValue;
		mTotalTime = aTotalTime;
		mElapsedTime = 0.f;
		mFunction = aType;
	}

	template<typename T>
	inline void CAnimator<T>::SetDegree(uint32 aDegree)
	{
		mDegree = aDegree;
	}

	template<typename T>
	inline bool CAnimator<T>::Update(float aElapsedTime, T & aValue)
	{
		mElapsedTime += aElapsedTime;
		bool finish = false;
		if (mElapsedTime >= mTotalTime)
		{
			finish = true;
			mElapsedTime = mTotalTime;
		}

		// The mu alwais must be between 0 and 1
		float mu = mElapsedTime / mTotalTime;
		switch (mFunction)
		{
		case FUNC_LINEAR: {} break;
		case FUNC_INCREMENT: { mu = mathUtils::PowN(mu, mDegree); } break;
		case FUNC_DECREMENT: { mu = sqrt(mu); } break;
		}

		aValue = mathUtils::Lerp(mInitValue, mEndValue, mu);
		return finish;
	}

	template<typename T>
	inline bool CAnimator<T>::IsFinish()
	{
		return mElapsedTime >= mTotalTime;
	}
}