#include "CinematicManager.h"
#include "Utils\Logger\Logger.h"
#include "Paths.h"
#include "Debug\imgui.h"

namespace engine
{
	CCinematicManager::CCinematicManager()
	{

	}

	CCinematicManager::~CCinematicManager()
	{
		Destroy();
	}

	bool CCinematicManager::Load(const std::string& Filename)
	{
		LOG_INFO_APPLICATION("Loading Cinematics from '%s'", Filename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((/*PATH_CINEMATICS*/ Filename).c_str());

		if (base::xml::SucceedLoad(error)) 
		{
			LOG_INFO_APPLICATION("Reading Cinematics");
			m_Filename = Filename;
			tinyxml2::XMLElement *cinematics = document.FirstChildElement("cinematics"); 
			if (cinematics) 
			{
				for (tinyxml2::XMLElement *cinematic = cinematics->FirstChildElement("cinematic"); cinematic != nullptr; cinematic = cinematic->NextSiblingElement("cinematic")) 
				{
					CCinematic *lCinematic = new CCinematic();
					lCinematic->Load(cinematic);
					Add(cinematic->Attribute("name"), lCinematic);
				}	
			}
		}else 
		{
			LOG_ERROR_APPLICATION("Cinematics XML '%s' does not exist", Filename.c_str());
		}

		return base::xml::SucceedLoad(error);
	}


	bool CCinematicManager::Reload()
	{
		Destroy();
		Load(m_Filename);

		return true;
	}

	bool CCinematicManager::Update(float elapsedTime)
	{		
		for (unsigned int i = 0; i < base::utils::CTemplatedMapVector<CCinematic>::GetCount(); ++i)
		{
			GetIndex(i)->Update(elapsedTime);
		}

		return true;
	}

	bool CCinematicManager::Play(const std::string& Filename)
	{
		Get(Filename)->Play();

		return true;
	}

	void CCinematicManager::RenderDebugGUI()
	{
		for (int i = 0; i < GetCount(); ++i)
		{
			ImGui::Indent();
			GetIndex(i)->RenderDebugGUI();
			ImGui::Unindent();
		}
	}
	
}