#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Cinematic.h"

namespace engine
{
	class CCinematicManager : public base::utils::CTemplatedMapVector<CCinematic>
	{
	public:
		CCinematicManager();
		virtual ~CCinematicManager();
		bool Load(const std::string& Filename);
		bool Reload();
		bool Update(float elapsedTime);
		bool Play(const std::string& Filename);
		void RenderDebugGUI();
	protected:
		std::string m_Filename;
	};

}