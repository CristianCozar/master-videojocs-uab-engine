#pragma once

#include "CinematicKey.h"

namespace engine
{
	class CCinematicObjectKey : public CCinematicKey
	{
	public:
		CCinematicObjectKey();
		virtual ~CCinematicObjectKey();
		virtual bool Load(const CXMLElement* aElement);
		GET_SET_REF(Vect3f, Rotation);
		GET_SET_REF(Vect3f, Scale);

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicObjectKey);
		Vect3f m_Rotation;
		Vect3f m_Scale;

	};
}