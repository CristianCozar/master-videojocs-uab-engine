#pragma once

#include "Utils\Name.h"
#include "Utils\TemplatedMapVector.h"
#include "CinematicPlayer.h"
#include "CinematicCameraPlayer.h"
#include "CinematicObjectPlayer.h"
#include "Camera\CameraController.h"
#include "Camera\FPSCameraController.h"
#include "Camera\TPSCameraController.h"
#include "Camera\SphericalCameraController.h"

namespace engine
{
	class CCinematic : public CName, public base::utils::CTemplatedMapVector<CCinematicPlayer>
	{
	public:
		CCinematic();
		virtual ~CCinematic();
		bool Load(const CXMLElement* aElement);
		void Update(float elapsedTime);
		void Play();
		GET_SET_BOOL(Active);
		GET_SET_BOOL(Loop);
		GET_SET_BOOL(Reversed);
		GET_SET_BOOL(Finish);
		GET_SET_REF(float, TotalTime);
		GET_SET_REF(float, CurrentTime);
		void RenderDebugGUI();
		void Destroy();

		void ChangeDirection();
		void Pause();
		void Stop();


	protected:
		bool m_Active;
		bool m_Finish;
		bool m_Loop;
		bool m_Reversed;
		bool m_pause;
		bool m_Changed;
		float m_TotalTime;
		bool m_PlayingForward;
		bool m_PlayingBackward;
		float m_CurrentTime;
	//	void ChangeDirection();
	//	void Pause();
	//	void Stop();
		CCameraController::CameraType prevType;
		CCameraController* prevCamera;
		CCameraController* cinematicCamera;

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematic);
		typedef std::vector< CCinematicPlayer*> TPlayers;
		TPlayers mPlayers;
	};

}