#include "CinematicObjectPlayer.h"
#include "CinematicObjectKey.h"
#include "Scenes\SceneManager.h"
#include "Animator\Animator.h"

#include "Math\MathTypes.h"

namespace engine
{
	CCinematicObjectPlayer::CCinematicObjectPlayer(const std::string& aName)
	{
		m_SceneNode = nullptr;
	}

	CCinematicObjectPlayer::~CCinematicObjectPlayer()
	{

	}

	bool CCinematicObjectPlayer::Load(const CXMLElement* aElement)
	{
		CSceneManager& lSM = engine::CEngine::GetInstance().GetSceneManager();

		bool lOK = true;
		const std::string lSceneName = aElement->Attribute("scene");
		const std::string lSceneLayerName = aElement->Attribute("layer");
		const std::string lSceneNodeName = aElement->Attribute("scene_node");
		
		m_SceneNode = lSM.Get(lSceneName)->Get(lSceneLayerName)->Get(lSceneNodeName);

		for (const tinyxml2::XMLElement *key = aElement->FirstChildElement("key"); key != nullptr; key = key->NextSiblingElement("key"))
		{
			CCinematicKey* lObjKey = new CCinematicObjectKey();
			lOK = lObjKey->Load(key);
			mKeys.push_back(lObjKey);
		}

		return lOK;
	}

	void CCinematicObjectPlayer::Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B)
	{
		CCinematicObjectKey* lA = static_cast<CCinematicObjectKey*>(A);
		CCinematicObjectKey* lB = static_cast<CCinematicObjectKey*>(B);
		float totalTime;

	/*	if (lB->GetTime() == lA->GetTime())
		{
			if (lA->GetTime() != 0.0f)
			{
				totalTime = lB->GetTime();
			}
			else{
				totalTime = 0.1f;
			}
		}
		else{
			totalTime = abs(lB->GetTime() - lA->GetTime());
		}*/
		// Especialización por reprocutor
		CAnimator<Vect3f> lPositionAnimator;
		lPositionAnimator.Init(lA->GetPosition(), lB->GetPosition(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentPosition;
		lPositionAnimator.Update(aPercentage, lCurrentPosition);

		CAnimator<Vect3f> lRotationAnimator;
		lRotationAnimator.Init(lA->GetRotation(), lB->GetRotation(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentRotation;
		lRotationAnimator.Update(aPercentage, lCurrentRotation);

		CAnimator<Vect3f> lScaleAnimator;
		lRotationAnimator.Init(lA->GetScale(), lB->GetScale(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentScale;
		lRotationAnimator.Update(aPercentage, lCurrentScale);

		m_SceneNode->SetPosition(lCurrentPosition);
		m_SceneNode->SetPitch(lCurrentRotation.y * DEG2RAD);
		m_SceneNode->SetYaw(lCurrentRotation.x * DEG2RAD);
		m_SceneNode->SetRoll(lCurrentRotation.z * DEG2RAD);
		m_SceneNode->SetScale(lCurrentScale);
	}

}