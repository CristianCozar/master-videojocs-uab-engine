#pragma once

#include "CinematicKey.h"

namespace engine
{
	class CCinematicCameraKey : public CCinematicKey
	{
	public:
		CCinematicCameraKey();
		virtual ~CCinematicCameraKey();
		virtual bool Load(const CXMLElement* aElement);
		GET_SET_REF(float, NearPlane);
		GET_SET_REF(float, FarPlane);
		GET_SET_REF(float, FOV);
		//GET_SET_REF(Vect3f, Eye);
		GET_SET_REF(Vect3f, LookAt);
		GET_SET_REF(Vect3f, Up);

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicCameraKey);
		float m_NearPlane;
		float m_FarPlane;
		float m_FOV;
		Vect3f m_LookAt;
		Vect3f m_Up;
	};
}