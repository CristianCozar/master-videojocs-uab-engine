#include "CinematicCameraKey.h"



namespace engine
{
	CCinematicCameraKey::CCinematicCameraKey()
	{
		m_NearPlane = 0.0f;
		m_FarPlane = 1000.0f;
		m_FOV = 100.0f;
		Vect3f m_LookAt = Vect3f(1.0f, 0.0f, 0.0f);
		Vect3f m_Up = Vect3f(0.0f, 1.0f, 0.0f);;
	}

	CCinematicCameraKey::~CCinematicCameraKey()
	{

	}


	bool CCinematicCameraKey::Load(const CXMLElement* aElement)
	{
		m_Time = aElement->GetAttribute<float>("time", -1.0);
		m_Position = aElement->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f));
		m_LookAt = aElement->GetAttribute<Vect3f>("look_at", Vect3f(0.0f, 0.0f, 0.0f));
		m_Up = aElement->GetAttribute<Vect3f>("up", Vect3f(0.0f, 0.0f, 0.0f));
		m_FOV = aElement->GetAttribute<float>("fov", -1.0);
		m_NearPlane = aElement->GetAttribute<float>("near_plane", -1.0);
		m_FarPlane = aElement->GetAttribute<float>("far_plane", -1.0);

		return true;

	}

}