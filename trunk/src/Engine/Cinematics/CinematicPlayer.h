#pragma once

#include <vector>

#include "CinematicKey.h"
#include "XML\XML.h"
#include "Utils\Defines.h"

namespace engine
{
	class CCinematicPlayer
	{
	public:
		CCinematicPlayer();
		virtual ~CCinematicPlayer();
		virtual bool Load(const CXMLElement* aElement) = 0;
		void PlayForward(float currentTime);
		void PlayBackward(float currentTime);
	
	protected:
		typedef std::vector< CCinematicKey*> TKeys;
		TKeys mKeys;
		size_t m_CurrentKey, m_NextKey;
		void GetCurrentKeyForward(float currentTime);
		void GetCurrentKeyBackward(float currentTime);
		virtual void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B) = 0;

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicPlayer);
	};
}