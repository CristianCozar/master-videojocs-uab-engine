#pragma once

#include <string>

#include "CinematicPlayer.h"


namespace engine
{
	class CCinematicCameraPlayer : public CCinematicPlayer
	{
	public:
		CCinematicCameraPlayer(const std::string& aName);
		virtual ~CCinematicCameraPlayer();
		virtual bool Load(const CXMLElement* aElement);

	private:
		DISALLOW_COPY_AND_ASSIGN(CCinematicCameraPlayer);

	protected:
		virtual void Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B);//
	};
}