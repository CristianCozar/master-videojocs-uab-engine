#include "Cinematic.h"
#include "Debug\imgui.h"
#include "Utils\CheckedDelete.h"

namespace engine
{
	CCinematic::CCinematic() : CName()
	{
		m_CurrentTime = 0.0;
		m_PlayingForward = true;
		m_PlayingBackward = false;
		m_Finish = true;
		m_Active = false;
		m_Loop = false;
		m_Reversed = false;
		m_pause = true;
		m_Changed = false;
		m_TotalTime = 0.0f;
	}

	CCinematic::~CCinematic()
	{
		Destroy();
	}

	void CCinematic::Destroy()
	{
		CTemplatedMapVector::Destroy();
		base::utils::CheckedDelete(mPlayers);
		//delete prevCamera;
		delete cinematicCamera;
	}

	bool CCinematic::Load(const CXMLElement* aElement)
	{
		bool lOK = true;
		CCinematicPlayer* lPlayer;
		m_Name = aElement->Attribute("name");

	//	for (const tinyxml2::XMLElement *cinematic = aElement->FirstChildElement("cinematic"); cinematic != nullptr; cinematic = cinematic->NextSiblingElement("cinematic")) 
	//	{
		m_TotalTime = aElement->GetAttribute<float>("duration", 0.0);
		m_Loop = aElement->GetAttribute<bool>("loop", false);
		m_Reversed = aElement->GetAttribute<bool>("reverse", false);
			
			for (const tinyxml2::XMLElement *cinematicObjectPlayer = aElement->FirstChildElement("cinematic_object_player"); cinematicObjectPlayer != nullptr; cinematicObjectPlayer = cinematicObjectPlayer->NextSiblingElement("cinematic_object_player"))
			{
				lPlayer = new CCinematicObjectPlayer(aElement->Attribute("name"));
				lOK = lPlayer->Load(cinematicObjectPlayer);
				if (!lOK)
					break;
				mPlayers.push_back(lPlayer);
			}

			for (const tinyxml2::XMLElement *cinematicCameraPlayer = aElement->FirstChildElement("cinematic_camera_player"); cinematicCameraPlayer != nullptr; cinematicCameraPlayer = cinematicCameraPlayer->NextSiblingElement("cinematic_camera_player"))
			{
				lPlayer = new CCinematicCameraPlayer(aElement->Attribute("name"));
				lOK = lPlayer->Load(cinematicCameraPlayer);
				if (!lOK)
					break;
				mPlayers.push_back(lPlayer);
			}

	//	}


		return lOK;
	}

	void CCinematic::Update(float elapsedTime)
	{
		if (!m_pause)
		{ 
			if (m_PlayingForward && !m_Finish)
			{
				m_CurrentTime += elapsedTime;
				if (m_CurrentTime < m_TotalTime)
				{
					for (unsigned int i = 0; i < mPlayers.size(); ++i)
					{
						mPlayers[i]->PlayForward(m_CurrentTime);
					}
				}else{
					if (m_Reversed)
					{
						m_CurrentTime = m_TotalTime;
						for (unsigned int i = 0; i < mPlayers.size(); ++i)
						{
							mPlayers[i]->PlayBackward(m_CurrentTime);
						}
						m_PlayingForward = false;
						m_PlayingBackward = true;
					}				
					else{
						for (unsigned int i = 0; i < mPlayers.size(); ++i)
						{
							mPlayers[i]->PlayForward(m_CurrentTime);
						}
						m_Finish = true;
						m_pause = true;
						if (m_Changed)
						{
							m_PlayingForward = !m_PlayingForward;
							m_PlayingBackward = !m_PlayingBackward;
							m_Changed = !m_Changed;
						}
						//CEngine::GetInstance().SetCameraController(prevCamera);
					}
				}
			}else{
				if (m_PlayingBackward && !m_Finish)
				{
					m_CurrentTime -= elapsedTime;
					if (m_CurrentTime > 0.0)
					{
						for (unsigned int i = 0; i < mPlayers.size(); ++i)
						{
							mPlayers[i]->PlayBackward(m_CurrentTime);
						}
					}
					else{
						if (m_Loop)
						{
							m_CurrentTime = 0.0;
							for (unsigned int i = 0; i < mPlayers.size(); ++i)
							{
								mPlayers[i]->PlayForward(m_CurrentTime);
							}
							m_PlayingForward = true;
							m_PlayingBackward = false;
						}
						else{
							for (unsigned int i = 0; i < mPlayers.size(); ++i)
							{
								mPlayers[i]->PlayBackward(m_CurrentTime);
							}
							m_Finish = true;
							m_pause = true;
							if (m_Changed)
							{
								m_PlayingForward = !m_PlayingForward;
								m_PlayingBackward = !m_PlayingBackward;
								m_Changed = !m_Changed;
							}
							//CEngine::GetInstance().SetCameraController(prevCamera);
						}
					}

				}

			}
		}
	}

	void CCinematic::Play()
	{		
		/*if (m_Finish && m_pause)
		{
			if (!prevCamera)
				prevCamera = &CEngine::GetInstance().GetCameraController();

			if (!cinematicCamera)
				cinematicCamera = new CCameraController();

			CEngine::GetInstance().SetCameraController(cinematicCamera);
		}
		else{
			if (!m_Finish && m_pause)
			{
				if (!prevCamera)
					prevCamera = &CEngine::GetInstance().GetCameraController();

				if (!cinematicCamera)
					cinematicCamera = new CCameraController();

				CEngine::GetInstance().SetCameraController(cinematicCamera);
			}
		}*/
		m_pause = false;
		m_Finish = false;
		if (m_PlayingForward)
		{
			m_CurrentTime = 0.0f;
		}
		else{
			if (m_PlayingBackward)
			{
				m_CurrentTime = m_TotalTime;
			}

		}
		
	}

	void CCinematic::Pause()
	{
		if (!m_Finish)
		{
			m_pause = !m_pause;

			if (m_pause)
			{
				/*if (prevCamera)
				{
					CEngine::GetInstance().SetCameraController(prevCamera);
				}*/
			}
			else
			{
				/*if (cinematicCamera)
				{
					//	cinematicCamera = new CCameraController();
					CEngine::GetInstance().SetCameraController(cinematicCamera);
				}*/
			}
		}
	}

	void CCinematic::Stop()
	{
		m_Finish = true;
		m_pause = true;
		/*if (!prevCamera)
			prevCamera = &CEngine::GetInstance().GetCameraController();*/

		
		/*if (!cinematicCamera)
			cinematicCamera = new CCameraController();
		CEngine::GetInstance().SetCameraController(cinematicCamera);*/

		for (unsigned int i = 0; i < mPlayers.size(); ++i)
		{
			mPlayers[i]->PlayForward(0.0);
		}
		/*if (prevCamera)
			CEngine::GetInstance().SetCameraController(prevCamera);*/
	}


	void CCinematic::ChangeDirection()
	{
		if (m_PlayingForward)
		{
			m_PlayingForward = false;
			m_PlayingBackward = true;
		}
		else{
			m_PlayingForward = true;
			m_PlayingBackward = false;
		}
		m_Changed = !m_Changed;
	}


	void CCinematic::RenderDebugGUI()
	{
		if (ImGui::CollapsingHeader(GetName().c_str()))
		{
			if (ImGui::Button("Play")) {
				Play();
			}
			if (ImGui::Button("Pause/Resume")) {
				Pause();
			}
			if (ImGui::Button("Stop")) {
				Stop();
			}
			if (ImGui::Button("Cambiar sentido de reproduccion")) {
				ChangeDirection();
			}
		}
	}

}