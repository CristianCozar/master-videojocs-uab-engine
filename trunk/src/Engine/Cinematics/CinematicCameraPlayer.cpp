#include "CinematicCameraPlayer.h"
#include "CinematicCameraKey.h"
#include "Animator\Animator.h"
#include "Engine.h"
#include "RenderManager.h"
#include "Camera\CameraController.h"
#include "Camera\FPSCameraController.h"


namespace engine
{
	CCinematicCameraPlayer::CCinematicCameraPlayer(const std::string& aName)
	{

	}

	CCinematicCameraPlayer::~CCinematicCameraPlayer()
	{

	}

	bool CCinematicCameraPlayer::Load(const CXMLElement* aElement)
	{
		bool lOK = true;
		
		for (const tinyxml2::XMLElement *key = aElement->FirstChildElement("key"); key != nullptr; key = key->NextSiblingElement("key"))
		{
			CCinematicKey* lObjKey = new CCinematicCameraKey();
			lOK = lObjKey->Load(key);
			mKeys.push_back(lObjKey);
		}

		return lOK;
	}

	void CCinematicCameraPlayer::Apply(float aPercentage, CCinematicKey* A, CCinematicKey* B)
	{
		CCinematicCameraKey* lA = static_cast<CCinematicCameraKey*>(A);
		CCinematicCameraKey* lB = static_cast<CCinematicCameraKey*>(B);

		// Especialización por reprocutor
		CAnimator<Vect3f> lPositionAnimator;
		lPositionAnimator.Init(lA->GetPosition(), lB->GetPosition(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentPosition;
		lPositionAnimator.Update(aPercentage, lCurrentPosition);

		CAnimator<Vect3f> lLookAtAnimator;
		lLookAtAnimator.Init(lA->GetLookAt(), lB->GetLookAt(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentLookAt;
		lLookAtAnimator.Update(aPercentage, lCurrentLookAt);

		CAnimator<Vect3f> lUpAnimator;
		lUpAnimator.Init(lA->GetUp(), lB->GetUp(), 1.0f, FUNC_LINEAR);
		Vect3f lCurrentUp;
		lUpAnimator.Update(aPercentage, lCurrentUp);
		
		Vect3f lProjectionA;
		Vect3f lProjectionB;

		lProjectionA.x = lA->GetNearPlane();
		lProjectionA.y = lA->GetFarPlane();
		lProjectionA.z = lA->GetFOV();

		lProjectionB.x = lB->GetNearPlane();
		lProjectionB.y = lB->GetFarPlane();
		lProjectionB.z = lB->GetFOV();

		CAnimator<Vect3f> lProjectionAnimator;
		lUpAnimator.Init(lProjectionA, lProjectionB, 1.0f, FUNC_LINEAR);
		Vect3f lCurrentProjection;
		lUpAnimator.Update(aPercentage, lCurrentProjection);

		/*CAnimator<float> lNearAnimator;
		lNearAnimator.Init(lA->GetNearPlane(), lB->GetNearPlane(), 1.0f, FUNC_LINEAR);
		float lCurrentNear;
		lNearAnimator.Update(aPercentage, lCurrentNear);

		CAnimator<float> lFarAnimator;
		lFarAnimator.Init(lA->GetFarPlane(), lB->GetFarPlane(), 1.0f, FUNC_LINEAR);
		float lCurrentFar;
		lFarAnimator.Update(aPercentage, lCurrentFar);

		CAnimator<float> lFOVAnimator;
		lFOVAnimator.Init(lA->GetFOV(), lB->GetFOV(), 1.0f, FUNC_LINEAR);
		float lCurrentFOV;
		lFOVAnimator.Update(aPercentage, lCurrentFOV);*/
		
		CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();
		CCameraController& lCC = CEngine::GetInstance().GetCameraController();
	//	CFPSCameraController& lFPS= static_cast<CFPSCameraController&>(lCC);

		
		
		lCC.SetPosition(lCurrentPosition);
		lCC.SetUp(lCurrentUp);
		lCurrentLookAt = lCurrentLookAt - lCurrentPosition;
		lCC.SetFront(lCurrentLookAt);

	//	float Pitch = lCurrentLookAt.GetAngleX();
	//	float Yaw = lCurrentLookAt.GetAngleZ();
	//	float test = lCurrentLookAt.GetAngleY();

	//	float test2 = asin(lCurrentLookAt.y);
	//	float test3 = acos(lCurrentLookAt.x/ cos(test2));

		//m_Front.z = sin(m_Yaw) * cos(m_Pitch);
	//	m_Front.y = -sin(m_Pitch);
	//	m_Front.x = cos(m_Yaw) * cos(m_Pitch);

		//lFPS.SetPitch(0.424);
	//	lFPS.SetYaw(2.080);

	//	lFPS.SetPitch(test2);
	//	lFPS.SetYaw(test3);


	//	float dX = lCurrentPosition.x - lCurrentLookAt.x;
	//	float dY = lCurrentPosition.y - lCurrentLookAt.y;
	//	float dZ = lCurrentPosition.z - lCurrentLookAt.z;


	//	float lyaw = atan2(dZ, dX)-90.0*3.14/180.0;	
	//	float lpitch = -atan2(dY, sqrt(dZ * dZ + dX * dX));


	//	lyaw = atan((lCurrentLookAt.x - lCurrentPosition.x) / (lCurrentLookAt.z - lCurrentPosition.z));
	//	lpitch = atan((lCurrentLookAt.z - lCurrentPosition.z) / (lCurrentLookAt.y - lCurrentPosition.y));



	//	lFPS.SetPitch(lpitch);
	//	lFPS.SetYaw(lyaw);

		float lAspect = lRM.GetCurrentAspect();

		lRM.SetProjectionMatrix(lCurrentProjection.z, lAspect, lCurrentProjection.x, lCurrentProjection.y);
	
	}

}