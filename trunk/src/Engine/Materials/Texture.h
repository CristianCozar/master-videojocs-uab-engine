#pragma once
#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3dcsx.h>

#include "Utils\Name.h"
#include "Utils\Types.h"
#include "Utils\StringUtils.h"

#include "Math\MathUtils.h"
#include "Math\Vector2.h"


namespace engine
{
	class	CTexture : public	CName
	{
		enum EType
		{
			e2D_Texture = 0,
			e2D_TextureArray,
			e3D_Texture
		};

	public:
		CTexture(const std::string& aName); 
		CTexture(const std::string& aName, const CXMLElement* TreeNode);
		CTexture(const CXMLElement* TreeNode);
		virtual ~CTexture();
		bool Load();
		bool Reload();
		void Bind(uint32 aStageId, ID3D11DeviceContext* aContext);
		GET_SET_PTR(ID3D11ShaderResourceView, Texture);
		ID3D11SamplerState* GetSampler();
		const Vect2u & GetSize() const {return mSize;}


	protected:
		ID3D11ShaderResourceView *m_pTexture;
		ID3D11SamplerState *m_SamplerState;
		Vect2u mSize;
		bool m_GenerateMips;
	private:
		DISALLOW_COPY_AND_ASSIGN(CTexture);
		void Destroy();


	};
}