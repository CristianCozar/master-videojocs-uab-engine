#include "Material.h"

#include "Debug\CustomAssert.h"
#include "TemplatedMaterialParameter.h"

#include "Utils\Logger\Logger.h"
#include "Math\Vector2.h"
#include "Math\Vector3.h"
#include "Math\Vector4.h"
#include "Math\Color.h"
#include "Debug\imgui.h"
#include "TextureManager.h"
#include "Engine.h"
#include "Paths.h"
#include "RenderManager.h"
#include "Shaders/TechniquePoolManager.h"
#include "ConstantBufferManager.h"



namespace engine
{
	CMaterial::CMaterial(const CXMLElement* aElement) : CName(aElement), m_DebugUi(false), m_Copied(false) {
		std::string vertex_type = aElement->GetAttribute<std::string>("vertex_type", "");
		std::string technique = aElement->GetAttribute<std::string>("technique", "");
		if (technique != "")
		{
			mTechnique = CEngine::GetInstance().GetTechniquePoolManager().Get(technique);
		}
		else{
			mTechnique = CEngine::GetInstance().GetTechniquePoolManager().Get(vertex_type);
		}

		for (const tinyxml2::XMLElement *texture = aElement->FirstChildElement("texture"); texture != nullptr; texture = texture->NextSiblingElement("texture")) {
			engine::CTextureManager& lTM = engine::CEngine::GetInstance().GetTextureManager();
			std::string path = PATH_TEXTURE + std::string(texture->Attribute("filename"));
			engine::CTexture *tex = lTM.GetTexture(path.c_str(), texture);
			mTextures.push_back(tex);
		}

		for (const tinyxml2::XMLElement *parameter = aElement->FirstChildElement("parameter"); parameter != nullptr; parameter = parameter->NextSiblingElement("parameter")) {
			TParameterType lType;
			if (EnumString<TParameterType>::ToEnum(lType, parameter->GetAttribute<std::string>("type", "")))
			{
				CMaterialParameter* lParameter = nullptr;

				//MACRO
#define CASE_CREATE_MATERIAL_PARAMETER( parameter_type, parameter_enum, parameter_def_value )\
	case parameter_enum: \
													{ \
		lParameter = new CTemplatedMaterialParameter<parameter_type> \
		( \
			parameter->GetAttribute<std::string>("name", ""), \
			parameter->GetAttribute<parameter_type>("value", parameter_def_value), \
			parameter_enum \
		); \
													}\
	break;

				switch (lType)
				{
					CASE_CREATE_MATERIAL_PARAMETER(float, eFloat, 0.0f)
					CASE_CREATE_MATERIAL_PARAMETER(Vect2f, eFloat2, Vect2f())
					CASE_CREATE_MATERIAL_PARAMETER(Vect3f, eFloat3, Vect3f())
					CASE_CREATE_MATERIAL_PARAMETER(Vect4f, eFloat4, Vect4f())
					CASE_CREATE_MATERIAL_PARAMETER(CColor, eColor, CColor())
				}
#undef CASE_CREATE_MATERIAL_PARAMETER
				//END MACRO

				if (lParameter)
				{
					lParameter->SetStep(parameter->GetAttribute<float>("step", 0.1f));
					lParameter->SetMin(parameter->GetAttribute<float>("min", 0.0f));
					lParameter->SetMax(parameter->GetAttribute<float>("max", 80.0f));
					mParameters.push_back(lParameter);
				}

			}
		}
	}
	CMaterial::~CMaterial() {
		Destroy();
	}

	void CMaterial::Destroy()
	{
		if (!m_Copied) {
			base::utils::CheckedDelete(mParameters);
		}
		mTextures.clear();
		mTechnique = nullptr;
	}

	CXMLElement* CMaterial::getXMLNode(CXMLDocument *document) {
		CXMLElement* material = document->NewElement("material");
		material->SetAttribute("name", GetName().c_str());

		std::vector<CTexture*>::iterator texture_first = mTextures.begin(), texture_end = mTextures.end();
		for (; texture_first != texture_end; ++texture_first) {
			CXMLElement* texture = document->NewElement("texture");
			
			const std::string &texture_filename = (*texture_first)->GetName();
			const std::string &texture_type = "TODO";
			
			texture->SetAttribute("type", texture_type.c_str());
			texture->SetAttribute("filename", texture_filename.c_str());
			
			material->InsertEndChild(texture);
		}

		std::vector<CMaterialParameter*>::iterator parameter_first = mParameters.begin(), parameter_end = mParameters.end();

		for (; parameter_first != parameter_end; ++parameter_first) {
			CMaterialParameter *parameter = (*parameter_first);
			CMaterial::TParameterType parameter_type = parameter->GetType();

			CXMLElement* param = document->NewElement("parameter");

			switch (parameter_type) {
			case eFloat:
			{
				CTemplatedMaterialParameter<float>* mp = dynamic_cast<CTemplatedMaterialParameter<float>*>(parameter);
				param->SetAttribute("value", mp->GetValue());
			}
				break;
			case eFloat2:
			{
				CTemplatedMaterialParameter<Vect2f>* mp = dynamic_cast<CTemplatedMaterialParameter<Vect2f>*>(parameter);
				param->SetAttribute<Vect2f>("value", mp->GetValue());
			}
				break;
			case eFloat3:
			{
				CTemplatedMaterialParameter<Vect3f>* mp = dynamic_cast<CTemplatedMaterialParameter<Vect3f>*>(parameter);
				param->SetAttribute<Vect3f>("value", mp->GetValue());
			}
				break;
			case eFloat4:
			{
				CTemplatedMaterialParameter<Vect4f>* mp = dynamic_cast<CTemplatedMaterialParameter<Vect4f>*>(parameter);
				param->SetAttribute<Vect4f>("value", mp->GetValue());
			}
				break;
			case eColor:
			{
				CTemplatedMaterialParameter<CColor>* mp = dynamic_cast<CTemplatedMaterialParameter<CColor>*>(parameter);
				param->SetAttribute<CColor>("value", mp->GetValue());
			}
				break;
			}


			const std::string &sparameter_name = parameter->GetName();
			const std::string &sparameter_type = EnumString<TParameterType>::ToStr(parameter_type);

			param->SetAttribute("name", sparameter_name.c_str());
			param->SetAttribute("type", sparameter_type.c_str());

			material->InsertEndChild(param);

	
		}
		return material;
	}

	void CMaterial::Apply() {
		Assert(mTechnique, "El material %s no tiene t�cnica.", GetName().c_str());

		ID3D11DeviceContext * lContext = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
			// Bind the technique that will render this geometry
			mTechnique->Bind(lContext);
		for (size_t i = 0, lCount = mTextures.size(); i < lCount; ++i)
		{
			if (mTextures[i])
				mTextures[i]->Bind(i, lContext);
		}

		CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();

		uint32_t offset = 0;
		for (unsigned int i = 0; i < mParameters.size(); ++i)
		{
			CMaterialParameter* l_MP = mParameters[i];
			uint32_t size = l_MP->GetSize();
			memcpy(&l_CBM.mMaterialDesc.m_RawData[0].x + offset, l_MP->GetAddr(), size);
			offset += size/sizeof(float);
		}
		l_CBM.BindPSBuffer(CEngine::GetInstance().GetRenderManager().GetDeviceContext(), CConstantBufferManager::CB_Material);
		l_CBM.BindGSBuffer(CEngine::GetInstance().GetRenderManager().GetDeviceContext(), CConstantBufferManager::CB_MaterialGS);

	}

	void CMaterial::ActivateParameters() {

	}

	void CMaterial::ActivateTextures() {

	}

	std::vector<CTexture*> CMaterial::GetTextures()
	{
		return mTextures;
	}

	void CMaterial::UpdateMaterial(CMaterial* aMaterial)
	{
		Destroy();
		mTextures = aMaterial->GetTextures();
		mParameters = aMaterial->GetParameters();
		mTechnique = aMaterial->GetTechnique();
		aMaterial->m_Copied = true;
	}

	/*template <typename T> void CMaterial::SetParameter(const std::string &aName, T value)
	{
		for (int i = 0; i < mParameters.size(); i++)
		{
			if (mParameters[i]->GetName() == aName)
				break;
		}

		if (i == mParameters.size())
			return;

		CMaterialParameter* l_Param = mParameters[i];
		CTemplatedMaterialParameter<T>* l_Temp = (CTemplatedMaterialParameter<T>*)l_Param;
		l_Temp->SetValue(value);
	}*/

	void CMaterial::RenderDebugGUI()
	{
		if (m_DebugUi)
		{
			ImGui::Begin(GetName().c_str(), &m_DebugUi, ImGuiWindowFlags_AlwaysAutoResize);
			ImGui::Text("Material name: %s", GetName().c_str());
			ImGui::Text("Parametros:");
			for (uint32 i = 0; i < mParameters.size(); ++i)
			{
				uint32_t size = mParameters[i]->GetSize();
				TParameterType type = mParameters[i]->GetType();
				std::string name = mParameters[i]->GetName();
				switch (type)
				{
				case TParameterType::eFloat:
				{
					float *fp = static_cast<float*>(mParameters[i]->GetAddr());
					ImGui::DragFloat(name.c_str(), fp, mParameters[i]->GetStep(), mParameters[i]->GetMin(), mParameters[i]->GetMax());
				}
				break;
				case TParameterType::eFloat2:
				{
					Vect2f *fp2 = static_cast<Vect2f*>(mParameters[i]->GetAddr());
					ImGui::DragFloat2(name.c_str(), &fp2->x, 1.0f, 0.0f, 1000.0f);
				}
					break;
				case TParameterType::eFloat3:
				{
					Vect3f *fp3 = static_cast<Vect3f*>(mParameters[i]->GetAddr());
					ImGui::DragFloat3(name.c_str(), &fp3->x, 1.0f, 0.0f, 1000.0f);
				}
					break;
				case TParameterType::eFloat4:
				{
					Vect4f *fp4 = static_cast<Vect4f*>(mParameters[i]->GetAddr());
					ImGui::DragFloat4(name.c_str(), &fp4->x, 1.0f, 0.0f, 1000.0f);
				}
					break;
				case TParameterType::eColor:
				{
					CColor *fpc = static_cast<CColor*>(mParameters[i]->GetAddr());
					ImGui::DragFloat4(name.c_str(), &fpc->x, 0.1f, 0.0f, 1.0f);
				}
					break;
				}
			}
			if (ImGui::Button("Aplicar"))
			{
				Apply();
			}
			ImGui::End();

			if (mTextures.capacity() > 0) {
				ImGui::Begin((GetName() + "-Texturas").c_str(), &m_DebugUi, ImGuiWindowFlags_AlwaysAutoResize);
				for (int i = 0; i < mTextures.size(); ++i)
				{
					CTexture* lTexture = mTextures[i];
					if (lTexture)
					{
						ImTextureID tex_id = lTexture->GetTexture();
						ImGui::Text(lTexture->GetName().c_str());
						ImGui::Image(tex_id, ImVec2(150, 150), ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
					}
				}
				ImGui::End();
			}
			//Inicio pruebas Texturas
			/*if (mTextures.capacity() > 0)
			{
				CTexture* lTexture = mTextures[1];

				if (lTexture)
				{
					ImGui::Begin((GetName() + "-Textura").c_str(), &m_DebugUi, ImGuiWindowFlags_AlwaysAutoResize);
					ImTextureID tex_id = lTexture->GetTexture();
					ImGui::Text("%fx%f", 500.0f, 500.0f);
					ImGui::Image(tex_id, ImVec2(500, 500), ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
					ImGui::End();
				}

				CTexture* lTexture2 = mTextures[2];

				if (lTexture2)
				{
					ImGui::Begin((GetName() + "-TexturaNrm").c_str(), &m_DebugUi, ImGuiWindowFlags_AlwaysAutoResize);
					ImTextureID tex_id = lTexture2->GetTexture();
					ImGui::Text("%fx%f", 500.0f, 500.0f);
					ImGui::Image(tex_id, ImVec2(500, 500), ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
					ImGui::End();
				}
			}*/
			//Fin Pruebas Texturas
		}
	}

	CTechnique* CMaterial::GetTechnique()
	{
		return mTechnique;
	}

	std::vector<CMaterialParameter*> CMaterial::GetParameters()
	{
		return mParameters;
	}


}