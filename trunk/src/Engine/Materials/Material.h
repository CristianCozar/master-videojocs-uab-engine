#pragma once

#include "Utils\Name.h"

#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"

#include "Texture.h"
#include "Shaders\Technique.h"


#include <vector>
#include "Utils\EnumToString.h"

namespace engine
{
	class CMaterialParameter;

	class CMaterial : public CName
	{
	public:
		enum ETextureIndex
		{
			eDiffuse = 0,
			eBump,
			eLightMap,
			eSpecular,
			eIndexCount
		};

		enum TParameterType
		{
			eFloat = 0,
			eFloat2,
			eFloat3,
			eFloat4,
			eColor,
			eParametersCount
		};
	public:
		CMaterial(const CXMLElement* aElement);
		virtual  ~CMaterial();
		void Destroy();
		void Apply();
		void ActivateTextures();
		void ActivateParameters();
		void RenderDebugGUI();
		CXMLElement* getXMLNode(CXMLDocument *document);
		bool m_DebugUi;
		CTechnique* GetTechnique();
		std::vector<CMaterialParameter*> GetParameters();
		std::vector<CTexture*> GetTextures();
		void UpdateMaterial(CMaterial* aMaterial);
		//template <typename T> void SetParameter(const std::string &aName, T value);
		bool m_Copied;
	private:
		DISALLOW_COPY_AND_ASSIGN(CMaterial);
		CTechnique* mTechnique;
		std::vector<CTexture*> mTextures;
		std::vector<CMaterialParameter*> mParameters;
	};
	
}

Begin_Enum_String(engine::CMaterial::ETextureIndex)
{
	Enum_String_Id(engine::CMaterial::eDiffuse, "diffuse");
	Enum_String_Id(engine::CMaterial::eBump, "bump");
	Enum_String_Id(engine::CMaterial::eLightMap, "lightmap");
	Enum_String_Id(engine::CMaterial::eSpecular, "specular");
}
End_Enum_String;

//---------------------------------------------------------------------------------------------------------
Begin_Enum_String(engine::CMaterial::TParameterType)
{
	Enum_String_Id(engine::CMaterial::eFloat, "float");
	Enum_String_Id(engine::CMaterial::eFloat2, "float2");
	Enum_String_Id(engine::CMaterial::eFloat3, "float3");
	Enum_String_Id(engine::CMaterial::eFloat4, "float4");
	Enum_String_Id(engine::CMaterial::eColor, "color");
}
End_Enum_String;