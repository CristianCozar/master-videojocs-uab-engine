#pragma once

#include <stdint.h>
#include "Utils\Name.h"
#include "Material.h"
namespace engine
{
	class CMaterialParameter : public CName
	{
	public:
		CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType);
		virtual ~CMaterialParameter();
		virtual uint32_t GetSize() const = 0;
		virtual void * GetAddr(int index = 0) const = 0;
		GET_SET_REF(std::string, Description)
		GET_SET(CMaterial::TParameterType, Type)
		GET_SET(float, Step)
		GET_SET(float, Min)
		GET_SET(float, Max)
	protected:
		float m_Step;
		float m_Min;
		float m_Max;
		std::string m_Description;
		CMaterial::TParameterType m_Type;
	private:
		DISALLOW_COPY_AND_ASSIGN(CMaterialParameter);
	};
}