#include "TextureManager.h"

namespace engine
{
	CTextureManager::CTextureManager()
	{
		mTextures = base::utils::CTemplatedMapVector<CTexture>();
	}

	CTextureManager::~CTextureManager()
	{
		mTextures.Destroy();
	}


	CTexture* CTextureManager::GetTexture(const std::string& aFilename, const CXMLElement *aNode)
	{
		if (mTextures.Exist(aFilename))
		{
			return mTextures(aFilename);
		}
		else{
			CTexture* lText;
			if (aNode != nullptr)
			{
				lText = new CTexture(aFilename, aNode);
			}
			else
			{
				lText = new CTexture(aFilename);
			}
			bool lRes = lText->Load();
			if (lRes)
			{
				bool lAdd = mTextures.Add(aFilename, lText);
				if (lAdd)
				{
					return mTextures(aFilename);
				}
				else{
					return nullptr;
				}
			}
			else{
				return nullptr;
			}
		}
	}


	bool CTextureManager::Reload()
	{
		bool lRes = true;

		for (unsigned int i = 0; i < mTextures.GetCount(); ++i)
		{
			lRes = mTextures[i]->Reload();
			if (lRes == false)
			{
				return lRes;
			}
		}

		return true;
	}

	bool CTextureManager::AddTexture(const std::string& aTextureName, CTexture* aTexture)
	{
		if (mTextures.Exist(aTextureName))
		{
			return false;
		}
		else{
			mTextures.Add(aTextureName,aTexture);
			return true;
		}
	}


}
