#include "MaterialParameter.h"
namespace engine
{
	CMaterialParameter::CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType) : CName(aName) {
		SetType(aType);
		m_Description = "";
	}

	CMaterialParameter::~CMaterialParameter() {

	}
}