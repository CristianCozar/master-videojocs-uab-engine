#include "MaterialManager.h"
#include "Utils\Logger\Logger.h"

#include "Debug\imgui.h"
#include "Paths.h"

#include "Engine.h"
#include "Mesh\MeshManager.h"
#include "Mesh\Mesh.h"

namespace engine
{
	CMaterialManager::CMaterialManager() {
		m_LevelMaterialsFilename = "";
		m_DefaultMaterialsFilename = "data/Defaults.xml";
	}

	CMaterialManager::~CMaterialManager() {

	}

	bool CMaterialManager::Load(const std::string &Filename, const std::string &DefaultsFileName) {
		return (LoadMaterialsFromFile(DefaultsFileName, true) && LoadMaterialsFromFile(Filename, true));

	}
	bool CMaterialManager::Load() {
		m_DefaultMaterialsFilename = FILENAME_DEFAULT_MATERIALS;
		return LoadMaterialsFromFile(FILENAME_DEFAULT_MATERIALS);
	}

	bool CMaterialManager::Load(const std::string &Filename) {
		m_LevelMaterialsFilename = Filename;
		return LoadMaterialsFromFile(Filename, true);
	}

	bool CMaterialManager::Reload() {
		LOG_INFO_APPLICATION("Reloading materials");
		bool lOk = Load(m_LevelMaterialsFilename, m_DefaultMaterialsFilename);
		return lOk;
	}

	bool CMaterialManager::Save() {
		const std::string &Filename = "materialsSaved.xml";

		LOG_INFO_APPLICATION("Saving materials in '%s'", Filename.c_str());
		CXMLDocument doc;
		CXMLDocument *document = &doc;
		CXMLElement *materials = document->NewElement("materials");
		

		TMapResources::iterator begin_material = m_ResourcesMap.begin(), last_material = m_ResourcesMap.end();
		for (; begin_material != last_material; ++begin_material)
		{ 
			engine::CMaterial *material = begin_material->second;
			CXMLElement* material_node = material->getXMLNode(document);
			materials->InsertEndChild(material_node);

		}
		
		document->InsertFirstChild(materials);
		tinyxml2::XMLError eResult = document->SaveFile((PATH_MATERIAL + Filename).c_str());
		return base::xml::SucceedLoad(eResult);
	}

	bool CMaterialManager::LoadMaterialsFromFile(const std::string &Filename, bool Update) {

		LOG_INFO_APPLICATION("Loading materials from '%s'", Filename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((Filename).c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading materials");
			tinyxml2::XMLElement *materials = document.FirstChildElement("materials"); //Materials
			if (materials) {
				for (tinyxml2::XMLElement *material = materials->FirstChildElement("material"); material != nullptr; material = material->NextSiblingElement("material")) {//Material
					if (Update || !Exist(material->Attribute("name"))) {
						CMaterial *material_to_save = new CMaterial(material);
						if (Exist(material->Attribute("name"))) {
							LOG_INFO_APPLICATION("Material '%s' updated", material->Attribute("name"));
							Get(material->Attribute("name"))->UpdateMaterial(material_to_save);
							delete material_to_save;
						}
						else {
							LOG_INFO_APPLICATION("Material '%s' added", material->Attribute("name"));
							base::utils::CTemplatedMap<CMaterial>::Update(material->Attribute("name"), material_to_save);
						}
						
					}
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Materials XML '%s' does not exist", Filename.c_str());
		}

		return base::xml::SucceedLoad(error);
	}

	void CMaterialManager::RenderDebugGUI()
	{
		ImGui::ListBoxHeader("", 0, 10);
		for (std::map<std::string, CMaterial*>::iterator it = m_ResourcesMap.begin(); it != m_ResourcesMap.end(); ++it) {
			if (it->second != nullptr) {
				it->second->RenderDebugGUI();
				if (ImGui::Button(it->first.c_str()))
				{
					it->second->m_DebugUi = !it->second->m_DebugUi;
				}
			}
		}
		ImGui::ListBoxFooter();
		if (ImGui::Button("Save"))
		{
			Save();
		}
		ImGui::SameLine();
		if (ImGui::Button("Reload"))
		{
			Reload();
		}
		
	}
}