#include "Texture.h"
#include "Engine.h"
#include "RenderManager.h"
#include "DirectXTex\DirectXTexP.h"
#include "Utils\CheckedDelete.h"
#include "XML\XML.h"

namespace engine
{
	CTexture::CTexture(const std::string& aName) : CName(aName)
	{
		m_pTexture = nullptr;
		m_SamplerState = nullptr;
		mSize.x = 0;
		mSize.y = 0;
		m_GenerateMips = false;
	}

	CTexture::CTexture(const std::string& aName, const CXMLElement* TreeNode) : CName(aName)
	{
		m_pTexture = nullptr;
		m_SamplerState = nullptr;
		mSize.x = 0;
		mSize.y = 0;
		m_GenerateMips = TreeNode->GetAttribute<bool>("generate_mips", false);
	}

	CTexture::CTexture(const CXMLElement* TreeNode) : CName(TreeNode->Attribute("name"))
	{		
		m_pTexture = nullptr;
		m_SamplerState = nullptr;
		mSize.x = 0;
		mSize.y = 0;
		m_GenerateMips = false;
	}

	CTexture::~CTexture()
	{
		Destroy();
	}

	bool CTexture::Load()
	{
		engine::CRenderManager& lRM = engine::CEngine::GetInstance().GetRenderManager();
		ID3D11Device* lDevice = lRM.GetDevice();
		ID3D11DeviceContext* lDeviceContext = lRM.GetDeviceContext();

		ID3D11Resource* lResource = nullptr;

		// Convert the string filename to wide string
		std::string l_path =  m_Name;
		std::wstring lTextureFileNameWString = std::wstring(l_path.begin(), l_path.end());

		std::string lExtension = base::utils::GetFilenameExtension(m_Name);

		HRESULT lHR = E_FAIL;

		DirectX::TexMetadata info;
		DirectX::ScratchImage *image = new DirectX::ScratchImage();
		DirectX::ScratchImage *decompressed = new DirectX::ScratchImage();
		DirectX::ScratchImage *mipChain = new DirectX::ScratchImage();

		if (lExtension == "dds" || lExtension == "DDS")
			lHR = DirectX::LoadFromDDSFile(lTextureFileNameWString.c_str(), DirectX::DDS_FLAGS_NONE, &info, *image);
		else if (lExtension == "tga" || lExtension == "TGA")
			lHR = DirectX::LoadFromTGAFile(lTextureFileNameWString.c_str(), &info, *image);
		else
			lHR = DirectX::LoadFromWICFile(lTextureFileNameWString.c_str(), DirectX::WIC_FLAGS_NONE, &info, *image);

		if (SUCCEEDED(lHR) && m_GenerateMips == true)
		{
			if (lExtension == "dds" || lExtension == "DDS")
				lHR = Decompress(image->GetImages(), image->GetImageCount(), image->GetMetadata(), DXGI_FORMAT_UNKNOWN, *decompressed);
			lHR = DirectX::GenerateMipMaps(decompressed->GetImages(), decompressed->GetImageCount(), decompressed->GetMetadata(), DirectX::TEX_FILTER_DEFAULT, 0, *mipChain);
			if (SUCCEEDED(lHR))
				image = mipChain;
		}
		if (SUCCEEDED(lHR))
		{
			lHR = CreateTexture(lDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &lResource);
			if (SUCCEEDED(lHR))
				lHR = CreateShaderResourceView(lDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &m_pTexture);

			if (SUCCEEDED(lHR))
			{
				ID3D11Texture2D * lTex2D = static_cast<ID3D11Texture2D *>(lResource);
				D3D11_TEXTURE2D_DESC desc;
				lTex2D->GetDesc(&desc);

				mSize.x = desc.Width;
				mSize.y = desc.Height;

				D3D11_SAMPLER_DESC lSamplerDesc;
				ZeroMemory(&lSamplerDesc, sizeof(lSamplerDesc));
				lSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
				//lSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
				lSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
				lSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
				lSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
				lSamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
				lSamplerDesc.MinLOD = 0;
				lSamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
				lHR = lDevice->CreateSamplerState(&lSamplerDesc, &m_SamplerState);
			}
		}
		
		base::utils::CheckedDelete(image);

		return SUCCEEDED(lHR);
	
	}

	bool CTexture::Reload()
	{
		Destroy();
		bool lRes = false;
		lRes = Load();

		return lRes;
	}

	void CTexture::Destroy()
	{
		if (m_pTexture)
		{
			m_pTexture->Release();
		}
		if (m_SamplerState)
		{
			m_SamplerState->Release();
		}
	}

	void CTexture::Bind(uint32 aStageId, ID3D11DeviceContext* aContext)
	{
		aContext->PSSetSamplers(aStageId, 1, &m_SamplerState);
		aContext->PSSetShaderResources(aStageId, 1, &m_pTexture);
	}


	ID3D11SamplerState* CTexture::GetSampler()
	{
		return m_SamplerState;
	}
}