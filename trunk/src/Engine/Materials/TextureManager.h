#pragma once

#include "Texture.h"
#include "RenderPipeline\DynamicTexture.h"
#include "Utils\TemplatedMapVector.h"

namespace engine
{
	class CTextureManager
	{
	public:
		CTextureManager();
		virtual ~CTextureManager();
		CTexture* GetTexture(const std::string& aFilename, const CXMLElement *aNode = nullptr);
		bool AddTexture(const std::string& aTextureName, CTexture* aTexture);
		bool Reload();
	private:
		base::utils::CTemplatedMapVector<CTexture> mTextures;
	};
}
