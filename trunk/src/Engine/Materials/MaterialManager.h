#pragma once


#include "Utils\TemplatedMap.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"
#include "Material.h"

namespace engine
{
	class CMaterialManager : public base::utils::CTemplatedMap<CMaterial> {

	public:
		CMaterialManager();
		virtual ~CMaterialManager();
		bool Load(const std::string &Filename, const std::string &DefaultsFileName);
		bool Load(const std::string &Filename);
		bool Load();
		bool Reload();
		bool Save();
		void RenderDebugGUI();
	private:
		std::string m_LevelMaterialsFilename;
		std::string m_DefaultMaterialsFilename;
		bool LoadMaterialsFromFile(const std::string &Filename, bool Update = false);
	};
}