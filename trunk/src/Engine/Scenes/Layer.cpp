#include "Layer.h"

#include "XML\XML.h"
#include "Utils\Logger\Logger.h"

#include "RenderManager.h"
#include "Paths.h"
#include "SceneBasicPrimitive.h"

#include "Lights\LightManager.h"
#include "Scenes/SceneAnimatedModel.h"
#include "AnimatedModel/AnimatedModelManager.h"
#include "Utils\CheckedDelete.h"
#include "Debug\imgui.h"
#include "Mesh\MeshManager.h"
#include "Particles\ParticleManager.h"
#include "Particles\ParticleSystemType.h"
#include "Particles\ParticleSystemInstance.h"

namespace engine
{
	CLayer::CLayer(const std::string& aName)
	{
		SetName(aName);
	}

	CLayer::~CLayer()
	{
		Destroy();
	}

	bool CLayer::Load(CXMLElement* aElement, const std::string& aFolder)
	{
		bool lOk = false;
		CMeshManager& l_MeshManager = CEngine::GetInstance().GetMeshManager();
		CParticleManager& l_ParticleManager = CEngine::GetInstance().GetParticleManager();

		if (aElement)
		{
			for (tinyxml2::XMLElement *node = aElement->FirstChildElement("scene_mesh"); node != nullptr; node = node->NextSiblingElement("scene_mesh")) { // node
				if (Exist(node->Attribute("name"))) {
					LOG_INFO_APPLICATION("Node '%s' updated", node->Attribute("name"));
				}
				else {
					LOG_INFO_APPLICATION("Node '%s' added", node->Attribute("name"));
				}
				std::string name = node->Attribute("name");
				std::string meshName = node->Attribute("mesh");
				meshName = aFolder + PATH_MESHES + meshName + EXTENSION_MESH;
				CMesh *mesh = l_MeshManager.GetMesh(name, meshName);
				/*CMesh *mesh = new CMesh(name);
				mesh->Load(meshName);*/
				if (mesh != nullptr)
				{
					CSceneMesh *nodeObject = new CSceneMesh(node, mesh, m_Name);
					Add(nodeObject->GetName(), nodeObject);
				}
				else {
					CSceneNode *nodeObject = new CSceneNode(node, m_Name);
					Add(nodeObject->GetName(), nodeObject);
				}
			}

			for (tinyxml2::XMLElement *node = aElement->FirstChildElement("animated_model"); node != nullptr; node = node->NextSiblingElement("animated_model")) { // node
				std::string name = node->Attribute("name");
				if (Exist(name)) {
					LOG_INFO_APPLICATION("Node '%s' updated", name);
				}
				else {
//					LOG_INFO_APPLICATION("Node '%s' added", name);
				}
				
				std::string coreName = node->Attribute("core");
				CSceneAnimatedModel *sceneAnimatedModel = new CSceneAnimatedModel(*node, m_Name);
				CAnimatedCoreModel *animatedCoreModel = engine::CEngine::GetInstance().GetAnimatedModelManager().Get(coreName);
				if (animatedCoreModel != nullptr)
				{
					sceneAnimatedModel->Initialize(animatedCoreModel);
					Add(sceneAnimatedModel->GetName(), sceneAnimatedModel);
				}
				else
					base::utils::CheckedDelete(sceneAnimatedModel);
			}

			for (tinyxml2::XMLElement *node = aElement->FirstChildElement("scene_basic_primitive"); node != nullptr; node = node->NextSiblingElement("scene_basic_primitive"))
			{ // node
				if (Exist(node->Attribute("name"))) {
					LOG_INFO_APPLICATION("Node '%s' updated", node->Attribute("name"));
				}
				else {
					LOG_INFO_APPLICATION("Node '%s' added", node->Attribute("name"));
				}
				std::string name = node->Attribute("name");
				CSceneMesh *nodeObject = new CSceneBasicPrimitive(node, m_Name);// Deber�a ser un meshNode?

				Add(nodeObject->GetName(), nodeObject);
			}

			for (tinyxml2::XMLElement *lightNode = aElement->FirstChildElement("scene_light"); lightNode != nullptr; lightNode = lightNode->NextSiblingElement("scene_light")) { // lightNode
				if (Exist(lightNode->Attribute("name"))) {
					LOG_INFO_APPLICATION("Light Node '%s' updated", lightNode->Attribute("name"));
				}
				else {
					LOG_INFO_APPLICATION("Light Node '%s' added", lightNode->Attribute("name"));
				}
		
				CSceneNode *nodeObject = engine::CEngine::GetInstance().GetLightManager().Get(lightNode->Attribute("name"));
				Add(nodeObject->GetName(), nodeObject);
			}

			for (tinyxml2::XMLElement *node = aElement->FirstChildElement("particle_system"); node != nullptr; node = node->NextSiblingElement("particle_system")) { // particle node
				if (Exist(node->Attribute("name"))) {
					LOG_INFO_APPLICATION("Particle Node '%s' updated", node->Attribute("name"));
				}
				else {
					LOG_INFO_APPLICATION("Particle Node '%s' added", node->Attribute("name"));
				}
				std::string name = node->Attribute("name");
				CParticleSystemInstance* particleNode = new CParticleSystemInstance(node, m_Name);
				Add(particleNode->GetName(), particleNode);
			
			}

			lOk = true;
		}
		return lOk;
	}

	std::string CLayer::RandomInstanceID(int length)
	{
		auto randchar = []() -> char
		{
			const char charset[] =
				"0123456789"
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				"abcdefghijklmnopqrstuvwxyz";
			const size_t max_index = (sizeof(charset) - 1);
			return charset[rand() % max_index];
		};
		std::string str(length, 0);
		std::generate_n(str.begin(), length, randchar);
		return str;
	}

	std::string CLayer::AddFromNode(const std::string& aInstantiable, const std::string& aName)
	{
		std::string finalName = "";

		CXMLDocument document;
		CXMLElement* node = nullptr;
		tinyxml2::XMLError error = document.LoadFile("data/Instantiables.xml");

		if (base::xml::SucceedLoad(error))
		{
			CXMLElement *nodes = document.FirstChildElement("instantiables");
			if (nodes)
			{
				CXMLElement *nodeParent = nodes->FirstChildElement(aInstantiable.c_str());
				if (nodeParent != nullptr)
					node = nodeParent->FirstChildElement();
			}
		}
		if (node == nullptr)
			return "";

		if (aName != "Salt" && aName != "Mercury" && aName != "Sulphur")
		{
			finalName = aName + "-" + RandomInstanceID(16);
		}
		else{
			finalName = aName;
		}
		node->SetAttribute<std::string>("name", finalName);
		if (strcmp(node->Name(), "scene_mesh") == 0)
		{
			CMeshManager& l_MeshManager = CEngine::GetInstance().GetMeshManager();
			std::string name = node->Attribute("name");
			std::string meshName = node->Attribute("mesh");
			meshName = meshName + EXTENSION_MESH;
			CMesh *mesh = l_MeshManager.GetMesh(name, meshName);
			if (mesh != nullptr)
			{
				CSceneMesh *nodeObject = new CSceneMesh(node, mesh, m_Name);
				Add(nodeObject->GetName(), nodeObject);
			}
			else {
				CSceneNode *nodeObject = new CSceneNode(node,m_Name);
				Add(nodeObject->GetName(), nodeObject);
			}
		}
		else if (strcmp(node->Name(), "animated_model") == 0)
		{
			std::string name = node->Attribute("name");
			if (Exist(name)) {
				LOG_INFO_APPLICATION("Node '%s' updated", name);
			}
			else {
				//					LOG_INFO_APPLICATION("Node '%s' added", name);
			}

			std::string coreName = node->Attribute("core");
			CSceneAnimatedModel *sceneAnimatedModel = new CSceneAnimatedModel(*node, m_Name);
			CAnimatedCoreModel *animatedCoreModel = engine::CEngine::GetInstance().GetAnimatedModelManager().Get(coreName);
			if (animatedCoreModel != nullptr)
			{
				sceneAnimatedModel->Initialize(animatedCoreModel);
				Add(sceneAnimatedModel->GetName(), sceneAnimatedModel);
			}
			else
			{
				base::utils::CheckedDelete(sceneAnimatedModel);
			}
				
		}
		else if (strcmp(node->Name(), "scene_basic_primitive") == 0)
		{
			if (Exist(node->Attribute("name"))) {
				LOG_INFO_APPLICATION("Node '%s' updated", node->Attribute("name"));
			}
			else {
				LOG_INFO_APPLICATION("Node '%s' added", node->Attribute("name"));
			}
			std::string name = node->Attribute("name");
			CSceneMesh *nodeObject = new CSceneBasicPrimitive(node, m_Name);// Deber�a ser un meshNode?

			Add(nodeObject->GetName(), nodeObject);
		}
		else if (strcmp(node->Name(), "scene_light") == 0)
		{
			if (Exist(node->Attribute("name"))) {
				LOG_INFO_APPLICATION("Light Node '%s' updated", node->Attribute("name"));
			}
			else {
				LOG_INFO_APPLICATION("Light Node '%s' added", node->Attribute("name"));
			}

			CSceneNode *nodeObject = engine::CEngine::GetInstance().GetLightManager().Get(node->Attribute("name"));
			Add(nodeObject->GetName(), nodeObject);
		}
		else if (strcmp(node->Name(), "particle_system") == 0)
		{
			CParticleManager& l_ParticleManager = CEngine::GetInstance().GetParticleManager();
			if (Exist(node->Attribute("name"))) {
				LOG_INFO_APPLICATION("Particle Node '%s' updated", node->Attribute("name"));
			}
			else {
				LOG_INFO_APPLICATION("Particle Node '%s' added", node->Attribute("name"));
			}
			std::string name = node->Attribute("name");
			CParticleSystemInstance* particleNode = new CParticleSystemInstance(node, m_Name);
			Add(particleNode->GetName(), particleNode);
		}
		return finalName;
	}

	bool CLayer::Update(float elapsedTime)
	{
		bool lOk = false;
		int count = GetCount();
		for (unsigned int i = 0; i < count; ++i)
		{
			if (GetIndex(i)->IsActive())
			{
				GetIndex(i)->Update(elapsedTime);
				lOk = true;
			}
		}
		return lOk;
	}

	bool CLayer::Render(engine::CRenderManager& aRendermanager)
	{
		bool lOk = false;
		int count = GetCount();
		for (unsigned int i = 0; i < count; ++i)
		{
			if (GetIndex(i)->IsVisible())
			{
				GetIndex(i)->Render(aRendermanager);
				lOk = true;
			}
		}
		return lOk;
	}

	void CLayer::DebugModeLayer(bool DebugMode)
	{
		for (unsigned int i = 0; i < GetCount(); ++i)
		{
			GetIndex(i)->SetVisible(DebugMode);
		}
	}

	void CLayer::RenderDebugGUI()
	{
		if (ImGui::CollapsingHeader(GetName().c_str()))
		{
			for (int i = 0; i < GetCount(); ++i)
			{
				ImGui::Indent();
				GetIndex(i)->RenderDebugGUIBase();
				ImGui::Unindent();
			}
		}
	}

	void CLayer::SetScriptTransforms()
	{
		for (unsigned int i = 0; i < GetCount(); ++i)
		{
			GetIndex(i)->SetScriptTransform();
		}
	}
}