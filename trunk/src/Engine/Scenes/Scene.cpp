#include "Scene.h"

#include "Utils\Logger\Logger.h"
#include "XML\XML.h"
#include "Debug\imgui.h"
#include "Scripting\ScriptManager.h"

namespace engine
{
	CScene::CScene(const std::string& aName)
	{
		SetName(aName);
		m_Active = false;
		m_Pause = false;
		m_Debug = false;
	}

	CScene::~CScene()
	{
		Destroy();
	}

	bool CScene::Load(const std::string& aFilename, const std::string& aFolder)
	{
		LOG_INFO_APPLICATION("Loading scene from '%s'", aFilename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile(aFilename.c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading scene");
			tinyxml2::XMLElement *scene = document.FirstChildElement("scene"); //Scene
			if (scene) {
				for (tinyxml2::XMLElement *layer = scene->FirstChildElement("layer"); layer != nullptr; layer = layer->NextSiblingElement("layer")) { // Layer
					if (Exist(layer->Attribute("name"))) {
						LOG_INFO_APPLICATION("Layer '%s' updated", layer->Attribute("name"));
					}
					else {
						LOG_INFO_APPLICATION("Layer '%s' added", layer->Attribute("name"));
					}
					CLayer *layerObject = new CLayer(layer->Attribute("name"));
					Add(layerObject->GetName(), layerObject);

					layerObject->Load(layer, aFolder);

				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Scene XML '%s' does not exist", aFilename.c_str());
		}

		GLOBAL_LUA_FUNCTION("prim3_get_players", void);
		GLOBAL_LUA_FUNCTION("prim3_set_lookat", void);

		return base::xml::SucceedLoad(error);
	}

	bool CScene::Update(float elapsedTime)
	{
		bool lOk = false;
		for (unsigned int i = 0; i < GetCount(); ++i)
		{
			bool tempOk = false;
			tempOk = GetIndex(i)->Update(elapsedTime);
			if (tempOk == true) lOk = true;
		}
		return lOk;
	}

	bool CScene::Render(engine::CRenderManager& aRendermanager)
	{
		bool lOk = false;
		for (unsigned int i = 0; i < GetCount(); ++i)
		{
			bool tempOk = false;
			tempOk = GetIndex(i)->Render(aRendermanager);
			if (tempOk == true) lOk = true;
		}
		return lOk;
	}

	bool CScene::Render(engine::CRenderManager& aRendermanager, const std::string& aLayerName)
	{
		bool lOk = false;
		if (Exist(aLayerName))
		{
			lOk = Get(aLayerName)->Render(aRendermanager);
		}
		return lOk;
	}

	void CScene::DebugModeScene()
	{
		if (Exist("Light"))
		{
			Get("Light")->DebugModeLayer(m_Debug);
		}
	}

	void CScene::RenderDebugGUI()
	{
		ImGui::PushID(m_Name.c_str());
		if (ImGui::CollapsingHeader(GetName().c_str()))
		{
			if (ImGui::Button("Pause/Resume")) {
				m_Pause = !m_Pause;
			}

			ImGui::Checkbox("Active", &m_Active);

			for (int i = 0; i < GetCount(); ++i)
			{
				ImGui::Indent();
				GetIndex(i)->RenderDebugGUI();
				ImGui::Unindent();
			}
		}
		ImGui::PopID();
	}

	void CScene::SetScriptTransforms()
	{
		for (unsigned int i = 0; i < GetCount(); ++i)
		{
			GetIndex(i)->SetScriptTransforms();
		}
	}
}