#include "SceneNode.h"

#include "Debug\imgui.h"
#include "Scripting\ScriptableObject.h"
#include "Scripting\ScriptManager.h"

#include "Physx\RigidBodyNode.h"
#include "Physx\RigidStaticNode.h"
#include "Physx\CharacterControllerNode.h"
#include "Physx\Trigger.h"

#include "Physx/PhysXManager.h"
#include "Utils\Logger\Logger.h"

#include "Mesh\Mesh.h"
#include "Mesh\MeshManager.h"
#include "Paths.h"
#include "SceneMesh.h"
#include "SceneAnimatedModel.h"
#include "Engine.h"
#include "AnimatedModel\AnimatedModelManager.h"
#include "SceneBasicPrimitive.h"
#include "Lights\LightManager.h"
#include "Particles\ParticleSystemInstance.h"
#include "SceneManager.h"


namespace engine {
	CSceneNode::CSceneNode(const CXMLElement* aElement, std::string LayerName) : CTransform(aElement->FirstChildElement("transform")), CName(aElement)
	{
		m_Children = std::vector<CSceneNode*>();
		m_Parent = nullptr;
		m_Visible = aElement->GetAttribute<bool>("visible", true);
		m_Active = aElement->GetAttribute<bool>("active", true);
		m_TestFrustum = aElement->GetAttribute<bool>("test_frustum", true);
		m_Components = std::vector<std::string>();
		m_LayerName = LayerName;
		std::string tags = aElement->GetAttribute<std::string>("tags", "");
		m_Tags = base::utils::Split(tags, '|');
		m_movement = Vect3f(0.0f, 0.0f, 0.0f);
		m_rotation = Vect3f(0.0f, 0.0f, 0.0f);

		InitChildren(aElement);
		//InitLua();
		LoadScriptsFromNode(aElement);
		LoadPhysicsFromNode(aElement);

		
	}

	CSceneNode::CSceneNode() : CTransform() {
		m_Children = std::vector<CSceneNode*>();
		m_Parent = nullptr;
		m_Visible = true;
		m_Active = true;
		m_TestFrustum = true;
		m_Components = std::vector<std::string>();
		m_Tags = std::vector<std::string>();
	}

	void CSceneNode::InitChildren(const CXMLElement* aElement)
	{
		CMeshManager& l_MeshManager = CEngine::GetInstance().GetMeshManager();

		for (const CXMLElement *node = aElement->FirstChildElement("scene_mesh"); node != nullptr; node = node->NextSiblingElement("scene_mesh")) {
			std::string name = node->Attribute("name");
			std::string meshName = node->Attribute("mesh");
			meshName = meshName + EXTENSION_MESH;
			CMesh *mesh = l_MeshManager.GetMesh(name, meshName);
			if (mesh != nullptr)
			{
				CSceneMesh *nodeObject = new CSceneMesh(node, mesh, m_LayerName);
				AddChildren(nodeObject);
			}
			else {
				CSceneNode *nodeObject = new CSceneNode(node, m_LayerName);
				AddChildren(nodeObject);
			}
		}

		for (const CXMLElement *node = aElement->FirstChildElement("animated_model"); node != nullptr; node = node->NextSiblingElement("animated_model")) { // node
			std::string name = node->Attribute("name");
			std::string coreName = node->Attribute("core");
			CSceneAnimatedModel *sceneAnimatedModel = new CSceneAnimatedModel(*node, m_LayerName);
			CAnimatedCoreModel *animatedCoreModel = engine::CEngine::GetInstance().GetAnimatedModelManager().Get(coreName);
			if (animatedCoreModel != nullptr)
			{
				sceneAnimatedModel->Initialize(animatedCoreModel);
				AddChildren(sceneAnimatedModel);
			}
			else
				base::utils::CheckedDelete(sceneAnimatedModel);
		}

		for (const CXMLElement *node = aElement->FirstChildElement("scene_basic_primitive"); node != nullptr; node = node->NextSiblingElement("scene_basic_primitive"))
		{
			std::string name = node->Attribute("name");
			CSceneMesh *nodeObject = new CSceneBasicPrimitive(node, m_LayerName);// Deber�a ser un meshNode?

			AddChildren(nodeObject);
		}

		for (const CXMLElement *lightNode = aElement->FirstChildElement("scene_light"); lightNode != nullptr; lightNode = lightNode->NextSiblingElement("scene_light")) { // lightNode
			CSceneNode *nodeObject = engine::CEngine::GetInstance().GetLightManager().Get(lightNode->Attribute("name"));
			AddChildren(nodeObject);
		}

		for (const CXMLElement *node = aElement->FirstChildElement("particle_system"); node != nullptr; node = node->NextSiblingElement("particle_system")) { // particle node
			std::string name = node->Attribute("name");
			CParticleSystemInstance* particleNode = new CParticleSystemInstance(node, m_LayerName);
			AddChildren(particleNode);

		}
	}

	
	void CSceneNode::Update(float aDeltaTime) {
		ManageMovement(aDeltaTime);
		ManageRotation(aDeltaTime);
	}
	void CSceneNode::ManageMovement(float aDeltaTime) {
		if (m_PhysicNode != nullptr)
		{
			m_PhysicNode->Move(m_movement, aDeltaTime);
			m_PhysicNode->GetTransform((CTransform*)this);
		}
		else {
			//LOG_INFO_APPLICATION(std::to_string(m_movement.y).c_str());
			m_Position += m_movement;
		}
		//LOG_INFO_APPLICATION(std::to_string(m_movement.y).c_str());
		m_movement = Vect3f(0.0f, 0.0f, 0.0f);
	}

	void CSceneNode::ManageRotation(float aDeltaTime) {


		m_Yaw = fmodf(m_Yaw + m_rotation.y, 360.0f);
		m_Pitch = fmodf(m_Pitch + m_rotation.x, 360.0f);
		m_Roll = fmodf(m_Roll + m_rotation.z, 360.0f);

		m_rotation = Vect3f(0.0f, 0.0f, 0.0f);
	}


	void CSceneNode::Move(Vect3f aMovement) {
		if (m_Active)
		{
			m_movement += aMovement;
		}
		else{
			m_movement = Vect3f(0.0f, 0.0f, 0.0f);
		}
	}

	void CSceneNode::Rotate(Vect3f aRotation) {
		m_rotation += aRotation;
	}

	void CSceneNode::SetPosition(Vect3f aPosition) {
		m_Position = aPosition;
		if (m_PhysicNode != nullptr) {
			m_PhysicNode->SetPosition(aPosition);
		}
	}

	void CSceneNode::Start()
	{
		GLOBAL_LUA_FUNCTION("start", void, m_Name);
	}

	CSceneNode::~CSceneNode()
	{
		m_Scripts.Destroy();
		m_Components.clear();
		/*for (int i = 0; i < m_Children.size(); ++i)
		{
			if (m_Children[i])
			{
				delete m_Children[i];
			}
		}*/
		m_Children.clear();
		delete m_PhysicNode;
	}

	void CSceneNode::InitLua()
	{
		// Init of the lua state and the bindings
		m_State = luaL_newstate();
		luaL_openlibs(m_State);
		luabind::open(m_State);
		lua_atpanic(m_State, lua::atpanic);
		lua_register(m_State, "_ALERT", lua::atpanic);

		lua::OpenLibraries(m_State);
		engine::BindAllLibraries(m_State);
		engine::BindAllClasses(m_State);
	}

	bool CSceneNode::LoadScriptsFromNode(const CXMLElement* aElement)
	{
		GLOBAL_LUA_FUNCTION("register_object", void, m_Name);
		GLOBAL_LUA_FUNCTION("set_transform", void, m_Name, (CTransform*)this);

		for (int i = 0; i < m_Tags.size(); ++i)
		{
			GLOBAL_LUA_FUNCTION("add_tag", void, m_Name, m_Tags[i]);
		}

		for (const CXMLElement *script = aElement->FirstChildElement("script_object"); script != nullptr; script = script->NextSiblingElement("script_object")) {
			std::string l_ComponentName = script->GetAttribute<std::string>("name", "");
			GLOBAL_LUA_FUNCTION("register_component", void, m_Name, l_ComponentName);
			m_Components.push_back(l_ComponentName);
			for (const CXMLElement *parameter = script->FirstChildElement("parameter"); parameter != nullptr; parameter = parameter->NextSiblingElement("parameter")) {
				std::string l_ParameterName = parameter->GetAttribute<std::string>("name", "");
				std::string l_ParameterType = parameter->GetAttribute<std::string>("type", "");
				if (l_ParameterType == "string") {
					std::string l_Value = parameter->GetAttribute<std::string>("value", "");
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, l_Value);
				}
				else if (l_ParameterType == "int") {
					int l_Value = parameter->GetAttribute<int>("value", 0);
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, l_Value);
				}
				else if (l_ParameterType == "float") {
					float l_Value = parameter->GetAttribute<float>("value", 0.0f);
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, l_Value);
				}
				else if (l_ParameterType == "float4") {
					Vect4f l_Value = parameter->GetAttribute<Vect4f>("value", Vect4f());
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, Vect4f(l_Value));
				}
				else if (l_ParameterType == "float3") {
					Vect3f l_Value = parameter->GetAttribute<Vect3f>("value", Vect3f());
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, Vect3f(l_Value));
				}
				else if (l_ParameterType == "float2") {
					Vect2f l_Value = parameter->GetAttribute<Vect2f>("value", Vect2f());
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, Vect2f(l_Value));
				}
				else if (l_ParameterType == "color") {
					CColor l_Value = parameter->GetAttribute<CColor>("value", CColor());
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, CColor(l_Value));
				}
				else if (l_ParameterType == "bool") {
					bool l_Value = parameter->GetAttribute<bool>("value", false);
					GLOBAL_LUA_FUNCTION("set_parameter", void, m_Name, l_ComponentName, l_ParameterName, l_Value);
				}
			}
		}
		return true;
	}

	void CSceneNode::SetScriptTransform()
	{
		GLOBAL_LUA_FUNCTION("register_object", void, m_Name);
		GLOBAL_LUA_FUNCTION("set_transform", void, m_Name, (CTransform*)this);
		for (int i = 0; i < m_Components.size(); ++i) 
		{
			std::string l_ComponentName = m_Components[i];
			GLOBAL_LUA_FUNCTION("register_component", void, m_Name, l_ComponentName);
		}
		GLOBAL_LUA_FUNCTION("start", void, m_Name);
	}

	bool CSceneNode::LoadPhysicsFromNode(const CXMLElement* aElement)
	{
		bool lOk = true;
		m_PhysicNode = nullptr;
		for (const CXMLElement *physicnode = aElement->FirstChildElement("physic_node"); physicnode != nullptr; physicnode = physicnode->NextSiblingElement("physic_node")) {
			std::string l_Type = physicnode->GetAttribute<std::string>("type", "");
			if (l_Type == "rigidbody")
			{
				m_PhysicNode = new CRigidBodyNode(physicnode, (CTransform*)this, m_Name);
			}
			else if (l_Type == "rigidstatic")
			{
				m_PhysicNode = new CRigidStaticNode(physicnode, (CTransform*)this, m_Name);
			}
			else if (l_Type == "charactercontroller")
			{
				m_PhysicNode = new CCharacterControllerNode(physicnode, (CTransform*)this, m_Name);
			}
			else if (l_Type == "trigger")
			{
				m_PhysicNode = new CTrigger(physicnode, (CTransform*)this, m_Name);
			}
		}
		return lOk;
	}

	Mat44f CSceneNode::GetWorldMatrix()
	{
		if (m_Parent == nullptr)
			return GetMatrix();
		else
			return GetMatrix() * m_Parent->GetWorldMatrix();
	}

	void CSceneNode::DetachFromParent()
	{
		if (m_Parent != nullptr)
		{
			m_Parent->DetachChildren(this);
		}
	}

	void CSceneNode::SetParent(CSceneNode *aParentNode)
	{
		aParentNode->AddChildren(this);
	}

	void CSceneNode::AddChildren(CSceneNode *aChildNode)
	{
		m_Children.push_back(aChildNode);
		aChildNode->m_Parent = this;

		engine::CSceneManager &l_SceneManager = engine::CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SceneManager.GetCount(); ++i)
		{
			engine::CScene *l_Scene = l_SceneManager.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					engine::CLayer *l_Layer = l_Scene->GetIndex(j);
					if (l_Layer->GetName() == m_LayerName)
					{
						l_Layer->Add(aChildNode->m_Name, aChildNode);
					}
				}
			}

		}
	}

	void CSceneNode::DetachChildren(CSceneNode *aChildNode)
	{
		if (m_Children.size() == 0) return;
		auto it = std::find(m_Children.begin(), m_Children.end(), aChildNode);
		if (it != m_Children.end())
		{
			aChildNode->m_Parent = nullptr;
			m_Children.erase(it);
		}
	}

	bool CSceneNode::Render(engine::CRenderManager& aRendermanager)
	{
		for (int i = 0; i < m_Children.size(); ++i)
		{
			m_Children[i]->Render(aRendermanager);
		}
		return false;
	}

	void CSceneNode::RenderDebugGUI() {
		CTransform::RenderDebugGUI();
	}

	void CSceneNode::RenderDebugGUIBase()
	{
		if (ImGui::CollapsingHeader(GetName().c_str()))
		{
			ImGui::Indent();
			ImGui::PushID(m_Name.c_str());
			RenderDebugGUI();
			ImGui::Checkbox("Active", &m_Active);
			ImGui::Checkbox("Visible", &m_Visible);
			ImGui::Text("Tags: ");
			for (unsigned int i = 0; i < m_Tags.size(); ++i)
			{
				ImGui::SameLine();
				ImGui::Text((m_Tags[i] + ", ").c_str());
			}
			for (unsigned int i = 0; i < m_Scripts.GetCount(); ++i)
			{
				m_Scripts[i]->RenderDebugGUI();
			}
			for (int i = 0; i < m_Children.size(); ++i)
			{
				m_Children[i]->RenderDebugGUIBase();
			}
			ImGui::PopID();
			ImGui::Unindent();
		}
	}

	void CSceneNode::ResetDebugGUI() {
		m_Visible = true;
		m_Active = true;
		CTransform::ResetDebugGUI();
	}

}
