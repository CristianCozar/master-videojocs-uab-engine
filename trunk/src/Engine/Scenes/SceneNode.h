#pragma once

#include "Utils\Name.h"
#include "Math\Transform.h"

#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"

#include "Engine.h"

#include "Utils\TemplatedMapVector.h"
#include "Utils\lua_utils.h"

class engine::CRenderManager;


namespace engine {
	class CScriptableObject;
	class CPhysicNode;
	class CSceneNode : public CTransform, public CName
	{
	public:
		CSceneNode();
		CSceneNode(const CXMLElement* aElement, std::string LayerName);
		virtual ~CSceneNode();
		virtual void Update(float aDeltaTime);
		virtual bool Render(engine::CRenderManager& aRendermanager);
		void RenderDebugGUIBase();
		virtual void RenderDebugGUI();
		GET_SET_BOOL(Visible);
		GET_SET_BOOL(Active);
		GET_SET_BOOL(TestFrustum);
		void ResetDebugGUI();
		void SetScriptTransform();

		Mat44f GetWorldMatrix();
		void DetachFromParent();
		void SetParent(CSceneNode *aParentNode);
		void AddChildren(CSceneNode *aChildNode);
		void DetachChildren(CSceneNode *aChildNode);
		CSceneNode* m_Parent;
		void InitChildren(const CXMLElement* aElement);
		void Start();

		void Move(Vect3f aMovement);
		void Rotate(Vect3f aRotation);
		void SetPosition(Vect3f aMovement);
	protected:
		typedef base::utils::CTemplatedMapVector<CScriptableObject> ScriptableObjectsMap;
		std::vector<std::string> m_Components;
		ScriptableObjectsMap m_Scripts;
		bool LoadScriptsFromNode(const CXMLElement* aElement);
		bool LoadPhysicsFromNode(const CXMLElement* aElement);
		CPhysicNode *m_PhysicNode;
		void InitLua();
		lua_State *m_State;
		bool m_Visible;
		bool m_Active;
		bool m_TestFrustum;

		std::string m_LayerName;

		std::vector<CSceneNode*> m_Children;
		std::vector<std::string> m_Tags;

		void ManageMovement(float aDeltaTime);
		void ManageRotation(float aDeltaTime);
		Vect3f m_movement;
		Vect3f m_rotation;
	};
}