#include "SceneNode.h"
#include "Materials\Material.h"
#include "Mesh\Geometry.h"
#include "AnimatedModel\AnimatedCoreModel.h"
#include "cal3d\cal3d.h"

namespace engine
{

	struct CalAction {
		int Id;
		float DelayIn;
		float DelayOut;
		float WeightTarget;

	public:
		CalAction(int aId, float aDelayIn, float aDelayOut, float aWeightTarget)
		{
			Id = aId;
			DelayIn = aDelayIn;
			DelayOut = aDelayOut;
			WeightTarget = aWeightTarget;
		}
	};

	class CSceneAnimatedModel : public CSceneNode
	{
	public:
		CSceneAnimatedModel(const CXMLElement &TreeNode, std::string LayerName);
		virtual ~CSceneAnimatedModel();
		bool Initialize(CAnimatedCoreModel *AnimatedCoreModel);
		bool Render(CRenderManager &RenderManager);
		void Update(float ElapsedTime);
		void Destroy();
		void ExecuteAction(int Id, float DelayIn, float DelayOut, float WeightTarget = 1.0f, bool AutoLock = true);
		void QueueAction(int Id, float DelayIn, float DelayOut, float WeightTarget = 1.0f);
		void ClearQueue();
		void TryDequeueAction();
		void ForceAction(int Id, float DelayIn, float DelayOut, float WeightTarget = 1.0f);
		void BlendCycle(int Id, float Weight, float DelayIn);
		void ClearCycle(int Id, float DelayOut);
		bool IsCycleAnimationActive(int Id) const;
		bool IsActionAnimationActive(int Id) const;
		bool IsAnyActionAnimationActive() const;
		int GetAnimationId(const std::string& aName);
		void CalcTangentsAndBinormals(void *VtxsData, unsigned short *IdxsData, size_t VtxCount, size_t IdxCount, size_t VertexStride, size_t GeometryStride, size_t NormalStride, size_t TangentStride, size_t BiNormalStride, size_t TextureCoordsStride);
	private:
		CalModel *m_CalModel;
		CAnimatedCoreModel *m_AnimatedCoreModel;
		CalHardwareModel *m_CalHardwareModel;
		std::vector<CMaterial *> m_Materials;
		std::vector<CalAction> m_ActionsQueue;
		int m_LastAction;
		std::vector<int> m_PlayingCycles;
		CGeometry *m_Geometry;
		int m_NumVertices;
		int m_NumFaces;
		bool m_Bumped;
		bool LoadVertexBuffer();
		void LoadMaterials();
	};
}