#include "SceneManager.h"

#include "Utils\Logger\Logger.h"
#include "Paths.h"
#include "XML\XML.h"
#include "Debug\imgui.h"
#include "Materials\MaterialManager.h"
#include "Lights\LightManager.h"


#include "RenderPipeline\RenderPipeline.h"
#include "RenderPipeline\RenderCmd.h"

namespace engine {
	CSceneManager::CSceneManager()
	{
		m_Filename = "";
	}

	CSceneManager::~CSceneManager()
	{
		Destroy();
	}

	bool CSceneManager::Load(const std::string &Filename)
	{
		m_Filename = Filename;
		std::string l_Path = PATH_BASE + Filename;

		LOG_INFO_APPLICATION("Loading scenes from '%s'", l_Path.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile(l_Path.c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading scenes");
			tinyxml2::XMLElement *scenes = document.FirstChildElement("scenes"); //Scenes
			if (scenes) {
				for (tinyxml2::XMLElement *scene = scenes->FirstChildElement("scene"); scene != nullptr; scene = scene->NextSiblingElement("scene")) { //Scene
					if (Exist(scene->Attribute("name"))) {
						LOG_INFO_APPLICATION("Scene '%s' updated", scene->Attribute("name"));
					}
					else {
						LOG_INFO_APPLICATION("Scene '%s' added", scene->Attribute("name"));
					}
					CScene *sceneObject = new CScene(scene->Attribute("name"));
					sceneObject->SetActive(scene->BoolAttribute("active"));
					std::string l_NewSceneFolder = std::string(scene->Attribute("folder"));
					std::string l_SceneFolderPath = PATH_BASE + l_NewSceneFolder;
					std::string l_NewScenePath = PATH_BASE + l_NewSceneFolder + FILENAME_SCENE;
					
					LOG_INFO_APPLICATION("Scene materials added");
					std::string l_materilsFile = PATH_BASE + l_NewSceneFolder + FILENAME_MATERIALS;
					engine::CMaterialManager& lmM = engine::CEngine::GetInstance().GetMaterialManager();
					lmM.Load(l_materilsFile);

					LOG_INFO_APPLICATION("Loading scene lights");
					std::string l_LightsFile = PATH_BASE + l_NewSceneFolder + FILENAME_LIGHTS;
					engine::CLightManager& llM = engine::CEngine::GetInstance().GetLightManager();
					//llM.ReLoad(l_LightsFile);
					llM.Load();
					llM.Load(l_LightsFile);

					Add(sceneObject->GetName(), sceneObject);
					sceneObject->Load(l_NewScenePath, l_SceneFolderPath);
	
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Scenes XML '%s' does not exist", Filename.c_str());
		}

		return base::xml::SucceedLoad(error);
	}

	bool CSceneManager::Update(float elapsedTime)
	{
		bool lOk = false;
		size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			CScene *l_Scene = GetIndex(i);
			if (l_Scene->IsActive() && !l_Scene->IsPause())
			{
				l_Scene->Update(elapsedTime);
				lOk = true;
			}
		}
		return lOk;
	}

	bool CSceneManager::Render(engine::CRenderManager& aRendermanager, const std::string& aLayer)
	{
		bool lOk = false;
		size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			CScene *l_Scene = GetIndex(i);
			if (l_Scene->IsActive() && l_Scene->Exist(aLayer))
			{
				lOk = l_Scene->Render(aRendermanager, aLayer);
			}
		}
		return lOk;
	}

	bool CSceneManager::Reload()
	{
		Destroy();
		return Load(m_Filename);
	}

	void CSceneManager::Activate(const std::string& aScene, bool aBool)
	{
		if (Exist(aScene))
			Get(aScene)->SetActive(aBool);
	}

	void CSceneManager::PauseScenes()
	{
		size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			CScene *l_Scene = GetIndex(i);
			l_Scene->SetPause(!l_Scene->IsPause());
		}
	}

	void CSceneManager::DebugMode()
	{
		/*size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			CScene *l_Scene = GetIndex(i);
			l_Scene->SetDebug(!l_Scene->IsDebug());
			l_Scene->DebugModeScene();
		}*/
		CEngine::GetInstance().GetRenderPipeline().Activate("render_light", !(CEngine::GetInstance().GetRenderPipeline().Get("render_light")->IsActive()));
	}

	void CSceneManager::RenderDebugGUI()
	{
		for (int i = 0; i < GetCount(); ++i)
		{
			ImGui::Indent();
			GetIndex(i)->RenderDebugGUI();
			/*if (ImGui::Button("Reload"))
			{
				Reload();
			}*/
			ImGui::Unindent();
		}
	}

	void CSceneManager::SetScriptTransforms()
	{
		size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			GetIndex(i)->SetScriptTransforms();
		}
	}
}