#ifndef _H_SCENE_Primitive__
#define _H_SCENE_Primitive__
#pragma once

#include "SceneMesh.h"

XML_FORWARD_DECLARATION


namespace engine {
	class CSceneBasicPrimitive : public CSceneMesh
	{
	public:
		CSceneBasicPrimitive(const CXMLElement* aElement, std::string LayerName);
		virtual ~CSceneBasicPrimitive();
	};
}

#endif
