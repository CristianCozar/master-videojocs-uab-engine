#pragma once

#include "SceneNode.h"
#include "Mesh\Mesh.h"

namespace engine {
	class CSceneMesh : public CSceneNode
	{
	public:
		CSceneMesh(const CXMLElement* aElement, std::string LayerName);
		CSceneMesh(const CXMLElement* aElement, CMesh* aMesh, std::string LayerName);
		virtual ~CSceneMesh();
		virtual bool Render(engine::CRenderManager& aRendermanager);
	protected:
		CMesh* mMesh;
	};
}