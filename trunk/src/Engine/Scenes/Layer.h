#pragma once

#include "Utils\Name.h"
#include "Utils\TemplatedMapVector.h"
#include "SceneNode.h"
#include "SceneMesh.h"

namespace engine {
	class CLayer : public CName, public base::utils::CTemplatedMapVector<CSceneNode>
	{
	public:
		CLayer(const std::string& aName);
		virtual ~CLayer();
		bool Load(CXMLElement* aElement, const std::string& aFolder);
		bool Update(float elapsedTime);
		bool Render(engine::CRenderManager& aRendermanager);
		void RenderDebugGUI();
		void DebugModeLayer(bool DebugMode);
		void SetScriptTransforms();
		std::string AddFromNode(const std::string& aInstantiable, const std::string& aName);
		std::string RandomInstanceID(int len);
	private:
		DISALLOW_COPY_AND_ASSIGN(CLayer);
	};
}