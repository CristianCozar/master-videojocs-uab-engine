#include "SceneBasicPrimitive.h"
#include "XML/XML.h"
#include "Engine.h"
#include "RenderManager.h"
#include "ConstantBufferManager.h"
#include "Mesh\Plane.h"


namespace engine {
	CSceneBasicPrimitive::CSceneBasicPrimitive(const CXMLElement* aElement, std::string LayerName)
		: CSceneMesh(aElement, LayerName)
	{
		if (aElement->GetAttribute<std::string>("type", "") == "plane")
			mMesh = new CPlane(aElement);
	}

	CSceneBasicPrimitive::~CSceneBasicPrimitive()
	{
		base::utils::CheckedDelete(mMesh);
	}

}