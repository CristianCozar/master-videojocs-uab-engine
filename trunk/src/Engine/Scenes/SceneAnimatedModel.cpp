#include "SceneAnimatedModel.h"
#include "Mesh\GeometryMacros.h"
#include "Mesh\TemplatedGeometry.h"
#include "Mesh\TemplatedIndexedGeometry.h"
#include "Mesh\CVertexBuffer.h"
#include "Mesh\CIndexBuffer.h"
#include "LayoutUtility.h"
#include "ConstantBufferManager.h"
#include "Math\Quaternion.h"
#include "cal3d\cal3d.h"
#include "Utils\CheckedDelete.h"

namespace engine
{

	CSceneAnimatedModel::CSceneAnimatedModel(const CXMLElement &TreeNode, std::string LayerName) : CSceneNode(&TreeNode, LayerName)
	{
		m_CalModel = nullptr;
		m_AnimatedCoreModel = nullptr;
		m_CalHardwareModel = nullptr;
		m_Geometry = nullptr;
		m_NumVertices = 0;
		m_NumFaces = 0;
		m_Bumped = TreeNode.GetAttribute<bool>("bumped", false);
		m_ActionsQueue = std::vector<CalAction>();
		m_LastAction = -1;
		m_PlayingCycles = std::vector<int>();
	}

	CSceneAnimatedModel::~CSceneAnimatedModel()
	{
		Destroy();
	}

	bool CSceneAnimatedModel::Initialize(CAnimatedCoreModel *AnimatedCoreModel)
	{
		m_AnimatedCoreModel = AnimatedCoreModel;
		CalCoreModel *l_CalCoreModel = m_AnimatedCoreModel->GetCoreModel();

		m_CalModel = new CalModel(l_CalCoreModel);
					
		for (int i = 0; i<l_CalCoreModel->getCoreMeshCount(); ++i)
		{
			CalCoreMesh *l_CalCoreMesh = l_CalCoreModel->getCoreMesh(i);
			m_CalModel->attachMesh(i);
		}

		LoadVertexBuffer();
		LoadMaterials();
		
		BlendCycle(0, 1.0f, 0.0f);
		Update(0.0f);
		return true;
	}

	bool CSceneAnimatedModel::Render(CRenderManager &RenderManager)
	{
		if (m_CalHardwareModel == nullptr)
			return false;

		CConstantBufferManager &l_CB = CEngine::GetInstance().GetConstantBufferManager();
		l_CB.mObjDesc.m_World = GetMatrix();
		l_CB.BindVSBuffer(RenderManager.GetDeviceContext(),
			CConstantBufferManager::CB_Object);

		ID3D11DeviceContext* l_DeviceContext = RenderManager.GetDeviceContext();
		int count = m_CalHardwareModel->getHardwareMeshCount();
		for (int l_HardwareMeshId = 0; l_HardwareMeshId < count; ++l_HardwareMeshId)
		{
			CMaterial *l_Material = m_Materials[l_HardwareMeshId];
			if (l_Material != nullptr && l_Material->GetTechnique() != nullptr && l_Material ->GetTechnique()->GetEffect() != nullptr)
			{
				l_Material->Apply();
				m_CalHardwareModel->selectHardwareMesh(l_HardwareMeshId);
				Mat44f l_Transformations[MAXBONES];
				for (int l_BoneId = 0; l_BoneId<m_CalHardwareModel->getBoneCount();
					++l_BoneId)
				{
					Quatf l_Quaternion = (const Quatf &)m_CalHardwareModel ->getRotationBoneSpace(l_BoneId, m_CalModel->getSkeleton());
					l_Transformations[l_BoneId].SetIdentity();
					l_Transformations[l_BoneId].SetRotByQuat(l_Quaternion);

					CalVector translationBoneSpace = m_CalHardwareModel ->getTranslationBoneSpace(l_BoneId, m_CalModel->getSkeleton());
					l_Transformations[l_BoneId].SetPos(Vect3f(translationBoneSpace.x,translationBoneSpace.y, translationBoneSpace.z));
				}

				memcpy(&l_CB.mAnimatedModelDesc, l_Transformations,	MAXBONES*sizeof(float)* 4 * 4);
				l_CB.BindVSBuffer(RenderManager.GetDeviceContext(), CConstantBufferManager::CB_AnimatedModel);

				m_Geometry->RenderIndexed(l_DeviceContext, m_CalHardwareModel ->getFaceCount() * 3, m_CalHardwareModel->getStartIndex(), m_CalHardwareModel ->getBaseVertexIndex());
			}
		}

		return true;
	}

	void CSceneAnimatedModel::Update(float ElapsedTime)
	{
		CSceneNode::Update(ElapsedTime);
		m_CalModel->update(ElapsedTime);
		TryDequeueAction();
	}

	void CSceneAnimatedModel::Destroy()
	{
		delete m_CalModel;
		//delete m_AnimatedCoreModel; <-- Se elimina por otro sitio
		delete m_CalHardwareModel;
		delete m_Geometry;
		m_Materials.clear();
		m_ActionsQueue.clear();
		//base::utils::CheckedDelete(m_Materials);
	}

	void CSceneAnimatedModel::ExecuteAction(int Id, float DelayIn, float DelayOut, float WeightTarget, bool AutoLock)
	{
		m_LastAction = Id;
		m_CalModel->getMixer()->executeAction(Id, DelayIn, DelayOut, WeightTarget);
		m_CalModel->update(0.0f);
	}

	void CSceneAnimatedModel::QueueAction(int Id, float DelayIn, float DelayOut, float WeightTarget)
	{
		if (IsAnyActionAnimationActive()) {
			m_ActionsQueue.push_back(CalAction(Id, DelayIn, DelayOut, WeightTarget));
		}
		else {
			ExecuteAction(Id, DelayIn, DelayOut, WeightTarget);
		}
	}


	void CSceneAnimatedModel::ClearQueue()
	{
		m_ActionsQueue.clear();
	}

	void CSceneAnimatedModel::TryDequeueAction()
	{
		if (!IsAnyActionAnimationActive() && m_ActionsQueue.size() > 0) {
			CalAction l_Action = m_ActionsQueue[0];
			m_ActionsQueue.erase(m_ActionsQueue.begin());
			ExecuteAction(l_Action.Id, l_Action.DelayIn, l_Action.DelayOut, l_Action.WeightTarget);
			m_CalModel->update(0.0f);
		}
	}

	void CSceneAnimatedModel::ForceAction(int Id, float DelayIn, float DelayOut, float WeightTarget)
	{
		m_ActionsQueue.clear();
		ExecuteAction(Id, DelayIn, DelayOut, WeightTarget);
	}

	void CSceneAnimatedModel::BlendCycle(int Id, float Weight, float DelayIn)
	{
		m_PlayingCycles.push_back(Id);
		m_CalModel->getMixer()->blendCycle(Id, Weight, DelayIn);
	}

	void CSceneAnimatedModel::ClearCycle(int Id, float DelayOut)
	{
		m_PlayingCycles.erase(std::remove(m_PlayingCycles.begin(), m_PlayingCycles.end(), Id), m_PlayingCycles.end());
		m_CalModel->getMixer()->clearCycle(Id, DelayOut);
	}

	bool CSceneAnimatedModel::IsCycleAnimationActive(int Id) const
	{
		return std::find(m_PlayingCycles.begin(), m_PlayingCycles.end(), Id) != m_PlayingCycles.end();
	}

	bool CSceneAnimatedModel::IsActionAnimationActive(int Id) const
	{
		/*std::list<CalAnimationAction*> actions = m_CalModel->getMixer()->getAnimationActionList();
		std::vector<CalAnimationAction*> actionVector{ std::begin(actions), std::end(actions) };
		for (CalAnimationAction* action : actionVector)
		{
			action->
		}*/
		return m_CalModel->getMixer()->getAnimationActionList().size() > 0 && m_LastAction == Id;
	}

	bool CSceneAnimatedModel::IsAnyActionAnimationActive() const
	{
		return m_CalModel->getMixer()->getAnimationActionList().size() > 0;
	}

	int CSceneAnimatedModel::GetAnimationId(const std::string& aName)
	{
		return m_AnimatedCoreModel->GetAnimationIndex(aName);
	}

	bool CSceneAnimatedModel::LoadVertexBuffer()
	{
		m_NumVertices = 0;
		m_NumFaces = 0;
		CalCoreModel *l_CalCoreModel = m_AnimatedCoreModel->GetCoreModel();
		for (int i = 0; i<l_CalCoreModel->getCoreMeshCount(); ++i)
		{
			CalCoreMesh *l_CalCoreMesh = l_CalCoreModel->getCoreMesh(i);
			for (int j = 0; j<l_CalCoreMesh->getCoreSubmeshCount(); ++j)
			{
				CalCoreSubmesh *l_CalCoreSubmesh = l_CalCoreMesh->getCoreSubmesh(j);
				m_NumVertices += l_CalCoreSubmesh->getVertexCount();
				m_NumFaces += l_CalCoreSubmesh->getFaceCount();
			}
		}

		if (!m_Bumped) {
		VertexTypes::PositionWeightIndicesNormalUV *l_Vertexs = (VertexTypes::PositionWeightIndicesNormalUV*)malloc(m_NumFaces * 3 * sizeof(VertexTypes::PositionWeightIndicesNormalUV));
		CalIndex *l_Faces = (CalIndex *)malloc(m_NumFaces * 3 * sizeof(CalIndex));
		m_CalHardwareModel = new CalHardwareModel(m_CalModel->getCoreModel());

		m_CalHardwareModel->setVertexBuffer((char*)l_Vertexs, sizeof(VertexTypes::PositionWeightIndicesNormalUV));

			m_CalHardwareModel->setWeightBuffer(((char*)l_Vertexs) + 12,
				sizeof(VertexTypes::PositionWeightIndicesNormalUV));

		m_CalHardwareModel->setMatrixIndexBuffer(((char*)l_Vertexs) + 28,
			sizeof(VertexTypes::PositionWeightIndicesNormalUV));

		m_CalHardwareModel->setNormalBuffer(((char*)l_Vertexs) + 44,
			sizeof(VertexTypes::PositionWeightIndicesNormalUV));

		m_CalHardwareModel->setTextureCoordNum(1);

		m_CalHardwareModel->setTextureCoordBuffer(0, ((char*)l_Vertexs) + 56,
			sizeof(VertexTypes::PositionWeightIndicesNormalUV));

		m_CalHardwareModel->setIndexBuffer(l_Faces);
		m_CalHardwareModel->load(0, 0, MAXBONES);
		m_NumFaces = m_CalHardwareModel->getTotalFaceCount();
		m_NumVertices = m_CalHardwareModel->getTotalVertexCount();

		CRenderManager &l_RenderManager = CEngine::GetInstance().GetRenderManager();
			//CalcTangentsAndBinormals(l_Vertexs, (unsigned short*)l_Faces, m_NumVertices, m_NumFaces * 3, sizeof(VertexTypes::PositionWeightIndicesBumpUV), 0, 44, 56, 72, 88);
			m_Geometry = new CTemplatedIndexedGeometry<VertexTypes::PositionWeightIndicesNormalUV>(
				new CVertexBuffer<VertexTypes::PositionWeightIndicesNormalUV>(l_RenderManager, l_Vertexs, m_NumVertices),
				new CIndexBuffer(l_RenderManager, l_Faces, m_NumFaces * 3, sizeof(CalIndex) * 8),
				D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		free(l_Vertexs);
		free(l_Faces);
		}
		else {
			VertexTypes::PositionWeightIndicesBumpUV *l_Vertexs = (VertexTypes::PositionWeightIndicesBumpUV*)malloc(m_NumFaces * 3 * sizeof(VertexTypes::PositionWeightIndicesBumpUV));
			CalIndex *l_Faces = (CalIndex *)malloc(m_NumFaces * 3 * sizeof(CalIndex));
			m_CalHardwareModel = new CalHardwareModel(m_CalModel->getCoreModel());

			m_CalHardwareModel->setVertexBuffer((char*)l_Vertexs, sizeof(VertexTypes::PositionWeightIndicesBumpUV));

			m_CalHardwareModel->setWeightBuffer(((char*)l_Vertexs) + 12, sizeof(VertexTypes::PositionWeightIndicesBumpUV));

			m_CalHardwareModel->setMatrixIndexBuffer(((char*)l_Vertexs) + 28, sizeof(VertexTypes::PositionWeightIndicesBumpUV));

			m_CalHardwareModel->setNormalBuffer(((char*)l_Vertexs) + 44, sizeof(VertexTypes::PositionWeightIndicesBumpUV));

			m_CalHardwareModel->setTextureCoordNum(1);

			m_CalHardwareModel->setTextureCoordBuffer(0, ((char*)l_Vertexs) + 88, sizeof(VertexTypes::PositionWeightIndicesBumpUV));

			m_CalHardwareModel->setIndexBuffer(l_Faces);
			m_CalHardwareModel->load(0, 0, MAXBONES);
			m_NumFaces = m_CalHardwareModel->getTotalFaceCount();
			m_NumVertices = m_CalHardwareModel->getTotalVertexCount();

			CRenderManager &l_RenderManager = CEngine::GetInstance().GetRenderManager();
			CalcTangentsAndBinormals(l_Vertexs, (unsigned short*)l_Faces, m_NumVertices, m_NumFaces * 3, sizeof(VertexTypes::PositionWeightIndicesBumpUV), 0, 44, 56, 72, 88);
			m_Geometry = new CTemplatedIndexedGeometry<VertexTypes::PositionWeightIndicesBumpUV>(
				new CVertexBuffer<VertexTypes::PositionWeightIndicesBumpUV>(l_RenderManager, l_Vertexs, m_NumVertices),
				new CIndexBuffer(l_RenderManager, l_Faces, m_NumFaces * 3, sizeof(CalIndex) * 8),
				D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			free(l_Vertexs);
			free(l_Faces);
		}

		return true;
	}

	void CSceneAnimatedModel::CalcTangentsAndBinormals(void *VtxsData, unsigned short *IdxsData, size_t VtxCount,
		size_t IdxCount, size_t VertexStride, size_t GeometryStride, size_t NormalStride, size_t
		TangentStride, size_t BiNormalStride, size_t TextureCoordsStride)
	{
		Vect3f *tan1 = new Vect3f[VtxCount * 2];
		Vect3f *tan2 = tan1 + VtxCount;
		ZeroMemory(tan1, VtxCount * sizeof(Vect3f) * 2);
		unsigned char *l_VtxAddress = (unsigned char *)VtxsData;
		for (size_t b = 0; b<IdxCount; b += 3)
		{
			unsigned short i1 = IdxsData[b];
			unsigned short i2 = IdxsData[b + 1];
			unsigned short i3 = IdxsData[b + 2];
			Vect3f *v1 = (Vect3f *)&l_VtxAddress[i1*VertexStride + GeometryStride];
			Vect3f *v2 = (Vect3f *)&l_VtxAddress[i2*VertexStride + GeometryStride];
			Vect3f *v3 = (Vect3f *)&l_VtxAddress[i3*VertexStride + GeometryStride];
			Vect2f *w1 = (Vect2f
				*)&l_VtxAddress[i1*VertexStride + TextureCoordsStride];
			Vect2f *w2 = (Vect2f
				*)&l_VtxAddress[i2*VertexStride + TextureCoordsStride];
			Vect2f *w3 = (Vect2f
				*)&l_VtxAddress[i3*VertexStride + TextureCoordsStride];
			float x1 = v2->x - v1->x;
			float x2 = v3->x - v1->x;
			float y1 = v2->y - v1->y;
			float y2 = v3->y - v1->y;
			float z1 = v2->z - v1->z;
			float z2 = v3->z - v1->z;
			float s1 = w2->x - w1->x;
			float s2 = w3->x - w1->x;
			float t1 = w2->y - w1->y;
			float t2 = w3->y - w1->y;
			float r = 1.0F / (s1 * t2 - s2 * t1);
			Vect3f sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
				(t2 * z1 - t1 * z2) * r);
			Vect3f tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
				(s1 * z2 - s2 * z1) * r);
			assert(i1<VtxCount);
			assert(i2<VtxCount);
			assert(i3<VtxCount);
			tan1[i1] += sdir;
			tan1[i2] += sdir;
			tan1[i3] += sdir;
			tan2[i1] += tdir;
			tan2[i2] += tdir;
			tan2[i3] += tdir;
		}
		for (size_t b = 0; b<VtxCount; ++b)
		{
			Vect3f *l_NormalVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + NormalStride];
			Vect3f *l_TangentVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + TangentStride];
			Vect4f *l_TangentVtx4 = (Vect4f
				*)&l_VtxAddress[b*VertexStride + TangentStride];
			Vect3f *l_BiNormalVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + BiNormalStride];
			const Vect3f& t = tan1[b];
			// Gram-Schmidt orthogonalize
			Vect3f l_VAl = t - (*l_NormalVtx)*((*l_NormalVtx)*t);
			l_VAl.Normalize();
			*l_TangentVtx = l_VAl;
			// Calculate handedness
			Vect3f l_Cross;
			l_Cross = (*l_NormalVtx) ^ (*l_TangentVtx);
			l_TangentVtx4->w = (l_Cross*(tan2[b]))< 0.0f ? -1.0f : 1.0f;
			*l_BiNormalVtx = (*l_NormalVtx) ^ (*l_TangentVtx);
		}
		delete[] tan1;
	}

	void CSceneAnimatedModel::LoadMaterials()
	{
		m_Materials = m_AnimatedCoreModel->GetMaterials();
	}
	
}