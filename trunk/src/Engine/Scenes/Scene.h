#pragma once

#include "Utils\Name.h"
#include "Utils\TemplatedMapVector.h"
#include "Layer.h"

namespace engine {
	class CScene : public CName, public base::utils::CTemplatedMapVector<CLayer>
	{
	public:
		CScene(const std::string& aName);
		virtual ~CScene();
		bool Load(const std::string& aFilename, const std::string& aFolder);
		bool Update(float elapsedTime);
		bool Render(engine::CRenderManager& aRendermanager);
		bool Render(engine::CRenderManager& aRendermanager, const std::string& aLayerName);
		void RenderDebugGUI();
		void PauseScene();
		void DebugModeScene();
		void SetScriptTransforms();
		GET_SET_BOOL(Active);
		GET_SET_BOOL(Pause);
		GET_SET_BOOL(Debug);

	protected:
		bool m_Active;
		bool m_Pause;
		bool m_Debug;
	private:
		DISALLOW_COPY_AND_ASSIGN(CScene);
	};
}