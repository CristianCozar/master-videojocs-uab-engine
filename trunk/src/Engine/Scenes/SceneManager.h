#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Scene.h"

namespace engine {
	class CSceneManager : public base::utils::CTemplatedMapVector<CScene>
	{
	public:
		CSceneManager();
		virtual ~CSceneManager();
		bool Load(const std::string &Filename);
		bool Update(float elapsedTime);
		bool Render(engine::CRenderManager& aRendermanager, const std::string& aLayer);
		bool Reload();
		void Activate(const std::string& aScene, bool aBool);
		void RenderDebugGUI();
		void PauseScenes();
		void DebugMode();
		void SetScriptTransforms();
	protected:
		std::string m_Filename;
	};
}