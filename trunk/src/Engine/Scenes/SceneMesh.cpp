#include "SceneMesh.h"

#include "Math\Vector3.h"
#include "ConstantBuffer.h"
#include "ConstantBufferManager.h"

namespace engine {

	CSceneMesh::CSceneMesh(const CXMLElement* aElement, std::string LayerName) : CSceneNode(aElement, LayerName)
	{
		mMesh = nullptr;
	}

	CSceneMesh::CSceneMesh(const CXMLElement* aElement, CMesh* aMesh, std::string LayerName) : CSceneNode(aElement, LayerName), mMesh(aMesh)
	{
	}

	CSceneMesh::~CSceneMesh()
	{
		//delete mMesh;
	}

	bool CSceneMesh::Render(CRenderManager& aRendermanager)
	{
		if (m_Visible)
		{
			CSceneNode::Render(aRendermanager);
			{
				bool lOk = false;
				if (mMesh)
				{
					CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
					lCB.mObjDesc.m_World = GetWorldMatrix();
					lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), CConstantBufferManager::CB_Object);
					lOk = mMesh->Render(aRendermanager, GetPosition(), GetScale(), IsTestFrustum());
				}
				return lOk;
			}
		}
	}
}
