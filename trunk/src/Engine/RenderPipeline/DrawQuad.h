#pragma once

#include "RenderCmd.h"
#include "Materials\Texture.h"
#include "RenderStagedTexture.h"
#include "Quad.h"
#include "Shaders\PixelShader.h"

namespace engine
{
	class CDrawQuad : public CRenderStagedTexture
	{
	public:
		CDrawQuad();
		virtual ~CDrawQuad();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);

	protected:
		DISALLOW_COPY_AND_ASSIGN(CDrawQuad);
		CQuad* mQuad;
		CPixelShader* mPixelShader;
		CMaterial* m_Material;
	};
}