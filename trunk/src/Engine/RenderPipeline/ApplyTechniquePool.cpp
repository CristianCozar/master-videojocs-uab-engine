#include "ApplyTechniquePool.h"

#include "Engine.h"
#include "Shaders\TechniquePoolManager.h"
#include "XML/XML.h"
#include "Debug\imgui.h"

namespace engine
{
	CApplyTechniquePool::CApplyTechniquePool()
	{
		m_PoolName = "";
	}

	CApplyTechniquePool::~CApplyTechniquePool()
	{

	}
	static char buf[512] = "poolname";
	bool CApplyTechniquePool::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		m_PoolName = aElement->Attribute("pool_name");
		strcpy_s(buf, m_PoolName.c_str());
		return lOk && !m_PoolName.empty();
	}

	void CApplyTechniquePool::Execute(CRenderManager& lRM)
	{
		CEngine::GetInstance().GetTechniquePoolManager().Apply(m_PoolName);
	}

	
	void CApplyTechniquePool::RenderDebugGUI()
	{
		ImGui::InputText("UTF-8 input", buf, 512);
		if (ImGui::Button("Apply"))
		{
			if (CEngine::GetInstance().GetTechniquePoolManager().Apply(buf))
			{
				m_PoolName = std::string(buf);
			}
			else {
				strcpy_s(buf, m_PoolName.c_str());
			}
		}
	}
}