#include "BeginRenderCmd.h"

#include "RenderManager.h"

#include "Engine.h"
#include "ConstantBufferManager.h"
#include "XML\XML.h"
#include "Debug\imgui.h"

namespace engine
{
	CBeginRenderCmd::CBeginRenderCmd()
	{

	}

	CBeginRenderCmd::~CBeginRenderCmd()
	{

	}

	bool CBeginRenderCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);;
	}

	void CBeginRenderCmd::Execute(CRenderManager& lRM)
	{
	/*	CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
		lCB.mFrameDesc.m_View = lRM.GetViewMatrix();
		lCB.mFrameDesc.m_Projection = lRM.GetProjectionMatrix();
		lCB.mFrameDesc.m_ViewProjection = lRM.GetViewProjectionMatrix();
		lCB.mFrameDesc.m_ViewInverse = lRM.GetViewMatrix().Invert();
		lCB.mFrameDesc.m_InverseProjection = lRM.GetProjectionMatrix().Invert();
		lCB.BindVSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_Frame);
		lCB.BindPSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_FramePS);*/
	}

	void CBeginRenderCmd::RenderDebugGUI()
	{
		
	}
}