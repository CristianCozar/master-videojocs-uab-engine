#include "RasterizerState.h"

#include "Engine.h"
#include "RenderManager.h"
#include "XML/XML.h"
#include "Utils\EnumToString.h"

namespace engine
{
	CSetRasterizerState::CSetRasterizerState()
	{
		m_ClockWise = false;
		m_RasterizerState = nullptr;
		m_CullMode = D3D11_CULL_MODE::D3D11_CULL_NONE;
		m_FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
	}

	CSetRasterizerState::~CSetRasterizerState()
	{
		if (m_RasterizerState)
			m_RasterizerState->Release();

	}

	bool CSetRasterizerState::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			m_ClockWise = aElement->GetAttribute<bool>("clock_wise", true);
			EnumString<D3D11_CULL_MODE>::ToEnum(m_CullMode, aElement->GetAttribute<std::string>("cull_mode", "back"));
			EnumString<D3D11_FILL_MODE>::ToEnum(m_FillMode, aElement->GetAttribute<std::string>("fill_mode", "solid"));
		}
		return lOk;
	}

	bool CSetRasterizerState::CreateRasterizerState()
	{
		return CEngine::GetInstance().GetRenderManager().SetSolidRenderState(m_FillMode, m_CullMode, m_ClockWise);
	}

	void CSetRasterizerState::Execute(CRenderManager& lRM)
	{
		CreateRasterizerState();
	}

	void CSetRasterizerState::RenderDebugGUI()
	{
		ImGui::Checkbox("ClockWise", &m_ClockWise);
		ImGui::Text(EnumString<D3D11_CULL_MODE>::ToStr(m_CullMode).c_str());
		ImGui::InputInt("Cull Mode", (int*)&m_CullMode);
		ImGui::Text(EnumString<D3D11_FILL_MODE>::ToStr(m_FillMode).c_str());
		ImGui::InputInt("Fill Mode", (int*)&m_FillMode);
	}
}