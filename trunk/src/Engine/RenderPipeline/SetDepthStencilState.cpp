#include "SetDepthStencilState.h"



#include "RenderManager.h"
#include "XML\XML.h"
#include "Utils\Logger\Logger.h"
#include "Engine.h"

namespace engine
{
	CSetDepthStencilStateCmd::CSetDepthStencilStateCmd()
	{
		m_DepthStencilState = nullptr;
	}

	CSetDepthStencilStateCmd::~CSetDepthStencilStateCmd()
	{
		if (m_DepthStencilState)
			m_DepthStencilState->Release();
	}

	bool CSetDepthStencilStateCmd::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			D3D11_DEPTH_STENCIL_DESC l_DepthStencilStateDescription;
			ZeroMemory(&l_DepthStencilStateDescription, sizeof(D3D11_DEPTH_STENCIL_DESC));
			// Depth test
			l_DepthStencilStateDescription.DepthEnable = aElement->GetAttribute<bool>("enable_z_test", true) ? TRUE : FALSE;
			l_DepthStencilStateDescription.DepthWriteMask = aElement->GetAttribute<bool>("write_z_buffer", true) ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
			l_DepthStencilStateDescription.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

			// Stencil test
			l_DepthStencilStateDescription.StencilEnable = aElement->GetAttribute<bool>("enable_stencil", true) ? TRUE : FALSE;
			l_DepthStencilStateDescription.StencilReadMask = 0xFF;
			l_DepthStencilStateDescription.StencilWriteMask = 0xFF;

			// Stencil operations front-facing
			l_DepthStencilStateDescription.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			l_DepthStencilStateDescription.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
			l_DepthStencilStateDescription.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			l_DepthStencilStateDescription.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

			// Stencil operations back-facing
			l_DepthStencilStateDescription.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			l_DepthStencilStateDescription.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
			l_DepthStencilStateDescription.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			l_DepthStencilStateDescription.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;


			// Crear stencil
			HRESULT hr = CEngine::GetInstance().GetRenderManager().GetDevice()->CreateDepthStencilState(&l_DepthStencilStateDescription, &m_DepthStencilState);
			if (FAILED(hr))
			{
				LOG_ERROR_APPLICATION("Error on creating DEPTH STENCIL STATE");
				lOk = false;
			}
		}
		return lOk;
	}

	void CSetDepthStencilStateCmd::Execute(CRenderManager& lRM)
	{
		lRM.GetDeviceContext()->OMSetDepthStencilState(m_DepthStencilState, 0);
	}
}