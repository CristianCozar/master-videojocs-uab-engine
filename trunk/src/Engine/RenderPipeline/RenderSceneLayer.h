#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CRenderSceneLayer : public CRenderCmd
	{
	public:
		CRenderSceneLayer();
		virtual ~CRenderSceneLayer();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI();
	private:
		DISALLOW_COPY_AND_ASSIGN(CRenderSceneLayer);
		std::string m_LayerName;
	};
}