#pragma once

#include "Utils\Name.h"
#include "Utils\Defines.h"
#include "Debug\imgui.h"

namespace engine
{
	class CRenderManager;
	class CRenderCmd : public CName
	{
	public:
		CRenderCmd();
		virtual ~CRenderCmd();
		virtual bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM) = 0;
		GET_SET_BOOL(Active);
		void RenderDebugGUIBase();
		virtual void RenderDebugGUI() { ; };
	private:
		DISALLOW_COPY_AND_ASSIGN(CRenderCmd);
		bool m_Active;
	};
}