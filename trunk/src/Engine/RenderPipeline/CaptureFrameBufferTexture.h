#pragma once


#include "Materials\Texture.h"

XML_FORWARD_DECLARATION

namespace engine
{


	class CCaptureFrameBufferTexture : public CTexture
	{
	protected:
		ID3D11Texture2D     *m_DataTexture;
		void Init();
		bool CreateSamplerState();
	public:
		CCaptureFrameBufferTexture(const CXMLElement*TreeNode);
		virtual ~CCaptureFrameBufferTexture();
		bool Capture(unsigned int StageId);
		ID3D11Texture2D* CapturedTexture() { return m_DataTexture; }
	};


}