#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CApplyTechniquePool : public CRenderCmd
	{
	public:
		CApplyTechniquePool();
		virtual ~CApplyTechniquePool();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI() override;
	private:
		DISALLOW_COPY_AND_ASSIGN(CApplyTechniquePool);
		std::string m_PoolName;
	};
}