#pragma once

#include "RenderCmd.h"
#include "DrawQuad.h"

namespace engine
{
	class CEnableAdditiveAlphaBlend : public CRenderCmd
	{
	public:
		CEnableAdditiveAlphaBlend();
		virtual ~CEnableAdditiveAlphaBlend();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager &RM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CEnableAdditiveAlphaBlend);
		ID3D11BlendState *m_EnabledAlphaBlendState;
	};
}