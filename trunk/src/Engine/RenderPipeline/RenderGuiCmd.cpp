#include "RenderGuiCmd.h"

#include "Engine.h"
#include "GUI\GUIManager.h"

namespace engine
{
	CRenderGuiCmd::CRenderGuiCmd()
	{

	}

	CRenderGuiCmd::~CRenderGuiCmd()
	{

	}

	bool CRenderGuiCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CRenderGuiCmd::Execute(CRenderManager& lRM)
	{
		CEngine::GetInstance().GetGUIManager().Render(lRM);
	}
}