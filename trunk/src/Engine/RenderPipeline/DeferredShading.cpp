#include "DeferredShading.h"

#include "Engine.h"
#include "RenderManager.h"
#include "Lights\LightManager.h"
#include "ConstantBufferManager.h"
#include "Scenes\SceneManager.h"
#include "Scenes\Scene.h"
#include "Scenes\Layer.h"

namespace engine
{
	CDeferredShading::CDeferredShading()
		: CDrawQuad()
		, m_EnabledAlphaBlendState(nullptr)
	{
		D3D11_BLEND_DESC l_AlphablendDesc;
		ZeroMemory(&l_AlphablendDesc, sizeof(D3D11_BLEND_DESC));
		l_AlphablendDesc.RenderTarget[0].BlendEnable = true;
		l_AlphablendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		l_AlphablendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		l_AlphablendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		l_AlphablendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		l_AlphablendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		l_AlphablendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		l_AlphablendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		CEngine::GetInstance().GetRenderManager().GetDevice()->CreateBlendState(&l_AlphablendDesc, &m_EnabledAlphaBlendState);
	}

	CDeferredShading::~CDeferredShading()
	{
		if (m_EnabledAlphaBlendState)
			m_EnabledAlphaBlendState->Release();
	}

	bool CDeferredShading::Load(const CXMLElement* aElement)
	{
		return CDrawQuad::Load(aElement);
	}

	void CDeferredShading::Execute(CRenderManager &RM)
	{
		// Activamos el alpha
		RM.GetDeviceContext()->OMSetBlendState(m_EnabledAlphaBlendState, NULL, 0xffffffff);
		// Activamos todas las texturas
		ActivateTextures();
		// Ponemos las constantes de las luces y renderizamos por cada una.
		CConstantBufferManager &l_CBM = CEngine::GetInstance().GetConstantBufferManager();
		CLightManager &l_LM = CEngine::GetInstance().GetLightManager();
		CSceneManager &l_SM = CEngine::GetInstance().GetSceneManager();
		int sceneCount = l_SM.GetCount();

		for (int i = 0; i < sceneCount; ++i)
		{
			CScene* l_Scene = l_SM.GetIndex(i);
			if (l_Scene->IsActive())
			{
				CLayer *l_LightLayer = l_Scene->Get("Light");
				if (l_LightLayer)
				{
					int count = l_LightLayer->GetCount();
					for (int j = 0; j < count; ++j)
					{
						l_CBM.mLightDesc = {};
						l_LM.SetLightConstants(0, (CLight*)l_LightLayer->GetIndex(j));
						l_LM.UpdateConstants();
						mQuad->Render(m_Material);
					}
				}
			}
		}
	}
}