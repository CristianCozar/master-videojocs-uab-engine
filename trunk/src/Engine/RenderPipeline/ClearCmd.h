#pragma once

#include "RenderCmd.h"
#include "Math\Color.h"

namespace engine
{
	class CClearCmd : public CRenderCmd
	{
	public:
		CClearCmd();
		virtual ~CClearCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI();
	private:
		DISALLOW_COPY_AND_ASSIGN(CClearCmd);
		bool m_RenderTarget;
		bool m_DepthStencil;
		CColor m_Color;
	};
}