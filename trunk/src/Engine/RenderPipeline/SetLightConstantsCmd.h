#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CSetLightConstantsCmd : public CRenderCmd
	{
	public:
		CSetLightConstantsCmd();
		virtual ~CSetLightConstantsCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI() override;
	private:
		DISALLOW_COPY_AND_ASSIGN(CSetLightConstantsCmd);
	};
}