#pragma once

#include "RenderCmd.h"
#include "DrawQuad.h"

namespace engine
{
	class CDisableAlphaBlend : public CRenderCmd
	{
	public:
		CDisableAlphaBlend();
		virtual ~CDisableAlphaBlend();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager &RM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CDisableAlphaBlend);
	};
}