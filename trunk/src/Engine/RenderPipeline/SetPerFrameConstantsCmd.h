#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CSetUpMatricesCmd : public CRenderCmd
	{
	public:
		CSetUpMatricesCmd();
		virtual ~CSetUpMatricesCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CSetUpMatricesCmd);
	};
}