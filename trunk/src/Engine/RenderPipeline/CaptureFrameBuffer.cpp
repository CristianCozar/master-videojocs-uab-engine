#include "CaptureFrameBuffer.h"
#include "XML\XML.h"
#include "Materials\TextureManager.h"
#include "Engine.h"

namespace engine
{
	CCaptureFrameBuffer::CCaptureFrameBuffer()
	{
		m_CapturedFramBufferTexture = nullptr;
	}

	CCaptureFrameBuffer::~CCaptureFrameBuffer()
	{
		//delete m_CapturedFramBufferTexture;
	}

	void CCaptureFrameBuffer::Execute(CRenderManager& lRM)
	{
		m_CapturedFramBufferTexture->Capture(0);
	}

	bool CCaptureFrameBuffer::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			engine::CTextureManager &l_TextureManager = engine::CEngine::GetInstance().GetTextureManager();
			for (const tinyxml2::XMLElement *Capturedtexture = aElement->FirstChildElement("capture_texture"); Capturedtexture != nullptr; Capturedtexture = Capturedtexture->NextSiblingElement("capture_texture")) {
				CTexture* lTex = l_TextureManager.GetTexture(Capturedtexture->Attribute("name"));
				if (lTex == nullptr)
				{
					m_CapturedFramBufferTexture = new CCaptureFrameBufferTexture(Capturedtexture);
					l_TextureManager.AddTexture(Capturedtexture->Attribute("name"), m_CapturedFramBufferTexture);
				}
				else {
					m_CapturedFramBufferTexture = (CCaptureFrameBufferTexture*)lTex;
				}
			}
		}
		return lOk;
	}
}