#pragma once

#include "Utils\TemplatedMapVector.h"

namespace engine
{
	class CRenderCmd;
	class CRenderManager;
	class CRenderPipeline : public base::utils::CTemplatedMapVector< CRenderCmd >
	{
	public:
		CRenderPipeline();
		virtual ~CRenderPipeline();
		bool Load(const std::string& aFilename);
		void Execute(CRenderManager& lRM);
		void Reload();
		void Activate(const int& aIndex, const bool& aActive);
		void Activate(const std::string& aCommandName, const bool& aActive);
		void Activate(const bool& aActive);
		void RenderDebugGUI();
	private:
		std::string m_Filename;
	};
}