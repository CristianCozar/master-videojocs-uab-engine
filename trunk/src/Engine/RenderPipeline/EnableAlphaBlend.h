#pragma once

#include "RenderCmd.h"
#include "DrawQuad.h"

namespace engine
{
	class CEnableAlphaBlend : public CRenderCmd
	{
	public:
		CEnableAlphaBlend();
		virtual ~CEnableAlphaBlend();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager &RM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CEnableAlphaBlend);
		ID3D11BlendState *m_EnabledAlphaBlendState;
	};
}