#pragma once

#include <map>
#include <functional>

#include "RenderPipeline.h"
#include "BeginRenderCmd.h"
#include "EndRenderCmd.h"
#include "ClearCmd.h"
#include "SetPerFrameConstantsCmd.h"
#include "ApplyTechniquePool.h"
#include "RenderSceneLayer.h"
#include "RenderImGuiCmd.h"
#include "SetDepthStencilState.h"
#include "DrawQuad.h"
#include "SetRenderTarget.h"
#include "UnSetRenderTarget.h"
#include "RasterizerState.h"
#include "SetLightConstantsCmd.h"
#include "CaptureFrameBuffer.h"
#include "ApplyFilter.h"
#include "DeferredShading.h"
#include "EnableAlphaBlend.h"
#include "EnableAdditiveAlphaBlend.h"
#include "DisableAlphaBlend.h"
#include "GenerateShadowMaps.h"
#include "RenderGuiCmd.h"

#define RENDER_CMD_ENTRY(tag, command_class_name) {\
	tag, [] { \
	return new command_class_name(); \
	}\
}

std::map<std::string, std::function<engine::CRenderCmd*(void)>> sCommandsFactory =
{
	RENDER_CMD_ENTRY("begin_render", engine::CBeginRenderCmd),
	RENDER_CMD_ENTRY("end_render", engine::CEndRenderCmd),
	RENDER_CMD_ENTRY("set_per_frame_constants", engine::CSetUpMatricesCmd),
	RENDER_CMD_ENTRY("set_lights_constants", engine::CSetLightConstantsCmd),
	RENDER_CMD_ENTRY("set_rasterizer_state", engine::CSetRasterizerState),
	RENDER_CMD_ENTRY("apply_technique_pool", engine::CApplyTechniquePool),
	RENDER_CMD_ENTRY("render_layer", engine::CRenderSceneLayer),
	RENDER_CMD_ENTRY("render_imgui", engine::CRenderImGuiCmd),
	RENDER_CMD_ENTRY("set_depth_stencil_state", engine::CSetDepthStencilStateCmd),
	RENDER_CMD_ENTRY("clear", engine::CClearCmd),
	RENDER_CMD_ENTRY("draw_quad", engine::CDrawQuad),
	RENDER_CMD_ENTRY("set_render_target", engine::CSetRenderTarget),
	RENDER_CMD_ENTRY("unset_render_target", engine::CUnSetRenderTarget),
	RENDER_CMD_ENTRY("capture_frame_buffer", engine::CCaptureFrameBuffer),
	RENDER_CMD_ENTRY("apply_filter", engine::CApplyFilter),
	RENDER_CMD_ENTRY("deferred_shading", engine::CDeferredShading),
	RENDER_CMD_ENTRY("enable_alpha_blend", engine::CEnableAlphaBlend),
	RENDER_CMD_ENTRY("enable_additive_alpha_blend", engine::CEnableAdditiveAlphaBlend),
	RENDER_CMD_ENTRY("disable_alpha_blend", engine::CDisableAlphaBlend),
	RENDER_CMD_ENTRY("generate_shadow_maps", engine::CGenerateShadowMaps),
	RENDER_CMD_ENTRY("render_gui", engine::CRenderGuiCmd)

};