#include "SetAlphaBlendState.h"
#include "XML\XML.h"
#include "RenderManager.h"
#include "Engine.h"

namespace engine
{
	CSetAlphaBlendState::CSetAlphaBlendState()
	{
		m_AlphaBlendState = nullptr;
	}

	CSetAlphaBlendState::~CSetAlphaBlendState()
	{
		if (m_AlphaBlendState)
			m_AlphaBlendState->Release();
	}

	bool CSetAlphaBlendState::Load(const CXMLElement* aElement)
	{	
		bool lOk = CRenderCmd::Load(aElement);

		if(lOk && aElement	->GetAttribute<std::string>("state","enabled")==std::string("enbled"))
		{				
			D3D11_BLEND_DESC l_AlphablendDesc;
			ZeroMemory(&l_AlphablendDesc, sizeof(D3D11_BLEND_DESC));
			l_AlphablendDesc.RenderTarget[0].BlendEnable =true;


			// TODO Obtain the alpha blend states from the xml

			l_AlphablendDesc.RenderTarget[0].SrcBlend =	D3D11_BLEND_SRC_ALPHA;
			l_AlphablendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			l_AlphablendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
			l_AlphablendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			l_AlphablendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
			l_AlphablendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			l_AlphablendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();

			lOk = SUCCEEDED(lRM.GetDevice()->CreateBlendState(&l_AlphablendDesc,&m_AlphaBlendState));
		}

		return lOk;
	}

	void CSetAlphaBlendState::Execute(CRenderManager& lRM)
	{
		lRM.GetDeviceContext()->OMSetBlendState(m_AlphaBlendState,NULL, 0xffffffff);
	}
	
}