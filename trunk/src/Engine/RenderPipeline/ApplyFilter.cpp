#include "ApplyFilter.h"
#include "Materials\TextureManager.h"
#include "Materials\MaterialManager.h"
#include "RenderManager.h"
#include "Engine.h"
#include "Shaders\ShaderManager.h"
#include "Shaders\TechniquePoolManager.h"
#include "Shaders\EffectManager.h"

namespace engine
{
	CApplyFilter::CApplyFilter()
	{
	}
	CApplyFilter::~CApplyFilter()
	{
	}

	bool CApplyFilter::Load(const CXMLElement* aElement)
	{
		CRenderCmd::Load(aElement);
		bool lOk = CRenderStagedTexture::Load(aElement);
		CShaderManager& lSM = CEngine::GetInstance().GetShaderManager();
		CTechniquePoolManager& lTM = CEngine::GetInstance().GetTechniquePoolManager();
		CEffectManager& lEM = CEngine::GetInstance().GetEffectManager();
		
		//std::string shaderName = aElement->GetAttribute<std::string>("pixel_shader", "DrawQuadForward");

		//mPixelShader = static_cast<CPixelShader*>(lSM.GetShader(ShaderStage::ePixelShader, shaderName));

		mQuad = new CQuad();
		mQuad->Init();
	/*	mPixelShader = m_DynamicTexturesMaterials[0]->m_Material->GetTechnique()->GetEffect()->GetShader<CPixelShader>(ShaderStage::ePixelShader);
		mPixelShader->SetEntryPoint("mainPS");
		mPixelShader->Load();
		mQuad->Init(mPixelShader);*/
		return lOk;
	}

	void CApplyFilter::Execute(CRenderManager &RM)
	{		
		for (int i = 0; i < m_DynamicTexturesMaterials.size(); ++i)
		{
			ID3D11DepthStencilView* l_DepthStencilView = m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetDepthStencilView();
			
			//l_DepthStencilView = nullptr;

			RM.SetRenderTargets(1, &m_RenderTargetViews[i], l_DepthStencilView);
			RM.SetViewport(m_ViewportPosition, m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetSize());
			
			if (i == 0)
			{
				m_StagedTextures[0]->Activate();
			}
			else{
				m_DynamicTexturesMaterials[i - 1]->Activate(0);
				//RM.SetViewport(m_ViewportPosition, m_DynamicTexturesMaterials[i - 1]->m_DynamicTexture->GetSize());
				//m_DynamicTexturesMaterials[i]->m_Material->Apply();
			}

			

			mQuad->Render(m_DynamicTexturesMaterials[i]->m_Material);
			//RM.ResetViewport();
			RM.UnsetRenderTargets();
		}		
	}
	
}