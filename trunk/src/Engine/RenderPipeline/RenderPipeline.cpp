#include "RenderPipeline.h"

#include "Utils\Logger\Logger.h"
#include "XML\XML.h"
#include "Paths.h"
#include "CommandsFactory.h"

#include "Debug\imgui.h"

namespace engine
{
	CRenderPipeline::CRenderPipeline()
	{
		m_Filename = "";
	}

	CRenderPipeline::~CRenderPipeline()
	{
		Destroy();
	}

	bool CRenderPipeline::Load(const std::string& aFilename)
	{
		bool lOk = false;
		m_Filename = aFilename;
		
		LOG_INFO_APPLICATION("Loading render pipeline from '%s'", m_Filename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((PATH_BASE + m_Filename).c_str());

		if (base::xml::SucceedLoad(error)) {
			lOk = true;
			LOG_INFO_APPLICATION("Reading render pipeline");
			tinyxml2::XMLElement *commands = document.FirstChildElement("render_pipeline"); // Commands
			if (commands) {
				int i = 1;
				for (CXMLElement *command = commands->FirstChildElement(); command != nullptr; command = command->NextSiblingElement()) { // Command
					std::string name = command->Value();
					CRenderCmd *renderCmd = sCommandsFactory[name]();
					if (renderCmd->Load(command))
					{
						Add(std::string(renderCmd->GetName()/* + std::to_string(i)*/), renderCmd);
						i++;
					}
				}
			}
		}
		else {
			LOG_WARNING_APPLICATION("Render pipeline XML file not found.");
		}

		return lOk;
	}

	void CRenderPipeline::Execute(CRenderManager& lRM)
	{
		size_t count = GetCount();
		for (size_t i = 0; i < count; ++i)
		{
			if (GetIndex(i)->IsActive())
			{
				//LOG_INFO_APPLICATION("Executing render pipeline command %s", GetIndex(i)->GetName());
				GetIndex(i)->Execute(lRM);
			}
			else {
				//LOG_INFO_APPLICATION("Not executing render pipeline command %s (not active)", GetIndex(i)->GetName());
			}
		}
	}

	void CRenderPipeline::Reload()
	{
		LOG_INFO_APPLICATION("Reloading Render Pipeline");
		Destroy();
		Load(m_Filename);
	}

	void CRenderPipeline::Activate(const int& aIndex, const bool& aActive)
	{
		if (aIndex > 0 && aIndex < GetCount())
		{
			GetIndex(aIndex)->SetActive(aActive);
		}
	}

	void CRenderPipeline::Activate(const std::string& aCommandName, const bool& aActive)
	{
		if (Exist(aCommandName))
		{
			Get(aCommandName)->SetActive(aActive);
		}
	}

	void CRenderPipeline::Activate(const bool& aActive)
	{
		for (size_t i = 0; i < GetCount(); ++i)
		{
			Activate(i, aActive);
		}
	}

	void CRenderPipeline::RenderDebugGUI()
	{
		if (ImGui::CollapsingHeader("Commands"))
		{
			for (unsigned int i = 0; i < GetCount(); ++i)
			{
				GetIndex(i)->RenderDebugGUIBase();
			}
		}
		if (ImGui::Button("Reload"))
		{
			Reload();
		}
	}
}