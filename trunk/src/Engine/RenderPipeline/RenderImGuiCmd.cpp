#include "RenderImGuiCmd.h"

#include "Engine.h"
#include "Debug\imgui.h"

namespace engine
{
	CRenderImGuiCmd::CRenderImGuiCmd()
	{

	}

	CRenderImGuiCmd::~CRenderImGuiCmd()
	{

	}

	bool CRenderImGuiCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CRenderImGuiCmd::Execute(CRenderManager& lRM)
	{
		CEngine::GetInstance().DebugUI();
		ImGui::Render();
	}
}