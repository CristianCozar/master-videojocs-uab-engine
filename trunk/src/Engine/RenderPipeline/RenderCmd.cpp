#include "RenderCmd.h"
#include "XML\XML.h"
#include "Utils\Logger\Logger.h"


namespace engine
{
	CRenderCmd::CRenderCmd()
	{
		m_Active = false;
	}

	CRenderCmd::~CRenderCmd()
	{

	}

	bool CRenderCmd::Load(const CXMLElement* aElement)
	{
		m_Name = aElement->Attribute("name");
		m_Active = aElement->GetAttribute<bool>("active", true);
		LOG_INFO_APPLICATION_IF(!m_Name.empty(), "Can't find name in tag %s", aElement->Value());
		return !m_Name.empty();
	}

	void CRenderCmd::RenderDebugGUIBase()
	{
		ImGui::Indent();
		if (ImGui::CollapsingHeader(m_Name.c_str()))
		{
			ImGui::Indent();
			ImGui::PushID(m_Name.c_str());
			ImGui::Checkbox("Active", &m_Active);
			RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		ImGui::Unindent();
	}
}