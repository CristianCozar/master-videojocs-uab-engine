#pragma once

#include "DrawQuad.h"

namespace engine
{
	
	class CDeferredShading : public CDrawQuad
	{
		public:
			CDeferredShading();
			virtual ~CDeferredShading();
			bool Load(const CXMLElement* aElement);
			virtual void Execute(CRenderManager &RM);
		private:
			DISALLOW_COPY_AND_ASSIGN(CDeferredShading);
			ID3D11BlendState *m_EnabledAlphaBlendState;
	};
}