#include "GenerateShadowMaps.h"
#include "Lights\LightManager.h"
#include "Scenes\SceneManager.h"

namespace engine
{
	CGenerateShadowMaps::CGenerateShadowMaps()
	{

	}

	CGenerateShadowMaps::~CGenerateShadowMaps()
	{

	}

	bool CGenerateShadowMaps::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CGenerateShadowMaps::Execute(CRenderManager& lRM)
	{
		CLightManager& l_LM = CEngine::GetInstance().GetLightManager();
		CSceneManager& l_SM = CEngine::GetInstance().GetSceneManager();
		CScene* scene = nullptr;
		for (int i = 0; i < l_SM.GetCount(); ++i)
		{
			if (l_SM[i]->IsActive())
			{
				scene = l_SM[i];
				break;
			}
		}

		for (int i = 0; i < l_LM.GetCount(); ++i)
		{
			if (l_LM[i]->GetGenerateShadowMap() && l_LM[i]->IsActive())
			{

				l_LM[i]->SetShadowMap(lRM);
				lRM.UpdateFrustum();
			//	lRM.BeginRender();

				CColor l_Color;
				l_Color.SetZero();
			//	l_Color.SetRed(1);
				lRM.Clear(true, true, l_Color);
				std::vector<CLayer*> layers = l_LM[i]->GetShadowLayers();
				if (scene != nullptr)
				{
					for (int j = 0; j < layers.size(); ++j)
					{
						scene->Render(lRM, layers[j]->GetName());
					}
				}
				lRM.UnsetRenderTargets();
				//lRM.EndRender();
			}
		}
	}
}