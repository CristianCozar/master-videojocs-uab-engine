#pragma once


namespace engine
{
	class CGeometry;
	class CPixelShader;
	class CEffect;
	class CMaterial;

	class CQuad
	{
	public:
		CQuad();
		virtual ~CQuad();

		bool Init();
		bool Render(CMaterial *Material);

	private:
		CGeometry* mGeometry;
	};

}