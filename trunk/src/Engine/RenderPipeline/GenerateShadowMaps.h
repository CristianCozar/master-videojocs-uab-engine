#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CGenerateShadowMaps : public CRenderCmd
	{
	public:
		CGenerateShadowMaps();
		virtual ~CGenerateShadowMaps();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CGenerateShadowMaps);
	};
}