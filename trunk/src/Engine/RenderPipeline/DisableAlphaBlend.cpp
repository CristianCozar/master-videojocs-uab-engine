#include "DisableAlphaBlend.h"

#include "Engine.h"
#include "RenderManager.h"

namespace engine
{
	CDisableAlphaBlend::CDisableAlphaBlend()
	{
	}

	CDisableAlphaBlend::~CDisableAlphaBlend()
	{
	}

	bool CDisableAlphaBlend::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CDisableAlphaBlend::Execute(CRenderManager &RM)
	{
		RM.GetDeviceContext()->OMSetBlendState(NULL, NULL, 0xffffffff);
	}
}