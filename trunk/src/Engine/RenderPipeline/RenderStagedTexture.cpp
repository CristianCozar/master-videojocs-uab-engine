#include "RenderStagedTexture.h"
#include "XML\XML.h"
#include "Materials\TextureManager.h"
#include "Materials\MaterialManager.h"
#include "Materials\TemplatedMaterialParameter.h"
#include "Engine.h"
#include "RenderManager.h"
#include "Utils\CheckedDelete.h"

namespace engine
{
	CRenderStagedTexture::CRenderStagedTexture()
	{
		m_ViewportPosition.x = 0;
		m_ViewportPosition.y = 0;

	//	m_ViewportSize.x = 0;
	//	m_ViewportSize.y = 0;
	}

	CRenderStagedTexture::~CRenderStagedTexture()
	{
		base::utils::CheckedDelete(m_StagedTextures);
		base::utils::CheckedDelete(m_DynamicTexturesMaterials);
		/*for (int i = 0; i < m_RenderTargetViews.capacity(); i++)
		{
			if (m_RenderTargetViews[i])
				m_RenderTargetViews[i]->Release();
		}*/
	}

	bool CRenderStagedTexture::Load(const CXMLElement* aElement)
	{
		bool lOK = true;
		CTextureManager& lTM = CEngine::GetInstance().GetTextureManager();
		CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();
		CMaterialManager& lMM = engine::CEngine::GetInstance().GetMaterialManager();
		D3D11_VIEWPORT lVP = lRM.GetViewport();
		m_ViewportPosition.x = lVP.Width;
		m_ViewportPosition.y = lVP.Height;
		m_ViewportSize.x = lVP.TopLeftX;
		m_ViewportSize.y = lVP.TopLeftY;

		m_ViewportPosition = aElement->GetAttribute<Vect2u>("viewport_position", Vect2u(0, 0));
		m_ViewportSize = aElement->GetAttribute<Vect2u>("viewport_size", Vect2u(lVP.Width, lVP.Height));


		for (const tinyxml2::XMLElement *lText = aElement->FirstChildElement("dynamic_texture"); lText != nullptr; lText = lText->NextSiblingElement("dynamic_texture"))
		{
			CTexture* lTex = lTM.GetTexture(lText->Attribute("name"));
			CDynamicTexture* lDTex;
			if (lTex == nullptr)
			{
				lDTex = new CDynamicTexture(lText);
				/*lOK = */lTM.AddTexture(lText->Attribute("name"), lDTex);
				/*if (!lOK)
					break;*/
			}
			else {
				lDTex = (CDynamicTexture*)lTex;
			}
			engine::CMaterial* lMat = lMM(lText->GetAttribute<std::string>("material", "")); // No se si es correcto, en los ejemplos no veo que el material este por xml
			//m_DynamicTextures.push_back(lDTex);
			AddDynamicTextureMaterial(lDTex, lMat);

				//lTM.GetTexture(lText->Attribute("name"));
			if (!lText->GetAttribute<bool>("texture_width_as_frame_buffer", true))
			{
				m_ViewportPosition = aElement->GetAttribute<Vect2u>("viewport_position", Vect2u(0, 0));
				m_ViewportSize = aElement->GetAttribute<Vect2u>("viewport_size", Vect2u(0, 0));
			}
		}

		if (lOK)
		{
			for (const tinyxml2::XMLElement *lText = aElement->FirstChildElement("texture"); lText != nullptr; lText = lText->NextSiblingElement("texture"))
			{
				CTexture* lTex = lTM.GetTexture(lText->Attribute("name"));
				CStagedTexture* lSTex = new CStagedTexture(lText->UnsignedAttribute("stage"), lTex);
				//CStagedTexture* lSTex = new CStagedTexture(lText->GetAttribute<uint32>("stage_id", -1), lTex);
				m_StagedTextures.push_back(lSTex);
			}
		}

		CreateRenderTargetViewVector();


		return lOK;
	}

	void CRenderStagedTexture::CreateRenderTargetViewVector()
	{
		for (size_t i = 0, lCount = m_DynamicTexturesMaterials.size(); i < lCount; ++i)
		{
			m_RenderTargetViews.push_back(m_DynamicTexturesMaterials[i]->m_DynamicTexture->GetRenderTargetView());
		}
	}

	CRenderStagedTexture::CStagedTexture::CStagedTexture(uint32 aStageID, CTexture* aTexture)
	{
		m_StageID = aStageID;
		m_Texture = aTexture;
	}

	void CRenderStagedTexture::CStagedTexture::Activate()
	{
		ID3D11DeviceContext* lDC = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
		m_Texture->Bind(m_StageID, lDC);
	}

	CRenderStagedTexture::CDynamicTextureMaterial::CDynamicTextureMaterial(CDynamicTexture* DynamicTexture, CMaterial* Material)
	{
		m_DynamicTexture = DynamicTexture;
		m_Material = Material;
	}

	void CRenderStagedTexture::CDynamicTextureMaterial::Activate(int StageID)
	{
		ID3D11DeviceContext* lDC = CEngine::GetInstance().GetRenderManager().GetDeviceContext();
		m_DynamicTexture->Bind(StageID, lDC);
		m_Material->Apply(); // No se si se necesita hacer apply del material o no

	}

	void CRenderStagedTexture::ActivateTextures()
	{
		for (size_t i = 0, lCount = m_DynamicTexturesMaterials.size(); i < lCount; ++i)
		{
			m_DynamicTexturesMaterials[i]->Activate(i);
		}

		for (size_t i = 0, lCount = m_StagedTextures.size(); i < lCount; ++i)
		{
			m_StagedTextures[i]->Activate();
		}
	}


	void CRenderStagedTexture::AddDynamicTextureMaterial(CDynamicTexture *DynamicTexture, CMaterial *Material)
	{
		CDynamicTextureMaterial* lDTex = new CDynamicTextureMaterial(DynamicTexture, Material);

		if (Material != nullptr)
		{
			std::vector<CMaterialParameter*> lParameters = Material->GetParameters();

			for (unsigned int i = 0; i < lParameters.size(); ++i)
			{
				CMaterialParameter* l_MP = lParameters[i];
				if (l_MP->GetName() == "TextureSize")
				{
					CTemplatedMaterialParameter<Vect2f>* l_TMP = static_cast<CTemplatedMaterialParameter<Vect2f>*>(l_MP);
					Vect2f TexSize;
					TexSize.x = lDTex->m_DynamicTexture->GetSize().x;
					TexSize.y = lDTex->m_DynamicTexture->GetSize().y;
					l_TMP->SetValue(TexSize);
				}
			}
		}

		m_DynamicTexturesMaterials.push_back(lDTex);
	}
}