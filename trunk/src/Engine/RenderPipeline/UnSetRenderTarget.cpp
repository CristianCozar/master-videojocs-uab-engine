#include "UnSetRenderTarget.h"
#include "XML\XML.h"
#include "RenderManager.h"


namespace engine
{
	CUnSetRenderTarget::CUnSetRenderTarget()
	{

	}

	CUnSetRenderTarget::~CUnSetRenderTarget()
	{

	}

	void CUnSetRenderTarget::Execute(CRenderManager& lRM)
	{
		lRM.UnsetRenderTargets();
	}

	bool CUnSetRenderTarget::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}
}