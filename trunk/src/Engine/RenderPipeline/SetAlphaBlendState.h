#pragma once

#include "RenderCmd.h"
#include "RenderManager.h"

namespace engine
{
	class CSetAlphaBlendState : public CRenderCmd
	{
	public:
		CSetAlphaBlendState();
		virtual ~CSetAlphaBlendState();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CSetAlphaBlendState);
		ID3D11BlendState* m_AlphaBlendState;
	};
}