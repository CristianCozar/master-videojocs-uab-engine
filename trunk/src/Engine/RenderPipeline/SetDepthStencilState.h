#pragma once

#include <d3d11.h>

#include "RenderCmd.h"

namespace engine
{
	class CSetDepthStencilStateCmd : public CRenderCmd
	{
	public:
		CSetDepthStencilStateCmd();
		virtual ~CSetDepthStencilStateCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CSetDepthStencilStateCmd);
		ID3D11DepthStencilState *m_DepthStencilState;
	};
}