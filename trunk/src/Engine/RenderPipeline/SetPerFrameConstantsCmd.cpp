#include "SetPerFrameConstantsCmd.h"

#include "RenderManager.h"
#include "Engine.h"
#include "Camera\CameraController.h"
#include "ConstantBufferManager.h"

namespace engine
{
	CSetUpMatricesCmd::CSetUpMatricesCmd()
	{

	}

	CSetUpMatricesCmd::~CSetUpMatricesCmd()
	{

	}

	bool CSetUpMatricesCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CSetUpMatricesCmd::Execute(CRenderManager& lRM)
	{
		engine::CEngine::GetInstance().GetCameraController().SetToRenderManager(lRM);
		lRM.UpdateFrustum();
		CConstantBufferManager &lCB = CEngine::GetInstance().GetConstantBufferManager();
		float uptime = (float)clock();
		lCB.mFrameDesc.m_Time.x = uptime;
		
		lCB.mFrameDesc.m_View = lRM.GetViewMatrix();
		lCB.mFrameDesc.m_Projection = lRM.GetProjectionMatrix();
		lCB.mFrameDesc.m_ViewProjection = lRM.GetViewProjectionMatrix();
		lCB.mFrameDesc.m_ViewInverse = lRM.GetViewMatrix().Invert();
		lCB.mFrameDesc.m_InverseProjection = lRM.GetProjectionMatrix().Invert();
		lCB.BindVSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_Frame);
		lCB.BindPSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_FramePS);
		lCB.BindGSBuffer(lRM.GetDeviceContext(), CConstantBufferManager::CB_FrameGS);

	}
}