#include "SetRenderTarget.h"
#include "XML\XML.h"
#include "RenderManager.h"


namespace engine
{
	CSetRenderTarget::CSetRenderTarget()
	{

	}

	CSetRenderTarget::~CSetRenderTarget()
	{

	}

	void CSetRenderTarget::Execute(CRenderManager& lRM)
	{
		ID3D11DepthStencilView* l_DepthStencilView = m_DynamicTexturesMaterials.empty() ? nullptr :
			m_DynamicTexturesMaterials[0]->m_DynamicTexture->GetDepthStencilView();
		if (l_DepthStencilView)
			lRM.SetRenderTargets((UINT)m_DynamicTexturesMaterials.size(), &m_RenderTargetViews[0], l_DepthStencilView);
		else
			lRM.SetRenderTargets((UINT)m_DynamicTexturesMaterials.size(), &m_RenderTargetViews[0], nullptr);
	}

	bool CSetRenderTarget::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			lOk = CRenderStagedTexture::Load(aElement);
			/*for (const tinyxml2::XMLElement *texture = aElement->FirstChildElement("dynamic_texture"); texture != nullptr; texture = texture->NextSiblingElement("dynamic_texture"))
			{
				lOk = CRenderStagedTexture::Load(aElement);
				if (lOk == false)
					break;
			}*/
		}
		return lOk;

	}
}