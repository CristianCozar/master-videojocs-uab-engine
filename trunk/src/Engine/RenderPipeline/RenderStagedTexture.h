#pragma once

#include "RenderCmd.h"
#include "Materials\Texture.h"
#include "Materials\Material.h"
#include "DynamicTexture.h"

namespace engine
{
	class CRenderStagedTexture : public CRenderCmd
	{
	public:
		CRenderStagedTexture();
		virtual ~CRenderStagedTexture();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM) = 0;

		void CreateRenderTargetViewVector();
		void ActivateTextures();
		void AddDynamicTextureMaterial(CDynamicTexture *DynamicTexture, CMaterial *Material);

	private: 
		DISALLOW_COPY_AND_ASSIGN(CRenderStagedTexture);

	protected:
		class CStagedTexture
		{
		public:
			uint32 m_StageID;
			CTexture* m_Texture;

			CStagedTexture(uint32 aStageID, CTexture* aTexture);
			void Activate();
		};

		class CDynamicTextureMaterial
		{
		public:
			CDynamicTexture* m_DynamicTexture;
			CMaterial* m_Material;

			CDynamicTextureMaterial(CDynamicTexture* DynamicTexture, CMaterial* Material);
			void Activate(int StageId);
		};

		std::vector<CStagedTexture*> m_StagedTextures;
		std::vector<CDynamicTextureMaterial*> m_DynamicTexturesMaterials;
		std::vector<ID3D11RenderTargetView*> m_RenderTargetViews;

		Vect2u m_ViewportPosition;
		Vect2u m_ViewportSize;


	};

}