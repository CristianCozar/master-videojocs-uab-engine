#pragma once


#include "RenderStagedTexture.h"
#include "CaptureFrameBufferTexture.h"

namespace engine
{


	class CCaptureFrameBuffer: public CRenderStagedTexture
	{
	public:
		CCaptureFrameBuffer();
		virtual ~CCaptureFrameBuffer();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CCaptureFrameBuffer);
		CCaptureFrameBufferTexture* m_CapturedFramBufferTexture;
	};


}