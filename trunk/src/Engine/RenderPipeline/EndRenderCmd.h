#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CEndRenderCmd : public CRenderCmd
	{
	public:
		CEndRenderCmd();
		virtual ~CEndRenderCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CEndRenderCmd);
	};
}