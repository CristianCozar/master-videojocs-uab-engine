#pragma once

#include "Materials\Texture.h"
#include "Utils/EnumToString.h"

XML_FORWARD_DECLARATION

namespace engine
{
	class CDynamicTexture : public CTexture
	{
	public:
		// https://msdn.microsoft.com/en-us/library/windows/desktop/bb173059(v=vs.85).aspx
		enum TFormatType
		{
			RGBA32_FLOAT = 2,
			RGBA8_UNORM = 28,
			R32_FLOAT = 41,
		};
	public:
		CDynamicTexture(const CXMLElement *TreeNode);
		CDynamicTexture(int Width, int Height, std::string Name, bool CreateDepthStencil, TFormatType type);
		virtual ~CDynamicTexture();

		GET_SET_PTR(ID3D11RenderTargetView, RenderTargetView);
		GET_SET_PTR(ID3D11DepthStencilView, DepthStencilView);

	protected:
		ID3D11Texture2D         *m_RenderTargetTexture;
		ID3D11RenderTargetView  *m_pRenderTargetView;
		ID3D11Texture2D         *m_DepthStencilBuffer;
		ID3D11DepthStencilView  *m_pDepthStencilView;
		bool                    m_CreateDepthStencilBuffer;
		TFormatType             m_FormatType;

		void Init();
		virtual bool CreateSamplerState();
	};
}
	//---------------------------------------------------------------------------------------------------------
		Begin_Enum_String(engine::CDynamicTexture::TFormatType)
		{
			Enum_String_Id(engine::CDynamicTexture::R32_FLOAT, "R32_FLOAT");
			Enum_String_Id(engine::CDynamicTexture::RGBA32_FLOAT, "RGBA32_FLOAT");
			Enum_String_Id(engine::CDynamicTexture::RGBA8_UNORM, "RGBA8_UNORM");
		}
		End_Enum_String;	
