#pragma once

#include "RenderStagedTexture.h"

namespace engine
{
	class CUnSetRenderTarget : public CRenderStagedTexture
	{
	public:
		CUnSetRenderTarget();
		virtual ~CUnSetRenderTarget();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);

	private:
		DISALLOW_COPY_AND_ASSIGN(CUnSetRenderTarget);
	};
}