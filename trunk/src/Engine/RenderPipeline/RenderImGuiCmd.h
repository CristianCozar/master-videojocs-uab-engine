#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CRenderImGuiCmd : public CRenderCmd
	{
	public:
		CRenderImGuiCmd();
		virtual ~CRenderImGuiCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CRenderImGuiCmd);
	};
}