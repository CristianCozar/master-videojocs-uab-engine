#pragma once

#include "RenderStagedTexture.h"

namespace engine
{
	class CSetRenderTarget : public CRenderStagedTexture
	{
	public:
		CSetRenderTarget();
		virtual ~CSetRenderTarget();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);

	private:
		DISALLOW_COPY_AND_ASSIGN(CSetRenderTarget);
	};
}