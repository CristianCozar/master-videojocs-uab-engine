#include "RenderSceneLayer.h"

#include "XML\XML.h"
#include "Engine.h"
#include "Scenes\SceneManager.h"

namespace engine
{
	CRenderSceneLayer::CRenderSceneLayer()
	{
		m_LayerName = "";
	}

	CRenderSceneLayer::~CRenderSceneLayer()
	{

	}

	static char buf[512] = "layername";
	bool CRenderSceneLayer::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			m_LayerName = aElement->Attribute("layer");
			strcpy_s(buf, m_LayerName.c_str());
		}
		return lOk;
	}

	void CRenderSceneLayer::Execute(CRenderManager& lRM)
	{
		CEngine::GetInstance().GetSceneManager().Render(lRM, m_LayerName);
	}
	
	void CRenderSceneLayer::RenderDebugGUI()
	{
		ImGui::InputText("UTF-8 input", buf, 512);
		if (ImGui::Button("Apply"))
		{
			m_LayerName = std::string(buf);
		}
	}
}