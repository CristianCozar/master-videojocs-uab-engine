#include "EndRenderCmd.h"

#include "RenderManager.h"

namespace engine
{
	CEndRenderCmd::CEndRenderCmd()
	{

	}

	CEndRenderCmd::~CEndRenderCmd()
	{

	}

	bool CEndRenderCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);
	}

	void CEndRenderCmd::Execute(CRenderManager& lRM)
	{
		lRM.EndRender();
	}
}