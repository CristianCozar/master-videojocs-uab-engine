#include "DrawQuad.h"
#include "Engine.h"
#include "RenderManager.h"
#include "Shaders\ShaderManager.h"
#include "Materials\MaterialManager.h"

namespace engine
{

	CDrawQuad::CDrawQuad()
	{
		mQuad = nullptr;
		mPixelShader = nullptr;
		m_Material = nullptr;
	}

	CDrawQuad::~CDrawQuad()
	{
		delete mQuad;
		//delete mPixelShader;
	}

	void CDrawQuad::Execute(CRenderManager& lRM)
	{
		lRM.SetViewport(m_ViewportPosition, m_ViewportSize);
		for (int i = 0; i < m_StagedTextures.size(); ++i)
		{
			m_StagedTextures[i]->Activate();
		}
		mQuad->Render(m_Material);
		lRM.ResetViewport();
	}

	bool CDrawQuad::Load(const CXMLElement* aElement)
	{
		mQuad = new CQuad();
		CRenderCmd::Load(aElement);
		bool lOk = CRenderStagedTexture::Load(aElement);
		CShaderManager& lSM = CEngine::GetInstance().GetShaderManager();
		
		m_Material = CEngine::GetInstance().GetMaterialManager().Get(aElement->GetAttribute<std::string>("material", ""));
		mQuad->Init();
		return lOk;
	}
}