#pragma once

#include "RenderCmd.h"

namespace engine
{
	class CRenderGuiCmd : public CRenderCmd
	{
	public:
		CRenderGuiCmd();
		virtual ~CRenderGuiCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CRenderGuiCmd);
	};
}