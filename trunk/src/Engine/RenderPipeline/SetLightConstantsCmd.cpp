#include "SetLightConstantsCmd.h"

#include "RenderManager.h"

#include "Engine.h"
#include "Lights\LightManager.h"
#include "XML\XML.h"
#include "Debug\imgui.h"

namespace engine
{
	CSetLightConstantsCmd::CSetLightConstantsCmd()
	{

	}

	CSetLightConstantsCmd::~CSetLightConstantsCmd()
	{

	}

	bool CSetLightConstantsCmd::Load(const CXMLElement* aElement)
	{
		return CRenderCmd::Load(aElement);;
	}

	void CSetLightConstantsCmd::Execute(CRenderManager& lRM)
	{
		CEngine::GetInstance().GetLightManager().SetLightsConstants();
		CEngine::GetInstance().GetLightManager().UpdateConstants();
	}

	void CSetLightConstantsCmd::RenderDebugGUI()
	{

	}
}