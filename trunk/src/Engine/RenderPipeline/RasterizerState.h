#pragma once

#include <d3d11.h>

#include "RenderCmd.h"
#include "Utils\Defines.h"
#include "Utils\EnumToString.h"

namespace engine
{
	class CSetRasterizerState : public CRenderCmd
	{
	public:
		enum EFillMode
		{
			eSolid = 0,
			eWireFrame = 1
		};
	public:
		CSetRasterizerState();
		virtual ~CSetRasterizerState();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI() override;
	private:
		DISALLOW_COPY_AND_ASSIGN(CSetRasterizerState);
		ID3D11RasterizerState* m_RasterizerState;
		D3D11_CULL_MODE m_CullMode;
		bool m_ClockWise;
		D3D11_FILL_MODE m_FillMode;
		bool CreateRasterizerState();
	};
}

Begin_Enum_String(D3D11_FILL_MODE)
{
	Enum_String_Id(D3D11_FILL_MODE::D3D11_FILL_SOLID, "solid");
	Enum_String_Id(D3D11_FILL_MODE::D3D11_FILL_WIREFRAME, "wireframe");
}
End_Enum_String;

Begin_Enum_String(D3D11_CULL_MODE)
{
	Enum_String_Id(D3D11_CULL_MODE::D3D11_CULL_BACK, "back");
	Enum_String_Id(D3D11_CULL_MODE::D3D11_CULL_FRONT, "front");
	Enum_String_Id(D3D11_CULL_MODE::D3D11_CULL_NONE, "none");
}
End_Enum_String;