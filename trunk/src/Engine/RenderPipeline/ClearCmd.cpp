#include "ClearCmd.h"

#include "XML\XML.h"
#include "RenderManager.h"

namespace engine
{
	CClearCmd::CClearCmd()
	{
		m_RenderTarget = false;
		m_DepthStencil = false;
		m_Color.SetZero();
	}

	CClearCmd::~CClearCmd()
	{

	}

	bool CClearCmd::Load(const CXMLElement* aElement)
	{
		bool lOk = CRenderCmd::Load(aElement);
		if (lOk)
		{
			m_DepthStencil = aElement->BoolAttribute("depth_stencil");
			m_RenderTarget = aElement->BoolAttribute("render_target");
			m_Color = aElement->GetAttribute<CColor>("color", CColor());
		}
		return lOk;
	}

	void CClearCmd::Execute(CRenderManager& lRM)
	{
		lRM.Clear(m_RenderTarget, m_DepthStencil, m_Color);
	}

	void CClearCmd::RenderDebugGUI()
	{
		ImGui::Checkbox("Depth stencil", &m_DepthStencil);
		ImGui::Checkbox("Render target", &m_RenderTarget);
		ImGui::ColorEdit4("Background color", &m_Color.x);
	}
}