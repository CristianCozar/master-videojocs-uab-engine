#pragma once

#include <d3d11.h>

#include "RenderCmd.h"

namespace engine
{
	
	class CBeginRenderCmd : public CRenderCmd
	{
	public:
		CBeginRenderCmd();
		virtual ~CBeginRenderCmd();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager& lRM);
		virtual void RenderDebugGUI() override;
	private:
		DISALLOW_COPY_AND_ASSIGN(CBeginRenderCmd);
	};
}