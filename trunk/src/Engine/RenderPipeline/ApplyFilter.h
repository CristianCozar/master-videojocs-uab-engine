#pragma once

#include "RenderCmd.h"
#include "DrawQuad.h"

namespace engine
{
	class CApplyFilter : public CDrawQuad
	{
	public:
		CApplyFilter();
		virtual ~CApplyFilter();
		bool Load(const CXMLElement* aElement);
		virtual void Execute(CRenderManager &RM);
	private:
		DISALLOW_COPY_AND_ASSIGN(CApplyFilter);
	};
}