#pragma once

#include <vector>

#include "Math\Vector3.h"
#include "Utils\Name.h"
#include "cal3d\cal3d.h"

class CName;

namespace engine
{
	class CMaterial;
	class CAnimatedCoreModel : public CName
	{
	public:
		CAnimatedCoreModel();
		virtual ~CAnimatedCoreModel();
		CalCoreModel *GetCoreModel() { return m_CalCoreModel; };
		const std::vector<CMaterial *> & GetMaterials() const { return m_Materials; }
		bool Load(const std::string &Path);
		bool Reload();
		int GetAnimationIndex(const std::string& aName);
	private:
		CalCoreModel *m_CalCoreModel;
		std::vector<CMaterial *> m_Materials;
		std::string m_Path;
		Vect3f m_BSPosition;
		float m_BSRadius;
		bool LoadMesh(const std::string &Filename);
		bool LoadSkeleton(const std::string &Filename);
		bool LoadAnimation(const std::string &Name, const std::string &Filename);
		void Destroy();
		std::vector<std::string> m_Animations;
	};
}