#include "Utils\TemplatedMap.h"

namespace engine {
	class CAnimatedCoreModel;
	class CAnimatedModelManager : public base::utils::CTemplatedMap<CAnimatedCoreModel>
	{
	private:
		std::string m_Filename;
	public:
		CAnimatedModelManager();
		virtual ~CAnimatedModelManager();
		void Load(const std::string &Filename);
		void Reload();
	};
}