#include "AnimatedModelManager.h"
#include "Utils\Defines.h"
#include "AnimatedModel\AnimatedCoreModel.h"

#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"
#include "Paths.h"

#include "Utils\Logger\Logger.h"

namespace engine {
	CAnimatedModelManager::CAnimatedModelManager() {
		//CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS | LOADER_INVERT_V_COORD);
		CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS);
	}
	CAnimatedModelManager::~CAnimatedModelManager() {

	}


	void CAnimatedModelManager::Load(const std::string &Filename) {
		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((PATH_ANIMATED_MODELS + Filename).c_str());
		if (base::xml::SucceedLoad(error)) {
			m_Filename = Filename;
			LOG_INFO_APPLICATION("Reading AnimatedModels");
			CXMLElement *animated_models = document.FirstChildElement("animated_models");
			if (animated_models){	
				for (CXMLElement *animated_model = animated_models->FirstChildElement("animated_model"); animated_model != nullptr; animated_model = animated_model->NextSiblingElement("animated_model")) {
					if (animated_model->GetAttribute<bool>("invert_uv", false))
					{
						CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS | LOADER_INVERT_V_COORD);
					}
					else
					{
						CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS);
					}
					CAnimatedCoreModel *acm = new CAnimatedCoreModel();
					acm->Load(animated_model->Attribute("path"));
					base::utils::CTemplatedMap<CAnimatedCoreModel>::Update(animated_model->Attribute("name"), acm);
					LOG_INFO_APPLICATION("AnimatedModel '%s' added", animated_model->Attribute("name"));
				}
			}
		}
	}

	void CAnimatedModelManager::Reload() {
		CTemplatedMap::Destroy();
		Load(m_Filename);
	}
}

