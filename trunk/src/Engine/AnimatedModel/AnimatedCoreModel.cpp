#include "AnimatedCoreModel.h"

#include "Utils\Logger\Logger.h"
#include "XML\XML.h"
#include "Paths.h"
#include "Materials\Material.h"

#include "Engine.h"
#include "Materials\MaterialManager.h"


namespace engine
{
	CAnimatedCoreModel::CAnimatedCoreModel()
	{
		m_CalCoreModel = nullptr;
		m_BSRadius = 0.0f;
		m_Animations = std::vector<std::string>();
	}

	CAnimatedCoreModel::~CAnimatedCoreModel()
	{
		Destroy();
	}

	void CAnimatedCoreModel::Destroy()
	{
		m_Materials.clear();
		delete m_CalCoreModel;
	}

	bool CAnimatedCoreModel::LoadMesh(const std::string &Filename)
	{
		return m_CalCoreModel->loadCoreMesh((m_Path + Filename).c_str());;
	}

	bool CAnimatedCoreModel::LoadSkeleton(const std::string &Filename)
	{
		return m_CalCoreModel->loadCoreSkeleton((m_Path + Filename).c_str());
	}

	bool CAnimatedCoreModel::LoadAnimation(const std::string &Name, const std::string &Filename)
	{
		m_Animations.push_back(Name);
		return m_CalCoreModel->loadCoreAnimation((m_Path + Filename).c_str());;
	}

	bool CAnimatedCoreModel::Reload()
	{
		Destroy();
		return Load(m_Path);
	}

	bool CAnimatedCoreModel::Load(const std::string &Path)
	{
		m_Path = Path;
		LOG_INFO_APPLICATION("Loading Animated Core Model from '%s'", (Path + "actor.xml").c_str());

		

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((Path + "actor.xml").c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading Animated Core Model");
			tinyxml2::XMLElement *actor = document.FirstChildElement("actor"); // actor
			if (actor) {
				m_Name = actor->GetAttribute<std::string>("name", "AnimatedCoreModel");
				m_CalCoreModel = new CalCoreModel(m_Name);
				m_BSPosition = actor->GetAttribute<Vect3f>("pos_bs", Vect3f());
				m_BSRadius = actor->GetAttribute<float>("radius_bs", 1.0f);
				for (CXMLElement *skeleton = actor->FirstChildElement("skeleton"); skeleton != nullptr; skeleton = skeleton->NextSiblingElement("skeleton")) { // Skeleton
					LoadSkeleton(skeleton->Attribute("filename"));
				}
				for (CXMLElement *mesh = actor->FirstChildElement("mesh"); mesh != nullptr; mesh = mesh->NextSiblingElement("mesh")) { // mesh
					LoadMesh(mesh->Attribute("filename"));
				}
				for (CXMLElement *material = actor->FirstChildElement("material"); material != nullptr; material = material->NextSiblingElement("material")) { // material
					CMaterial *mat = new CMaterial(material);
					m_Materials.push_back(mat);
					CEngine::GetInstance().GetMaterialManager().Add(mat->GetName(), mat);
				}
				for (CXMLElement *animation = actor->FirstChildElement("animation"); animation != nullptr; animation = animation->NextSiblingElement("animation")) { // animation
					LoadAnimation(animation->Attribute("name"), animation->Attribute("filename"));
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Animated Model XML '%s' does not exist", (Path + "actor.xml").c_str());
		}

		return base::xml::SucceedLoad(error);
	}

	int CAnimatedCoreModel::GetAnimationIndex(const std::string& aName)
	{
		for (int i = 0; i < m_Animations.size(); ++i)
		{
			if (m_Animations[i] == aName)
				return i;
		}
		return -1;
	}
}