#include "EffectManager.h"
#include "Utils\Logger\Logger.h"
#include "XML\XML.h"
#include "Utils\TemplatedMapVector.h"
#include "Paths.h"
namespace engine{

	CEffectManager::CEffectManager()
	{
		mFilename = "";
	}

	CEffectManager::~CEffectManager()
	{

	}

	bool CEffectManager::Load(const std::string& aFilename)
	{
		mFilename = aFilename;

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((PATH_BASE + mFilename).c_str());

		if (base::xml::SucceedLoad(error))
		{
			LOG_INFO_APPLICATION("Loading effects from '%s'", aFilename.c_str());
			tinyxml2::XMLElement *effects = document.FirstChildElement("effects"); //Lista de Effects
			if (effects) {
				for (tinyxml2::XMLElement *effect = effects->FirstChildElement("effect"); effect != nullptr; effect = effect->NextSiblingElement("effect"))
				{
					if (!Exist(effect->Attribute("name")))
					{
						CEffect* LoadedEffect = new CEffect(effect);
						base::utils::CTemplatedMapVector<CEffect>::Add(effect->Attribute("name"), LoadedEffect);
						LOG_INFO_APPLICATION("Effect '%s' added", effect->Attribute("name"));
					}
				}
			}
		} else {
			LOG_ERROR_APPLICATION("Effects XML '%s' does not exist", aFilename.c_str());
		}

		return base::xml::SucceedLoad(error);
	}

	bool CEffectManager::Reload()
	{
		base::utils::CTemplatedMapVector<CEffect>::Destroy();
		bool lRes = Load(mFilename);
		return lRes;
	}


}