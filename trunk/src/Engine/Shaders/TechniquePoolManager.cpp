#include "TechniquePoolManager.h"
#include "TechniquePool.h"
#include "Technique.h"
#include "Effect.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"
#include "Utils\Logger\Logger.h"
#include <string>
#include "Paths.h"

namespace engine {
	CTechniquePoolManager::CTechniquePoolManager()  {
		m_Filename = "";
	}
	CTechniquePoolManager::~CTechniquePoolManager() {
		CTechniquePoolManager::Destroy();
	}

	bool CTechniquePoolManager::Load(const std::string& aFilename) {
		base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Loading technique pools from '%s'", aFilename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((PATH_TECHNIQUES + aFilename).c_str());

		if (base::xml::SucceedLoad(error)) {
			LOG_INFO_APPLICATION("Reading technique pools");

			tinyxml2::XMLElement *technique_pools = document.FirstChildElement("technique_pools");
			if (technique_pools) {

				tinyxml2::XMLElement *default_pool = technique_pools->FirstChildElement("default_pool");
				for (tinyxml2::XMLElement *technique = default_pool->FirstChildElement("technique"); technique != nullptr; technique = technique->NextSiblingElement("technique")) {
					engine::CTechnique *default_technique = new engine::CTechnique();
					Add(technique->Attribute("vertex_type"), default_technique);
				}
				

				for (tinyxml2::XMLElement *pool = technique_pools->FirstChildElement("pool"); pool != nullptr; pool = pool->NextSiblingElement("pool")) {
					engine::CTechniquePool * technique_pool = new engine::CTechniquePool(pool);
					mPools.Add(pool->Attribute("name"), technique_pool);
				}
			}
		}
		else {
			LOG_ERROR_APPLICATION("Technique pools XML does not exist");
		}

		return base::xml::SucceedLoad(error);
	}

	bool CTechniquePoolManager::Reload() {
		Destroy();
		return Load(m_Filename);
	}

	void CTechniquePoolManager::Destroy() {
		mPools.Clear();
		CTemplatedMapVector::Destroy();
	}

	bool CTechniquePoolManager::Apply(const std::string& aPoolName)
	{
		CTechniquePool* lPool = mPools.Get(aPoolName);
		bool lOk = false;
		if (lPool)
		{
			TMapResources::iterator lItb = m_ResourcesMap.begin(), lIte = m_ResourcesMap.end();
			for (; lItb != lIte; ++lItb)
			{
				const std::string& lVertexType = lItb->first;
				CEffect* lEffect = lPool->Get(lVertexType);
				lItb->second.m_Value->SetEffect(lEffect);
			}
			//LOG_INFO_APPLICATION("Technique pool '%s' added", aPoolName);
			lOk = true;
		}
		//LOG_INFO_APPLICATION("Technique pool '%s' does not exist", aPoolName);
		return lOk;;
	}

}