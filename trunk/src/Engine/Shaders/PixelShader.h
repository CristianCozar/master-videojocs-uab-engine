#pragma once

#include "Shader.h"

namespace engine
{
	class CPixelShader : public CShader
	{
	public:
		CPixelShader(const std::string& aShaderCode);
		CPixelShader(const CXMLElement* aElement);
		virtual ~CPixelShader();
		virtual bool Load();
		virtual void Bind(ID3D11DeviceContext* aContext);
		virtual void Reload();
		virtual void LoadFromCode();
		GET_SET_PTR(ID3D11PixelShader, PixelShader);
	private:
		DISALLOW_COPY_AND_ASSIGN(CPixelShader);
		ID3D11PixelShader *m_pPixelShader;
		virtual std::string GetShaderModel();
	};
}