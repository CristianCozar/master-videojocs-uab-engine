#pragma once

#include "Effect.h"
#include "Utils\TemplatedMapVector.h"

namespace engine
{
	class CEffectManager : public base::utils::CTemplatedMapVector<CEffect>
	{
	public:
		CEffectManager();
		virtual ~CEffectManager();
		bool Load(const std::string& aFilename);
		bool Reload();

	private:
		DISALLOW_COPY_AND_ASSIGN(CEffectManager);
		std::string mFilename;
	};
}