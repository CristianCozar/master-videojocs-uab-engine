#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Utils\Defines.h"

namespace engine {
	class CTechnique;
	class CTechniquePool;

	class CTechniquePoolManager : public base::utils::CTemplatedMapVector<CTechnique>
	{
	public:
		CTechniquePoolManager();
		virtual ~CTechniquePoolManager();
		bool Load(const std::string& aFilename);
		bool Reload();
		bool Apply(const std::string& aPoolName);
	private:
		void Destroy();
		DISALLOW_COPY_AND_ASSIGN(CTechniquePoolManager);
		std::string m_Filename;
		typedef base::utils::CTemplatedMapVector<CTechniquePool> TMapPools;
		TMapPools mPools;
	};
}