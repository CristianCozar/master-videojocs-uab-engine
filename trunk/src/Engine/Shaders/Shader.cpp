#include "Shader.h"

#include <fstream>
#include <d3dcompiler.h>

#include "Utils\Types.h"
#include "Utils\CheckedDelete.h"
#include "Utils\Logger\Logger.h"
#include "Paths.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "Utils\StringUtils.h"

#include "Debug\imgui.h"

namespace engine {
	namespace ShaderUtils {
		static std::string sShadersDirectory = PATH_SHADERS;

		class CShaderInclude : public ID3DInclude
		{
		public:
			CShaderInclude() {}
			HRESULT __stdcall Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData,
				LPCVOID *ppData, UINT *pBytes)
			{
				std::ifstream lBinaryFile(sShadersDirectory + pFileName, std::ios::in |
					std::ios::binary | std::ios::ate);
				HRESULT lHR = E_FAIL;
				if (lBinaryFile.is_open())
				{
					std::streamoff fileSize = lBinaryFile.tellg();
					char* buf = new char[(uint32)fileSize];
					lBinaryFile.seekg(0, std::ios::beg);
					lBinaryFile.read(buf, fileSize);
					lBinaryFile.close();
					*ppData = buf;
					*pBytes = (UINT)fileSize;
					lHR = S_OK;
				}
				return lHR;
			}
			HRESULT __stdcall Close(LPCVOID pData)
			{
				char* buf = (char*)pData;
				base::utils::DeleteArray(buf);
				return S_OK;
			}
		};

		ID3DBlob* CompileShader(const std::string& aShader,
			const std::string& aEntryPoint,
			const std::string& aShaderModel,
			const D3D_SHADER_MACRO* aDefines)
		{
			UINT lFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
			lFlags |= D3DCOMPILE_DEBUG;
#endif
			ID3DBlob* lErrorBlob = nullptr;
			ID3DBlob* lShaderBlob = nullptr;
			CShaderInclude* lShaderInclude = new CShaderInclude();
			HRESULT lHR = D3DCompile(aShader.c_str(), aShader.size(),
				sShadersDirectory.c_str(),
				aDefines,
				lShaderInclude,
				aEntryPoint.c_str(), aShaderModel.c_str(),
				0, 0, &lShaderBlob, &lErrorBlob);
			if (FAILED(lHR))
			{
				LOG_ERROR_APPLICATION("Error compiling shader\n\n");
				if (lErrorBlob)
				{
					LOG_ERROR_APPLICATION("%s\n --------- %s \n ---------",
						(char*)lErrorBlob->GetBufferPointer(), aShader.c_str());
				}
				// TODO
				if (lErrorBlob)
					lErrorBlob->Release();
				if (lShaderBlob)
					lShaderBlob->Release();
				//CRenderManager::Release(lErrorBlob);
				//CRenderManager::Release(lShaderBlob);
			}
			base::utils::CheckedDelete(lShaderInclude);
			return lShaderBlob;
		}
	}

	CShader::CShader(const std::string& aShaderCode, ShaderStage aType) :
		m_ShaderCode(aShaderCode), m_Type(aType)
	{
		m_Preprocessor = "";
		m_Filename = "";
	}

	CShader::CShader(const CXMLElement* aElement, ShaderStage aType) :
		CName(aElement), m_Type(aType), m_Filename(aElement->Attribute("file")), m_EntryPoint(aElement->Attribute("entry_point")), m_Preprocessor(aElement->Attribute("preprocessor"))
	{
		m_ShaderCode = "";
		CreateShaderMacro();
	}

	CShader::~CShader()
	{
		if (m_pBlob)
			m_pBlob->Release();
		if (m_ShaderMacros)
			delete m_ShaderMacros;
	}

	bool CShader::Save()
	{
		bool lOk = false;
		if (!m_Filename.empty() && !m_ShaderCode.empty())
		{
			std::ofstream ofs(PATH_SHADERS + m_Filename, std::ofstream::out);

			ofs << m_ShaderCode;

			ofs.close();
			lOk = true;
			LOG_INFO_APPLICATION("Shader guardado en %s", (PATH_SHADERS + m_Filename).c_str());
		}
		return lOk;
	}

	bool CShader::Load()
	{
		CShader::LoadFromFile();
		CShader::LoadFromCode();
		LOG_WARNING_APPLICATION_IF(m_pBlob, "El Shader %s en stage %s no ha podido ser cargado.", GetName().c_str(), EnumString<ShaderStage>::ToStr(m_Type).c_str());
		LOG_INFO_APPLICATION_IF(!m_pBlob, "El Shader %s en stage %s ha sido cargado.", GetName().c_str(), EnumString<ShaderStage>::ToStr(m_Type).c_str());
		return m_pBlob != nullptr;
	}

	void CShader::LoadFromFile()
	{
		if (!m_Filename.empty())
		{
			std::ifstream lStream(PATH_SHADERS + m_Filename);
			m_ShaderCode = std::string((std::istreambuf_iterator<char>(lStream)), (std::istreambuf_iterator<char>()));
			LOG_INFO_APPLICATION("Shader cargado de %s", (PATH_SHADERS + m_Filename).c_str());
		}
	}

	void CShader::LoadFromCode()
	{
		if (!m_ShaderCode.empty())
		{
			const D3D_SHADER_MACRO lDefines[] =
			{
				"EXAMPLE_DEFINE", "1",
				NULL, NULL
			};
			m_pBlob = ShaderUtils::CompileShader(m_ShaderCode, m_EntryPoint, GetShaderModel(), m_ShaderMacros);
		}
	}

	void CShader::Reload()
	{
	}

	static char buf[16384] = "Shader code";

	void CShader::RenderDebugGUI()
	{
		if (m_DebugUi)
		{
			ImGui::Begin(GetName().c_str(), &m_DebugUi, ImGuiWindowFlags_AlwaysAutoResize);
			strcpy_s(buf, m_ShaderCode.c_str());
			static bool read_only = false;
			ImGui::InputTextMultiline("UTF-8 input", buf, 16384, ImVec2(800,400));
			m_ShaderCode = std::string(buf);
			if (ImGui::Button("Apply"))
			{
				Reload();
			}
			if (ImGui::Button("Save"))
			{
				Save();
			}
			
			ImGui::End();
		}
	}

	void CShader::CreateShaderMacro()
	{
		m_PreprocessorMacros.clear();
		if (m_Preprocessor.empty())
		{
			m_ShaderMacros = NULL;
			return;
		}
		std::vector<std::string> l_PreprocessorItems = base::utils::Split(m_Preprocessor, '|');
		m_ShaderMacros = new D3D10_SHADER_MACRO[l_PreprocessorItems.size() + 1];
		for (size_t i = 0; i<l_PreprocessorItems.size(); ++i)
		{
			std::vector<std::string> l_PreprocessorItem = base::utils::Split(
				l_PreprocessorItems[i], '=');
			if (l_PreprocessorItem.size() == 1)
			{
				m_PreprocessorMacros.push_back(l_PreprocessorItems[i]);
				m_PreprocessorMacros.push_back("1");
			}
			else if (l_PreprocessorItem.size() == 2)
			{
				m_PreprocessorMacros.push_back(l_PreprocessorItem[0]);
				m_PreprocessorMacros.push_back(l_PreprocessorItem[1]);
			}
			else
			{
				base::utils::DeleteArray(m_ShaderMacros);
				return;
			}
		}
		for (size_t i = 0; i<l_PreprocessorItems.size(); ++i)
		{
			m_ShaderMacros[i].Name = m_PreprocessorMacros[i * 2].c_str();
			m_ShaderMacros[i].Definition = m_PreprocessorMacros[(i * 2) + 1].c_str();
		}
		m_ShaderMacros[l_PreprocessorItems.size()].Name = NULL;
		m_ShaderMacros[l_PreprocessorItems.size()].Definition = NULL;
	}
}