#include "GeometryShader.h"

#include "Engine.h"
#include "RenderManager.h"

namespace engine
{
	CGeometryShader::CGeometryShader(const std::string& aShaderCode) :
		CShader(aShaderCode, ShaderStage::eGeometryShader)
	{
		m_pGeometryShader = nullptr;
	}

	CGeometryShader::CGeometryShader(const CXMLElement* aElement) :
		CShader(aElement, ShaderStage::eGeometryShader)
	{
		m_pGeometryShader = nullptr;
	}

	CGeometryShader::~CGeometryShader()
	{
		if (m_pGeometryShader)
			m_pGeometryShader->Release();
	}

	bool CGeometryShader::Load()
	{
		bool lOk = CShader::Load();
		if (lOk)
		{
			CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
			ID3D11Device *l_Device = lRenderManager.GetDevice();
			HRESULT lHR = l_Device->CreateGeometryShader(m_pBlob->GetBufferPointer(), m_pBlob->GetBufferSize(), nullptr, &m_pGeometryShader);
			lOk = SUCCEEDED(lHR);
		}
		return lOk;
	}

	void CGeometryShader::Bind(ID3D11DeviceContext* aContext)
	{
		aContext->GSSetShader(m_pGeometryShader, NULL, 0);
	}

	void CGeometryShader::Reload()
	{
		if (m_pGeometryShader)
			m_pGeometryShader->Release();
		if (m_pBlob)
			m_pBlob->Release();
		LoadFromCode();
	}

	void CGeometryShader::LoadFromCode()
	{
		CShader::LoadFromCode();
		CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
		ID3D11Device *l_Device = lRenderManager.GetDevice();
		HRESULT lHR = l_Device->CreateGeometryShader(m_pBlob->GetBufferPointer(), m_pBlob->GetBufferSize(), nullptr, &m_pGeometryShader);
	}

	std::string CGeometryShader::GetShaderModel()
	{
		// Query the current feature level:
		D3D_FEATURE_LEVEL featureLevel = CEngine::GetInstance().GetRenderManager().GetDevice() ->GetFeatureLevel();
		switch (featureLevel)
		{
		case D3D_FEATURE_LEVEL_11_1:
		case D3D_FEATURE_LEVEL_11_0:
		{
			return "gs_5_0";
		}
		break;
		case D3D_FEATURE_LEVEL_10_1:
		{
			return "gs_4_1";
		}
		break;
		case D3D_FEATURE_LEVEL_10_0:
		{
			return "gs_4_0";
		}
			break;
		case D3D_FEATURE_LEVEL_9_3:
		{
			return "gs_4_0_level_9_3";
		}
		break;
		case D3D_FEATURE_LEVEL_9_2:
		case D3D_FEATURE_LEVEL_9_1:
		{
			return "gs_4_0_level_9_1";
		}
		break;
		}
		return "";
	}
}