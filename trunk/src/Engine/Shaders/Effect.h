#pragma once

#include <vector>

#include "Utils\Name.h"
#include "Shader.h"


namespace engine
{
	class CEffect : public CName
	{
	public:
		CEffect();
		CEffect(const CXMLElement* aElement);
		virtual ~CEffect();
		void SetShader(engine::CShader::EShaderStage aType, engine::CShader* aShader);

		template <typename TShaderType> TShaderType* GetShader(engine::CShader::EShaderStage aType) const
		{
			return static_cast<TShaderType*>(mShaders[aType]);
		}

		void Bind(ID3D11DeviceContext* aContext);

		std::vector<engine::CShader*> mShaders;

	private:
		DISALLOW_COPY_AND_ASSIGN(CEffect);
	};
}