#include "Effect.h"

#include "XML\tinyxml2\tinyxml2.h"
#include "Shader.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "ShaderManager.h"
#include "Engine.h"

namespace engine {

	CEffect::CEffect(const CXMLElement* aElement) : CName(aElement)
	{
		m_Name = aElement->Attribute("name");
		engine::CShaderManager& lSM = engine::CEngine::GetInstance().GetShaderManager();
		for (const tinyxml2::XMLElement *shadElement = aElement->FirstChildElement("shader"); shadElement != nullptr; shadElement = shadElement->NextSiblingElement("shader"))
		{
			engine::CShader::EShaderStage lShaderStage;
			const char* lname = shadElement->Attribute("name");
			const char* lstage = shadElement->Attribute("stage");

			if (std::strcmp(lstage, "vertex") == 0)
			{
				lShaderStage = engine::CShader::EShaderStage::eVertexShader;
			}
			else{
				if (std::strcmp(lstage, "pixel") == 0)
				{
					lShaderStage = engine::CShader::EShaderStage::ePixelShader;
				}
				else{
					if (std::strcmp(lstage, "geometry") == 0)
					{
						lShaderStage = engine::CShader::EShaderStage::eGeometryShader;
					}
				}
			}

			CShader* lShader = lSM.GetShader(lShaderStage, lname);
			mShaders.push_back(lShader);
		}
	}

	CEffect::CEffect()
	{

	}

	CEffect::~CEffect()
	{
		mShaders.clear();
	}

	void CEffect::SetShader(engine::CShader::EShaderStage aType, engine::CShader* aShader)
	{
		if (aType < mShaders.capacity())
		{
			mShaders[aType] = aShader;
		}
		else {
			mShaders.push_back(aShader);
		}
		
	}

	void CEffect::Bind(ID3D11DeviceContext* aContext)
	{		
		aContext->VSSetShader(nullptr, nullptr, 0);
		aContext->PSSetShader(nullptr, nullptr, 0);
		aContext->GSSetShader(nullptr, nullptr, 0);

		for (unsigned int i = 0;  i < mShaders.size(); ++i)
		{
			if (mShaders[i]!=nullptr)
				mShaders[i]->Bind(aContext);
		}		
	}
}