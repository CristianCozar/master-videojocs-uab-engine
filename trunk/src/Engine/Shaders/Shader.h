#pragma once

#include <d3d11.h>
#include <vector>

#include "Utils\Name.h"
#include "Utils\EnumToString.h"

namespace engine
{
	class CShader : public CName
	{
	public:
		enum EShaderStage
		{
			eVertexShader = 0,
			ePixelShader,
			eGeometryShader,
			EStageCount
		};
	public:
		CShader(const std::string& aShaderCode, EShaderStage aType);
		CShader(const CXMLElement* aElement, EShaderStage aType);
		virtual ~CShader();
		virtual bool Load();
		virtual void LoadFromCode();
		virtual void LoadFromFile();
		virtual void Reload();
		virtual bool Save();
		virtual void Bind(ID3D11DeviceContext*) = 0;
		GET_SET(EShaderStage, Type)
		GET_SET_REF(std::string, Preprocessor)
		GET_SET_REF(std::string, EntryPoint)
		GET_SET_REF(std::string, Filename)
		GET_SET_REF(std::string, ShaderCode)
		GET_SET_PTR(ID3DBlob, Blob)
		GET_SET_BOOL(DebugUi);

		virtual void RenderDebugGUI();
	protected:
		DISALLOW_COPY_AND_ASSIGN(CShader);
		EShaderStage m_Type;
		std::string m_Preprocessor;
		std::string m_EntryPoint;
		std::string m_Filename;
		std::string m_ShaderCode;
		D3D_SHADER_MACRO *m_ShaderMacros;
		std::vector<std::string> m_PreprocessorMacros;
		ID3DBlob *m_pBlob;
		bool m_DebugUi;
		void CreateShaderMacro();
		virtual std::string GetShaderModel() = 0;
	};

	
}

Begin_Enum_String(engine::CShader::EShaderStage)
{
	Enum_String_Id(engine::CShader::eVertexShader, "vertex");
	Enum_String_Id(engine::CShader::eGeometryShader, "geometry");
	Enum_String_Id(engine::CShader::ePixelShader, "pixel");
}
End_Enum_String;

#define ShaderStage engine::CShader::EShaderStage