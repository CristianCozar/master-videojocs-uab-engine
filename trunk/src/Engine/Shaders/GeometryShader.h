#pragma once

#include "Shader.h"

namespace engine
{
	class CGeometryShader : public CShader
	{
	public:
		CGeometryShader(const std::string& aShaderCode);
		CGeometryShader(const CXMLElement* aElement);
		virtual ~CGeometryShader();
		virtual bool Load();
		virtual void Bind(ID3D11DeviceContext* aContext);
		virtual void Reload();
		virtual void LoadFromCode();
		GET_SET_PTR(ID3D11GeometryShader, GeometryShader);
	private:
		DISALLOW_COPY_AND_ASSIGN(CGeometryShader);
		ID3D11GeometryShader *m_pGeometryShader;
		virtual std::string GetShaderModel();
	};
}