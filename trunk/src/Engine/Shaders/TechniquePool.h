#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Utils\Name.h"

namespace engine {
	class CEffect;

	class CTechniquePool : public base::utils::CTemplatedMapVector<CEffect>, public CName
	{
	public:
		CTechniquePool(CXMLElement* aElement);
		virtual ~CTechniquePool();
	private:
		DISALLOW_COPY_AND_ASSIGN(CTechniquePool);
	};
}