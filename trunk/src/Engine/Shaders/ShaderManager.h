#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Shader.h"

namespace engine
{
	class CShaderManager
	{
		public:
			CShaderManager();
			virtual ~CShaderManager();
			bool Load(const std::string& aFilename);
			bool Reload();
			CShader* GetShader(ShaderStage aStage, const std::string& aShaderName);
			virtual void RenderDebugGUI();
		private:
			DISALLOW_COPY_AND_ASSIGN(CShaderManager);
			void LoadStage(ShaderStage aStage, CXMLElement *aShaderStage);
			void Destroy();
			typedef base::utils::CTemplatedMapVector<CShader> TShadersLibrary;
			typedef std::vector<TShadersLibrary> TShadersVectorLibrary;
			TShadersVectorLibrary m_Library;
			std::string m_Filename;
	};
}