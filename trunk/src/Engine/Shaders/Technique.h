#pragma once

#include "Utils\Defines.h"
#include <d3d11.h>

namespace engine {
	class CEffect;

	class CTechnique
	{
	public:
		CTechnique();
		virtual ~CTechnique();
		void SetEffect(CEffect* aEffect);
		CEffect* GetEffect() const;
		void Bind(ID3D11DeviceContext* aContext);
	private:
		DISALLOW_COPY_AND_ASSIGN(CTechnique);
		CEffect* mEffect;
	};
}