#include "ShaderManager.h"

#include "Utils\CheckedDelete.h"
#include "XML\XML.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "GeometryShader.h"
#include "Paths.h"

#include "Debug\imgui.h"
#include "Utils\EnumToString.h"

namespace engine
{
	CShaderManager::CShaderManager()
	{
		m_Library = std::vector<TShadersLibrary>(ShaderStage::EStageCount);
		m_Filename = "";
	}

	CShaderManager::~CShaderManager()
	{
		Destroy();
	}

	bool CShaderManager::Load(const std::string& aFilename)
	{
		bool lOk = false;
		m_Filename = aFilename;
		CXMLDocument lDocument;
		tinyxml2::XMLError error = lDocument.LoadFile((PATH_BASE + aFilename).c_str());
		if (base::xml::SucceedLoad(error))
		{
			lOk = true;
			CXMLElement *lShaders = lDocument.FirstChildElement("shaders");
			std::string shadersPath = lShaders->Attribute("path");
			// Vertex shaders
			CXMLElement *lVertexShaders = lShaders->FirstChildElement("vertex_shaders");
			// Geometry shaders
			CXMLElement *lGeometryShaders = lShaders->FirstChildElement("geometry_shaders");
			// Pixel shaders
			CXMLElement *lPixelShaders = lShaders->FirstChildElement("pixel_shaders");

			LoadStage(ShaderStage::eVertexShader, lVertexShaders);
			LoadStage(ShaderStage::eGeometryShader, lGeometryShaders);
			LoadStage(ShaderStage::ePixelShader, lPixelShaders);
		}
		return lOk;
	}

	void CShaderManager::LoadStage(ShaderStage aStage, CXMLElement *aShaderStage)
	{
		for (CXMLElement *shader = aShaderStage->FirstChildElement(); shader != nullptr; shader = shader->NextSiblingElement())
		{
			CShader *newShader = nullptr;
			switch (aStage)
			{
				case ShaderStage::eVertexShader:
					newShader = new CVertexShader(shader);
					break;
				case ShaderStage::eGeometryShader:
					newShader = new CGeometryShader(shader);
					break;
				case ShaderStage::ePixelShader:
					newShader = new CPixelShader(shader);
					break;
				default:
					break;
			}
			if (newShader != nullptr)
			{
				if (newShader->Load())
				{
					m_Library[aStage].Add(newShader->GetName(), newShader);
				}
			}
		}
	}

	CShader* CShaderManager::GetShader(ShaderStage aStage, const std::string& aShaderName)
	{
		return m_Library[aStage].Get(aShaderName);
	}

	void CShaderManager::Destroy()
	{
		for (unsigned int i = 0; i < m_Library.size(); ++i)
		{
			m_Library[i].Destroy();
		}
		m_Library.clear();
	}

	bool CShaderManager::Reload()
	{
		bool lOk = false;
		for (unsigned int i = 0; i < ShaderStage::EStageCount; ++i)
		{
			for (unsigned int j = 0; j < m_Library[i].GetCount(); ++j)
			{
				lOk = m_Library[i][j]->Load();
			}
		}
		return lOk;
	}

	
	void CShaderManager::RenderDebugGUI()
	{
		if (ImGui::Button("Reload"))
		{
			Reload();
		}
		for (unsigned int i = 0; i < m_Library.size()-1; ++i)
		{
			std::string fase = EnumString<ShaderStage>::ToStr((ShaderStage)i);
			TShadersLibrary *tsl = &m_Library[i];
			ImGui::ListBoxHeader(fase.c_str());
			for (unsigned int k = 0; k < (int)tsl->GetCount(); ++k)
			{
				tsl->GetIndex(k)->RenderDebugGUI();
				if (ImGui::Button(tsl->GetIndex(k)->GetName().c_str()))
				{
					tsl->GetIndex(k)->SetDebugUi(true);
				}
			}
			ImGui::ListBoxFooter();
		}
	}
}