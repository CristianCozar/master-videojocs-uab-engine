#include "TechniquePool.h"
#include "Effect.h"
#include "EffectManager.h"
#include "Engine.h"
#include "Utils\Logger\Logger.h"

#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"

namespace engine {
	CTechniquePool::CTechniquePool(tinyxml2::XMLElement* pool) : CName(pool) {
		for (tinyxml2::XMLElement *technique = pool->FirstChildElement("technique"); technique != nullptr; technique = technique->NextSiblingElement("technique")) {
			const std::string &name = technique->Attribute("effect");
			engine::CEffectManager& lEM = engine::CEngine::GetInstance().GetEffectManager();
			if (lEM.Exist(name)) {
				engine::CEffect *effect = lEM.Get(name);
				Add(technique->Attribute("vertex_type"), effect);

				LOG_INFO_APPLICATION("Effect '%s' from the technique pool '%s' added", name.c_str(), pool->Attribute("name"));
			}
			else {
				LOG_ERROR_APPLICATION("Effect '%s' from the technique pool '%s' does not exist", name.c_str(), pool->Attribute("name"));
			}
		}
	}
	CTechniquePool::~CTechniquePool() {

	}
}