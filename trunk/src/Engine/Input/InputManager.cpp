#include "InputManager.h"
#include "Utils\Logger\Logger.h"

#include <windowsx.h>

#include "Gamepad.h"

namespace engine {

	float ProcessXInputStickValue(SHORT value, SHORT deadZoneThreshold)
	{
		float result = 0;

		if (value < -deadZoneThreshold)
		{
			result = (float)((value + deadZoneThreshold) / (32768.0f - deadZoneThreshold));
		}
		else if (value > deadZoneThreshold)
		{
			result = (float)((value - deadZoneThreshold) / (32767.0f - deadZoneThreshold));
		}

		return result;
	}

	float ProcessXInputTriggerValue(BYTE value, BYTE deadZoneThreshold)
	{
		float result = 0;

		if (value > deadZoneThreshold)
		{
			result = (float)((value - deadZoneThreshold) / 255.0f - deadZoneThreshold);
		}
		return result;
	}

	CInputManager::CInputManager()
	{

	}

	CInputManager::~CInputManager()
	{
		int m_MouseMovementX = 0;
		int m_MouseMovementY = 0;
		int m_MouseMovementZ = 0;
		int m_MouseX = 0;
		int m_MouseY = 0;
		bool m_ButtonRight = false, m_PreviousButtonRight = false;
		bool m_ButtonLeft = false, m_PreviousButtonLeft = false;
		bool m_ButtonMiddle = false, m_PreviousButtonMiddle = false;
		BOOL m_HighPrecisionMouseRegistered = 0;
		m_HasGamepad[MAX_PLAYERS] = { 0 };
		m_GamepadStickLeftX[MAX_PLAYERS] = { 0.0f }, m_GamepadStickLeftY[MAX_PLAYERS] = { 0.0f };
		m_GamepadStickRightX[MAX_PLAYERS] = { 0.0f }, m_GamepadStickRightY[MAX_PLAYERS] = { 0.0f };
		// Gatillos
		m_GamepadTL[MAX_PLAYERS] = { 0.0f };
		m_GamepadTR[MAX_PLAYERS] = { 0.0f };
	}

	void CInputManager::Init(const HWND &hWnd)
	{
		// Inicializamos mouse de alta precisión
		RAWINPUTDEVICE Rid[1];
		Rid[0].usUsagePage = 0x01;
		Rid[0].usUsage = 0x02;
		Rid[0].dwFlags = RIDEV_INPUTSINK;
		Rid[0].hwndTarget = hWnd;
		m_HighPrecisionMouseRegistered = (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) != 0);

		m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
		m_KeyboardState = {};


		// Inicializamos gamepads
		HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
		if (!XInputLibrary)
		{
			XInputLibrary = LoadLibraryA("xinput9_1_0.dll");
		}
		if (!XInputLibrary)
		{
			XInputLibrary = LoadLibraryA("xinput1_3.dll");
		}

		if (XInputLibrary)
		{
			InputGetState = (TInputGetState *)GetProcAddress(XInputLibrary, "XInputGetState");
			if (!InputGetState)
			{
				InputGetState = InputGetStateStub;
			}

			InputSetState = (TInputSetState *)GetProcAddress(XInputLibrary, "XInputSetState");
			if (!InputSetState)
			{
				InputSetState = InputSetStateStub;
			}
		}
	}

	bool CInputManager::IsGamepadConnected(int player)
	{
		return m_HasGamepad[player];
	}

	void CInputManager::Update() {
	}

	void CInputManager::PreUpdate(bool isWindowActive)
	{
		m_PreviousKeyboardState = m_KeyboardState;

		m_PreviousButtonLeft = m_ButtonLeft;
		m_PreviousButtonMiddle = m_ButtonMiddle;
		m_PreviousButtonRight = m_ButtonRight;

		m_PreviousButtonLeft = m_ButtonLeft;
		m_PreviousButtonMiddle = m_ButtonMiddle;
		m_PreviousButtonRight = m_ButtonRight;

		m_MouseMovementX = m_MouseMovementY = m_MouseMovementZ = 0;

		for (int i = 0; i < MAX_PLAYERS; i++)
		{
			m_PreviousGamepad[i] = m_Gamepad[i];
			m_GamepadStickLeftX[i] = m_GamepadStickLeftY[i] = m_GamepadStickRightX[i] = m_GamepadStickRightY[i] = m_GamepadTL[i] = m_GamepadTR[i] = 0.0f;
		}

		if (!isWindowActive)
		{
			m_KeyboardState = {};
			m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
		}
	}

	void CInputManager::PostUpdate()
	{
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			XINPUT_STATE l_ControllerState;
			if (InputGetState(i, &l_ControllerState) == ERROR_SUCCESS)
			{
				m_HasGamepad[i] = true;
				m_Gamepad[i].SetButtons(l_ControllerState);

				m_GamepadStickLeftX[i] = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
				m_GamepadStickLeftY[i] = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
				m_GamepadStickRightX[i] = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
				m_GamepadStickRightY[i] = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

				m_GamepadTL[i] = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bLeftTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
				m_GamepadTR[i] = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bRightTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
			}
			else {
				m_HasGamepad[i] = false;
			}
		}

	}

	bool CInputManager::HandleKeyboard(const MSG& msg)
	{
		unsigned char VKCode = (unsigned char)msg.wParam;

		bool wasDown = ((msg.lParam & (1 << 30)) != 0);
		bool isDown = ((msg.lParam & (1 << 31)) == 0);

		int repeat = msg.lParam & 0xffff;
		bool altKeyWasDown = ((msg.lParam & (1 << 29)) != 0);

		/*if (wasDown != isDown) {
			if (isDown) {
				m_KeyboardState.SetKey(VKCode, isDown);
				return true;
			}
			else {
				m_KeyboardState = {};
				return true;
			}
		}*/

		if (wasDown != isDown)
		{
			m_KeyboardState.SetKey(VKCode, isDown);
			return true;
		}

		return false;
	}

	bool CInputManager::HandleMouse(const MSG& msg)
	{
		switch (msg.message)
		{
		case WM_LBUTTONDOWN:
			m_ButtonLeft = true;
			return true;
		case WM_MBUTTONDOWN:
			m_ButtonMiddle = true;
			return true;
		case WM_RBUTTONDOWN:
			m_ButtonRight = true;
			return true;
		case WM_LBUTTONUP:
			m_ButtonLeft = false;
			return true;
		case WM_MBUTTONUP:
			m_ButtonMiddle = false;
			return true;
		case WM_RBUTTONUP:
			m_ButtonRight = false;
			return true;
		case WM_MOUSEMOVE:
			//m_MouseX = GET_X_LPARAM(msg.lParam); // Mejor que LOWORD, requiere windowsx.h
			//m_MouseY = GET_Y_LPARAM(msg.lParam); // Mejor que HIWORD, requiere widnwosx.h
			POINT cursor;
			if (GetCursorPos(&cursor))
			{
				if (ScreenToClient(msg.hwnd, &cursor))
				{
					m_MouseX = cursor.x;
					m_MouseY = cursor.y;
				}
			}
			return true;
		case WM_MOUSEWHEEL:
			m_MouseMovementZ = GET_WHEEL_DELTA_WPARAM(msg.wParam);
			return true;
		case WM_INPUT:
			UINT dwSize = 60;
			static BYTE  lpb[60];

			UINT read = GetRawInputData((HRAWINPUT)msg.lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));

			RAWINPUT *raw = (RAWINPUT*)lpb;

			if (raw->header.dwType == RIM_TYPEMOUSE && read <= dwSize)
			{
				m_MouseMovementX += raw->data.mouse.lLastX;
				m_MouseMovementY += raw->data.mouse.lLastY;
				return true;
			}
			return false;
		}

		return false;
	}

	void CInputManager::SetLastChar(WPARAM wParam) {
		unsigned char VKCode = (unsigned char)wParam;
		m_KeyboardState.SetLastChar(VKCode);
	}

	char CInputManager::ConsumeLastChar() {
		char lastChar = m_KeyboardState.lastChar;
		m_KeyboardState.SetLastChar('\0'); //Null character
		return lastChar;	
	}

	bool CInputManager::IsLeftMouseButtonPressed() const
	{
		return m_ButtonLeft;
	}

	bool CInputManager::IsRightMouseButtonPressed() const
	{
		return m_ButtonRight;
	}

	bool CInputManager::IsMiddleMouseButtonPressed() const
	{
		return m_ButtonMiddle;
	}

	bool CInputManager::IsKeyPressed(unsigned char keyCode) const
	{
		return m_KeyboardState.raw[keyCode];
	}

	bool CInputManager::KeyBecomesPressed(unsigned char keyCode) const
	{
		return (!m_PreviousKeyboardState.raw[keyCode] && m_KeyboardState.raw[keyCode]);
	}

	bool CInputManager::KeyBecomesReleased(unsigned char keyCode) const
	{
		return (m_PreviousKeyboardState.raw[keyCode] && !m_KeyboardState.raw[keyCode]);
	}

	bool CInputManager::AnyKeyBecomesReleased() const {
		return  (!m_KeyboardState.anyKeyDown && m_PreviousKeyboardState.anyKeyDown);
	}

	bool CInputManager::AnyKeyBecomesPressed() const {
		return (m_KeyboardState.anyKeyDown && !m_PreviousKeyboardState.anyKeyDown);
	}

	int CInputManager::GetMouseMovementX() const
	{
		return m_MouseMovementX;
	}

	int CInputManager::GetMouseMovementY() const
	{
		return m_MouseMovementY;
	}

	int CInputManager::GetMouseMovementZ() const
	{
		return m_MouseMovementZ;
	}

	int CInputManager::GetMouseAxis(InputDefinitions::MouseAxis axis) const
	{
		switch (axis)
		{
		case InputDefinitions::DX:
			return GetMouseMovementX();
		case InputDefinitions::DY:
			return GetMouseMovementY();
		case InputDefinitions::DZ:
			return GetMouseMovementZ();
		default:
			return 0;
		}
	}

	int CInputManager::GetMouseX() const
	{
		return m_MouseX;
	}

	int CInputManager::GetMouseY() const
	{
		return m_MouseY;
	}

	bool CInputManager::LeftMouseButtonBecomesPressed() const
	{
		return (!m_PreviousButtonLeft && m_ButtonLeft);
	}

	bool CInputManager::RightMouseButtonBecomesPressed() const
	{
		return (!m_PreviousButtonRight && m_ButtonRight);
	}

	bool CInputManager::MiddleMouseButtonBecomesPressed() const
	{
		return (!m_PreviousButtonMiddle && m_ButtonMiddle);
	}

	bool CInputManager::LeftMouseButtonBecomesReleased() const
	{
		return (m_PreviousButtonLeft && !m_ButtonLeft);
	}

	bool CInputManager::RightMouseButtonBecomesReleased() const
	{
		return (m_PreviousButtonRight && !m_ButtonRight);
	}

	bool CInputManager::MiddleMouseButtonBecomesReleased() const
	{
		return (m_PreviousButtonMiddle && !m_ButtonMiddle);
	}

	bool CInputManager::IsMouseButtonPressed(InputDefinitions::MouseButton button) const
	{
		switch (button)
		{
		case InputDefinitions::MouseButton::LEFT:
			return IsLeftMouseButtonPressed();
		case InputDefinitions::MouseButton::RIGHT:
			return IsRightMouseButtonPressed();
		case InputDefinitions::MouseButton::MIDDLE:
			return IsMiddleMouseButtonPressed();
		default:
			return false;
		}
	}

	bool CInputManager::MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const
	{
		switch (button)
		{
		case InputDefinitions::MouseButton::LEFT:
			return LeftMouseButtonBecomesPressed();
		case InputDefinitions::MouseButton::RIGHT:
			return RightMouseButtonBecomesPressed();
		case InputDefinitions::MouseButton::MIDDLE:
			return MiddleMouseButtonBecomesPressed();
		default:
			return false;
		}
	}

	bool CInputManager::MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const
	{
		switch (button)
		{
		case InputDefinitions::MouseButton::LEFT:
			return LeftMouseButtonBecomesReleased();
		case InputDefinitions::MouseButton::RIGHT:
			return RightMouseButtonBecomesReleased();
		case InputDefinitions::MouseButton::MIDDLE:
			return MiddleMouseButtonBecomesReleased();
		default:
			return false;
		}
	}

	bool CInputManager::IsGamepadButtonPressed(InputDefinitions::GamepadButton button, int player)
	{
		if (!m_HasGamepad[player]) return false;
		return m_Gamepad[player].GetButton(button);
	}

	bool CInputManager::GamepadButtonBecomesPressed(InputDefinitions::GamepadButton button, int player)
	{
		if (!m_HasGamepad[player]) return false;
		bool prev = m_PreviousGamepad[player].GetButton(button);
		bool act = m_Gamepad[player].GetButton(button);
		return (!prev && act);
	}

	bool CInputManager::GamepadButtonBecomesReleased(InputDefinitions::GamepadButton button, int player)
	{
		if (!m_HasGamepad[player]) return false;
		return (m_PreviousGamepad[player].GetButton(button) && !m_Gamepad[player].GetButton(button));
	}

	float CInputManager::GetGamepadAxis(InputDefinitions::GamepadAxis axis, int player)
	{
		if (!m_HasGamepad[player]) return 0.0f;
		switch (axis)
		{
		case InputDefinitions::GamepadAxis::LEFT_THUMB_X:
			return m_GamepadStickLeftX[player];
		case InputDefinitions::GamepadAxis::LEFT_THUMB_Y:
			return m_GamepadStickLeftY[player];
		case InputDefinitions::GamepadAxis::RIGHT_THUMB_X:
			return m_GamepadStickRightX[player];
		case InputDefinitions::GamepadAxis::RIGHT_THUMB_Y:
			return m_GamepadStickRightY[player];
		case InputDefinitions::GamepadAxis::LEFT_TRIGGER:
			return m_GamepadTL[player];
		case InputDefinitions::GamepadAxis::RIGHT_TRIGGER:
			return m_GamepadTR[player];
		default:
			return 0.0f;;
		}
	}
}