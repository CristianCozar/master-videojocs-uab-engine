#pragma once

#include <Windows.h>
#include <Xinput.h>

#include "Utils\Logger\Logger.h"


namespace InputDefinitions
{
	

	struct KeyboardData
	{
		bool raw[256];
		bool escape, space;
		bool numpad[10];
		bool fkey[24];
		bool left, right, up, down;
		bool anyKeyDown;
		unsigned char lastChar;


		void SetLastChar(unsigned char key) {
			lastChar = key;
		}

		void SetKey(unsigned char key, bool state)
		{

			raw[key] = state;
			lastChar = key;

			if (key >= VK_NUMPAD0 || key <= VK_NUMPAD9)
			{
				numpad[key - VK_NUMPAD0] = state;
			}
			else if (key >= VK_F1 || key <= VK_F24)
			{
				fkey[key - VK_F1] = state;
			}
			else
			{
				switch (key)
				{
				case VK_ESCAPE:
					escape = state;
					break;
				case VK_SPACE:
					space = state;
					break;
				case VK_LEFT:
					left = state;
					break;
				case VK_UP:
					up = state;
					break;
				case VK_RIGHT:
					right = state;
					break;
				case VK_DOWN:
					down = state;
					break;
				}
			}
		};
	};

	enum MouseButton
	{
		LEFT, MIDDLE, RIGHT
	};
	enum MouseAxis
	{
		MOUSE_X, MOUSE_Y, DX, DY, DZ
	};
	enum GamepadButton
	{
		A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	};
	enum GamepadAxis
	{
		LEFT_THUMB_X, LEFT_THUMB_Y, RIGHT_THUMB_X, RIGHT_THUMB_Y, LEFT_TRIGGER, RIGHT_TRIGGER
	};

	struct GamepadData
	{
		// Botones de la derecha
		bool m_GamepadA;
		bool m_GamepadB;
		bool m_GamepadX;
		bool m_GamepadY;
		// Botones traseros
		bool m_GamepadLB;
		bool m_GamepadRB;
		// Botones de los sticks
		bool m_GamepadPL;
		bool m_GamepadPR;
		// Cruceta
		bool m_GamepadLeft;
		bool m_GamepadUp;
		bool m_GamepadRight;
		bool m_GamepadDown;
		// Botones de funcion
		bool m_GamepadStart;
		bool m_GamepadBack;

		void SetButtons(XINPUT_STATE s)
		{
			m_GamepadA = (s.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0;
			m_GamepadB = (s.Gamepad.wButtons & XINPUT_GAMEPAD_B) != 0;
			m_GamepadX = (s.Gamepad.wButtons & XINPUT_GAMEPAD_X) != 0;
			m_GamepadY = (s.Gamepad.wButtons & XINPUT_GAMEPAD_Y) != 0;

			m_GamepadLB = (s.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) != 0;
			m_GamepadRB = (s.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) != 0;

			m_GamepadPL = (s.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) != 0;
			m_GamepadPR = (s.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) != 0;

			m_GamepadLeft = (s.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) != 0;
			m_GamepadUp = (s.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) != 0;
			m_GamepadRight = (s.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) != 0;
			m_GamepadDown = (s.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) != 0;

			m_GamepadStart = (s.Gamepad.wButtons & XINPUT_GAMEPAD_START) != 0;
			m_GamepadBack = (s.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) != 0;

		}

		bool GetButton(GamepadButton button)
		{
			switch (button)
			{
			case GamepadButton::A:
				return m_GamepadA;
			case GamepadButton::B:
				return m_GamepadB;
			case GamepadButton::X:
				return m_GamepadX;
			case GamepadButton::Y:
				return m_GamepadY;
			case GamepadButton::LB:
				return m_GamepadLB;
			case GamepadButton::RB:
				return m_GamepadRB;
			case GamepadButton::LEFT_THUMB:
				return m_GamepadPL;
			case GamepadButton::RIGHT_THUMB:
				return m_GamepadPR;
			case GamepadButton::DPAD_DOWN:
				return m_GamepadDown;
			case GamepadButton::DPAD_LEFT:
				return m_GamepadLeft;
			case GamepadButton::DPAD_RIGHT:
				return m_GamepadRight;
			case GamepadButton::DPAD_UP:
				return m_GamepadUp;
			case GamepadButton::START:
				return m_GamepadStart;
			case GamepadButton::BACK:
				return m_GamepadBack;
			default:
				return false;
			}
		}
	};

	inline MouseButton GetMouseButtonFromString(const char* str, MouseButton defaultValue = (MouseButton)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "LEFT") == 0)
			return LEFT;
		else if (strcmp(str, "MIDDLE") == 0)
			return MIDDLE;
		else if (strcmp(str, "RIGHT") == 0)
			return RIGHT;
		else
			return (MouseButton)-1;
	}

	inline MouseAxis GetMouseAxisFromString(const char* str, MouseAxis defaultValue = (MouseAxis)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "MOUSE_X") == 0)
			return MOUSE_X;
		else if (strcmp(str, "MOUSE_Y") == 0)
			return MOUSE_Y;
		else if (strcmp(str, "DX") == 0)
			return DX;
		else if (strcmp(str, "DY") == 0)
			return DY;
		else if (strcmp(str, "DZ") == 0)
			return DZ;
		else
			return (MouseAxis)-1;
	}

	inline GamepadButton GetGamepadButtonFromString(const char* str, GamepadButton defaultValue = (GamepadButton)-1)
	{
		if (strcmp(str, "A") == 0)
		{
			return GamepadButton::A;
		}
		else if (strcmp(str, "B") == 0)
		{
			return GamepadButton::B;
		}
		else if (strcmp(str, "X") == 0)
		{
			return GamepadButton::X;
		}
		else if (strcmp(str, "Y") == 0)
		{
			return GamepadButton::Y;
		}
		else if (strcmp(str, "LB") == 0)
		{
			return GamepadButton::LB;
		}
		else if (strcmp(str, "RB") == 0)
		{
			return GamepadButton::RB;
		}
		else if (strcmp(str, "UP") == 0)
		{
			return GamepadButton::DPAD_UP;
		}
		else if (strcmp(str, "DOWN") == 0)
		{
			return GamepadButton::DPAD_DOWN;
		}
		else if (strcmp(str, "LEFT") == 0)
		{
			return GamepadButton::DPAD_LEFT;
		}
		else if (strcmp(str, "RIGHT") == 0)
		{
			return GamepadButton::DPAD_RIGHT;
		}
		else if (strcmp(str, "PL") == 0)
		{
			return GamepadButton::LEFT_THUMB;
		}
		else if (strcmp(str, "PR") == 0)
		{
			return GamepadButton::RIGHT_THUMB;
		}
		else if (strcmp(str, "BACK") == 0)
		{
			return GamepadButton::BACK;
		}
		else if (strcmp(str, "START") == 0)
		{
			return GamepadButton::START;
		}
		return (GamepadButton)-1;
	}

	inline GamepadAxis GetGamepadAxisFromString(const char* str, GamepadAxis defaultValue = (GamepadAxis)-1)
	{
		if (strcmp(str, "LEFTX") == 0)
		{
			return GamepadAxis::LEFT_THUMB_X;
		}
		else if (strcmp(str, "LEFTY") == 0)
		{
			return GamepadAxis::LEFT_THUMB_Y;
		}
		else if (strcmp(str, "RIGHTX") == 0)
		{
			return GamepadAxis::RIGHT_THUMB_X;
		}
		else if (strcmp(str, "RIGHTY") == 0)
		{
			return GamepadAxis::RIGHT_THUMB_Y;
		}
		else if (strcmp(str, "TR") == 0)
		{
			return GamepadAxis::RIGHT_TRIGGER;
		}
		else if (strcmp(str, "TL") == 0)
		{
			return GamepadAxis::LEFT_TRIGGER;
		}
		return (GamepadAxis)-1;
	}
}