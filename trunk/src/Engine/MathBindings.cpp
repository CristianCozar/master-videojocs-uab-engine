#include "Math/Vector3.h"
#include "Math\Transform.h"
#include "Utils\lua_utils.h"
#include "Math\Quaternion.h"
#include "Math/Color.h"

using namespace luabind;

namespace lua
{

	template <> void BindClass<Vect3f>(lua_State *aLua)
	{
		module(aLua)
			[
				class_<Vect4f>("float4")
				.def(constructor<>())
				.def(constructor<float, float, float, float>())
				.def(const_self + const_self)
				.def(const_self - const_self)
				.def(const_self * const_self)
				.def(const_self == const_self)
				.def(self * float())
				.def(const_self * float())
				.def(const_self / float())
				.def(float() * self)
				.def(float() * const_self)
				.def(float() / const_self)
				.def(const_self + float())
				.def(const_self - float())
				.def(float() + const_self)
				.def(float() - const_self)
				.def(-const_self)
				.def_readwrite("x", &Vect4f::x)
				.def_readwrite("y", &Vect4f::y)
				.def_readwrite("z", &Vect4f::z)
				.def_readwrite("w", &Vect4f::w)
				.def("set", &Vect4f::Set)
			];

		module(aLua)
			[
				class_<Vect3f>("float3")
				.def(constructor<>())
				.def(constructor<float, float, float>())
				.def(const_self + const_self)
				.def(const_self - const_self)
				.def(const_self * const_self)
				.def(const_self ^ const_self)
				.def(const_self == const_self)
				.def(self * float())
				.def(const_self * float())
				.def(const_self / float())
				.def(float() * self)
				.def(float() * const_self)
				.def(float() / const_self)
				.def(const_self + float())
				.def(const_self - float())
				.def(float() + const_self)
				.def(float() - const_self)
				.def(-const_self)
				.def_readwrite("x", &Vect3f::x)
				.def_readwrite("y", &Vect3f::y)
				.def_readwrite("z", &Vect3f::z)
				.def("set", &Vect3f::Set)
				.def("normalized", &Vect3f::GetNormalized)
				.def("length", &Vect3f::Length)
				.def("sqlength", &Vect3f::SquaredLength)
				.def("distance", &Vect3f::Distance)
				.def("sqdistance", &Vect3f::SqDistance)
				.def("angle", &Vect3f::GetAngleWith)
				.def("lerp", &Vect3f::GetLerp)
			];

		module(aLua)
			[
				class_<Vect2f>("float2")
				.def(constructor<>())
				.def(constructor<float, float>())
				.def(const_self + const_self)
				.def(const_self - const_self)
				.def(const_self * const_self)
				.def(const_self == const_self)
				.def(self * float())
				.def(const_self * float())
				.def(const_self / float())
				.def(float() * self)
				.def(float() * const_self)
				.def(float() / const_self)
				.def(const_self + float())
				.def(const_self - float())
				//.def(float() + const_self)
				//.def(float() - const_self)
				.def(-const_self)
				.def_readwrite("x", &Vect2f::x)
				.def_readwrite("y", &Vect2f::y)
				.def("set", &Vect2f::Set)
			];

		module(aLua)
			[
				class_<Quatf>("quat")
				.def(constructor<>())
				.def(constructor<float, float, float, float>())
				.def(constructor<Vect3f, float>())
				.def("quat_from_yaw_pitch_roll", &Quatf::QuatFromYawPitchRoll)
				.def_readwrite("x", &Quatf::x)
				.def_readwrite("y", &Quatf::y)
				.def_readwrite("x", &Quatf::z)
				.def_readwrite("y", &Quatf::w)
			];
	}
}