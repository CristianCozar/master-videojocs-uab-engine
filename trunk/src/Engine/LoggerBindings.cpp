#include "Utils\lua_utils.h"
#include "Scripting\ScriptManager.h"
#include "Utils\Logger\Logger.h"

namespace lua {
	

	void AddNewErrorLog(const char* format)
	{
		LOG_ERROR_APPLICATION(format);
	}

	void AddNewWarningLog(const char* format)
	{
		LOG_WARNING_APPLICATION(format);
	}

	void AddNewInfoLog(const char* format)
	{
		LOG_INFO_APPLICATION(format);
	}

	void AddNewErrorLogIf(bool condition, const char* format)
	{
		LOG_ERROR_APPLICATION_IF(condition, format);
	}

	void AddNewWarningLogIf(bool condition, const char* format)
	{
		LOG_WARNING_APPLICATION_IF(condition, format);
	}

	void AddNewInfoLogIf(bool condition, const char* format)
	{
		LOG_INFO_APPLICATION_IF(condition, format);
	}

	template <> void BindLibrary<Logger>(lua_State *aLua)
	{
		REGISTER_LUA_FUNCTION(aLua, "add_new_error_log", &AddNewErrorLog);
		REGISTER_LUA_FUNCTION(aLua, "add_new_warning_log", &AddNewWarningLog);
		REGISTER_LUA_FUNCTION(aLua, "add_new_info_log", &AddNewInfoLog);

	}
}