#include "RigidBodyNode.h"

#include "Math\Transform.h"
#include "XML\XML.h"

#include "Math\Vector3.h"
#include "Math\Quaternion.h"
#include "Math\Vector3.h"

#include "Engine.h"
#include "PhysXManager.h"

#include "Debug\CustomAssert.h"

namespace engine
{
	CRigidBodyNode::CRigidBodyNode(const CXMLElement *node, CTransform* transform, const std::string &aName) : CPhysicNode(aName)
	{
		m_Type = PhysicTypes::Rigidbody;
		std::string l_ShapeType = node->GetAttribute<std::string>("shape", "box");
		ShapeTypes l_ShapeTypeE;
		Vect3f l_Offset = node->GetAttribute<Vect3f>("offset", Vect3f(0.0f, 0.0f, 0.0f));
		Vect3f l_FinalPosition = transform->m_Position + l_Offset;
		EnumString<ShapeTypes>::ToEnum(l_ShapeTypeE, l_ShapeType);
		float l_Density = node->GetAttribute<float>("density", 1.0f);
		Quatf q;
		q.QuatFromEuler(Vect3f(transform->GetRoll(), transform->GetPitch(), transform->GetYaw()));
		switch (l_ShapeTypeE)
		{
		case ShapeTypes::Box:
		{
			Vect3f l_Scale = node->GetAttribute<Vect3f>("scale", Vect3f(1.0f, 1.0f, 1.0f));
			m_Manager.createDynamicBox(m_Name, l_Scale.x, l_Scale.y, l_Scale.z, l_FinalPosition, q, l_Density, "Default");
		}
			break;
		case ShapeTypes::Sphere:
		{
			float l_Radius = node->GetAttribute<float>("radius", 1.0f);
			m_Manager.createDynamicSphere(m_Name, l_Radius, l_FinalPosition, q, l_Density, "Default");
		}
			break;
		case ShapeTypes::Plane:
			Assert(false, "Un plano no puede ser RigidBody.");
			break;
		case ShapeTypes::ConvexMesh:
		{
			Vect3f l_Scale = node->GetAttribute<Vect3f>("scale", Vect3f(1.0f, 1.0f, 1.0f));
			m_Manager.createDynamicConvexMesh(m_Name, GetMesh(node->GetAttribute<std::string>("shapefile", ""), l_Scale), l_FinalPosition, q, l_Density, "Default");
			break;
		}
		case ShapeTypes::TriangleMesh:
			Assert(false, "Un TriangleMesh no puede ser RigidBody.");
			break;
		}
	}

	CRigidBodyNode::~CRigidBodyNode()
	{

	}

	void CRigidBodyNode::SetPosition(const Vect3f aPosition)
	{
		m_Manager.SetActorPosition(m_Name, aPosition);
	}

	void CRigidBodyNode::Move(const Vect3f &offset, float dt)
	{

	}

	void CRigidBodyNode::GetTransform(CTransform *aTransform)
	{
		Quatf q = Quatf();
		m_Manager.GetActorTransform(m_Name, aTransform->m_Position, q);
		Vect3f eulerRot = q.EulerFromQuat();
		
		aTransform->SetRoll(eulerRot.x);
		aTransform->SetPitch(eulerRot.y);
		aTransform->SetYaw(eulerRot.z);
	}
}