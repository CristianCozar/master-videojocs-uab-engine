#pragma once

#include "PhysicNode.h"

namespace engine
{
	class CRigidStaticNode : public CPhysicNode
	{
	public:
		CRigidStaticNode(const CXMLElement *node, CTransform* transform, const std::string &aName);
		virtual ~CRigidStaticNode();
		virtual void SetPosition(const Vect3f aPosition) override;
		virtual void Move(const Vect3f &offset, float dt) override;
		virtual void GetTransform(CTransform *aTransform) override;
	};
}