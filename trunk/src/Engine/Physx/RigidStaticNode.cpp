#include "RigidStaticNode.h"

#include "Math\Transform.h"
#include "XML\XML.h"

#include "Math\Vector3.h"
#include "Math\Quaternion.h"
#include "Math\Vector3.h"

#include "Engine.h"
#include "PhysXManager.h"

#include "Debug\CustomAssert.h"

namespace engine
{
	CRigidStaticNode::CRigidStaticNode(const CXMLElement *node, CTransform* transform, const std::string &aName) : CPhysicNode(aName)
	{
		m_Type = PhysicTypes::Rigidstatic;
		std::string l_ShapeType = node->GetAttribute<std::string>("shape", "box");
		Vect3f l_Offset = node->GetAttribute<Vect3f>("offset", Vect3f(0.0f, 0.0f, 0.0f));
		Vect3f l_FinalPosition = transform->m_Position + l_Offset;
		ShapeTypes l_ShapeTypeE;
		EnumString<ShapeTypes>::ToEnum(l_ShapeTypeE, l_ShapeType);
		Quatf q;
		Vect3f l_Euler = node->GetAttribute<Vect3f>("rotation", Vect3f(0.0f, 0.0f, 0.0f)) * DEG2RAD;
		Vect3f l_ParentScale = transform->GetScale();
		Vect3f l_Scale = node->GetAttribute<Vect3f>("scale", Vect3f(1.0f, 1.0f, 1.0f));
		/*l_Scale.x *= l_ParentScale.x;
		l_Scale.y *= l_ParentScale.y;
		l_Scale.z *= l_ParentScale.z;*/
		//if (aName == "A1_PlataformaMovil" || aName == "A3_Portal")
		//{
		q.QuatFromEuler(Vect3f(transform->GetRoll() + l_Euler.z, transform->GetYaw() + l_Euler.x, transform->GetPitch() + l_Euler.y));//?? Si pongo el yaw como toca el collider de physX no esta correctamente rotado.
		//}
		//else{
		//	q.QuatFromEuler(Vect3f(transform->GetRoll(), transform->GetPitch(), transform->GetYaw()));
		//}
		switch (l_ShapeTypeE)
		{
			case ShapeTypes::Box:
			{
				m_Manager.createStaticBox(m_Name, l_Scale.x, l_Scale.y, l_Scale.z, l_FinalPosition, q, "Default");
			}
			break;
			case ShapeTypes::Sphere:
			{
				float l_Radius = node->GetAttribute<float>("radius", 1.0f);
				m_Manager.createStaticSphere(m_Name, l_Radius, l_FinalPosition, q, "Default");
			}
			break;
			case ShapeTypes::Plane:
			{
				Vect3f l_Normal = node->GetAttribute<Vect3f>("normal", Vect3f(0.0f, 1.0f, 0.0f));
				float l_Distance = node->GetAttribute<float>("distance", 0.0f);
				m_Manager.createStaticPlane(m_Name, l_Normal.x, l_Normal.y, l_Normal.z, l_Distance, "Default");
				break;
			}
			case ShapeTypes::ConvexMesh:
			{
				m_Manager.createStaticConvexMesh(m_Name, GetMesh(node->GetAttribute<std::string>("shapefile", ""), l_Scale), l_FinalPosition, q, "Default");
				break;
			}
			case ShapeTypes::TriangleMesh:
			{
				m_Manager.createStaticTriangleMesh(m_Name, GetMesh(node->GetAttribute<std::string>("shapefile", ""), l_Scale), GetIndices(node->GetAttribute<std::string>("indicesfile", "")), l_FinalPosition, q, "Default");
				break;
			}
		}
	}

	CRigidStaticNode::~CRigidStaticNode()
	{

	}

	void CRigidStaticNode::SetPosition(const Vect3f aPosition)
	{
		m_Manager.SetActorPosition(m_Name, aPosition);
	}

	void CRigidStaticNode::Move(const Vect3f &offset, float dt)
	{

	}

	void CRigidStaticNode::GetTransform(CTransform *aTransform)
	{

	}
}