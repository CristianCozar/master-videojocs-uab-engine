#pragma once

#include <vector>

#include "XML\XML.h"
#include "Utils\Name.h"
#include "Math\Vector3.h"
#include "Utils\EnumToString.h"

#include "PhysXManager.h"

class CTransform;
namespace engine {
	class CPhysXManager;
	enum ShapeTypes {
		Box = 0,
		Sphere,
		Plane,
		ConvexMesh,
		TriangleMesh
	};
	enum PhysicTypes {
		Rigidbody = 0,
		Rigidstatic,
		Charactercontroller
	};
	class CPhysicNode : public CName
	{
	public:
		CPhysicNode();
		CPhysicNode(const std::string &m_Name);
		virtual ~CPhysicNode();
		virtual void SetPosition(const Vect3f position) = 0;
		virtual void Move(const Vect3f &offset, float dt) = 0;
		virtual void GetTransform(CTransform *aTransform) = 0;
		bool IsCharacterController();
	protected:
		std::vector<Vect3f> GetMesh(const std::string &aFileName, const Vect3f& scale = Vect3f());
		std::vector<Vect3w> GetIndices(const std::string &aFileName);
		PhysicTypes m_Type;
		CPhysXManager &m_Manager;
	};
}

Begin_Enum_String(engine::ShapeTypes)
{
	Enum_String_Id(engine::ShapeTypes::Box, "box");
	Enum_String_Id(engine::ShapeTypes::Sphere, "sphere");
	Enum_String_Id(engine::ShapeTypes::Plane, "plane");
	Enum_String_Id(engine::ShapeTypes::ConvexMesh, "convexmesh");
	Enum_String_Id(engine::ShapeTypes::TriangleMesh, "trianglemesh");
}
End_Enum_String;