#pragma once

#include "PhysicNode.h"

namespace engine
{
	class CTrigger : public CPhysicNode
	{
	public:
		CTrigger(const CXMLElement *node, CTransform* transform, const std::string &aName);
		virtual ~CTrigger();
		virtual void SetPosition(const Vect3f aPosition) override;
		virtual void Move(const Vect3f &offset, float dt) override;
		virtual void GetTransform(CTransform *aTransform) override;
		void CTrigger::OnTrigger();
	};
}