#include "Trigger.h"

#include "Math\Transform.h"
#include "XML\XML.h"

#include "Math\Vector3.h"
#include "Math\Quaternion.h"
#include "Math\Vector3.h"

#include "Engine.h"
#include "PhysXManager.h"

#include "Debug\CustomAssert.h"

namespace engine
{
	CTrigger::CTrigger(const CXMLElement *node, CTransform* transform, const std::string &aName) : CPhysicNode(aName)
	{
		m_Type = PhysicTypes::Rigidbody;
		std::string l_ShapeType = node->GetAttribute<std::string>("shape", "box");
		ShapeTypes l_ShapeTypeE;
		EnumString<ShapeTypes>::ToEnum(l_ShapeTypeE, l_ShapeType);
		switch (l_ShapeTypeE)
		{
		case ShapeTypes::Box:
		{
			Vect3f l_Scale = node->GetAttribute<Vect3f>("scale", Vect3f());
			m_Manager.AddTriggerBox(m_Name, l_Scale.x, l_Scale.y, l_Scale.z, transform->m_Position, Quatf(0.0f, 0.0f, 0.0f, 1.0f), "Default");
		}
			break;
		case ShapeTypes::Sphere:
		{
			Vect3f l_Scale = node->GetAttribute<float>("radius", 1.0f);
			m_Manager.AddTriggerSphere(m_Name, l_Scale.x, transform->m_Position, Quatf(0.0f, 0.0f, 0.0f, 1.0f), "Default");
		}
			break;
		case ShapeTypes::Plane:
			Assert(false, "Un plano no puede ser RigidBody.");
			break;
		case ShapeTypes::ConvexMesh:
			Assert(false, "No implementado.");
			break;
		case ShapeTypes::TriangleMesh:
			Assert(false, "Un TriangleMesh no puede ser RigidBody.");
			break;
		}
	}

	CTrigger::~CTrigger()
	{

	}

	void CTrigger::SetPosition(const Vect3f aPosition)
	{
		m_Manager.SetActorPosition(m_Name, aPosition);
	}

	void CTrigger::Move(const Vect3f &offset, float dt)
	{
		m_Manager.MoveTrigger(m_Name, offset);
	}

	void CTrigger::GetTransform(CTransform *aTransform)
	{
		Quatf q = Quatf();
		m_Manager.GetActorTransform(m_Name, aTransform->m_Position, q);
	}

	void CTrigger::OnTrigger()
	{
		
	}
}