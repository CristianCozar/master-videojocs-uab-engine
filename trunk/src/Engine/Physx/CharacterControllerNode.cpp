#include "CharacterControllerNode.h"

#include "Math\Transform.h"
#include "XML\XML.h"

#include "Math\Vector3.h"
#include "Math\Quaternion.h"
#include "Math\Vector3.h"

#include "Engine.h"
#include "PhysXManager.h"
#include "PhysXManagerImplementation.h"

#include "Debug\CustomAssert.h"

namespace engine
{
	CCharacterControllerNode::CCharacterControllerNode(const CXMLElement *node, CTransform* transform, const std::string &aName) : CPhysicNode(aName)
	{
		m_Type = PhysicTypes::Charactercontroller;
		float l_Height = node->GetAttribute<float>("height", 1.0f);
		float l_Radius = node->GetAttribute<float>("radius", 0.5f);
		std::string l_Material = node->GetAttribute<std::string>("material", "Default");
		float l_Density = node->GetAttribute<float>("density", 1.0f);
		m_Height = l_Height;
		//m_Manager.AddCharacterControllerRequest(m_Name, l_Height, l_Radius, transform->m_Position, Quatf(0.0f, 0.0f, 0.0f, 1.0f), l_Material, l_Density, m_Name);
		//m_Manager.AddRequestedCharControllers();
		((CPhysXManagerImplementation*)&m_Manager)->AddCharacterController(m_Name, l_Height, l_Radius, transform->m_Position, Quatf(0.0f, 0.0f, 0.0f, 1.0f), l_Material, l_Density, m_Name);
		SetPosition(transform->m_Position);
	}

	CCharacterControllerNode::~CCharacterControllerNode()
	{

	}

	void CCharacterControllerNode::SetPosition(const Vect3f aPosition) 
	{
		m_Manager.SetActorPosition(m_Name, aPosition);
	}

	void CCharacterControllerNode::Move(const Vect3f &offset, float dt)
	{
		m_Manager.MoveCharacterController(m_Name, offset, dt);
	}

	void CCharacterControllerNode::GetTransform(CTransform *aTransform)
	{
		Quatf q = Quatf();
		m_Manager.GetActorTransform(m_Name, aTransform->m_Position, q);
		aTransform->m_Position.y -= m_Height;
		//Vect3f eulerRot = q.EulerFromQuat();
	//	aTransform->SetRoll(eulerRot.x);
	//	aTransform->SetPitch(eulerRot.y);
	//	aTransform->SetYaw(eulerRot.z);
	}
}