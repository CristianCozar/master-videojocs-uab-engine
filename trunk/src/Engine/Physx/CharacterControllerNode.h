#pragma once

#include "PhysicNode.h"

namespace engine
{
	class CCharacterControllerNode : public CPhysicNode
	{
	public:
		CCharacterControllerNode(const CXMLElement *node, CTransform* transform, const std::string &aName);
		virtual ~CCharacterControllerNode();
		virtual void Move(const Vect3f &offset, float dt) override;
		virtual void GetTransform(CTransform *aTransform) override;
		virtual void SetPosition(Vect3f pos);

	private:
		float m_Height;
	};
}