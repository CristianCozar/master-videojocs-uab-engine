#include "PhysXManager.h"
#include "PhysXManagerImplementation.h"

#include <PxPhysicsAPI.h>

#include "Math\Vector3.h"
#include "Math\Quaternion.h"
#include "PhysXManagerImplementation.h"
#include "Debug\CustomAssert.h"
#include "Utils\Logger\Logger.h"
#include "Wwise\CSoundManager.h"
#include "Utils\lua_utils.h"
#include "Scripting\ScriptManager.h"

static physx::PxDefaultErrorCallback gDefaultErrorCallback;
static physx::PxDefaultAllocator gDefaultAllocatorCallback;

#define PHYSX_UPDATE_STEP 0.016

#ifdef _DEBUG
#pragma comment(lib, "PhysX3DEBUG_x86.lib")
#pragma comment(lib, "PhysX3CommonDEBUG_x86.lib")
#pragma comment(lib, "PhysX3ExtensionsDEBUG.lib")
#pragma comment(lib, "PhysXProfileSDKDEBUG.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDKDEBUG.lib")
#pragma comment(lib, "PhysX3CharacterKinematicDEBUG_x86.lib")
#pragma comment(lib, "PhysX3CookingDEBUG_x86.lib")
#else
#pragma comment(lib, "PhysX3_x86.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PhysXProfileSDK.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDK.lib")
#pragma comment(lib, "PhysX3CharacterKinematic_x86.lib")
#pragma comment(lib, "PhysX3Cooking_x86.lib")
#endif

#if USE_PHYSX_DEBUG
#define					PVD_HOST			"127.0.0.1"
#endif

#ifdef CHECKED_RELEASE
#undef CHECKED_RELEASE
#endif
#define CHECKED_RELEASE(x) if(x!=nullptr) {x->release(); x=nullptr;}

inline physx::PxVec3 CastVec(const Vect3f& v)
{
	return physx::PxVec3(v.x, v.y, v.z);
}
inline Vect3f CastVec(const physx::PxVec3& v)
{
	return Vect3f(v.x, v.y, v.z);
}
inline Vect3f CastVec(const physx::PxExtendedVec3& v)
{
	return Vect3f((float)v.x, (float)v.y, (float)v.z);
}

inline physx::PxQuat CastQuat(const Quatf& q)
{
	return physx::PxQuat(q.x, q.y, q.z, q.w);
}
inline Quatf CastQuat(const physx::PxQuat& q)
{
	return Quatf(q.x, q.y, q.z, q.w);
}

namespace engine
{
	CPhysXManager* CPhysXManager::CreatePhysXManager()
	{
		return new CPhysXManagerImplementation();
	}

	CPhysXManager::CPhysXManager()
	{

	}

	CPhysXManager::~CPhysXManager()
	{
		CHECKED_RELEASE(m_ControllerManager);
		CHECKED_RELEASE(m_Scene);
		CHECKED_RELEASE(m_Dispatcher);
		 physx::PxProfileZoneManager* profileZoneManager = m_PhysX->getProfileZoneManager(); // TODO con el 3.3
#if USE_PHYSX_DEBUG
		CHECKED_RELEASE(m_DebugConnection);
#endif
		CHECKED_RELEASE(m_Cooking);
		CHECKED_RELEASE(m_PhysX);
		CHECKED_RELEASE(profileZoneManager);
		CHECKED_RELEASE(m_Foundation);
	/*	for (auto it = m_CharacterControllers.begin(); it != m_CharacterControllers.end(); ++it)
		{
			physx::PxController *cct = it->second;
			cct->release();
		}
		m_CharacterControllers.clear();*/
		
	}

	void CPhysXManager::RegisterMaterial(const std::string &name, float staticFriction, float dynamicFriction, float restitution)
	{
		auto it = m_Materials.find(name);
		if (it != m_Materials.end())
		{
			it->second->release();
		}

		//Assert(m_Materials.find(name) == m_Materials.end(), "Material duplicada");

		m_Materials[name] = m_PhysX->createMaterial(staticFriction, dynamicFriction, restitution);
	}


	CPhysXManagerImplementation::CPhysXManagerImplementation()
	{
		m_Foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
		Assert(m_Foundation != nullptr, "No se ha podido crear la Foundation de PhysX");

		physx::PxProfileZoneManager* profileZoneManager = nullptr;

#ifdef _DEBUG
		profileZoneManager = &physx::PxProfileZoneManager::createProfileZoneManager(m_Foundation);
#endif

		m_PhysX = PxCreatePhysics(PX_PHYSICS_VERSION, *m_Foundation, physx::PxTolerancesScale(), true, nullptr);
		Assert(m_PhysX != nullptr, "No se ha podido crear PhysX");

		m_Cooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_Foundation, physx::PxCookingParams(physx::PxTolerancesScale()));
		Assert(m_Cooking != nullptr, "No se ha podido crear el cooking de PhysX");

#	if USE_PHYSX_DEBUG
		if (m_PhysX->getPvdConnectionManager())
		{
			m_PhysX->getVisualDebugger()->setVisualizeConstraints(true);
			m_PhysX->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
			m_PhysX->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_SCENEQUERIES, true);
			m_DebugConnection = physx::PxVisualDebuggerExt::createConnection(m_PhysX->getPvdConnectionManager(), PVD_HOST, 5425, 10);
		}
		else
		{
			m_DebugConnection = nullptr;
		}
#	endif

		m_Dispatcher = physx::PxDefaultCpuDispatcherCreate(2); // TODO: Encontrar el numero de cores del PC

		physx::PxSceneDesc sceneDesc(m_PhysX->getTolerancesScale());
		sceneDesc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
		sceneDesc.cpuDispatcher = m_Dispatcher;

		sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
		sceneDesc.flags = physx::PxSceneFlag::eENABLE_ACTIVETRANSFORMS;
		m_Scene = m_PhysX->createScene(sceneDesc);
		Assert(m_Scene != nullptr, "La escena no ha sido correctamente creada");

		m_Scene->setSimulationEventCallback(this);

		m_ControllerManager = PxCreateControllerManager(*m_Scene, true);
		m_ControllerManager->setOverlapRecoveryModule(true);

		m_LeftoverSeconds = 0.0f;
	}


	void CPhysXManager::createStaticBox(const std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		//Crea una caja estatica
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();


		physx::PxShape* shape = m_PhysX->createShape(physx::PxBoxGeometry(sizeX / 2.0f, sizeY / 2.0f, sizeZ / 2.0f), *l_Material);

		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, true);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, true);

		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		body->userData = (void*)index;
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);

		shape->release();
	}

	void CPhysXManager::createStaticPlane(const std::string& name, float x, float y, float z, float  dist, const std::string& materialName)
	{
		//Crea un plano estatico
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();


		physx::PxRigidStatic* body = physx::PxCreatePlane(*m_PhysX, physx::PxPlane(x, y, z, dist), *l_Material);
		body->userData = (void*)index;
		m_Scene->addActor(*body);

		physx::PxShape* shape;
		size_t numShapes = body->getShapes(&shape, 1);
		physx::PxTransform transform = shape->getLocalPose();	
		//physx::PxTransform transform = shape->getActor()->getGlobalPose();


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(CastVec(transform.p));
		m_ActorOrientations.push_back(CastQuat(transform.q));
		m_Actors.push_back(body);

		//shape->release();
		
	}

	void CPhysXManager::createDynamicBox(const std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		//Crear caja dinamica
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();

		physx::PxShape* shape = m_PhysX->createShape(physx::PxBoxGeometry(sizeX / 2, sizeY / 2, sizeZ / 2), *l_Material);
		physx::PxRigidDynamic* body = m_PhysX->createRigidDynamic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		physx::PxRigidBodyExt::updateMassAndInertia(*body, density);
		body->userData = (void*)index;
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();

	}

	void CPhysXManager::createDynamicSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		//Crear esfera dinamica
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();

		physx::PxShape* shape = m_PhysX->createShape(physx::PxSphereGeometry(radius), *l_Material);
		physx::PxRigidDynamic* body = m_PhysX->createRigidDynamic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		physx::PxRigidBodyExt::updateMassAndInertia(*body, density);
		body->userData = (void*)index;
		m_Scene->addActor(*body);

		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();

	}


	void CPhysXManager::createStaticSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		//Crear caja dinamica
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();

		physx::PxShape* shape = m_PhysX->createShape(physx::PxSphereGeometry(radius), *l_Material);
		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		body->userData = (void*)index;
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();
	}

	void CPhysXManager::createDynamicConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, float density, const std::string& materialName)
	{
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();


		physx::PxConvexMeshDesc convexDesc;
		convexDesc.points.count = vertices.size();
		convexDesc.points.stride = sizeof(Vect3f);
		convexDesc.points.data = &vertices[0];
		convexDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX;

		physx::PxDefaultMemoryOutputStream buf;
		physx::PxConvexMeshCookingResult::Enum result;

		bool sucess = m_Cooking->cookConvexMesh(convexDesc, buf, &result);
		Assert(sucess, "Error al hacer coocking de la ConvexMesh");

		physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
		physx::PxConvexMesh* convexMesh = m_PhysX->createConvexMesh(input);

		physx::PxRigidDynamic* body = m_PhysX->createRigidDynamic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		physx::PxShape* shape = m_PhysX->createShape(physx::PxConvexMeshGeometry(convexMesh), *l_Material);
		body->attachShape(*shape);
		physx::PxRigidBodyExt::updateMassAndInertia(*body, density);
		body->userData = (void*)index;
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();

	}

	void CPhysXManager::createStaticConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();

		physx::PxConvexMeshDesc convexDesc;
		convexDesc.points.count = vertices.size();
		convexDesc.points.stride = sizeof(Vect3f);
		convexDesc.points.data = &vertices[0];
		convexDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX;

		physx::PxDefaultMemoryOutputStream buf;
		physx::PxConvexMeshCookingResult::Enum result;

		bool sucess = m_Cooking->cookConvexMesh(convexDesc, buf, &result);
		Assert(sucess, "Error al hacer coocking de la ConvexMesh");

		physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
		physx::PxConvexMesh* convexMesh = m_PhysX->createConvexMesh(input);

		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		physx::PxShape* shape = m_PhysX->createShape(physx::PxConvexMeshGeometry(convexMesh), *l_Material);

		body->attachShape(*shape);
		body->userData = (void*)index;
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();

	}

	void CPhysXManager::createStaticTriangleMesh(const std::string& name, std::vector<Vect3f> vertices, std::vector<Vect3w> indices, Vect3f position, Quatf orientation, const std::string& materialName)
	{
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();


		physx::PxTriangleMeshDesc triangDesc;
		triangDesc.points.count = vertices.size();
		triangDesc.points.stride = sizeof(Vect3f);
		triangDesc.points.data = &vertices[0];
		triangDesc.flags = physx::PxMeshFlag::e16_BIT_INDICES;

		triangDesc.triangles.count = indices.size();
		triangDesc.triangles.stride = sizeof(Vect3w);
		triangDesc.triangles.data = &indices[0];

		physx::PxDefaultMemoryOutputStream buf;


		bool sucess = m_Cooking->cookTriangleMesh(triangDesc, buf);
		Assert(sucess, "Error al hacer coocking de la TriangleMesh");

		physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
		physx::PxTriangleMesh* triangleMesh = m_PhysX->createTriangleMesh(input);

		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		physx::PxShape* shape = m_PhysX->createShape(physx::PxTriangleMeshGeometry(triangleMesh), *l_Material);
		body->userData = (void*)index;

		body->attachShape(*shape);
		m_Scene->addActor(*body);


		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);


		shape->release();

	}

	Vect3f CPhysXManager::GetActorPosition(const std::string& aName) const
	{
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se busca la posici�n.");
		size_t index = it->second;

		return m_ActorPositions[index];
	}


	Quatf CPhysXManager::GetActorOrientation(const std::string& aName) const
	{
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se busca la orientaci�n.");
		size_t index = it->second;

		return m_ActorOrientations[index];
	}

	void CPhysXManager::GetActorTransform(const std::string& aName, Vect3f& position, Quatf& orientation) const
	{
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se busca la Transform.");
		size_t index = it->second;

		position = m_ActorPositions[index];
		orientation = m_ActorOrientations[index];
	}

	void CPhysXManager::SetActorTransform(const std::string& aName,  Vect3f position,  Quatf orientation) 
	{
		
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se quiere setear la Transform.");
		size_t index = it->second;

		//m_ActorPositions[index] = position;

		SetActorPosition(aName, position);
		SetActorOrientation(aName, orientation);
	}

	void CPhysXManager::SetActorOrientation(const std::string& aName, Quatf orientation) {
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se quiere setear la Orientacion.");
		size_t index = it->second;


		//TODO: Rotacion en el nodo de physX
		if (m_Actors[index]->isRigidStatic()) { 	//RigidStatic
				physx::PxRigidStatic* myActor = m_Actors[index]->is<physx::PxRigidStatic>();
				physx::PxTransform l_transform = myActor->getGlobalPose();
				l_transform.q = CastQuat(orientation);
				myActor->setGlobalPose(l_transform);

				m_ActorOrientations[index] = CastQuat(myActor->getGlobalPose().q);

			}else if (m_Actors[index]->isRigidDynamic()) { 	//RigidDynamic
				physx::PxRigidDynamic* myActor = m_Actors[index]->is<physx::PxRigidDynamic>();

				physx::PxTransform l_transform;
				if (myActor->getKinematicTarget(l_transform)) { //RigidDynamic Kinematic
					l_transform.q = CastQuat(orientation);;
					myActor->setKinematicTarget(l_transform);

					m_ActorOrientations[index] = CastQuat(myActor->getGlobalPose().q);
				}
				else { //RigidDynamic Non Kinematic
					physx::PxTransform l_transform = myActor->getGlobalPose();
					l_transform.q = CastQuat(orientation);;
					myActor->setGlobalPose(l_transform);

					m_ActorOrientations[index] = CastQuat(myActor->getGlobalPose().q);
				}
			}
	}

	void CPhysXManager::SetActorPosition(const std::string& aName, Vect3f position)
	{
		auto it = m_ActorIndexs.find(aName);
		Assert(it != m_ActorIndexs.end(), "No existe el actor del cual se quiere setear la Position.");
		size_t index = it->second;
		
		auto it2 = m_CharacterControllers.find(aName);
		if (it2 != m_CharacterControllers.end()) { 	//CharacterController
			physx::PxController* controller = it2->second;
			physx::PxExtendedVec3 vec = physx::PxExtendedVec3(position.x, position.y, position.z);
			controller->setFootPosition(vec);
		}
		else {
			if (m_Actors[index]->isRigidStatic()) { 	//RigidStatic
				physx::PxRigidStatic* myActor = m_Actors[index]->is<physx::PxRigidStatic>();
				physx::PxTransform l_transform = myActor->getGlobalPose();
				l_transform.p = physx::PxVec3(position.x, position.y, position.z);
				myActor->setGlobalPose(l_transform);

				m_ActorPositions[index] = CastVec(myActor->getGlobalPose().p);

			}
			else if (m_Actors[index]->isRigidDynamic()) { 	//RigidDynamic
				physx::PxRigidDynamic* myActor = m_Actors[index]->is<physx::PxRigidDynamic>();

				physx::PxTransform l_transform;
				if (myActor->getKinematicTarget(l_transform)) { //RigidDynamic Kinematic
					l_transform.p = physx::PxVec3(position.x, position.y, position.z);
					myActor->setKinematicTarget(l_transform);

					m_ActorPositions[index] = CastVec(myActor->getGlobalPose().p);
				}
				else { //RigidDynamic Non Kinematic
					physx::PxTransform l_transform = myActor->getGlobalPose();
					l_transform.p = physx::PxVec3(position.x, position.y, position.z);
					myActor->setGlobalPose(l_transform);

					m_ActorPositions[index] = CastVec(myActor->getGlobalPose().p);
				}
			}
			else {
				LOG_INFO_APPLICATION("El actor a mover no dispone de Physic Node");
			}
		}
	
		
		/*
		if (m_Actors[index]->isRigidStatic())
		{
			physx::PxRigidActor* l_RA = static_cast<physx::PxRigidActor*>(m_Actors[index]);
			physx::PxTransform l_trans = l_RA->getGlobalPose();
			l_trans.p = CastVec(position);
			l_RA->setGlobalPose(l_trans);
		}
		else{
			m_ActorPositions[index] = position;//Seguramente se tenga que hacer otra cosa, PxRigidDynamic::setKinematicTarget
		}
		*/

	}

	void CPhysXManager::DeleteAllActors()
	{
		for (int i = 0; i < m_Actors.size(); ++i)
		{
			m_Scene->removeActor(*m_Actors[i]);
		}

		m_ActorIndexs.clear();
		m_ActorNames.clear();
		m_ActorOrientations.clear();
		m_ActorPositions.clear();
		m_Actors.clear();
		m_CharacterControllers.clear();
	}


	void CPhysXManager::DeleteRequestedActors()
	{
		//TODO: Eliminar actor
		bool deleted = false;
		for (int i = 0; i < m_ActorsToDestroy.size(); ++i)
		{
			

			auto it = m_ActorIndexs.find(m_ActorsToDestroy[i]);
			size_t index = it->second;

			m_Scene->removeActor(*m_Actors[index]);

			m_ActorIndexs.erase(m_ActorsToDestroy[i]);
			m_ActorNames.erase(m_ActorNames.begin() + index);
			m_ActorOrientations.erase(m_ActorOrientations.begin() + index);
			m_ActorPositions.erase(m_ActorPositions.begin() + index);
			m_Actors.erase(m_Actors.begin() + index);
			deleted = true;

			for (int j = 0; j < m_ActorNames.size(); ++j)
			{
				auto it = m_ActorIndexs.find(m_ActorNames[j]);
				it->second = j;

				m_Actors[j]->userData = (void*)j;

			}

			auto it2 = m_CharacterControllers.find(m_ActorsToDestroy[i]);
			if (it2 != m_CharacterControllers.end())
			{
				m_CharacterControllers.erase(m_ActorsToDestroy[i]);
				int ind = 0;
				for (std::map<std::string, physx::PxController*>::iterator it3 = m_CharacterControllers.begin(); it3 != m_CharacterControllers.end() ; ++it3)
				{
					physx::PxRigidDynamic* act = it3->second->getActor();
					//act->userData = (void*) ind;
					it3->second->setUserData(act->userData);
					++ind;
				}
			}
		}
		
		
		/*if (deleted)
		{
			for (int i = 0; i < m_ActorNames.size(); ++i)
			{
				auto it = m_ActorIndexs.find(m_ActorNames[i]);
				it->second = i;

				m_Actors[i]->userData = (void*)i;

			}
		}*/
		m_ActorsToDestroy.clear();
	}

	void CPhysXManager::DeleteActorRequest(const std::string& aName) {
		m_ActorsToDestroy.push_back(aName);
	}

/*	void CPhysXManager::AddStaticBoxRequest(const std::string& aName, Vect3f pos, Vect3f size)
	{
		m_StaticBoxToAdd.push_back(aName);
		m_StaticBoxPos.push_back(pos);
		m_StaticBoxSize.push_back(size);
	}

	void CPhysXManager::AddRequestedStaticBox()
	{
		for (int i = 0; i < m_StaticBoxToAdd.size(); ++i)
		{
			createStaticBox(m_StaticBoxToAdd[i], m_StaticBoxSize[i].x, m_StaticBoxSize[i].y, m_StaticBoxSize[i].z, m_StaticBoxPos[i], Quatf(0.0, 0.0, 1.0, 0.0), "Default");
		}
		m_StaticBoxToAdd.clear();
		m_StaticBoxPos.clear();
		m_StaticBoxSize.clear();
	}*/

	void CPhysXManager::AddCharacterControllerRequest(const std::string& actorName, float height, float radius, const Vect3f& position, const Quatf& orientation, const std::string& material, float density, const std::string& characterControllerName)
	{
		m_CharacterControllerToAdd.push_back(characterControllerName);
		m_CharacterControllerDensity.push_back(density);
		m_CharacterControllerHeight.push_back(height);
		m_CharacterControllerOrientation.push_back(orientation);
		m_CharacterControllerPos.push_back(position);
		m_CharacterControllerRadius.push_back(radius);
	}

	void CPhysXManager::AddRequestedCharControllers()
	{
		for (int i = 0; i < m_CharacterControllerToAdd.size(); ++i)
		{
			((engine::CPhysXManagerImplementation*)this)->AddCharacterController(m_CharacterControllerToAdd[i], m_CharacterControllerHeight[i], m_CharacterControllerRadius[i], m_CharacterControllerPos[i], m_CharacterControllerOrientation[i], "Default", m_CharacterControllerDensity[i], m_CharacterControllerToAdd[i]);
			//AddCharacterController(name, height, radius, position, orientation, materialName, density, name);
		}
		m_CharacterControllerToAdd.clear();
		m_CharacterControllerDensity.clear();
		m_CharacterControllerHeight.clear();
		m_CharacterControllerOrientation.clear();
		m_CharacterControllerPos.clear();
		m_CharacterControllerRadius.clear();
	}





	void CPhysXManager::Update(float _dt)
	{

		AddRequestedCharControllers();
		m_LeftoverSeconds += _dt;
		if (m_LeftoverSeconds >= PHYSX_UPDATE_STEP)
		{
			m_Scene->simulate(PHYSX_UPDATE_STEP);
			m_Scene->fetchResults(true);


			physx::PxU32 numActiveTransforms;
			const physx::PxActiveTransform* activeTransforms = m_Scene->getActiveTransforms(numActiveTransforms);
			for (physx::PxU32 i = 0; i < numActiveTransforms; ++i)
			{
				uintptr_t index = (uintptr_t)(activeTransforms[i].userData);
				m_ActorPositions[index] = CastVec(activeTransforms[i].actor2World.p);
				m_ActorOrientations[index] = CastQuat(activeTransforms[i].actor2World.q);
			}
			m_LeftoverSeconds = fmod(m_LeftoverSeconds, PHYSX_UPDATE_STEP);
		}
		if (m_RequestDeleteAllActors)
		{
			DeleteAllActors();
			m_RequestDeleteAllActors = false;
			m_ActorsToDestroy.clear();
		}
		DeleteRequestedActors();
	}

	void CPhysXManager::AddTriggerSphere(const std::string& name, float radius, const Vect3f& position, const Quatf& orientation, const std::string& materialName)
	{
		//Crea una caja Trigger
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();
		/*
		physx::PxShape* shape = m_PhysX->createShape(physx::PxSphereGeometry(radius), *l_Material);
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		body->userData = (void*)index;
		m_Scene->addActor(*body);

		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);*/

		physx::PxShape* shape = m_PhysX->createShape(physx::PxSphereGeometry(radius), *l_Material);
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		physx::PxRigidDynamic* body = m_PhysX->createRigidDynamic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		physx::PxRigidBodyExt::updateMassAndInertia(*body, 10.0);
		body->userData = (void*)index;
		m_Scene->addActor(*body);

		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);

		shape->release();
	}

	void CPhysXManager::AddTriggerBox(const std::string& name, float sizeX, float sizeY, float sizeZ, const Vect3f& position, const Quatf& orientation, const std::string& materialName)
	{
		//Crea una caja Trigger
		auto *l_Material = m_Materials[materialName];
		size_t index = m_Actors.size();

		physx::PxShape* shape = m_PhysX->createShape(physx::PxBoxGeometry(sizeX / 2, sizeY / 2, sizeZ / 2), *l_Material);
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		physx::PxRigidStatic* body = m_PhysX->createRigidStatic(physx::PxTransform(CastVec(position), CastQuat(orientation)));
		body->attachShape(*shape);
		body->userData = (void*)index;
		m_Scene->addActor(*body);

		m_ActorIndexs[name] = index;
		m_ActorNames.push_back(name);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(orientation);
		m_Actors.push_back(body);

		shape->release();
	}

	void CPhysXManagerImplementation::onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) 
	{
		for (physx::PxU32 i = 0; i < nbPairs; i++)
		{
			size_t indexActor1 = (size_t)pairHeader.actors[0]->userData;
			size_t indexActor2 = (size_t)pairHeader.actors[0]->userData;

			std::string	Actor1Name = m_ActorNames[indexActor1];
			std::string Actor2Name = m_ActorNames[indexActor2];

			if (pairs[i].events == physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
			{
				GLOBAL_LUA_FUNCTION("on_collision_enter", void, Actor1Name, Actor2Name);
			}
			else if (pairs[i].events == physx::PxPairFlag::eNOTIFY_TOUCH_LOST)
			{
				GLOBAL_LUA_FUNCTION("on_collision_exit", void, Actor1Name, Actor2Name);
			}
			else if (pairs[i].events == physx::PxPairFlag::eNOTIFY_TOUCH_PERSISTS)
			{
				GLOBAL_LUA_FUNCTION("on_collision_stay", void, Actor1Name, Actor2Name);
			}
		}
	}

	void CPhysXManagerImplementation::onShapeHit(const physx::PxControllerShapeHit& hit) {
		size_t indexActor = (size_t)hit.controller->getUserData();
		size_t indexShape = (size_t)hit.shape->userData;
		if (indexActor < m_ActorNames.size() && indexShape < m_ActorNames.size())
		{
			std::string	ActorName = m_ActorNames[indexActor];
			std::string	ShapeName = m_ActorNames[indexShape];

			if (ActorName != ShapeName) {
				Vect3f hitPosition = Vect3f(hit.worldPos.x, hit.worldPos.y, hit.worldPos.z);
				Vect3f hitNormal = Vect3f(hit.worldNormal.x, hit.worldNormal.y, hit.worldNormal.z);

				GLOBAL_LUA_FUNCTION("on_shape_hit", void, ActorName, ShapeName, Vect3f(hitPosition), Vect3f(hitNormal));
			}
		}
	}

	void CPhysXManagerImplementation::onControllerHit(const physx::PxControllersHit& hit) {
		int a = 1;
	}
	void CPhysXManagerImplementation::onObstacleHit(const physx::PxControllerObstacleHit& hit) {
		int a = 1;
	}

	void CPhysXManagerImplementation::onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count)
	{
		
		for (physx::PxU32 i = 0; i < count; i++)
		{
			//ignore pairs when shapes have been deleted
			if ((pairs[i].flags & (physx::PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | physx::PxTriggerPairFlag::eREMOVED_SHAPE_OTHER)))
				continue;

			size_t indexTrigger = (size_t)pairs[i].triggerActor->userData;
			size_t indexActor = (size_t)pairs[i].otherActor->userData;

			//TO DO Customizar Trigger

			std::string triggerName = m_ActorNames[indexTrigger];
			std::string actorName = m_ActorNames[indexActor];

			if (pairs[i].status == physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
			{
				GLOBAL_LUA_FUNCTION("on_trigger_enter", void, triggerName, actorName);
			}
			else if (pairs[i].status == physx::PxPairFlag::eNOTIFY_TOUCH_LOST)
			{
				GLOBAL_LUA_FUNCTION("on_trigger_exit", void, triggerName, actorName);
			}
			else if (pairs[i].status == physx::PxPairFlag::eNOTIFY_TOUCH_PERSISTS)
			{
				//??
			}

			/*CSoundManager& l_SM = CEngine::GetInstance().GetSoundManager();

			if (triggerName == "Event")
			{
				SoundEvent se;
				se.eventName = "EventTest";

				l_SM.PlayEvent(se);
			} else if (triggerName == "Switch1")
			{
				SoundSwitchValue ssv;
				ssv.switchName = "SwitchTest";
				ssv.switchValue = "Dirt";
				l_SM.SetSwitch(ssv);

				SoundEvent se;
				se.eventName = "MoveEvent";
				l_SM.PlayEvent(se);
			}
			else if (triggerName == "Switch2")
			{
				SoundSwitchValue ssv;
				ssv.switchName = "SwitchTest";
				ssv.switchValue = "Forest";
				l_SM.SetSwitch(ssv);


				SoundEvent se;
				se.eventName = "MoveEvent";
				l_SM.PlayEvent(se);
			}
			else if (triggerName == "State1")
			{
				SoundStateValue ssv;
				ssv.stateName = "StateTest";
				ssv.ValueName = "Music_On";
				l_SM.BroadCastState(ssv);
				
			}else if (triggerName == "State2")
			{
				SoundStateValue ssv;
				ssv.stateName = "StateTest";
				ssv.ValueName = "Music_Off";
				l_SM.BroadCastState(ssv);
			}
			else if (triggerName == "RTPC")
			{
				//TO DO
			}
			*/

			//LOG_INFO_APPLICATION("Trigger \"%s\" fired with \"%s\"", triggerName.c_str(), actorName.c_str());

		}
	}


	void CPhysXManagerImplementation::AddCharacterController(const std::string& actorName, float height, float radius, const Vect3f& position, const Quatf& orientation, const std::string& material, float density, const std::string& characterControllerName)
	{
		Assert(m_Materials.find(material) != m_Materials.end(), "No existe el material");
		Assert(m_CharacterControllers.find(characterControllerName) == m_CharacterControllers.end(), "Ya existe este controlador");
		Assert(m_ActorIndexs.find(characterControllerName) == m_ActorIndexs.end(),"Ya existe este Actor");

		auto *l_Material = m_Materials[material];
		size_t index = m_Actors.size();
		
		physx::PxCapsuleControllerDesc desc;
		desc.height = height;
		desc.radius = radius;
		desc.climbingMode = physx::PxCapsuleClimbingMode::eEASY;
		desc.slopeLimit = cosf(3.1415f / 6); 
		desc.stepOffset = 0.5;
		desc.density = density;
		desc.reportCallback = this; 
		desc.position = physx::PxExtendedVec3(position.x, position.y + radius + height*0.5f, position.z);
		desc.material = l_Material;
		desc.userData = (void*)index;

		physx::PxController* cct = m_ControllerManager->createController(desc);
		cct->getActor()->userData = (void*)index;

		m_CharacterControllers[characterControllerName] = cct;

		m_ActorIndexs[characterControllerName] = index;
		m_ActorNames.push_back(characterControllerName);
		m_ActorPositions.push_back(position);
		m_ActorOrientations.push_back(Quatf(0.0f, 0.0f, 0.0f, 1.0f));//Nunca deber�a rotar
		m_Actors.push_back(cct->getActor());

	}

	void CPhysXManager::MoveTrigger(const std::string& triggerName, const Vect3f& movement)
	{
		size_t index = m_ActorIndexs[triggerName];
		Vect3f l_newPosition = m_ActorPositions[index] + movement;
		SetActorPosition(triggerName, l_newPosition);
	}


	CPhysXManager::CharacterControllerData CPhysXManager::MoveCharacterController(const std::string& characterControllerName, const Vect3f& movement, float elapsedTime)
	{
		physx::PxController* cct = m_CharacterControllers[characterControllerName];
		const physx::PxControllerFilters filters(nullptr, nullptr, nullptr);

		size_t index = (size_t)cct->getUserData();
		physx::PxControllerCollisionFlags collisionFlags = cct->move(CastVec(movement), movement.Length()*0.01f, elapsedTime, filters);

		physx::PxRigidDynamic* actor = cct->getActor();

		physx::PxExtendedVec3 p = cct->getFootPosition();
		physx::PxVec3 v = actor->getLinearVelocity();

		CharacterControllerData result;
		result.position = CastVec(p);
		result.linearVelocity = CastVec(v);

		return result;
	}

	bool CPhysXManager::Raycast(const Vect3f& origin, const Vect3f& end, RaycastData* result_)
	{
		Vect3f dir = end - origin;
		float len = dir.Length();
		dir = dir.Normalize();

		physx::PxFilterData filterData;
		filterData.setToDefault();
		filterData.word0 = 1;

		physx::PxRaycastBuffer hit;
		bool status = m_Scene->raycast(CastVec(origin), CastVec(dir), len, hit);//, physx::PxHitFlags(physx::PxHitFlag::eDEFAULT), physx::PxQueryFilterData(filterData, physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC));
		if (status && result_ != nullptr)
		{
			result_->position = Vect3f(hit.block.position.x, hit.block.position.y, hit.block.position.z);
			result_->normal = Vect3f(hit.block.normal.x, hit.block.normal.y, hit.block.normal.z);
			result_->distance = hit.block.distance;
			result_->actor = m_ActorNames[(size_t)hit.block.actor->userData];
		}
		return status;
	}

}