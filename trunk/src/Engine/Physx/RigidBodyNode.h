#pragma once

#include "PhysicNode.h"

namespace engine
{
	class CRigidBodyNode : public CPhysicNode
	{
	public:
		CRigidBodyNode(const CXMLElement *node, CTransform* transform, const std::string &m_Name);
		virtual ~CRigidBodyNode();
		virtual void SetPosition(const Vect3f aPosition) override;
		virtual void Move(const Vect3f &offset, float dt) override;
		virtual void GetTransform(CTransform *aTransform) override;
	};
}