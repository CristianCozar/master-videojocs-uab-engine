#pragma once
#include <map>
#include <vector>
#include "Math\MathTypes.h"
#include "Math\Vector3.h"
#include "Math\Quaternion.h"

namespace physx
{
	class PxFoundation;
	class PxPhysics;
	class PxDefaultCpuDispatcher;
	class PxScene;
	class PxCooking;
	class PxMaterial;
	class PxActor;
	class PxRigidActor;
	class PxControllerManager;
	class PxController;
	class PxJoint;

	namespace debugger
	{
		namespace comm
		{
			class PvdConnection;
		}
	}

	typedef debugger::comm::PvdConnection PxVisualDebuggerConnection;

}

namespace engine
{
	

	struct RaycastData
	{
		Vect3f position;
		Vect3f normal;
		float distance;
		std::string actor;
	};

	class CPhysXManager
	{
		struct CharacterControllerData
		{
			Vect3f position;
			Vect3f linearVelocity;
		};

	protected:
		CPhysXManager();
		physx::PxFoundation					*m_Foundation;
		physx::PxPhysics					*m_PhysX;
#ifdef _DEBUG
		physx::PxVisualDebuggerConnection	*m_DebugConnection;
#endif
		physx::PxDefaultCpuDispatcher		*m_Dispatcher;
		physx::PxScene						*m_Scene;
		physx::PxCooking					*m_Cooking;
		physx::PxControllerManager			*m_ControllerManager;
		std::map<std::string, physx::PxMaterial*> m_Materials;
		std::map<std::string, size_t> m_ActorIndexs;
		std::vector<std::string> m_ActorNames;
		std::vector<Vect3f> m_ActorPositions;
		std::vector<Quatf> m_ActorOrientations;
		std::vector<physx::PxActor*> m_Actors;
		std::vector<std::string> m_ActorsToDestroy;
	//	std::vector<std::string> m_StaticBoxToAdd;
	//	std::vector<Vect3f> m_StaticBoxPos;
	//	std::vector<Vect3f> m_StaticBoxSize;
		std::vector<std::string> m_CharacterControllerToAdd;
		std::vector<Vect3f> m_CharacterControllerPos;
		std::vector<float> m_CharacterControllerHeight;
		std::vector<float> m_CharacterControllerRadius;
		std::vector<Quatf> m_CharacterControllerOrientation;
		std::vector<float> m_CharacterControllerDensity;


		std::map<std::string, physx::PxController*> m_CharacterControllers;
		float m_LeftoverSeconds;

	public:
		static CPhysXManager* CreatePhysXManager();
		void RegisterMaterial(const std::string &name, float staticFriction, float dynamicFriction, float restitution);
		virtual ~CPhysXManager();
		CharacterControllerData MoveCharacterController(const std::string& characterControllerName, const Vect3f& movement, float elapsedTime);	
		void MoveTrigger(const std::string& triggerName, const Vect3f& movement);
		void Update(float _dt);
		void createStaticBox(const  std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, const std::string& materialName);
		void createStaticSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, const std::string& materialName);
		void createStaticConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, const std::string& materialName);
		void createStaticTriangleMesh(const std::string& name, std::vector<Vect3f> vertices, std::vector<Vect3w> indices, Vect3f position, Quatf orientation, const std::string& materialName);
		void createStaticPlane(const std::string& name, float x, float y, float z, float dist, const std::string& materialName);
		void createDynamicBox(const std::string& name, float sizeX, float sizeY, float sizeZ, Vect3f position, Quatf orientation, float density, const std::string& materialName);
		void createDynamicSphere(const std::string& name, float radius, Vect3f position, Quatf orientation, float density, const std::string& materialName);
		void createDynamicConvexMesh(const std::string& name, std::vector<Vect3f> vertices, Vect3f position, Quatf orientation, float density, const std::string& materialName);
		void AddTriggerBox(const std::string& name, float sizeX, float sizeY, float sizeZ, const Vect3f& position, const Quatf& orientation, const std::string& materialName);
		void AddTriggerSphere(const std::string& name, float radius, const Vect3f& position, const Quatf& orientation, const std::string& materialName);
		void DeleteRequestedActors();
		void DeleteActorRequest(const std::string& aName);
		void DeleteAllActors();
		//void AddStaticBoxRequest(const std::string& aName, Vect3f pos, Vect3f size);
		//void AddRequestedStaticBox();
		void AddCharacterControllerRequest(const std::string& actorName, float height, float radius, const Vect3f& position, const Quatf& orientation, const std::string& material, float density, const std::string& characterControllerName);
		void AddRequestedCharControllers();

		//void AddCharacterController(const std::string& actorName, float height, float radius, const Vect3f& position, const Quatf& orientation, const std::string& material, float density, const std::string& characterControllerName);
		bool Raycast(const Vect3f& origin, const Vect3f& end, RaycastData* result_);
		Vect3f GetActorPosition(const std::string& aName) const;
		Quatf GetActorOrientation(const std::string& aName) const;
		void GetActorTransform(const std::string& aName, Vect3f& position, Quatf& orientation) const;
		void SetActorTransform(const std::string& aName, Vect3f position, Quatf orientation);
		void SetActorPosition(const std::string& aName, Vect3f position);
		void SetActorOrientation(const std::string& aName, Quatf orientation);
		bool m_RequestDeleteAllActors = false;
	};
}