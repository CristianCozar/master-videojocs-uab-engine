#pragma once

#include "Utils\TemplatedMapVector.h"
#include "Mesh.h"

namespace engine
{
	class CMeshManager
	{
	public:
		CMeshManager();
		virtual ~CMeshManager();
		CMesh* GetMesh(const std::string& aName, const std::string& aFilename);
		void AddMesh(const std::string& aName, CMesh* aMesh);
		void Destroy();
		void ReloadMaterials();
	private:
		base::utils::CTemplatedMapVector<CMesh> mMeshes;

	};
}

