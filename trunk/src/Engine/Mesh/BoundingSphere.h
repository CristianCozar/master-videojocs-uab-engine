#pragma once

#include "Utils\Defines.h"
#include "Math\Vector3.h"

class CBoundingSphere
{
public:
	CBoundingSphere()
	{
		m_Radius = 0.0f;
		m_Center.SetZero();
	}

	virtual ~CBoundingSphere()
	{
	}

	GET_SET_REF(float, Radius);
	GET_SET_REF(Vect3f, Center);


protected:
	float m_Radius;
	Vect3f m_Center;

};





