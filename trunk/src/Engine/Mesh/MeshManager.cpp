#include "MeshManager.h"

namespace engine
{

	CMeshManager::CMeshManager()
	{

	}

	CMeshManager::~CMeshManager()
	{
		Destroy();
	}

	CMesh* CMeshManager::GetMesh(const std::string& aName, const std::string& aFilename)
	{
		if (mMeshes.Exist(aName))
		{
			return mMeshes(aName);
		}
		else{
			CMesh* lMesh = new CMesh(aName);
			bool lRes = lMesh->Load(aFilename);
			if (lRes)
			{
				bool lAdd = mMeshes.Add(aName, lMesh);
				if (lAdd)
				{
					return mMeshes(aName);
				}
				else{
					return nullptr;
				}
			}
			else{
				return nullptr;
			}
		}
	}

	void CMeshManager::AddMesh(const std::string& aName, CMesh* aMesh)
	{
		mMeshes.Add(aName, aMesh);
	}

	void CMeshManager::ReloadMaterials()
	{
		for (unsigned int i = 0; i < mMeshes.GetCount(); ++i)
		{
			mMeshes[i]->ReloadMaterials();
		}
	}
	
	void CMeshManager::Destroy()
	{
		for (unsigned int i = 0; i < mMeshes.GetCount(); ++i)
		{
			mMeshes[i]->~CMesh();
		}
	}
}
