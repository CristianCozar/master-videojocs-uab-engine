#pragma once

#include "Mesh/Geometry.h"
#include "Mesh/CVertexBuffer.h"
#include "Mesh/CIndexBuffer.h"

#include "TemplatedIndexedGeometry.h"
#include "TemplatedGeometry.h"

namespace engine{

	//VB

#define GEOMETRY_DEFINITION(ClassName, TopologyType)\
	template< class T > \
	class ClassName : public CTemplatedGeometry<T>\
	{\
	public:\
	ClassName(CVertexBuffer<T>* aVB)\
	: CTemplatedGeometry(aVB, TopologyType)\
	{}\
}; \

	GEOMETRY_DEFINITION(CGeometryPointList, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	GEOMETRY_DEFINITION(CGeometryLinesList, D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	GEOMETRY_DEFINITION(CGeometryTriangleList, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	GEOMETRY_DEFINITION(CGeometryTriangleStrip, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);



	//VB + IB

#define DEFINE_TOPOLGY_INDEXED_GEOMETRY(ClassName, TopologyType) \
template< class T >  \
	class ClassName : public CTemplatedIndexedGeometry < T >  \
{\
	public:\
	ClassName(CVertexBuffer< T > * aVB, CIndexBuffer* aIB) \
	:  CTemplatedIndexedGeometry(aVB, aIB, TopologyType)  \
	{  \
	}  \
	virtual ~ClassName()\
	{}   \
	};

	DEFINE_TOPOLGY_INDEXED_GEOMETRY(CIndexedGeometryLineList, D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	DEFINE_TOPOLGY_INDEXED_GEOMETRY(CIndexedGeometryTriangleList, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	DEFINE_TOPOLGY_INDEXED_GEOMETRY(CIndexedGeometryTriangleStrip, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

}