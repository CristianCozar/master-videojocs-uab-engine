#pragma once


#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3dcsx.h>
#include "Debug\CustomAssert.h"

#include "Utils\Types.h"

namespace engine
{


	class CGeometry
	{
	public:
		CGeometry(D3D11_PRIMITIVE_TOPOLOGY PrimitiveTopology) : m_PrimitiveTopology(PrimitiveTopology)
		{
		}

		virtual ~CGeometry()
		{
		}

		virtual bool Render(ID3D11DeviceContext*)
		{
			Assert(false, "Este metodo no se deberia llamar");
			return false;
		}

		virtual bool RenderIndexed(ID3D11DeviceContext*, uint32 IndexCount = -1, uint32 StartIndexLocation = 0, uint32 BaseVertexLocation = 0)
		{
			Assert(false, "Este metodo no se deberia llamar");
			return false;
		}

	protected:
		D3D11_PRIMITIVE_TOPOLOGY m_PrimitiveTopology;
	};


}