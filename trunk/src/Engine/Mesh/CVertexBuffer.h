#pragma once

#include "Debug\CustomAssert.h"
#include "CBuffer.h"
#include "RenderManager.h"


template< class TVertexType>
class CVertexBuffer : public CBuffer
{
public:
	CVertexBuffer(engine::CRenderManager& RenderManager, void* aRawData, uint32 aNumVertexs, bool Dynamic = false) : mNumVertexs(aNumVertexs)
	{
		D3D11_BUFFER_DESC lVertexBufferDesc;
		ZeroMemory(&lVertexBufferDesc, sizeof(lVertexBufferDesc));
		lVertexBufferDesc.Usage = Dynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
		lVertexBufferDesc.ByteWidth = sizeof(TVertexType)*mNumVertexs;
		lVertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		lVertexBufferDesc.CPUAccessFlags = Dynamic ? D3D11_CPU_ACCESS_WRITE : 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = aRawData;
		ID3D11Device* lDevice = RenderManager.GetDevice();
		HRESULT l_HR = lDevice->CreateBuffer(&lVertexBufferDesc, &InitData, &m_pBuffer);
		// Ya no necesitamos los datos raw, as� que liberamos memoria
		//free(aRawData);
		Assert(SUCCEEDED(l_HR), "Error al crear el Vertex Buffer");
	}

	virtual ~CVertexBuffer()
	{
		if (m_pBuffer)
			m_pBuffer->Release();
	}
	
	virtual void Bind(ID3D11DeviceContext* aContext)
	{
		uint32 offset = 0, stride = GetStride();
		aContext->IASetVertexBuffers(0, 1, &m_pBuffer, &stride, &offset);
	}

	bool UpdateVertexs(void *Vtxs, unsigned int VtxsCount)
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(mappedResource));

		ID3D11DeviceContext* l_DeviceContext = engine::CEngine::GetInstance().GetRenderManager().GetDeviceContext();
		HRESULT l_HR = l_DeviceContext->Map(m_pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(l_HR))
			return false;

		memcpy(mappedResource.pData, Vtxs, sizeof(TVertexType) * VtxsCount);

		l_DeviceContext->Unmap(m_pBuffer, 0);

		return true;

	}

	inline uint32 GetNumVertex() const { return mNumVertexs; }
	inline uint32 GetStride() const { return sizeof(TVertexType); }


protected:
	uint32 mNumVertexs;
};