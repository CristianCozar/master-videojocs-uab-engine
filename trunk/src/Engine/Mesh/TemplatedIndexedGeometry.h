#pragma once

#include "Geometry.h"
#include "CVertexBuffer.h"
#include "CIndexBuffer.h"
#include "Utils\CheckedDelete.h"

namespace engine{


	template <class TVertexType>
	class CTemplatedIndexedGeometry : public CGeometry
	{
	public:
		CTemplatedIndexedGeometry(CVertexBuffer<TVertexType>* aVertexBuffer, CIndexBuffer* aIndexBuffer, D3D11_PRIMITIVE_TOPOLOGY PrimitiveTopology) : CGeometry(PrimitiveTopology), m_VertexBuffer(aVertexBuffer), m_IndexBuffer(aIndexBuffer)
		{
		}

		virtual ~CTemplatedIndexedGeometry()
		{
			base::utils::CheckedDelete(m_VertexBuffer);
			base::utils::CheckedDelete(m_IndexBuffer);
		}

		//virtual bool Render(ID3D11DeviceContext* aContext, uint32 IndexCount = -1, uint32 StartIndexLocation = 0, uint32 BaseVertexLocation = 0)
		virtual bool RenderIndexed(ID3D11DeviceContext* aContext, uint32 IndexCount = -1, uint32 StartIndexLocation = 0, uint32 BaseVertexLocation = 0)
		{
			//Send the Vertex buffer to the GPU
			m_VertexBuffer->Bind(aContext);
			m_IndexBuffer->Bind(aContext);

			// Configure the type of topology to be renderer ( p.e. Triangles, Quads, points,... ) 
			aContext->IASetPrimitiveTopology(m_PrimitiveTopology);


			// Finally draw the geometry 
			aContext->DrawIndexed(IndexCount == -1 ? m_IndexBuffer->GetIndexCount() : IndexCount, StartIndexLocation, BaseVertexLocation);

			return true;
		}

	private:
		CVertexBuffer<TVertexType> *m_VertexBuffer;
		CIndexBuffer* m_IndexBuffer;
	};

}