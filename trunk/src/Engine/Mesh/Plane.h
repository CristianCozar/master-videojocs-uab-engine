#ifndef __CPlane_HH__
#define __CPlane_HH__

#pragma once

#include "Mesh.h"


namespace engine {
	class CGeometry;
	class CMaterial;

	class CPlane : public CMesh
	{
	public:
		CPlane(const CXMLElement* aElement);
		virtual ~CPlane();
	};
}

#endif
