#pragma once

#include "Geometry.h"
#include "CVertexBuffer.h"
#include "Utils\CheckedDelete.h"


namespace engine
{
	template <class TVertexType>
	class CTemplatedGeometry : public CGeometry
	{
	public:
		CTemplatedGeometry(CVertexBuffer<TVertexType>* VertexBuffer, D3D11_PRIMITIVE_TOPOLOGY PrimitiveTopology) : CGeometry(PrimitiveTopology), m_VertexBuffer(VertexBuffer)
		{
		}

		virtual ~CTemplatedGeometry()
		{
			base::utils::CheckedDelete(m_VertexBuffer);
		}

		bool Render(ID3D11DeviceContext* aContext, uint32 aActiveParticles)
		{
			//Send the Vertex buffer to the GPU
			m_VertexBuffer->Bind(aContext);

			// Configure the type of topology to be renderer ( p.e. Triangles, Quads, points,... ) 
			aContext->IASetPrimitiveTopology(m_PrimitiveTopology);


			// Finally draw the geometry
			aContext->Draw(aActiveParticles, 0);
			

			return true;
		}

		virtual bool Render(ID3D11DeviceContext* aContext)
		{
			//Send the Vertex buffer to the GPU
			m_VertexBuffer->Bind(aContext);

			// Configure the type of topology to be renderer ( p.e. Triangles, Quads, points,... ) 
			aContext->IASetPrimitiveTopology(m_PrimitiveTopology);


			// Finally draw the geometry
			aContext->Draw(m_VertexBuffer->GetNumVertex(), 0);


			return true;
		}

		CVertexBuffer<TVertexType> *m_VertexBuffer;

	protected:
		
	};
}