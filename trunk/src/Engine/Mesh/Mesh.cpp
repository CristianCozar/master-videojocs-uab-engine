#include "Mesh.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3dcsx.h>
#include "Debug\CustomAssert.h"

#include "Utils/BinFileReader.h"
#include "Materials/Material.h"
#include "TemplatedGeometry.h"
#include "TemplatedIndexedGeometry.h"
#include "CVertexBuffer.h"
#include "CIndexBuffer.h"
#include "Engine.h"
#include "VertexsTypes_HelperMacros.h"
#include "LayoutUtility.h"
#include "Materials/MaterialManager.h"
#include "Utils\CheckedDelete.h"
#include "Utils\Logger\Logger.h"
#include "GeometryMacros.h"

#include "MeshManager.h"

#define HEADER 0xFE55
#define FOOTER 0x55FE 
#define LONG_HEADER 0xFE56
#define LONG_FOOTER 0x56FE

/*
FILE FORMAT

Header 0xFE55 -- unsigned short
#mats -- unsigned short
{
namesize -- unsigned short
materialname -- string
flags -- unsigned short
VB.size -- unsigned short
VB -- lista floats
IB.size -- unsigned short
IB -- lista unsigned shorts
BOUNDINGBOX AABB -- 6 float
BOUNDINGSPHERE -- 4 float
}
Footer 0x55FE -- unsigned short
*/

/*
FILE FORMAT (LONG)

Header 0xFE56 -- unsigned short
#mats -- unsigned short
{
namesize -- unsigned short
materialname -- string
flags -- unsigned short
VB.size -- unsigned long*
VB -- lista floats
IB.size -- unsigned long*
IB -- lista unsigned longs*
BOUNDINGBOX AABB -- 6 float
BOUNDINGSPHERE -- 4 float
}
Footer 0x56FE -- unsigned short
*/

namespace engine
{
	CMesh::CMesh()
	{
		mCount = 0;
		mAABB = CAxisAlignedBoundingBox();
		mBoundingSphere = CBoundingSphere();
	}

	CMesh::~CMesh()
	{
		base::utils::CheckedDelete(mGeometries);
		mMaterialNames.clear();
		mMaterials.clear(); 
	}

	void CMesh::ReloadMaterials()
	{
		CMaterialManager& lMM = engine::CEngine::GetInstance().GetMaterialManager();
		for (int i = 0; i < mMaterialNames.size(); ++i)
		{
			mMaterials[i] = lMM(mMaterialNames[i]);
		}
	}


	bool CMesh::Load(const std::string& aFilename)
	{
		bool lOk = false;
		base::utils::CBinFileReader Reader(aFilename);

		if (Reader.Open())
		{
			unsigned short lHeader = Reader.Read<unsigned short>();
			if (lHeader == HEADER)
			{
				CRenderManager& lRM = engine::CEngine::GetInstance().GetRenderManager();
				CMaterialManager& lMM = engine::CEngine::GetInstance().GetMaterialManager();
				mCount = Reader.Read<unsigned short>();
				mMaterials.resize(mCount);
				mMaterialNames.resize(mCount);
				mGeometries.resize(mCount);
				for (int i = 0; i < mCount; ++i)
				{
					std::string lName = Reader.Read<std::string>();

					engine::CMaterial* lMat = lMM(lName);
					LOG_ERROR_APPLICATION_IF((lMat != nullptr), "No se ha encontrado material %s para el mesh.", lName.c_str());
					Assert(lMat != nullptr, "No se ha encontrado material %s para el mesh.", lName.c_str());

					mMaterials[i] = lMat;
					mMaterialNames[i] = lName;

					// Obtain the flags of the vertex of the geometry and the number of vertex
					unsigned short lVertexFlags = Reader.Read<unsigned short>();
					unsigned short lNumVertices = Reader.Read<unsigned short>();
					uint32 lVertexSize = VertexTypes::GetVertexSize(lVertexFlags);
					size_t lNumBytesVertices = lNumVertices * lVertexSize;
					void* lVertexData = Reader.ReadRaw(lNumBytesVertices);

					unsigned short IBsize = Reader.Read<unsigned short>();
					void* lIBdata = Reader.ReadRaw(IBsize*sizeof(unsigned short));

					uint32 lSize = sizeof( unsigned short );

					CIndexBuffer* lIB = new CIndexBuffer(lRM, lIBdata, IBsize, 16);

					CGeometry* lGeometry = nullptr;
					if (lVertexFlags == VertexTypes::PositionNormal::GetVertexFlags()) // 5
					{
						CVertexBuffer< VertexTypes::PositionNormal >* lVB = new CVertexBuffer< VertexTypes::PositionNormal >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormal >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionNormalUV::GetVertexFlags()) // 37
					{
						CVertexBuffer< VertexTypes::PositionNormalUV >* lVB = new CVertexBuffer< VertexTypes::PositionNormalUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormalUV >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionNormalUVUV2::GetVertexFlags()) // 101
					{
						CVertexBuffer< VertexTypes::PositionNormalUVUV2 >* lVB = new CVertexBuffer< VertexTypes::PositionNormalUVUV2 >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormalUVUV2 >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBump::GetVertexFlags()) // 29
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, sizeof(VertexTypes::PositionBump), 0, 12, 24, 40, 56);
						CVertexBuffer< VertexTypes::PositionBump >* lVB = new CVertexBuffer< VertexTypes::PositionBump >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBump >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBumpUV::GetVertexFlags()) // 61
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, sizeof(VertexTypes::PositionBumpUV), 0, 12, 24, 40, 56);
						CVertexBuffer< VertexTypes::PositionBumpUV >* lVB = new CVertexBuffer< VertexTypes::PositionBumpUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBumpUV >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBumpUVUV2::GetVertexFlags()) // 125
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, 64.0f, 0.0f, 12.0f, 24.0f, 36.0f, 48.0f);
						CVertexBuffer< VertexTypes::PositionBumpUVUV2 >* lVB = new CVertexBuffer< VertexTypes::PositionBumpUVUV2 >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBumpUVUV2 >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionWeightIndicesNormalUV::GetVertexFlags()) // 1573
					{
						CVertexBuffer< VertexTypes::PositionWeightIndicesNormalUV >* lVB = new CVertexBuffer< VertexTypes::PositionWeightIndicesNormalUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionWeightIndicesNormalUV >(lVB, lIB);
					}

					delete lVertexData;
					delete lIBdata;
					
					mGeometries[i] = lGeometry;

					float lX = Reader.Read<float>();
					float lY = Reader.Read<float>();
					float lZ = Reader.Read<float>();
					mAABB.SetMin(Vect3f(lX, lY, lZ));

					lX = Reader.Read<float>();
					lY = Reader.Read<float>();
					lZ = Reader.Read<float>();
					mAABB.SetMax(Vect3f(lX, lY, lZ));

					Vect3f BS;
					lX = Reader.Read<float>();
					lY = Reader.Read<float>();
					lZ = Reader.Read<float>();
					BS.Set(lX, lY, lZ);
					mBoundingSphere.SetCenter(BS);
					mBoundingSphere.SetRadius(Reader.Read<float>());
				}
				if (Reader.Read<unsigned short>() == FOOTER)
				{
					Reader.Close();
					lOk = true;
				}
				else{
					Reader.Close();
					lOk = false;
				}

			}
			// COMIENZA CASO LONG
			else if (lHeader == LONG_HEADER)
			{
				LOG_INFO_APPLICATION("El mesh de nombre %s utiliza unsigned LONG en vez de unsigned SHORT porque tiene la cabecera 0xFE56", aFilename);
				CRenderManager& lRM = engine::CEngine::GetInstance().GetRenderManager();
				CMaterialManager& lMM = engine::CEngine::GetInstance().GetMaterialManager();
				mCount = Reader.Read<unsigned short>();
				mMaterials.resize(mCount);
				mMaterialNames.resize(mCount);
				mGeometries.resize(mCount);
				for (int i = 0; i < mCount; ++i)
				{
					std::string lName = Reader.Read<std::string>();

					engine::CMaterial* lMat = lMM(lName);
					LOG_ERROR_APPLICATION_IF((lMat != nullptr), "No se ha encontrado material %s para el mesh.", lName.c_str());
					Assert(lMat != nullptr, "No se ha encontrado material %s para el mesh.", lName.c_str());

					mMaterials[i] = lMat;
					mMaterialNames[i] = lName;

					// Obtain the flags of the vertex of the geometry and the number of vertex
					unsigned short lVertexFlags = Reader.Read<unsigned short>();
					unsigned long lNumVertices = Reader.Read<unsigned long>();
					uint32 lVertexSize = VertexTypes::GetVertexSize(lVertexFlags);
					size_t lNumBytesVertices = lNumVertices * lVertexSize;
					void* lVertexData = Reader.ReadRaw(lNumBytesVertices);

					unsigned short IBsize = Reader.Read<unsigned long>();
					void* lIBdata = Reader.ReadRaw(IBsize*sizeof(unsigned long));

					uint32 lSize = sizeof(unsigned long);

					CIndexBuffer* lIB = new CIndexBuffer(lRM, lIBdata, IBsize, 32);

					CGeometry* lGeometry = nullptr;
					if (lVertexFlags == VertexTypes::PositionNormal::GetVertexFlags()) // 5
					{
						CVertexBuffer< VertexTypes::PositionNormal >* lVB = new CVertexBuffer< VertexTypes::PositionNormal >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormal >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionNormalUV::GetVertexFlags()) // 37
					{
						CVertexBuffer< VertexTypes::PositionNormalUV >* lVB = new CVertexBuffer< VertexTypes::PositionNormalUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormalUV >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionNormalUVUV2::GetVertexFlags()) // 101
					{
						CVertexBuffer< VertexTypes::PositionNormalUVUV2 >* lVB = new CVertexBuffer< VertexTypes::PositionNormalUVUV2 >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionNormalUVUV2 >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBump::GetVertexFlags()) // 29
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, sizeof(VertexTypes::PositionBump), 0, 12, 24, 40, 56);
						CVertexBuffer< VertexTypes::PositionBump >* lVB = new CVertexBuffer< VertexTypes::PositionBump >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBump >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBumpUV::GetVertexFlags()) // 61
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, sizeof(VertexTypes::PositionBumpUV), 0, 12, 24, 40, 56);
						CVertexBuffer< VertexTypes::PositionBumpUV >* lVB = new CVertexBuffer< VertexTypes::PositionBumpUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBumpUV >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionBumpUVUV2::GetVertexFlags()) // 125
					{
						CalcTangentsAndBinormals(lVertexData, (unsigned short*)lIBdata, lNumVertices, IBsize, 64.0f, 0.0f, 12.0f, 24.0f, 36.0f, 48.0f);
						CVertexBuffer< VertexTypes::PositionBumpUVUV2 >* lVB = new CVertexBuffer< VertexTypes::PositionBumpUVUV2 >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionBumpUVUV2 >(lVB, lIB);
					}
					else if (lVertexFlags == VertexTypes::PositionWeightIndicesNormalUV::GetVertexFlags()) // 1573
					{
						CVertexBuffer< VertexTypes::PositionWeightIndicesNormalUV >* lVB = new CVertexBuffer< VertexTypes::PositionWeightIndicesNormalUV >(lRM, lVertexData, lNumVertices);
						lGeometry = new CIndexedGeometryTriangleList< VertexTypes::PositionWeightIndicesNormalUV >(lVB, lIB);
					}

					delete lVertexData;
					delete lIBdata;

					mGeometries[i] = lGeometry;

					float lX = Reader.Read<float>();
					float lY = Reader.Read<float>();
					float lZ = Reader.Read<float>();
					mAABB.SetMin(Vect3f(lX, lY, lZ));

					lX = Reader.Read<float>();
					lY = Reader.Read<float>();
					lZ = Reader.Read<float>();
					mAABB.SetMax(Vect3f(lX, lY, lZ));

					Vect3f BS;
					lX = Reader.Read<float>();
					lY = Reader.Read<float>();
					lZ = Reader.Read<float>();
					BS.Set(lX, lY, lZ);
					mBoundingSphere.SetCenter(BS);
					mBoundingSphere.SetRadius(Reader.Read<float>());
				}
				if (Reader.Read<unsigned short>() == LONG_FOOTER)
				{
					Reader.Close();
					lOk = true;
				}
				else{
					Reader.Close();
					lOk = false;
				}

			}
			// TERMINA CASO LONG
			else{
				Reader.Close();
				lOk = false;
			}

		}
		else{
			lOk = false;
		}
		return lOk;
	}

	void CMesh::CalcTangentsAndBinormals(void *VtxsData, unsigned short *IdxsData, size_t VtxCount,
		size_t IdxCount, size_t VertexStride, size_t GeometryStride, size_t NormalStride, size_t
		TangentStride, size_t BiNormalStride, size_t TextureCoordsStride)
	{
		Vect3f *tan1 = new Vect3f[VtxCount * 2];
		Vect3f *tan2 = tan1 + VtxCount;
		ZeroMemory(tan1, VtxCount * sizeof(Vect3f) * 2);
		unsigned char *l_VtxAddress = (unsigned char *)VtxsData;
		for (size_t b = 0; b<IdxCount; b += 3)
		{
			unsigned short i1 = IdxsData[b];
			unsigned short i2 = IdxsData[b + 1];
			unsigned short i3 = IdxsData[b + 2];
			Vect3f *v1 = (Vect3f *)&l_VtxAddress[i1*VertexStride + GeometryStride];
			Vect3f *v2 = (Vect3f *)&l_VtxAddress[i2*VertexStride + GeometryStride];
			Vect3f *v3 = (Vect3f *)&l_VtxAddress[i3*VertexStride + GeometryStride];
			Vect2f *w1 = (Vect2f
				*)&l_VtxAddress[i1*VertexStride + TextureCoordsStride];
			Vect2f *w2 = (Vect2f
				*)&l_VtxAddress[i2*VertexStride + TextureCoordsStride];
			Vect2f *w3 = (Vect2f
				*)&l_VtxAddress[i3*VertexStride + TextureCoordsStride];
			float x1 = v2->x - v1->x;
			float x2 = v3->x - v1->x;
			float y1 = v2->y - v1->y;
			float y2 = v3->y - v1->y;
			float z1 = v2->z - v1->z;
			float z2 = v3->z - v1->z;
			float s1 = w2->x - w1->x;
			float s2 = w3->x - w1->x;
			float t1 = w2->y - w1->y;
			float t2 = w3->y - w1->y;
			float r = 1.0F / (s1 * t2 - s2 * t1);
			Vect3f sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
				(t2 * z1 - t1 * z2) * r);
			Vect3f tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
				(s1 * z2 - s2 * z1) * r);
			assert(i1<VtxCount);
			assert(i2<VtxCount);
			assert(i3<VtxCount);
			tan1[i1] += sdir;
			tan1[i2] += sdir;
			tan1[i3] += sdir;
			tan2[i1] += tdir;
			tan2[i2] += tdir;
			tan2[i3] += tdir;
		}
		for (size_t b = 0; b<VtxCount; ++b)
		{
			Vect3f *l_NormalVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + NormalStride];
			Vect3f *l_TangentVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + TangentStride];
			Vect4f *l_TangentVtx4 = (Vect4f
				*)&l_VtxAddress[b*VertexStride + TangentStride];
			Vect3f *l_BiNormalVtx = (Vect3f
				*)&l_VtxAddress[b*VertexStride + BiNormalStride];
			const Vect3f& t = tan1[b];
			// Gram-Schmidt orthogonalize
			Vect3f l_VAl = t - (*l_NormalVtx)*((*l_NormalVtx)*t);
			l_VAl.Normalize();
			*l_TangentVtx = l_VAl;
			// Calculate handedness
			Vect3f l_Cross;
			l_Cross = (*l_NormalVtx) ^ (*l_TangentVtx);
			l_TangentVtx4->w = (l_Cross*(tan2[b]))< 0.0f ? -1.0f : 1.0f;
			*l_BiNormalVtx = (*l_NormalVtx) ^ (*l_TangentVtx);
		}
		delete[] tan1;
	}




	bool CMesh::Render(engine::CRenderManager& aRenderManager, const Vect3f &aPos, const Vect3f &aScale, const bool aTestFrustum)
	{
		bool lOk = false;
		if (!aTestFrustum || aRenderManager.GetFrustum().IsVisible(mBoundingSphere, aPos, aScale))
		{
			lOk = true;
			ID3D11DeviceContext* lContext = aRenderManager.GetDeviceContext();
			for (size_t i = 0, lCount = mGeometries.size(); i < lCount; ++i)
			{
				mMaterials[i]->Apply();
				mGeometries[i]->RenderIndexed(lContext);
			}
		}
		return lOk;

	}

}