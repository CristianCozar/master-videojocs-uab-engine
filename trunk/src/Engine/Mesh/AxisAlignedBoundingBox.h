#pragma once

#include "Utils\Defines.h"
#include "Math\Vector3.h"

class CAxisAlignedBoundingBox
{
public:
	CAxisAlignedBoundingBox()
	{
		m_Min.SetZero();
		m_Max.SetZero();
	}

	virtual ~CAxisAlignedBoundingBox()
	{
	}

	GET_SET_REF(Vect3f, Min);
	GET_SET_REF(Vect3f, Max);


protected:
	Vect3f m_Min;
	Vect3f m_Max;

};





