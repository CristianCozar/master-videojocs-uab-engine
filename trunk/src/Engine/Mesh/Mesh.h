#pragma once

#include <vector>

#include "Utils\Name.h"
#include "RenderManager.h"
#include "Materials\Material.h"
#include "Geometry.h"
#include "AxisAlignedBoundingBox.h"
#include "BoundingSphere.h"
#include "TemplatedIndexedGeometry.h"

namespace engine
{

	class CMesh : public CName
	{
	public:
		CMesh();
		CMesh(const std::string& aName) : CName(aName) {};
		virtual ~CMesh();
		bool Load(const std::string& aFilename);
		void ReloadMaterials();
		void CalcTangentsAndBinormals(void *VtxsData, unsigned short *IdxsData, size_t VtxCount, size_t IdxCount, size_t VertexStride, size_t GeometryStride, size_t NormalStride, size_t TangentStride, size_t BiNormalStride, size_t TextureCoordsStride);
		bool Render(CRenderManager& aRenderManager, const Vect3f &aPos, const Vect3f &aScale, const bool aTestFrustum);

	protected:
		uint16 mCount;
		std::vector<engine::CMaterial*> mMaterials;
		std::vector<std::string> mMaterialNames;
		std::vector<CGeometry*> mGeometries;
		CAxisAlignedBoundingBox mAABB;
		CBoundingSphere mBoundingSphere;

	};
}