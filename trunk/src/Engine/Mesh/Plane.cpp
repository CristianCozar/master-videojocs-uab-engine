#include "Plane.h"
#include "Mesh/TemplatedIndexedGeometry.h"
#include "LayoutUtility.h"
#include "Engine.h"
#include "Materials/Material.h"
#include "Materials/MaterialManager.h"
#include "Mesh\GeometryMacros.h"

namespace engine
{

	CPlane::CPlane(const CXMLElement* aElement)
		: CMesh()
	{
		mMaterials.resize(1);
		mGeometries.resize(1);

		CMaterialManager& lMM = CEngine::GetInstance().GetMaterialManager();
		mMaterials[0] = lMM(aElement->GetAttribute<std::string>("material", "PlaneMaterial"));
		//mMaterials[0] = lMM("PlaneMaterial");

		bool lOk = false;
		/*VertexTypes::PositionNormalUV lPlaneVertex[4] =
		{
			{ Vect3f(-0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f), Vect2f(0.0f, 0.0f) },
			{ Vect3f(0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f), Vect2f(0.0f, 1.0f) },
			{ Vect3f(0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f), Vect2f(1.0f, 1.0f) },
			{ Vect3f(-0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f), Vect2f(1.0f, 0.0f) }
		};*/

		VertexTypes::PositionNormal lPlaneVertex[4] =
		{
			{ Vect3f(-0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f), },
			{ Vect3f(0.5f, 0.0f, 0.5f), Vect3f(0.0f, 1.0f, 0.0f), },
			{ Vect3f(0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f), },
			{ Vect3f(-0.5f, 0.0f, -0.5f), Vect3f(0.0f, 1.0f, 0.0f) }
		};

		//uint16 lIndices[6] = { 0, 3, 1, 3, 2, 1 };
		uint16 lIndices[6] = { 0, 1, 3, 3, 1, 2 };
		CRenderManager& lRenderManager = CEngine::GetInstance().GetRenderManager();
		CVertexBuffer<VertexTypes::PositionNormal> * lVB = new CVertexBuffer<VertexTypes::PositionNormal>(lRenderManager, lPlaneVertex, 4);
		CIndexBuffer* lIB = new CIndexBuffer(lRenderManager, &lIndices, 6, 16);
		mGeometries[0] = new CIndexedGeometryTriangleList< VertexTypes::PositionNormal >(lVB, lIB);
	}

	CPlane::~CPlane()
	{
	}
}