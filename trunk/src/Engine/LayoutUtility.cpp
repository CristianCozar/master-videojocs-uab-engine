#include "LayoutUtility.h"

#include "Debug\CustomAssert.h"

#include "Engine.h"

namespace VertexTypes
{
    uint32 GetVertexSize(uint32 aVertexFlags)
    {
        if (aVertexFlags == PositionNormal::GetVertexFlags())
            return sizeof(PositionNormal);
        if (aVertexFlags == PositionNormalUV::GetVertexFlags())
            return sizeof(PositionNormalUV);
		if (aVertexFlags == PositionNormalUVUV2::GetVertexFlags())
			return sizeof(PositionNormalUVUV2);
        if (aVertexFlags == PositionBump::GetVertexFlags())
            return sizeof(PositionBump);
        if (aVertexFlags == PositionBumpUV::GetVertexFlags())
            return sizeof(PositionBumpUV);
        if (aVertexFlags == PositionBumpUVUV2::GetVertexFlags())
            return sizeof(PositionBumpUVUV2);
		if (aVertexFlags == PositionUV::GetVertexFlags())
			return sizeof(PositionUV);
		if (aVertexFlags == PositionWeightIndicesNormalUV::GetVertexFlags())
			return sizeof(PositionWeightIndicesNormalUV);
		if (aVertexFlags == PositionWeightIndicesBumpUV::GetVertexFlags())
			return sizeof(PositionWeightIndicesBumpUV);
		if (aVertexFlags == PositionNormalUVColor::GetVertexFlags())
			return sizeof(PositionNormalUVColor);
		if (aVertexFlags == ParticleVertex::GetVertexFlags())
			return sizeof(ParticleVertex);
		if (aVertexFlags == GUIVertex::GetVertexFlags())
			return sizeof(GUIVertex);

        Assert(false, "No existe el tipo de VertexSize con flags %d", aVertexFlags);
        return 0;
    }

    bool CreateInputLayout(engine::CRenderManager& aRenderManager, uint32 aVertexFlags, ID3DBlob* aBlob, ID3D11InputLayout ** aVertexLayout)
    {
        if (aVertexFlags == PositionNormal::GetVertexFlags())
            return PositionNormal::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
        if (aVertexFlags == PositionNormalUV::GetVertexFlags())
            return PositionNormalUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == PositionNormalUVUV2::GetVertexFlags())
			return PositionNormalUVUV2::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
        if (aVertexFlags == PositionBump::GetVertexFlags())
            return PositionBump::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
        if (aVertexFlags == PositionBumpUV::GetVertexFlags())
            return PositionBumpUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
        if (aVertexFlags == PositionBumpUVUV2::GetVertexFlags())
            return PositionBumpUVUV2::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == PositionUV::GetVertexFlags())
			return PositionUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == PositionWeightIndicesNormalUV::GetVertexFlags())
			return PositionWeightIndicesNormalUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == PositionWeightIndicesBumpUV::GetVertexFlags())
			return PositionWeightIndicesBumpUV::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == PositionNormalUVColor::GetVertexFlags())
			return PositionNormalUVColor::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == ParticleVertex::GetVertexFlags())
			return ParticleVertex::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
		if (aVertexFlags == GUIVertex::GetVertexFlags())
			return GUIVertex::CreateInputLayout(aRenderManager, aBlob, aVertexLayout);
    }

    uint32 GetFlagsFromString(const std::string & aString)
    {
        if (aString == "PositionNormal")
            return PositionNormal::GetVertexFlags();
        if (aString == "PositionNormalUV")
            return PositionNormalUV::GetVertexFlags();
		if (aString == "PositionNormalUVUV2")
			return PositionNormalUVUV2::GetVertexFlags();
        if (aString == "PositionBump")
            return PositionBump::GetVertexFlags();
        if (aString == "PositionBumpUV")
            return PositionBumpUV::GetVertexFlags();
        if (aString == "PositionBumpUVUV2")
			return PositionBumpUVUV2::GetVertexFlags();
		if (aString == "PositionBumpUV")
			return PositionBumpUVUV2::GetVertexFlags();
		if (aString == "PositionWeightIndicesNormalUV")
			return PositionWeightIndicesNormalUV::GetVertexFlags();
		if (aString == "PositionWeightIndicesBumpUV")
			return PositionWeightIndicesBumpUV::GetVertexFlags();
		if (aString == "PositionUV")
			return PositionUV::GetVertexFlags();
		if (aString == "PositionNormalUVColor")
			return PositionNormalUVColor::GetVertexFlags();
		if (aString == "ParticleVertex")
			return ParticleVertex::GetVertexFlags();
		if (aString == "GUIVertex")
			return GUIVertex::GetVertexFlags();
		

        Assert(false, "No existe conjunto de flags con el nombre %s", aString.c_str());
        return 0;
    }
}