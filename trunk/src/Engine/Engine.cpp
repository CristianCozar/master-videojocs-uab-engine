#include "Engine.h"

#include "Input\ActionManager.h"
#include "RenderManager.h"
#include "Materials\MaterialManager.h"
#include "Camera\SphericalCameraController.h"
#include "Camera\FPSCameraController.h"
#include "Camera\TPSCameraController.h"
#include "Shaders\ShaderManager.h"
#include "Debug\imgui.h"
#include "Debug\imgui_impl_dx11.h"
#include "Materials\TextureManager.h"
#include "Materials\Texture.h"
#include "Shaders\EffectManager.h"
#include "Shaders\TechniquePoolManager.h"
#include "Mesh\MeshManager.h"
#include "Mesh\Mesh.h"
#include "ConstantBufferManager.h"
#include "Scenes\SceneManager.h"
#include "Cinematics\CinematicManager.h"
#include "RenderPipeline\RenderPipeline.h"
#include "Lights\LightManager.h"
#include "AnimatedModel\AnimatedModelManager.h"
#include "Scripting\ScriptManager.h"
#include "Paths.h"
#include "Physx\PhysXManager.h"
#include "Physx\PhysXManagerImplementation.h"
#include "GUI\GUIManager.h"
#include "Particles\ParticleManager.h"
#include "Wwise\CSoundManager.h"


/*void PrintInt(int i)
{
	printf("[PrintInt] %d", i);
}

void PrintFloat(float i)
{
	printf("[PrintInt] %f", i);
}

class float2
{
public:
	float x, y;
public:
	float2()
		: x(0), y(0)
	{

	}
	float2(float _x, float _y)
		: x(_x), y(_y)
	{

	}

	float2  operator + (const float2& otro) const
	{
		return (float2(x + otro.x, y + otro.y));
	}
};

bool LuaTest()
{
	printf("LUA + LUABIND DEMO\n");

	// Init of the lua state and the bindings
	lua_State* lua = luaL_newstate();
	luaL_openlibs(lua);
	luabind::open(lua);
	lua_atpanic(lua, lua::atpanic);
	lua_register(lua, "_ALERT", lua::atpanic);

	// Binding functionality

	// Simple methods
	REGISTER_LUA_FUNCTION(lua, "print_int", &PrintInt);
	REGISTER_LUA_FUNCTION(lua, "print_float", &PrintFloat);

	// Binding a class
	module(lua)
		[
			class_<float2>("float2")
			.def(constructor<>())
			.def(constructor<float, float>())
			.def(const_self + const_self)
			.def_readwrite("x", &float2::x)
			.def_readwrite("y", &float2::y)
		];

	// Calling directly a piece of code
	luaL_dostring(lua, "print_float(5.0)");

	// Loading and executing a file
	int status = luaL_loadfile(lua, "example.lua");
	auto const lua_ok = LUA_OK;
	if (status != lua_ok)
	{
		if (status == LUA_ERRSYNTAX)
			printf("[LUA]Sintax Error: %s", lua_tostring(lua, -1));

		else if (status == LUA_ERRFILE)
			printf("[LUA]File Error: %s", lua_tostring(lua, -1));

		else if (status == LUA_ERRMEM)
			printf("[LUA]Memory Error: %s", lua_tostring(lua, -1));

		return false;
	}

	status = lua_pcall(lua, 0, LUA_MULTRET, 0);
	if (status != lua_ok)
	{
		std::string err = lua_tostring(lua, -1);
		printf("[LUA]Error executing code %s", err.c_str());
	}

	float2 a(5.0f, 10.0f);
	float2 b(10.0f, 5.0f);
	float2 position = call_function<float2>(lua, "sum_float2", std::ref(a), std::ref(b));
	float2 position2 = call_function<float2>(lua, "get_float2");

	// Close the lua state
	lua_close(lua);
	return false;
}*/


namespace engine
{
	CEngine::CEngine() : m_RenderManager(nullptr)
		, m_ActionManager(nullptr)
		, m_CameraController(nullptr)
		, m_Clock()
		, m_PrevTime(m_Clock.now())
	{

	}

	void CEngine::InitRender(HWND hWnd, int width, int height)
	{
		SethWnd(hWnd);
		SetWidth(width);
		SetHeight(height);
		bool lOk = GLOBAL_LUA_FUNCTION("reinit_imgui", bool);
	}

	void CEngine::Init(HWND hWnd, int width, int height)
	{
		SethWnd(hWnd);
		SetWidth(width);
		SetHeight(height);
		/*// A�adimos action manager
		engine::CActionManager *l_ActionManager = new engine::CActionManager();
		l_ActionManager->InitInputManager(hWnd);
		l_ActionManager->LoadActions("actions.xml");
		SetActionManager(l_ActionManager);
		
		// A�adimos render manager
		engine::CRenderManager *l_RenderManager = new engine::CRenderManager();
		l_RenderManager->Init(hWnd, width, height);
		l_RenderManager->SetModelMatrix(Mat44f().SetIdentity());
		l_RenderManager->SetProjectionMatrix(0.8f, (float)width / (float)height, 0.1f, 100.0f);
		l_RenderManager->SetViewMatrix(Vect3f(5.0f, 2.0f, 3.5f), Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 1.0f, 0.0f));
		SetRenderManager(l_RenderManager);

		// A�adir Texture manager
		engine::CTextureManager *l_TextureManager = new engine::CTextureManager();
		SetTextureManager(l_TextureManager);


		// A�adir shader manager
		engine::CShaderManager *l_ShaderManager = new engine::CShaderManager();
		l_ShaderManager->Load("shaders.xml");
		SetShaderManager(l_ShaderManager);

		// A�adir effect manager
		engine::CEffectManager *l_EffectManager = new engine::CEffectManager();
		l_EffectManager->Load("effects.xml");
		SetEffectManager(l_EffectManager);

		//A�adir Constant Buffer Manager
		engine::CConstantBufferManager *l_ConstantBufferManager = new engine::CConstantBufferManager();
		SetConstantBufferManager(l_ConstantBufferManager);

		// A�adir Mesh manager
		engine::CMeshManager *l_MeshManager = new engine::CMeshManager();
		SetMeshManager(l_MeshManager);

		// A�adir techniques pool manager
		engine::CTechniquePoolManager *l_TechniquePoolManager = new engine::CTechniquePoolManager();
		l_TechniquePoolManager->Load(FILENAME_TECHNIQUE_POOLS);
		SetTechniquePoolManager(l_TechniquePoolManager);

		//A�adir AnimatedModel Manager 
		engine::CAnimatedModelManager *l_animatedModelManager = new engine::CAnimatedModelManager();
		l_animatedModelManager->Load("animated_models.xml");
		SetAnimatedModelManager(l_animatedModelManager);

		// A�adir material manager
		engine::CMaterialManager *l_MaterialManager = new engine::CMaterialManager();
		l_MaterialManager->Load();
		//l_MaterialManager->Save();
		SetMaterialManager(l_MaterialManager);

		//A�adir Lights Manager
		engine::CLightManager *l_LightManager = new engine::CLightManager();
		l_LightManager->Load("lights.xml");
		SetLightManager(l_LightManager);

		// A�adir Scene Manager
		engine::CSceneManager *l_SceneManager = new engine::CSceneManager();
		l_SceneManager->Load(FILENAME_SCENES);
		SetSceneManager(l_SceneManager);

		// A�adimos camara  
		engine::CCameraController *cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
		SetCameraController(cc);

		// A�adir Cinematic Manager
		engine::CCinematicManager *l_CinematicManager = new engine::CCinematicManager();
		l_CinematicManager->Load("data/CINEMATICS.xml");
		SetCinematicManager(l_CinematicManager);

		// A�adir render pipeline
		engine::CRenderPipeline *l_RenderPipeline = new engine::CRenderPipeline();
		l_RenderPipeline->Load(FILENAME_RENDER_PIPELINE);
		SetRenderPipeline(l_RenderPipeline);

		ImGui_ImplDX11_Init(hWnd, m_RenderManager->GetDevice(), m_RenderManager->GetDeviceContext());*/

		// A�adir script manager
		engine::CScriptManager *l_ScriptManager = new engine::CScriptManager();
		l_ScriptManager->LoadFromPath("data/globalScripts/");
		l_ScriptManager->LoadFromFile("data/ScriptableObjects.xml");
		SetScriptManager(l_ScriptManager);

		bool lOk = GLOBAL_LUA_FUNCTION("init_engine", bool);

		l_ScriptManager->Start();

		SoundEvent se;
		se.eventName = "Level1Music";
		CScene* l_Scene = m_SceneManager->GetIndex(0);
		if (l_Scene->GetName() == "Level1")
		{
			m_SoundManager->PlayEvent(se);
		}


		


		//engine::CPhysXManager::CreatePhysXManager();

		//Test PhysX

		/*m_PhysXManager->RegisterMaterial("Default", 0.1f, 0.1f, 0.5f);
		m_PhysXManager->createStaticPlane("TestPlane", 0.0f, 1.0f, 0.0f, 0.0f);
		Vect3f testPos = Vect3f(0.0f, 5.0f, 0.0f);
		Quatf testOrient = Quatf(0.0f, 0.0f, 1.0f, 0.0f);
		Vect3f testforward = testOrient.GetForwardVector();
		Vect3f testvect = testOrient.EulerFromQuat();

		m_PhysXManager->createStaticBox("TestStaticBox", 1.0f, 1.0f, 1.0f, testPos, testOrient);
		testPos.y += 5.0f;
		testPos.x += 5.0f;
		m_PhysXManager->createDynamicBox("TestDynamicBox", 1.0f, 1.0f, 1.0f, testPos, testOrient, 1.0f);
		testPos.y += 5.0f;
		testPos.x -= 5.0f;		
		m_PhysXManager->createDynamicSphere("TestDynamicSphere", 1.0f, testPos, testOrient, 1.0f);
		testPos.y += 5.0f;
		m_PhysXManager->createDynamicBox("TestDynamicBox3", 1.0f, 1.0f, 1.0f, testPos, testOrient, 1.0f);
		testPos.y = 0.5f;
		testPos.x -= 5.0f;
		m_PhysXManager->AddTriggerBox("TestTrigger", 1.0f, 1.0f, 1.0f, testPos, testOrient);
		testPos.y += 5.0f;
		m_PhysXManager->createDynamicBox("TestDynamicBox3", 1.0f, 1.0f, 1.0f, testPos, testOrient, 1.0f);
		testPos.y = 0.5f;
		testPos.x -= 5.0f;
		m_PhysXManager->createStaticBox("TestStatic2", 1.0f, 5.0f, 1.0f, testPos, testOrient);
		testPos.y = 0.0f;
		testPos.x = 10.0f;
		CPhysXManagerImplementation* test = static_cast<CPhysXManagerImplementation*>(m_PhysXManager);


		test->AddCharacterController("TestCharacter", 1.5f, 1.0f, testPos, testOrient, "Default", 1.0f, "TestController");*/

	/*	bool hit = m_PhysXManager->Raycast(Vect3f(0.0f, 0.0f, 0.0f), Vect3f(0.0f, 15.0f, 0.0f), &res);
		if (hit)
		{
			LOG_INFO_APPLICATION("%s", res->actor);
		}*/

	/*	Vect3f testGetPos = m_PhysXManager->GetActorPosition("TestPlane");
		Quatf testGetOrient = m_PhysXManager->GetActorOrientation("TestPlane");

		

		Vect3f testGetPos2;
		Quatf testGetOrient2;

		m_PhysXManager->GetActorTransform("TestPlane", testGetPos2, testGetOrient2);*/

	}

	CEngine::~CEngine()
	{
		delete m_ScriptManager;
		delete m_ActionManager;
		delete m_RenderManager;
		delete m_CameraController;
		delete m_ShaderManager;
		delete m_MaterialManager;
		delete m_EffectManager;
		delete m_TextureManager;
		delete m_MeshManager;
		//delete m_ConstantBufferManager;
		delete m_SceneManager;
		delete m_CinematicManager;
		delete m_RenderPipeline;
		delete m_TechniquePoolManager;
		//delete m_LightManager;
		delete m_AnimatedModelManager;
		delete m_PhysXManager;
		delete m_ParticleManager;
		delete m_GUIManager;
		ImGui_ImplDX11_Shutdown();
	}

	void CEngine::ProcessInputs()
	{
		ImGui_ImplDX11_NewFrame();
		m_ActionManager->Update();
		m_CameraController->HandleActions(m_ActionManager);
	}

	void CEngine::Update()
	{
		// EMPIEZA CALCULO DELTA TIME
		auto currentTime = m_Clock.now();
		std::chrono::duration<float> chronoDeltaTime = currentTime - m_PrevTime;
		m_PrevTime = currentTime;

		float dt = chronoDeltaTime.count() > 0.5f ? 0.5f : chronoDeltaTime.count();
		// TERMINA CALCULO DELTA TIME

		Vect3f spherePos = Vect3f(0.0f, 0.0f, 0.0f);
		if (m_ActionManager->Get("SELECTCAMERAMODE")->active) {
			if (m_ActionManager->Get("FIGURE")->active)
			{
				engine::CCameraController *cc;
				switch ((int)m_ActionManager->Get("FIGURE")->value)
				{
					case 0:
						cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
					case 1:
						cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
					case 2:
						cc = new engine::CTPSCameraController();
						break;
					default:
						cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
						break;
				}
				SetCameraController(cc);
			}
		}
		else 
		{
			if (m_ActionManager->Get("FIGURE")->active)
			{
				selection = (int)m_ActionManager->Get("FIGURE")->value;
			}
		}
		m_GUIManager->Update(dt);
		m_SceneManager->Update(dt);
		m_ScriptManager->Update(dt);
		m_CinematicManager->Update(dt);
		m_PhysXManager->Update(dt);
		m_GUIManager->PostUpdate();
		m_SoundManager->Update(m_CameraController);
		//m_PhysXManager->MoveCharacterController("TestController", Vect3f(-1.0f,0.0f,0.0f), dt);
		m_CameraController->Update(dt);
		m_CameraController->SetToRenderManager(*m_RenderManager);

		//prueba sonido RTPC

		SoundRTPC sRTPC;
		sRTPC.RTPCName = "CameraDistance";
		float distance = m_CameraController->GetPosition().Distance(Vect3f(0.0f, 0.0f, 0.0f));
		m_SoundManager->SetRTPCValue(sRTPC, distance);


	}

	void CEngine::Render()
	{
		m_RenderPipeline->Execute(*m_RenderManager);
	}

	void CEngine::DebugUI()
	{
		/*static bool ParticleManager = true;
		ImGui::Begin("Particle Manager", &ParticleManager);
		if (ParticleManager)
		{
			m_ParticleManager->RenderDebugGUI();
		}
		ImGui::End();*/

		bool SceneManager;
		ImGui::Begin("Scene Manager", &SceneManager);
		//if (SceneManager)
		//{
			if (ImGui::Button("Pause/Resume Scenes")) {
				m_SceneManager->PauseScenes();
			}
			if (ImGui::Button("Debug Mode")) {
				m_SceneManager->DebugMode();
			}
		//}
		ImGui::End();

		/*bool CinemManager;
		ImGui::Begin("Cinematic Manager", &CinemManager);

		m_CinematicManager->RenderDebugGUI();

		ImGui::End();*/

		if (ImGui::CollapsingHeader("General debug"))
		{
			ImGui::Indent();
			ImGui::Text("FPS: %d", (int)ImGui::GetIO().Framerate);
			for (int i = 0; i < MAX_PLAYERS; i++) {
				if (m_ActionManager->IsGamepadConnected(i))
				{
					ImGui::Text("Player %d: Gamepad connected", i + 1);
				}
				else
				{
					ImGui::Text("Player %d: Gamepad not connected", i + 1);
				}
			}

			if (ImGui::Button("FPS Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CFPSCameraController(Vect3f(-3.5f, 1.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
				SetCameraController(cc);
			}
			ImGui::SameLine();
			if (ImGui::Button("Spherical Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CSphericalCameraController(Vect3f(0.0f, 0.0f, 0.0f), 1.5f, -1.5f, 20.0f, 1.0f);
				SetCameraController(cc);
			}
			ImGui::SameLine();
			if (ImGui::Button("TPS Camera")) {
				engine::CCameraController *cc;
				cc = new engine::CTPSCameraController();
				SetCameraController(cc);
			}
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Material manager"))
		{
			ImGui::Indent();
			ImGui::PushID("MaterialManager");
			m_MaterialManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Camera settings"))
		{
			ImGui::Indent();
			ImGui::PushID("CameraSettings");
			m_CameraController->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Shader manager"))
		{
			ImGui::Indent();
			ImGui::PushID("ShaderManager");
			m_ShaderManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Render pipeline"))
		{
			ImGui::Indent();
			ImGui::PushID("RenderPipeline");
			m_RenderPipeline->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Scene manager"))
		{
			ImGui::Indent();
			ImGui::PushID("SceneManager");
			ImGui::DragFloat3("Ambient color", &m_LightManager->m_AmbientLightColor.x, 0.0f, 0.0f, 1.0f);
			m_SceneManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Cinematics manager"))
		{
			ImGui::Indent();
			ImGui::PushID("CinematicsManager");
			m_CinematicManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Scripts manager"))
		{
			ImGui::Indent();
			ImGui::PushID("ScriptsManager");
			m_ScriptManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		if (ImGui::CollapsingHeader("Particle manager"))
		{
			ImGui::Indent();
			ImGui::PushID("ParticleManager");
			m_ParticleManager->RenderDebugGUI();
			ImGui::PopID();
			ImGui::Unindent();
		}
		//if (ImGui::CollapsingHeader("Sound manager"))
		//{
		//	ImGui::Indent();
		//	ImGui::PushID("SoundManager");
			m_SoundManager->RenderDebugGUI();
		//	ImGui::PopID();
		//	ImGui::Unindent();
		//}
	}



	void CEngine::SethWnd(HWND hWnd)
	{
		m_hWnd = hWnd;
	}

	void CEngine::SetWidth(int width)
	{
		m_width = width;
	}
	void CEngine::SetHeight(int height)
	{
		m_height = height;
	}

	HWND CEngine::GethWnd()
	{
		return m_hWnd;
	}

	int CEngine::GetWidth()
	{
		return m_width;
	}
	int CEngine::GetHeight()
	{
		return m_height;
	}

}