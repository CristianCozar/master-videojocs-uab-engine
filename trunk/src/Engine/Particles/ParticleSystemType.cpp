#include "ParticleSystemType.h"
#include "Engine.h"
#include "Materials\MaterialManager.h"
#include "Debug\imgui.h"


namespace engine{
	CParticleSystemType::CParticleSystemType()
	{

	}

	CParticleSystemType::~CParticleSystemType()
	{

	}

	CParticleSystemType::CParticleSystemType(const CXMLElement* aElement) : CName(aElement), m_DebugUi(false)
	{
		CMaterialManager& l_MM = CEngine::GetInstance().GetMaterialManager();
		std::string l_MaterialName = aElement->GetAttribute<std::string>("material", "");
		if (l_MaterialName != "")
		{
			m_Material = l_MM.Get(l_MaterialName);
			m_NumFrames = aElement->GetAttribute<int>("frames", 0);
			m_TimePerFrame = aElement->GetAttribute<float>("time_per_frame", 0.0f);
			m_LoopFrames = aElement->GetAttribute<bool>("loop_frames", false);
			m_MaxActiveParticles = aElement->GetAttribute<int>("max_particles", 99999);

			m_EmitAbsolute = aElement->GetAttribute<bool>("emit_absolute", false);
			m_StartingDirectionAngle = aElement->GetAttribute<float>("starting_direction_angle", 0.0f);
			m_StartingAccelerationAngle = aElement->GetAttribute<float>("starting_acceleration_angle", 0.0f);
			m_Size = aElement->GetAttribute<Vect2f>("size", Vect2f(0.0f, 0.0f));

			m_EmitRate = aElement->GetAttribute<Vect2f>("emit_rate", Vect2f(0.0f, 0.0f));
			m_AwakeTime = aElement->GetAttribute<Vect2f>("awake_time", Vect2f(0.0f, 0.0f));
			m_SleepTime = aElement->GetAttribute<Vect2f>("sleep_time", Vect2f(0.0f, 0.0f));
			m_Life = aElement->GetAttribute<Vect2f>("life_time", Vect2f(0.0f, 0.0f));

			m_StartingAngle = aElement->GetAttribute<Vect2f>("starting_angle", Vect2f(0.0f, 0.0f));
			m_StartingAngularSpeed = aElement->GetAttribute<Vect2f>("starting_angular_speed", Vect2f(0.0f, 0.0f));
			m_AngularAcceleration = aElement->GetAttribute<Vect2f>("angular_acceleration", Vect2f(0.0f, 0.0f));

			m_StartingSpeed1 = aElement->GetAttribute<Vect3f>("starting_speed_1", Vect3f(0.0f, 0.0f, 0.0f));
			m_StartingSpeed2 = aElement->GetAttribute<Vect3f>("starting_speed_2", Vect3f(0.0f, 0.0f, 0.0f));

			m_StartingAcceleration1 = aElement->GetAttribute<Vect3f>("starting_acceleration_1", Vect3f(0.0f, 0.0f, 0.0f));
			m_StartingAcceleration2 = aElement->GetAttribute<Vect3f>("starting_acceleration_2", Vect3f(0.0f, 0.0f, 0.0f));

			m_Color1 = aElement->GetAttribute<CColor>("color_1", CColor(0.0f, 0.0f, 0.0f));
			m_Color2 = aElement->GetAttribute<CColor>("color_2", CColor(0.0f, 0.0f, 0.0f));

			for (const tinyxml2::XMLElement *PointSize = aElement->FirstChildElement("control_size"); PointSize != nullptr; PointSize = PointSize->NextSiblingElement("control_size"))
			{
				ControlPointSize cps;
				cps.m_Size = PointSize->GetAttribute<Vect2f>("control_point_size",Vect2f(0.0,0.0));
				cps.m_Time = PointSize->GetAttribute<Vect2f>("control_point_time", Vect2f(0.0, 0.0));
				m_ControlPointSizes.push_back(cps);
			}

			for (const tinyxml2::XMLElement *PointColor = aElement->FirstChildElement("control_color"); PointColor != nullptr; PointColor = PointColor->NextSiblingElement("control_color"))
			{
				ControlPointColor cpc;
				cpc.m_Color1 = PointColor->GetAttribute<CColor>("control_point_color1", CColor(0.0f, 0.0f, 0.0f));
				cpc.m_Color2 = PointColor->GetAttribute<CColor>("control_point_color2", CColor(0.0f, 0.0f, 0.0f));
				cpc.m_Time = PointColor->GetAttribute<Vect2f>("control_point_time", Vect2f(0.0, 0.0));

				m_ControlPointColors.push_back(cpc);
			}

		}
	}

	CMaterial* CParticleSystemType::GetMaterial()
	{
		return m_Material;
	}

	void CParticleSystemType::RenderDebugGUI()
	{
		if (m_DebugUi)
		{
			ImGui::Begin(GetName().c_str(), &m_DebugUi);
			ImGui::Text("Particle System name: %s", GetName().c_str());
			ImGui::Text("Parametros:");
			ImGui::Checkbox("Loop Frames", &m_LoopFrames);
			ImGui::Checkbox("Emit Absolute", &m_EmitAbsolute);
			ImGui::DragFloat("Time Per Frame", &m_TimePerFrame, 1.0f, 1.0f, 100.0f);
			ImGui::DragFloat("Starting Direction Angle", &m_StartingDirectionAngle, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat("Starting Acceleration Angle", &m_StartingAccelerationAngle, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Emit Rate", (float*)&m_EmitRate, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Awake Time", (float*)&m_AwakeTime, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Sleep Time", (float*)&m_SleepTime, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Life", (float*)&m_Life, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Starting Angle", (float*)&m_StartingAngle, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Starting Angluar Speed", (float*)&m_StartingAngularSpeed, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat2("Angular Acceleration", (float*)&m_AngularAcceleration, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat3("Starting Speed 1", (float*)&m_StartingSpeed1, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat3("Starting Speed 2", (float*)&m_StartingSpeed2, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat3("Starting Acceleration 1", (float*)&m_StartingAcceleration1, 1.0f, 0.0f, 100.0f);
			ImGui::DragFloat3("Starting Acceleration 2", (float*)&m_StartingAcceleration2, 1.0f, 0.0f, 100.0f);

			for (int i = 0; i < m_ControlPointSizes.size(); ++i)
			{
				std::string temp("Control Point Time (Size) ");
				temp += std::to_string(i);
				std::string temp2("Control Point Size ");
				temp2 += std::to_string(i);

				ImGui::DragFloat2(temp.c_str(), (float*) &m_ControlPointSizes[i].m_Time, 1.0f, 0.0f, m_Life.y);
				ImGui::DragFloat2(temp2.c_str(), (float*)&m_ControlPointSizes[i].m_Size, 1.0f, 0.0f, 100.0f);

			}

			if (ImGui::Button("Add Size Control Point"))
			{
				ControlPointSize l_ControlPointSize;
				l_ControlPointSize.m_Time = Vect2f(0.0, 0.0);
				l_ControlPointSize.m_Size = Vect2f(0.0, 0.0);

				m_ControlPointSizes.push_back(l_ControlPointSize);
			}

			for (int i = 0; i < m_ControlPointColors.size(); ++i)
			{
				std::string temp("Control Point Time (Color) ");
				temp += std::to_string(i);
				std::string temp2("Control Point Color first ");
				temp2 += std::to_string(i);
				std::string temp3("Control Point Color second ");
				temp3 += std::to_string(i);

				ImGui::DragFloat2(temp.c_str(), (float*)&m_ControlPointColors[i].m_Time, 1.0f, 0.0f, m_Life.y);
				ImGui::DragFloat4(temp2.c_str(), (float*)&m_ControlPointColors[i].m_Color1, 0.1f, 0.0f, 1.0f);
				ImGui::DragFloat4(temp3.c_str(), (float*)&m_ControlPointColors[i].m_Color2, 0.1f, 0.0f, 1.0f);

			}

			if (ImGui::Button("Add Color Control Point"))
			{
				ControlPointColor l_ControlPointColors;
				l_ControlPointColors.m_Time = Vect2f(0.0, 0.0);
				l_ControlPointColors.m_Color1 = CColor(0.0f, 0.0f, 0.0f);
				l_ControlPointColors.m_Color2 = CColor(0.0f, 0.0f, 0.0f);

				m_ControlPointColors.push_back(l_ControlPointColors);
			}
			ImGui::End();
		}
	}

	CXMLElement* CParticleSystemType::getXMLNode(CXMLDocument *document) {
		CXMLElement* particle = document->NewElement("particle_system");
		particle->SetAttribute("name", GetName().c_str());
		particle->SetAttribute("material", m_Material->GetName().c_str());
		particle->SetAttribute("frames", m_NumFrames);
		particle->SetAttribute("time_per_frame", m_TimePerFrame);
		particle->SetAttribute("loop_frames", m_LoopFrames);

		particle->SetAttribute("emit_absolute", m_EmitAbsolute);
		particle->SetAttribute("starting_direction_angle", m_StartingDirectionAngle);
		particle->SetAttribute("starting_acceleration_angle", m_StartingAccelerationAngle);
		particle->SetAttribute<Vect2f>("size", m_Size);

		particle->SetAttribute<Vect2f>("emit_rate", m_EmitRate);
		particle->SetAttribute<Vect2f>("awake_time", m_AwakeTime);
		particle->SetAttribute<Vect2f>("sleep_time", m_SleepTime);
		particle->SetAttribute<Vect2f>("life_time", m_Life);

		particle->SetAttribute<Vect2f>("starting_angle", m_StartingAngle);
		particle->SetAttribute<Vect2f>("starting_angular_speed", m_StartingAngularSpeed);
		particle->SetAttribute<Vect2f>("angular_acceleration", m_AngularAcceleration);

		particle->SetAttribute<Vect3f>("starting_speed_1", m_StartingSpeed1);
		particle->SetAttribute<Vect3f>("starting_speed_2", m_StartingSpeed2);

		particle->SetAttribute<Vect3f>("starting_acceleration_1", m_StartingAcceleration1);
		particle->SetAttribute<Vect3f>("starting_acceleration_2", m_StartingAcceleration2);

		particle->SetAttribute<CColor>("color_1", m_Color1);
		particle->SetAttribute<CColor>("color_2", m_Color2);

		for (int i = 0; i < m_ControlPointSizes.size(); ++i)
		{
			CXMLElement* cs = document->NewElement("control_size");

			cs->SetAttribute<Vect2f>("control_point_size", m_ControlPointSizes[i].m_Size);
			cs->SetAttribute<Vect2f>("control_point_time", m_ControlPointSizes[i].m_Time);
			particle->InsertEndChild(cs);
		}

		for (int i = 0; i < m_ControlPointColors.size(); ++i)
		{
			CXMLElement* cc = document->NewElement("control_color");

			cc->SetAttribute<CColor>("control_point_color1", m_ControlPointColors[i].m_Color1);
			cc->SetAttribute<CColor>("control_point_color2", m_ControlPointColors[i].m_Color2);
			cc->SetAttribute<Vect2f>("control_point_time", m_ControlPointColors[i].m_Time);
			particle->InsertEndChild(cc);
		}

		return particle;
	}


}