#pragma once

#include "ParticleSystemType.h"
#include "Utils\TemplatedMap.h"
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"

#include "ParticleSystemInstance.h"

namespace engine
{
	class CParticleManager : public base::utils::CTemplatedMap<CParticleSystemType>
	{
	public:
		CParticleManager();
		~CParticleManager();
		void Load(const std::string &Filename);
		void Reload();
		bool Save();
		void RenderDebugGUI();

	private:		
		std::string m_Filename;
		CParticleSystemInstance *psi;
	};
}