#include "ParticleSystemInstance.h"
#include "Math\MathUtils.h"
#include "ParticleManager.h"
#include "Math\Vector3.h"
#include "Camera\CameraController.h"
#include "ConstantBufferManager.h"
#include "Mesh\GeometryMacros.h"
#include "Debug\imgui.h"

namespace engine{

	CParticleSystemInstance::CParticleSystemInstance()
	{
		//PrepareSortForTest();
	}

	CParticleSystemInstance::~CParticleSystemInstance()
	{
		delete m_Geometry;
		//delete m_Type;
	}
	/*
	void CParticleSystemInstance::PrepareSortForTest() {
		//Creamos una lista de particulas al azar;
		for (int i = 0; i < MAX_PARTICLES_PER_INSTANCE; i++) {
			ParticleData *particle_data = new ParticleData();
			Vector3<float> position = GetRandomValue(Vector3<float>(0.0f, 0.0f, 0.0f), Vector3<float>(100.0f, 100.0f, 100.0f));
			particle_data->Position = position;
			m_ParticleData[i] = *particle_data;
		}
		m_ActiveParticles = MAX_PARTICLES_PER_INSTANCE;
		InsertSort();
	}*/

	void CParticleSystemInstance::InsertSort() {
		//double duration = (float)clock();

		float particleDistances[MAX_PARTICLES_PER_INSTANCE] = { 0 };
		CCameraController *cc = &CEngine::GetInstance().GetCameraController();

		for (int i = 0; i < m_ActiveParticles; ++i)
		{
			
			ParticleData* particle = &m_ParticleData[i];


			Vect3f P = particle->Position * 2 - cc->GetPosition();

			float distanceToCamera =  P * cc->GetFront();
			//float distanceToCamera = particle->Position.Distance(cc->GetPosition());
			particleDistances[i] = distanceToCamera;
			
			if (i != 0 && particleDistances[i - 1] < distanceToCamera) {
				for (int n = i; n > 0; n--) {
					if (particleDistances[n - 1] < distanceToCamera) {
						ParticleData tempParticle = m_ParticleData[n];
						m_ParticleData[n] = m_ParticleData[n - 1];
						m_ParticleData[n - 1] = tempParticle;

						particleDistances[n] = particleDistances[n - 1];
						particleDistances[n - 1] = distanceToCamera;
					}
					else {
						break;
					}
				}
			}
		}
		//duration = (clock() - duration) / (double)CLOCKS_PER_SEC;
		//return duration * 1000;
	}

	/*void CParticleSystemInstance::MoveParticlesRandomly() {

		for (int i = 0; i < m_ActiveParticles; ++i)
		{
			Vect3f randomMovement = GetRandomValue(Vector3<float>(-2.0f, -2.0f, -2.0f), Vector3<float>(2.0f, 2.0f, 2.0f));
			m_ParticleData[i].Position += randomMovement;
		}
	}
	*/

	CParticleSystemInstance::CParticleSystemInstance(const CXMLElement* aElement, std::string LayerName) : CSceneNode(aElement, LayerName)
	{
		std::string l_ParticleSystemName = aElement->GetAttribute<std::string>("particle_system_type", "");
		CParticleManager& l_PM = CEngine::GetInstance().GetParticleManager();

		

		if (l_ParticleSystemName != "")
		{
			m_Type = l_PM.Get(l_ParticleSystemName);
			m_EmissionBoxHalfSize = aElement->GetAttribute<Vect3f>("emission_box_size", Vect3f(1.0f, 1.0f, 1.0f) * 0.5f);
			m_EmissionVolume = m_EmissionBoxHalfSize.x * m_EmissionBoxHalfSize.y * m_EmissionBoxHalfSize.z * 8;
			m_EmissionScaler = m_Type->GetEmitAbsolute() ? 1 : 1.0f / m_EmissionVolume;
			CRenderManager& l_RM = CEngine::GetInstance().GetRenderManager();

			CVertexBuffer< VertexTypes::ParticleVertex>* lVB = new CVertexBuffer< VertexTypes::ParticleVertex >(l_RM, m_ParticleRenderableData, MAX_PARTICLES_PER_INSTANCE, true);
			m_Geometry = new CGeometryPointList<VertexTypes::ParticleVertex>(lVB);
		}
	}

	void CParticleSystemInstance::Update(float ElapsedTime)
	{
		m_AwakeTimer -= ElapsedTime;
		while (m_AwakeTimer < 0)
		{
			m_Awake = !m_Awake;
			m_AwakeTimer += GetRandomValue(m_Awake ? m_Type->GetAwakeTime() : m_Type->GetSleepTime());
		}

		if (m_Awake)
		{
			m_NextParticleEmission -= ElapsedTime;
			while (m_NextParticleEmission < 0)
			{
				if (m_ActiveParticles < MAX_PARTICLES_PER_INSTANCE && m_ActiveParticles < m_Type->GetMaxActiveParticles())
				{
					ParticleData particle = {};
					particle.Position = GetRandomValue(-m_EmissionBoxHalfSize, m_EmissionBoxHalfSize);
					particle.Velocity = GetRandomValue(m_Type->GetStartingSpeed1(), m_Type->GetStartingSpeed2());
					particle.Acceleration = GetRandomValue(m_Type->GetStartingAcceleration1(), m_Type->GetStartingAcceleration2());
					particle.Angle = GetRandomValue(m_Type->GetStartingAngle());
					particle.AngularSpeed = GetRandomValue(m_Type->GetStartingAngularSpeed());
					particle.AngularAcceleration = GetRandomValue(m_Type->GetAngularAcceleration());
					particle.currentFrame = 1;
					particle.TimeToNextFrame = m_Type->GetTimePerFrame();
					particle.Lifetime = 0;
					particle.TotalLife = GetRandomValue(m_Type->GetLife());
					particle.SizeControlPoint = 0;
					particle.LastSizeControlTime = 0;
					particle.LastSize = GetRandomValue(m_Type->GetControlPointSizes()[0].m_Size);
					particle.NextSizeControlTime = m_Type->GetControlPointSizes().size() < 2 ? particle.TotalLife : GetRandomValue(m_Type->GetControlPointSizes()[1].m_Time);
					particle.NextSize = m_Type->GetControlPointSizes().size() < 2 ? particle.LastSize : GetRandomValue(m_Type->GetControlPointSizes()[1].m_Size);
					particle.CurrentSize = particle.LastSize;


					particle.ColorControlPoint = 0;
					particle.LastColorControlTime = 0;
					particle.LastColor = GetRandomValue(m_Type->GetControlPointColors()[0].m_Color1, m_Type->GetControlPointColors()[0].m_Color2);
					particle.NextColorControlTime = m_Type->GetControlPointColors().size() < 2 ? particle.TotalLife : GetRandomValue(m_Type->GetControlPointColors()[1].m_Time);
					particle.NextColor = m_Type->GetControlPointColors().size() < 2 ? particle.LastColor : GetRandomValue(m_Type->GetControlPointColors()[1].m_Color1, m_Type->GetControlPointColors()[1].m_Color2);
					particle.CurrentColor = particle.LastColor;

					m_ParticleData[m_ActiveParticles] = particle;
					++m_ActiveParticles;
					InsertSort();
				}

				m_NextParticleEmission += ComputeTimeToNextParticle();
			}

			for (int i = 0; i < m_ActiveParticles; ++i)
			{
				ParticleData* particle = &m_ParticleData[i];
				particle->Position += particle->Velocity * ElapsedTime + 0.5f * ElapsedTime * ElapsedTime * particle->Acceleration;
				particle->Velocity += particle->Acceleration * ElapsedTime;
				particle->Angle += particle->AngularSpeed * ElapsedTime + 0.5f * ElapsedTime * ElapsedTime * particle->AngularAcceleration;
				particle->AngularSpeed += particle->AngularAcceleration * ElapsedTime;
				particle->TimeToNextFrame -= ElapsedTime;
				particle->Lifetime += ElapsedTime;

				while (particle->TimeToNextFrame < 0 && (m_Type->GetLoopFrames() || particle->currentFrame < m_Type->GetNumFrames() - 1))
				{
					particle->currentFrame = (particle->currentFrame + 1) % m_Type->GetNumFrames();
					particle->TimeToNextFrame += m_Type->GetTimePerFrame();
				}

				while (particle->Lifetime > particle->NextSizeControlTime && particle->Lifetime < particle->TotalLife)
				{
					++particle->SizeControlPoint;
					particle->LastSize = particle->NextSize;
					particle->LastSizeControlTime = particle->NextSizeControlTime;

					if (particle->SizeControlPoint + 1 < m_Type->GetControlPointSizes().size())
					{
						particle->NextSize = GetRandomValue(m_Type->GetControlPointSizes()[particle->SizeControlPoint + 1].m_Size);
						particle->NextSizeControlTime = GetRandomValue(m_Type->GetControlPointSizes()[particle->SizeControlPoint + 1].m_Time);
					}
					else{
						particle->NextSizeControlTime = particle->TotalLife;
					}
				}


				while (particle->Lifetime > particle->NextColorControlTime && particle->Lifetime < particle->TotalLife)
				{
					++particle->ColorControlPoint;
					particle->LastColor = particle->NextColor;
					particle->LastColorControlTime = particle->NextColorControlTime;

					if (particle->ColorControlPoint + 1 < m_Type->GetControlPointColors().size())
					{
						particle->NextColor = GetRandomValue(m_Type->GetControlPointColors()[particle->ColorControlPoint + 1].m_Color1, m_Type->GetControlPointColors()[particle->ColorControlPoint + 1].m_Color2);
						particle->NextColorControlTime = GetRandomValue(m_Type->GetControlPointColors()[particle->ColorControlPoint + 1].m_Time);
					}
					else{
						particle->NextColorControlTime = particle->TotalLife;
					}

				}

				float SizeControlAlpha = (particle->Lifetime < particle->NextSizeControlTime) ? 
					(particle->Lifetime - particle->LastSizeControlTime) / (particle->NextSizeControlTime - particle->LastSizeControlTime) : 1.0f;

				particle->CurrentSize = mathUtils::LerpFloat(particle->LastSize, particle->NextSize, SizeControlAlpha);

				float ColorControlAlpha = (particle->Lifetime < particle->NextColorControlTime) ?
					(particle->Lifetime - particle->LastColorControlTime) / (particle->NextColorControlTime - particle->LastColorControlTime) : 1.0f;

				particle->CurrentColor = particle->CurrentColor.interpolate(particle->LastColor, particle->NextColor, ColorControlAlpha);

				m_ParticleRenderableData[i].position = particle->Position;
				m_ParticleRenderableData[i].color = particle->CurrentColor;
				m_ParticleRenderableData[i].uv.x = particle->CurrentSize;
				m_ParticleRenderableData[i].uv.y = particle->Angle;
				m_ParticleRenderableData[i].uv2.x = mathUtils::LerpFloat((float)particle->currentFrame,(float)particle->currentFrame + 1.0f, particle->TimeToNextFrame);
				m_ParticleRenderableData[i].uv2.y = 0;

				if (m_ParticleData[i].Lifetime > m_ParticleData[i].TotalLife)
				{
					--m_ActiveParticles;
					m_ParticleData[i] = m_ParticleData[m_ActiveParticles];
					--i;
				}

			}



		}
		//InsertSort();

		}

	bool CParticleSystemInstance::Render(CRenderManager& aRendermanager)
	{
		InsertSort();

		CConstantBufferManager& lCB = CEngine::GetInstance().GetConstantBufferManager();
		lCB.mObjDesc.m_World = GetMatrix();
		lCB.BindVSBuffer(aRendermanager.GetDeviceContext(), CConstantBufferManager::CB_Object);


		m_Type->GetMaterial()->Apply();

		m_Geometry->m_VertexBuffer->UpdateVertexs(m_ParticleRenderableData, MAX_PARTICLES_PER_INSTANCE);

		m_Geometry->Render(aRendermanager.GetDeviceContext(), m_ActiveParticles);

		

		return true;
	}
	
	float CParticleSystemInstance::ComputeTimeToNextParticle()
	{
		float particlesPerSecPerM3 = GetRandomValue(m_Type->GetEmitRate());
		return m_EmissionScaler / particlesPerSecPerM3;
	}

	float CParticleSystemInstance::GetRandomValue(float min, float max)
	{
		float a = m_UnitDistribution(m_RandomEngine);
		float value = mathUtils::LerpFloat(min, max, a);
		return value;
	}

	Vect3f CParticleSystemInstance::GetRandomValue(Vect3f min, Vect3f max)
	{
		float a1 = m_UnitDistribution(m_RandomEngine);
		float a2 = m_UnitDistribution(m_RandomEngine);
		float a3 = m_UnitDistribution(m_RandomEngine);

		Vect3f value;
		value.x = mathUtils::LerpFloat(min.x, max.x, a1);
		value.y = mathUtils::LerpFloat(min.y, max.y, a2);
		value.z = mathUtils::LerpFloat(min.z, max.z, a3);

		return value;

	}

	CColor CParticleSystemInstance::GetRandomValue(CColor min, CColor max)
	{
		float a = m_UnitDistribution(m_RandomEngine);
		CColor value = min.Lerp(max, a);
		return value;
	}

	CColor CParticleSystemInstance::GetRandomValueHSV(CColor min, CColor max)
	{
		float a = m_UnitDistribution(m_RandomEngine);
		CColor value;

		value = value.interpolate(min, max, a);

		return value;
	}


	float CParticleSystemInstance::GetRandomValue(Vect2f value)
	{
		return GetRandomValue(value.x, value.y);
	}

	void CParticleSystemInstance::ReloadType()
	{
		CParticleManager& l_PM = CEngine::GetInstance().GetParticleManager();

		CParticleSystemType* l_PT = l_PM(m_Type->GetName());

		if (l_PT != nullptr)
		{
			m_Type = l_PT;
		}
	}
}