#pragma once

#include "Materials\Material.h"
#include "Math\Color.h"

namespace engine
{
	struct ControlPointColor
	{
		Vect2f m_Time;
		CColor m_Color1, m_Color2;
	};

	struct ControlPointSize
	{
		Vect2f m_Time;
		Vect2f m_Size;
	};


	class CParticleSystemType : public CName
	{
	public:
		CParticleSystemType();
		CParticleSystemType(const CXMLElement* aElement);
		~CParticleSystemType();
		CMaterial* GetMaterial();
		void RenderDebugGUI();
		bool m_DebugUi;
		CXMLElement* getXMLNode(CXMLDocument *document);
		GET_SET(Vect2f, EmitRate);
		GET_SET(Vect2f, AwakeTime);
		GET_SET(Vect2f, SleepTime);
		GET_SET(Vect3f, StartingSpeed1);
		GET_SET(Vect3f, StartingSpeed2);
		GET_SET(Vect3f, StartingAcceleration1);
		GET_SET(Vect3f, StartingAcceleration2);
		GET_SET(Vect2f, Life);
		GET_SET(Vect2f, StartingAngle);
		GET_SET(Vect2f, StartingAngularSpeed);
		GET_SET(Vect2f, AngularAcceleration);
		GET_SET(float, TimePerFrame);
		GET_SET(bool, LoopFrames);
		GET_SET(int, NumFrames);
		GET_SET(bool, EmitAbsolute);
		GET_SET(int, MaxActiveParticles);
		GET_SET(std::vector<ControlPointColor>, ControlPointColors);
		GET_SET(std::vector<ControlPointSize>, ControlPointSizes);

	private:
		CMaterial* m_Material;
		int m_NumFrames;
		float m_TimePerFrame;
		bool m_LoopFrames;

		bool m_EmitAbsolute;
		int m_MaxActiveParticles;
		float m_StartingDirectionAngle, m_StartingAccelerationAngle;
		Vect2f m_Size;
		Vect2f m_EmitRate, m_AwakeTime, m_SleepTime, m_Life;
		Vect2f m_StartingAngle, m_StartingAngularSpeed, m_AngularAcceleration;
		Vect3f m_StartingSpeed1, m_StartingSpeed2;
		Vect3f m_StartingAcceleration1, m_StartingAcceleration2;
		CColor m_Color1, m_Color2;
		std::vector<ControlPointColor> m_ControlPointColors;
		std::vector<ControlPointSize> m_ControlPointSizes;
	};
}