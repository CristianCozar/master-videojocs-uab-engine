#pragma once

#include "Scenes\SceneNode.h"
#include "ParticleSystemType.h"
#include <random>
#include "LayoutUtility.h"
#include "Mesh\CVertexBuffer.h"
#include "LayoutUtility.h"
#include "Mesh\TemplatedGeometry.h"

//DEBUG
#include <ctime>
#include "Utils\Logger\Logger.h"

#define MAX_PARTICLES_PER_INSTANCE 2000


namespace engine
{
	struct ParticleData
	{
		Vect3f Position, Velocity, Acceleration;
		int currentFrame;
		float TimeToNextFrame;
		float Lifetime, TotalLife;
		float Angle, AngularSpeed, AngularAcceleration;

		int ColorControlPoint, SizeControlPoint;
		
		float LastColorControlTime, NextColorControlTime;
		float LastSizeControlTime, NextSizeControlTime;

		CColor LastColor, NextColor;
		float LastSize, NextSize;

		CColor CurrentColor;
		float CurrentSize;
	};

	class CParticleSystemInstance : public CSceneNode
	{
	public:
		CParticleSystemInstance();
		CParticleSystemInstance(const CXMLElement* aElement, std::string LayerName);
		~CParticleSystemInstance();
		void Update(float ElapsedTime);
		bool Render(CRenderManager& aRendermanager);
		void InsertSort();
		void ReloadType();

		//void PrepareSortForTest(); //DEBUG
		//void MoveParticlesRandomly(); //DEBUG

	private:
		float GetRandomValue(float min, float max);
		Vect3f GetRandomValue(Vect3f min, Vect3f max);
		CColor GetRandomValue(CColor min, CColor max);
		CColor GetRandomValueHSV(CColor min, CColor max);
		float GetRandomValue(Vect2f value);
		float ComputeTimeToNextParticle();

		CParticleSystemType* m_Type;
		float m_NextParticleEmission;
		bool m_Awake;
		float m_AwakeTimer;
		Vect3f m_EmissionBoxHalfSize;
		float m_EmissionVolume, m_EmissionScaler;
		int m_ActiveParticles;
		ParticleData m_ParticleData[MAX_PARTICLES_PER_INSTANCE];
		VertexTypes::ParticleVertex m_ParticleRenderableData[MAX_PARTICLES_PER_INSTANCE];
		CTemplatedGeometry<VertexTypes::ParticleVertex>* m_Geometry;
		std::mt19937 m_RandomEngine;
		std::uniform_real_distribution<float> m_UnitDistribution;
		std::random_device rnd;

	};
}