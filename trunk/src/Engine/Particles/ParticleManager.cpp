#include "ParticleManager.h"
#include "Utils\Logger\Logger.h"
#include "Utils\CheckedDelete.h"
#include "Debug\imgui.h"
#include "Scenes\SceneManager.h"

namespace engine{

	CParticleManager::CParticleManager()
	{
		//psi = new CParticleSystemInstance();
	}

	CParticleManager::~CParticleManager()
	{

	}


	/* DEBUG
	double CParticleManager::TestSort() {
		psi->MoveParticlesRandomly();
		return psi->InsertSort();
	}*/


	void CParticleManager::Load(const std::string &Filename)
	{
		LOG_INFO_APPLICATION("Loading particle systems from '%s'", Filename.c_str());

		CXMLDocument document;
		tinyxml2::XMLError error = document.LoadFile((Filename).c_str());
		m_Filename = Filename;

		if (base::xml::SucceedLoad(error)) {
			tinyxml2::XMLElement *particleSystems = document.FirstChildElement("particle_systems");
			if (particleSystems) {
				for (tinyxml2::XMLElement *particleSystem = particleSystems->FirstChildElement("particle_system"); particleSystem != nullptr; particleSystem = particleSystem->NextSiblingElement("particle_system"))
				{
					CParticleSystemType* l_ParticleSystemType = new CParticleSystemType(particleSystem);
					Add(l_ParticleSystemType->GetName(), l_ParticleSystemType);
					//base::utils::CheckedDelete(l_ParticleSystemType);
				}
			}
		}else {
			LOG_ERROR_APPLICATION("Particle System XML '%s' does not exist", Filename.c_str());
		}
	}

	void CParticleManager::RenderDebugGUI()
	{
		for (std::map<std::string, CParticleSystemType*>::iterator it = m_ResourcesMap.begin(); it != m_ResourcesMap.end(); ++it) {
			if (it->second != nullptr) {
				ImGui::Indent();
				it->second->RenderDebugGUI();
				if (ImGui::Button(it->first.c_str()))
				{
					it->second->m_DebugUi = !it->second->m_DebugUi;
				}
				ImGui::Unindent();
			}
		}
		if (ImGui::Button("Save"))
		{
			Save();
		}
		ImGui::SameLine();
		if (ImGui::Button("Reload"))
		{
			Reload();
		}
	}

	bool CParticleManager::Save()
	{
		LOG_INFO_APPLICATION("Saving particles in '%s'", m_Filename.c_str());
		CXMLDocument doc;
		CXMLDocument *document = &doc;
		CXMLElement *particles = document->NewElement("particle_systems");

		TMapResources::iterator begin_particle = m_ResourcesMap.begin(), last_particle = m_ResourcesMap.end();
		for (; begin_particle != last_particle; ++begin_particle)
		{
			engine::CParticleSystemType *particle = begin_particle->second;
			CXMLElement* particle_node = particle->getXMLNode(document);
			particles->InsertEndChild(particle_node);

		}

		document->InsertFirstChild(particles);
		tinyxml2::XMLError eResult = document->SaveFile((m_Filename).c_str());
		return base::xml::SucceedLoad(eResult);
	}

	void CParticleManager::Reload()
	{
		m_ResourcesMap.clear();
		Load(m_Filename);

		CSceneManager& l_SM = CEngine::GetInstance().GetSceneManager();
		for (int i = 0; i < l_SM.GetCount(); ++i)
		{
			CScene* l_Scene = l_SM.GetIndex(i);
			if (l_Scene->IsActive())
			{
				for (int j = 0; j < l_Scene->GetCount(); ++j)
				{
					CLayer* l_Layer = l_Scene->GetIndex(j);
					if (l_Layer->GetName() == "Transparent")
					{
						
						for (int z = 0; z < l_Layer->GetCount(); ++z)
						{
							if (l_Layer->GetIndex(z)->GetName().find("Particle") != std::string::npos)
							{
								CParticleSystemInstance* l_PI = static_cast<CParticleSystemInstance*>(l_Layer->GetIndex(z));
								l_PI->ReloadType();
							}
						}
					}
				}
			}
		}

	}
}