#pragma once

#include <Windows.h>
#include <chrono>

#include "Utils\Singleton.h"

#define BUILD_GET_SET_ENGINE_MANAGER( Manager ) \
private: \
C##Manager* m_##Manager = nullptr; \
public: \
void Set##Manager(C##Manager* a##Manager) { m_##Manager = a##Manager;  } \
const C##Manager& Get##Manager() const { return *m_##Manager; } \
C##Manager& Get##Manager() { return *m_##Manager; } \
bool Has##Manager() { return m_##Manager != nullptr; } \

namespace engine
{
	class CActionManager;
	class CRenderManager;
	class CCameraController;
	class CMaterialManager;
	class CShaderManager;
	class CEffectManager;
	class CTechniquePoolManager;
	class CTextureManager;
	class CMeshManager;
	class CConstantBufferManager;
	class CSceneManager;
	class CLightManager;
	class CRenderPipeline;
	class CCinematicManager;
	class CAnimatedModelManager;
	class CScriptManager;
	class CPhysXManager;
	class CParticleManager;
	class CGUIManager;
	class CSoundManager;
	class CEngine : public base::utils::CSingleton<CEngine>
	{
	public:
		
		virtual ~CEngine();

		void ProcessInputs();
		void Update();
		void Render();
		void Init(HWND hWnd, int width, int height);
		void InitRender(HWND hWnd, int width, int height);
		void DebugUI();
		void SethWnd(HWND hWnd);
		void SetWidth(int width);
		void SetHeight(int height);

		HWND GethWnd();
		int GetWidth();
		int GetHeight();

		BUILD_GET_SET_ENGINE_MANAGER(ActionManager);
		BUILD_GET_SET_ENGINE_MANAGER(RenderManager);
		BUILD_GET_SET_ENGINE_MANAGER(CameraController);
		BUILD_GET_SET_ENGINE_MANAGER(MaterialManager);
		BUILD_GET_SET_ENGINE_MANAGER(ShaderManager);
		BUILD_GET_SET_ENGINE_MANAGER(EffectManager);
		BUILD_GET_SET_ENGINE_MANAGER(TechniquePoolManager);
		BUILD_GET_SET_ENGINE_MANAGER(TextureManager);
		BUILD_GET_SET_ENGINE_MANAGER(MeshManager);
		BUILD_GET_SET_ENGINE_MANAGER(ConstantBufferManager);
		BUILD_GET_SET_ENGINE_MANAGER(SceneManager);
		BUILD_GET_SET_ENGINE_MANAGER(LightManager);
		BUILD_GET_SET_ENGINE_MANAGER(AnimatedModelManager);
		BUILD_GET_SET_ENGINE_MANAGER(CinematicManager);
		BUILD_GET_SET_ENGINE_MANAGER(RenderPipeline);
		BUILD_GET_SET_ENGINE_MANAGER(ScriptManager);
		BUILD_GET_SET_ENGINE_MANAGER(PhysXManager);
		BUILD_GET_SET_ENGINE_MANAGER(ParticleManager);
		BUILD_GET_SET_ENGINE_MANAGER(GUIManager);
		BUILD_GET_SET_ENGINE_MANAGER(SoundManager);

	protected:
		CEngine();

		friend class base::utils::CSingleton<CEngine>;

	private:
		std::chrono::monotonic_clock m_Clock;
		std::chrono::monotonic_clock::time_point m_PrevTime;

		// Temporal
		float scale = 1.0f;
		float rotation = 0.0f;
		int selection = 1;

		HWND m_hWnd;
		int m_width;
		int m_height;

		//RaycastData res;//TEST
	};
}

#undef BUILD_GET_SET_ENGINE_MANAGER