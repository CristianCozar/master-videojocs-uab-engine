#pragma once

#include "ConstantBuffer.h"
#include "LayoutUtility.h"
#include "Particles\ParticleSystemInstance.h"

#define MAXBONES 40
#define MAX_OBJECT_RAW_PARAMETER 16
#define MAX_LIGHTS_BY_SHADER 4
#define UAB_ID_SHADOW_MAP 8


namespace engine
{
	struct PerFrameDesc
	{
		Mat44f m_View;
		Mat44f m_Projection;
		Mat44f m_ViewProjection;
		Mat44f m_ViewInverse;
		Mat44f m_InverseProjection;
		Vect4f m_Time; 
	};

	struct PerLightsDesc
	{
		Vect4f m_LightAmbient;
		float m_LightEnabled[MAX_LIGHTS_BY_SHADER];
		float m_LightType[MAX_LIGHTS_BY_SHADER];
		Vect4f m_LightPosition[MAX_LIGHTS_BY_SHADER];
		Vect4f m_LightDirection[MAX_LIGHTS_BY_SHADER];
		float m_LightAngle[MAX_LIGHTS_BY_SHADER];
		float m_LightFallOffAngle[MAX_LIGHTS_BY_SHADER];
		float m_LightAttenuationStartRange[MAX_LIGHTS_BY_SHADER];
		float m_LightAttenuationEndRange[MAX_LIGHTS_BY_SHADER];
		float m_LightIntensity[MAX_LIGHTS_BY_SHADER];
		Vect4f m_LightColor[MAX_LIGHTS_BY_SHADER];
		float m_UseShadowMap[MAX_LIGHTS_BY_SHADER];
		float m_UseShadowMask[MAX_LIGHTS_BY_SHADER];
		float m_ShadowMapBias[MAX_LIGHTS_BY_SHADER];
		float m_ShadowMapStrength[MAX_LIGHTS_BY_SHADER];
		Mat44f m_LightView[MAX_LIGHTS_BY_SHADER];
		Mat44f m_LightProjection[MAX_LIGHTS_BY_SHADER];
	};



	struct PerObjectDesc
	{
		Mat44f m_World;
	};

	struct PerAnimatedModelDesc
	{
		Mat44f m_Bones[MAXBONES];
	};

	struct PerMaterialDesc
	{
		Vect4f m_RawData[MAX_OBJECT_RAW_PARAMETER];
	};

	struct PerPartDesc
	{
		Vect4f m_RawData[MAX_PARTICLES_PER_INSTANCE];
	};

	class CConstantBufferManager
	{
	public:
		enum ConstanBufferVS
		{
			CB_Frame = 0,
			CB_Object = 1,
			CB_AnimatedModel = 2,
			CB_LightVS = 3,
			NumConstantBuffersVS
		};

		enum ConstanBufferPS
		{
			CB_FramePS = 0,
			CB_LightPS = 3,
			CB_Material = 4,
			NumConstantBuffersPS
		};

		enum ConstanBufferGS
		{
			CB_FrameGS = 0,
			CB_MaterialGS = 4,
			NumConstantBuffersGS
		};

		PerObjectDesc mObjDesc;
		PerFrameDesc mFrameDesc;
		PerAnimatedModelDesc mAnimatedModelDesc;
		PerMaterialDesc mMaterialDesc;
		PerLightsDesc mLightDesc;
		VertexTypes::ParticleVertex mPartDesc;


		CConstantBufferManager()
		{
			CRenderManager& lRM = CEngine::GetInstance().GetRenderManager();

			// Create all the engine constant buffers, note that we only need to init the size of the buffer, the data will be update for each frame.
			m_VSConstantBuffers[CB_Frame] = new  CConstantBuffer(lRM, sizeof(PerFrameDesc));
			m_VSConstantBuffers[CB_Object] = new CConstantBuffer(lRM, sizeof(PerObjectDesc));
			m_VSConstantBuffers[CB_AnimatedModel] = new CConstantBuffer(lRM, sizeof(PerAnimatedModelDesc));
			m_VSConstantBuffers[CB_LightVS] = new CConstantBuffer(lRM, sizeof(PerLightsDesc));


			// TODO: Create PS buffers
			m_PSConstantBuffers[CB_Material] = new CConstantBuffer(lRM, sizeof(PerMaterialDesc));
			m_PSConstantBuffers[CB_LightPS] = new    CConstantBuffer(lRM, sizeof(PerLightsDesc));
			m_PSConstantBuffers[CB_FramePS] = new    CConstantBuffer(lRM, sizeof(PerFrameDesc));

			//Geometry Buffer, No creo que sea correcto!!
			m_GSConstantBuffers[CB_FrameGS] = new CConstantBuffer(lRM, sizeof(PerFrameDesc));
			m_GSConstantBuffers[CB_MaterialGS] = new CConstantBuffer(lRM, sizeof(PerMaterialDesc));
		}

		virtual void BindVSBuffer(ID3D11DeviceContext* aContext, uint32 aConstantBufferID)
		{
			// Update the data of the buffer
			CConstantBuffer* lConstantBuffer = m_VSConstantBuffers[aConstantBufferID];
			void *lRawData = nullptr;
			switch (aConstantBufferID)
			{
			case CB_Frame:
				lRawData = &mFrameDesc;
				break;
			case CB_Object:
				lRawData = &mObjDesc;
				break;
			case CB_AnimatedModel:
				lRawData = &mAnimatedModelDesc;
				break;
			case CB_LightVS:
				lRawData = &mLightDesc;
				break;
			}
			lConstantBuffer->Update(aContext, lRawData);
			lConstantBuffer->BindVS(aContext, aConstantBufferID);
		}

		virtual	void BindPSBuffer(ID3D11DeviceContext*	aContext, uint32 aConstantBufferID)
		{
			CConstantBuffer* lConstantBuffer = m_PSConstantBuffers[aConstantBufferID];
			// Update constant buffers of PS
			void *lRawData = nullptr;
			switch (aConstantBufferID)
			{
			case CB_FramePS:
				lRawData = &mFrameDesc;
				break;
			case CB_Material:
				lRawData = &mMaterialDesc;
				break;
			case CB_LightPS:
				lRawData = &mLightDesc;
				break;
			}
			lConstantBuffer->Update(aContext, lRawData);
			lConstantBuffer->BindPS(aContext, aConstantBufferID);
		}

		virtual	void BindGSBuffer(ID3D11DeviceContext*	aContext, uint32 aConstantBufferID)
		{
			CConstantBuffer* lConstantBuffer = m_GSConstantBuffers[aConstantBufferID];
			// Update constant buffers of GS
			void *lRawData = nullptr;
			switch (aConstantBufferID)
			{
			case CB_FrameGS:
				lRawData = &mFrameDesc;
				break;
			case CB_MaterialGS:
				lRawData = &mMaterialDesc;
				break;
			}
			lConstantBuffer->Update(aContext, lRawData);
			lConstantBuffer->BindGS(aContext, aConstantBufferID);
		}

		virtual ~CConstantBufferManager()
		{
			delete m_VSConstantBuffers[CB_Frame];
			delete m_VSConstantBuffers[CB_Object];
			delete m_VSConstantBuffers[CB_AnimatedModel];
			delete m_VSConstantBuffers[CB_LightVS];
			delete m_PSConstantBuffers[CB_Material];
			delete m_PSConstantBuffers[CB_LightPS];
			delete m_PSConstantBuffers[CB_FramePS];
			delete m_GSConstantBuffers[CB_FrameGS];
			delete m_GSConstantBuffers[CB_MaterialGS];
		}

	protected:

		CConstantBuffer* m_VSConstantBuffers[NumConstantBuffersVS];
		CConstantBuffer* m_PSConstantBuffers[NumConstantBuffersPS];
		CConstantBuffer* m_GSConstantBuffers[NumConstantBuffersGS];


	};

}