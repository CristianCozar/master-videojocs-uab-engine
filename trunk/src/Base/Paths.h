#pragma once

#define PATH_BASE "data/"
#define PATH_SHADERS "data/shaders/"
#define PATH_TECHNIQUES "data/"
#define PATH_MATERIAL "data/"
#define PATH_TEXTURE "" //data/Textures
#define PATH_MESHES "/meshes/"
#define PATH_LIGHTS "data/" 
#define PATH_CINEMATICS "data/cinematics/"
#define PATH_ANIMATED_MODELS "data/"

#define FILENAME_SCENE "/Scene.xml"
#define FILENAME_SCENES "/Scenes.xml"
#define FILENAME_TECHNIQUE_POOLS "technique_pools.xml"
#define FILENAME_RENDER_PIPELINE "render_pipeline.xml"
#define FILENAME_MATERIALS "/Materials.xml"
#define FILENAME_DEFAULT_MATERIALS "data/Defaults.xml"
#define FILENAME_LIGHTS "/Lights.xml"
#define FILENAME_DEFAULT_LIGHTS "data/Lights.xml"

#define EXTENSION_MESH ".mesh"