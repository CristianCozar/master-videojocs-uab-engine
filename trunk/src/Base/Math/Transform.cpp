#include "MathUtils.h"
#include "Transform.h"
#include "XML/XML.h"
#include "Debug\imgui.h"

CTransform::CTransform(void)
    : m_Position(Vect3f(0, 0, 0))
    , m_PrevPos(Vect3f(0, 0, 0))
    , m_Scale(Vect3f(1.0f, 1.0f, 1.0f))
    , m_Yaw(0.0f)
    , m_Pitch(0.0f)
    , m_Roll(0.0f)
{
}

CTransform::CTransform(const Vect3f &Position)
    : m_Position(Position)
    , m_PrevPos(Position)
    , m_Scale(Vect3f(1.0f, 1.0f, 1.0f))
    , m_Yaw(0.0f)
    , m_Pitch(0.0f)
    , m_Roll(0.0f)
{
}

CTransform::CTransform(const Vect3f &Position, float Yaw, float Pitch, float Roll)
    : m_Position(Position)
    , m_PrevPos(Position)
    , m_Scale(Vect3f(1.0f, 1.0f, 1.0f))
	, m_Yaw(Yaw)
	, m_Pitch(Pitch)
	, m_Roll(Roll)
{
}

CTransform::CTransform(float Yaw, float Pitch, float Roll)
    : m_Position(Vect3f(0, 0, 0))
    , m_PrevPos(Vect3f(0, 0, 0))
    , m_Scale(Vect3f(1.0f, 1.0f, 1.0f))
	, m_Yaw(Yaw)
	, m_Pitch(Pitch)
	, m_Roll(Roll)
{
}

CTransform::CTransform(const CXMLElement* aTreeNode)
    : m_Position(aTreeNode->GetAttribute<Vect3f>("position", Vect3f(0.0f, 0.0f, 0.0f)))
    , m_PrevPos(m_Position)
    , m_Scale(aTreeNode->GetAttribute<Vect3f>("scale", Vect3f(1.0f, 1.0f, 1.0f)))
    , m_Yaw(aTreeNode->GetAttribute<float>("yaw", 0.0f))
    , m_Pitch(aTreeNode->GetAttribute<float>("pitch", 0.0f))
    , m_Roll(aTreeNode->GetAttribute<float>("roll", 0.0f))
{
	if (!aTreeNode->FindAttribute("yaw") || !aTreeNode->FindAttribute("pitch") || !aTreeNode->FindAttribute("roll")) {
		if (!aTreeNode->FindAttribute("rotation")) {
			SetForward(aTreeNode->GetAttribute<Vect3f>("forward", Vect3f()));
		} else {
			Vect3f rot = aTreeNode->GetAttribute<Vect3f>("rotation", Vect3f());
			rot.x *= DEG2RAD;
			rot.y *= DEG2RAD;
			rot.z *= DEG2RAD;
			SetYaw(rot.x);
			SetPitch(rot.y);
			SetRoll(rot.z);
			//SetForward(rot);
		}
	}
}

CTransform::~CTransform(void)
{
}

const Mat44f & CTransform::GetMatrix()
{
    // Translation
    m_TranslationMatrix.SetIdentity();
    m_TranslationMatrix.SetPos(m_Position.x, m_Position.y, m_Position.z);

    // Scaling
    m_ScaleMatrix.SetIdentity();
    m_ScaleMatrix.Scale(m_Scale.x, m_Scale.y, m_Scale.z);

    // Rotation
    Mat44f lRotationY;
    Mat44f lRotationX;
    Mat44f lRotationZ;
    lRotationX.SetFromAngleX(-m_Pitch);
    lRotationY.SetFromAngleY(m_Yaw);
    lRotationZ.SetFromAngleZ(m_Roll);

    m_RotationMatrix.SetIdentity();
    m_RotationMatrix = lRotationY * lRotationX * lRotationZ;

    // Acumulate whole transformation in transform matrix
    m_TransformMatrix = m_ScaleMatrix*m_RotationMatrix*m_TranslationMatrix;

    return m_TransformMatrix;
}

void CTransform::SetPosition(const Vect3f &Position)
{
    m_PrevPos = m_Position;
    m_Position = Position;
}

const Vect3f & CTransform::GetPosition() const
{
    return m_Position;
}

const Vect3f & CTransform::GetPrevPosition() const
{
    return m_PrevPos;
}


Vect3f CTransform::GetForward() const
{
	return  Vect3f(mathUtils::Sin(m_Yaw) * mathUtils::Cos(m_Pitch),
		mathUtils::Sin(m_Pitch),
		mathUtils::Cos(m_Yaw) * mathUtils::Cos(m_Pitch));
}

Vect3f CTransform::GetRight() const
{
	return  Vect3f(mathUtils::Cos(m_Yaw) * mathUtils::Cos(m_Pitch),
		mathUtils::Sin(m_Pitch),
		mathUtils::Sin(m_Yaw) * mathUtils::Cos(m_Pitch));
}

Vect3f CTransform::GetUp() const
{
	return  Vect3f(-mathUtils::Cos(m_Yaw) * mathUtils::Sin(m_Pitch),
		mathUtils::Cos(m_Pitch),
		-mathUtils::Sin(m_Yaw) * mathUtils::Sin(m_Pitch));
}

void CTransform::SetForward(const Vect3f& aForward)
{
	SetYaw(mathUtils::ATan2(aForward.x, aForward.z));
	SetPitch(mathUtils::ATan2(aForward.y, mathUtils::Sqrt(aForward.z * aForward.z + aForward.x * aForward.x)));
	//SetRoll(0.0f);
}

void CTransform::SetRight(const Vect3f& aRight)
{

}

void CTransform::SetUp(const Vect3f& aUp)
{

}

void CTransform::RenderDebugGUI() {
	ImGui::PushID("Transform");
	if (ImGui::Button("Reset Node")) {
		ResetDebugGUI();
	}
	ImGui::PopID();

	if (ImGui::CollapsingHeader("Rotation"))
	{
		m_Yaw *= RAD2DEG;
		m_Pitch *= RAD2DEG;
		m_Roll *= RAD2DEG;
		ImGui::DragFloat("Yaw", &m_Yaw, 0.0f, 360.0f);
		ImGui::DragFloat("Pitch", &m_Pitch, 0.0f, 360.0f);
		ImGui::DragFloat("Roll", &m_Roll, 0.0f, 360.0f);
		m_Yaw = mathUtils::Repeat(m_Yaw, 360.0f);
		m_Pitch = mathUtils::Repeat(m_Pitch, 360.0f);
		m_Roll = mathUtils::Repeat(m_Roll, 360.0f);
		m_Yaw *= DEG2RAD;
		m_Pitch *= DEG2RAD;
		m_Roll *= DEG2RAD;
	}
	if (ImGui::CollapsingHeader("Position"))
	{
		ImGui::DragFloat3("Position", &m_Position.x);		
	}
	if (ImGui::CollapsingHeader("Scale"))
	{
		ImGui::DragFloat3("Scale", &m_Scale.x);
	}
}

void CTransform::ResetDebugGUI() {

	m_Position = Vect3f(0.0f, 0.0f, 0.0f);
	m_Scale = Vect3f(1.0f, 1.0f, 1.0f);
	m_Yaw = 0.0f;
	m_Pitch = 0.0f;
	m_Roll = 0.0f;
}
