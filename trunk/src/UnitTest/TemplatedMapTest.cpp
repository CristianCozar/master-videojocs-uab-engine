#include  <gtest/gtest.h>

#include "Base\Utils\TemplatedMap.h"

namespace unit_test {
	class Element{};

	TEST(TemplatedMapTest, TestTemplateMap1Add) {
		base::utils::CTemplatedMap<Element> m_elementsContainer;

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());

		m_elementsContainer.Add("Element1", new Element());

		ASSERT_EQ(1, m_elementsContainer.GetResourcesMap().size());
	}

	TEST(TemplatedMapTest, TestTemplateMap1Remove) {
		base::utils::CTemplatedMap<Element> m_elementsContainer;

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());

		m_elementsContainer.Add("Element1", new Element());

		ASSERT_EQ(1, m_elementsContainer.GetResourcesMap().size());

		m_elementsContainer.Remove("Element1");

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());
	}

	TEST(TemplatedMapTest, TestTemplateMap1Update) {
		base::utils::CTemplatedMap<Element> m_elementsContainer;

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());

		Element *element = new Element();

		m_elementsContainer.Add("Element1", element);

		ASSERT_EQ(true, m_elementsContainer("Element1") == element);

		Element *element2 = new Element();

		m_elementsContainer.Update("Element1", element2);

		ASSERT_EQ(true, m_elementsContainer("Element1") == element2);
	}

	TEST(TemplatedMapTest, TestTemplateMap1Clear) {
		base::utils::CTemplatedMap<Element> m_elementsContainer;

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());

		m_elementsContainer.Add("Element1", new Element());

		ASSERT_EQ(1, m_elementsContainer.GetResourcesMap().size());

		m_elementsContainer.Clear();

		ASSERT_EQ(0, m_elementsContainer.GetResourcesMap().size());
	}
}