#include <gtest/gtest.h>

#include "Base/Utils/PublisherSubscriber/Broker.h"
#include "Base/Utils/PublisherSubscriber/Event.h"
#include "Base/Utils/PublisherSubscriber/Subscriber.h"


namespace unit_test {

	using namespace base::utils::patterns;

	struct MyEvent : public Event
	{
		MyEvent()
		{
			m_name = "MyEvent";
			value = 0;
		}

		public:
			int value;
	};

	typedef ::boost::function<void(const Event&)> CallbackType;

	void test(const Event& e) {
		int a;
		a = 0;
	}

	TEST(PublisherSubscriberTest, SubscribePublishTest1) {
		/*Subscriber s = Subscriber();
		MyEvent e;

		e.value = 1;

		CallbackType cb = *test;

		s.subscribe(cb, e.m_name);
		Broker::GetInstance().publish(e);*/
	}
}

