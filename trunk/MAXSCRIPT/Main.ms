filein "Utils.ms"
filein "Debug.ms"
filein "xml.ms"

function DoMainMaterials filePath copyTextures sceneName =
(
	mats = GetAllSceneMaterials()
	XMLBuilder mats filePath copyTextures sceneName
)

function GetAllSceneMaterials =
(
	select $*
	local matArray = #()
	if classof $ == ObjectSet then
	(
		for i = 1 to $.count do
		(
			local mat = $[i].material
			if classOf mat == Multimaterial then
			(
				matArray = AppendArrayNoRepeat matArray mat
			) else if classOf mat == Standardmaterial then
			(
				inArray = IsInArray matArray mat
				if (inArray == false) then
				(
					append matArray mat
				)
			)
		)
	) else (
		local mat = $.material
		if classOf mat == Multimaterial then
		(
			matArray = AppendArrayNoRepeat matArray mat
		) else if classOf mat == Standardmaterial then
		(
			append matArray mat
		)
	)
	return matArray
)

function GetTextures mat dir copyTextures =
(
	/*
	ambientMap
	bumpMap
	diffuseMap
	displacementMap
	filterMap
	glossinessMap
	opacityMap
	reflectionMap
	refractionMap
	selfIllumMap
	specularLevelMap
	specularMap
	*/
	texturesArray = #()
	
	-- diffuseMap
	local diffuseMapEnabled = mat.diffuseMapEnable
	if diffuseMapEnabled == true then
	(
		try (
			local mapBitmap= mat.diffuseMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "diffuse"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de diffuse del material " + mat.name
			LogError debugText
		)
	)
	
	-- bumpMap
	local bumpMapEnabled = mat.bumpMapEnable
	if bumpMapEnabled == true then
	(
		try (
			if (classof(mat.bumpmap) != bitmaptexture) then
			(
				local mapBitmap = mat.bumpMap.normal.bitmap
			) else (
				local mapBitmap= mat.bumpMap.bitmap
			)
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "bump"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de bump del material " + mat.name
			LogError debugText
		)
	)
	
	-- selfIllumMap
	local selfIllumMapEnabled = mat.selfIllumMapEnable
	if selfIllumMapEnabled == true then
	(
		try (
			local mapBitmap= mat.selfIllumMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "selfIllum"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de lightmap del material " + mat.name
			LogError debugText
		)
	)
	
	-- ambientMap
	local ambientMapEnabled = mat.ambientMapEnable
	if ambientMapEnabled == true then
	(
		try (
			local mapBitmap= mat.ambientMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "ambient"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de ambient del material " + mat.name
			LogError debugText
		)
	)	
	
	-- displacementMap
	local displacementMapEnabled = mat.displacementMapEnable
	if displacementMapEnabled == true then
	(
		try (
			local mapBitmap= mat.displacementMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "displacement"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de displacement del material " + mat.name
			LogError debugText
		)
	)
	
	-- filterMap
	local filterMapEnabled = mat.filterMapEnable
	if filterMapEnabled == true then
	(
		try (
			local mapBitmap= mat.filterMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "filter"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de filter del material " + mat.name
			LogError debugText
		)
	)
	
	-- glossinessMap
	local glossinessMapEnabled = mat.glossinessMapEnable
	if glossinessMapEnabled == true then
	(
		try (
			local mapBitmap= mat.glossinessMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "glossiness"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de glossiness del material " + mat.name
			LogError debugText
		)
	)
	
	-- opacityMap
	local opacityMapEnabled = mat.opacityMapEnable
	if opacityMapEnabled == true then
	(
		try (
			local mapBitmap= mat.opacityMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "opacity"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de opacity del material " + mat.name
			LogError debugText
		)
	)
	
	-- reflectionMap
	local reflectionMapEnabled = mat.reflectionMapEnable
	if reflectionMapEnabled == true then
	(
		try (
			local mapBitmap= mat.reflectionMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "reflection"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de reflection del material " + mat.name
			LogError debugText
		)
	)
	
	-- refractionMap
	local refractionMapEnabled = mat.refractionMapEnable
	if refractionMapEnabled == true then
	(
		try (
			local mapBitmap= mat.refractionMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "refraction"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de refraction del material " + mat.name
			LogError debugText
		)
	)
	
	-- specularLevelMap
	local specularLevelMapEnabled = mat.specularLevelMapEnable
	if specularLevelMapEnabled == true then
	(
		try (
			local mapBitmap= mat.specularLevelMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "specularLevel"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de specular level del material " + mat.name
			LogError debugText
		)
	)
	
	-- specularMap
	local specularMapEnabled = mat.specularMapEnable
	if specularMapEnabled == true then
	(
		try (
			local mapBitmap= mat.specularMap.bitmap
			CopyTexture mapBitmap dir copyTextures
			local finalTexture = texStruct()
			finalTexture.mType = "specular"
			finalTexture.mFilename =  filenameFromPath (mapBitmap .filename)
			append texturesArray finalTexture
		) catch
		(
			debugText =  "Error al obtener la textura de specular del material " + mat.name
			LogError debugText
		)
	)
	
	return texturesArray
)

function GetVertexType materialNode mat =
(
	vtype = "Position"
	boolBump = mat.bumpMapEnable
	boolUV = mat.diffuseMapEnable
	boolUV2 = mat.selfIllumMapEnable
	
	if (boolBump == true) then
	(
		vtype = vtype + "Bump"
	) else (
		vtype = vtype + "Normal"
	)
	if (boolUV == true) then
	(
		vtype = vtype + "UV"
		if (boolUV2 == true) then
		(
			vtype = vtype + "UV2"
		)
	)
	debugText =  "Vertex type is: " + vtype
	LogInfo debugText
	debugText =  "Material is: " + mat.name
	LogInfo debugText
	materialNode.AddAttribute "vertex_type" vtype
)

function XMLBuilder matArray filePath copyTextures sceneName =
(
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"materials"
	xmlDoc.rootNode = rootNode
	
	--local f = getSaveFilename "Save XML File" filename:( (getdir #userscripts) + "\\" + (getFilenameFile maxFilename) + "-MATERIALS") \
	--															 types:"XML File(.xml)|*.xml|All|*.*|"
		
	local f = filePath + "\\Materials.xml"
	if f != undefined then
	(
		xmlDoc.filename = f
		local dir = getFilenamePath f
		for m in matArray do
		(
			if (m != undefined) then (
				local matNode = XmlNode tag:"material"
				
				rootNode.AddChild matNode
				matNode.AddAttribute "name" m.name
				GetVertexType matNode m
				
				local skyboxNode = XmlNode tag:"texture"
				matNode.AddChild skyboxNode
				skyboxNode.AddAttribute "type" "skybox"
				local skyboxPath = "data\\" + sceneName + "\\textures\\" + "skybox.dds"
				skyboxNode.AddAttribute "filename" skyboxPath
				skyboxNode.AddAttribute "generate_mips" true
				
				local texArray = GetTextures m dir copyTextures
				for t in texArray do
				(
					local textureNode = XmlNode tag:"texture"
					matNode.AddChild textureNode
					textureNode.AddAttribute "type" t.mType
					local fullPath = "data\\" + sceneName + "\\textures\\" + t.mFilename 
					textureNode.AddAttribute "filename" fullPath
				)
				
				--AddParameter matNode "ambient" "float" (m.ambientMapAmount / 100.0)
				--AddParameter matNode "bump" "float" (m.bumpMapAmount / 100.0)
				--AddParameter matNode "diffuse" "float" (m.diffuseMapAmount / 100.0)
				--AddParameter matNode "displacement" "float" (m.displacementMapAmount / 100.0)
				--AddParameter matNode "filter" "float" (m.filterMapAmount / 100.0)
				--AddParameter matNode "glossiness" "float" (m.glossinessMapAmount / 100.0)
				--AddParameter matNode "opacity" "float" (m.opacityMapAmount / 100.0)
				--AddParameter matNode "reflection" "float" (m.reflectionMapAmount / 100.0)
				--AddParameter matNode "refraction" "float" (m.refractionMapAmount / 100.0)
				--AddParameter matNode "selfIllum" "float" (m.selfIllumMapAmount / 100.0)
				--AddParameter matNode "specularLevel" "float" (m.specularLevelMapAmount / 100.0)
				--AddParameter matNode "specular" "float" (m.specularMapAmount / 100.0)
				ambientArray = #()
				append ambientArray 1.0
				append ambientArray 1.0
				append ambientArray 1.0
				append ambientArray 1.0
				a = GetStringFromArray ambientArray
				AddParameter matNode "skyboxAmbient" "color" a 0.1 0 1
				diffuseArray = #()
				append diffuseArray (m.diffuse.r / 255.0)
				append diffuseArray (m.diffuse.g / 255.0)
				append diffuseArray (m.diffuse.b / 255.0)
				append diffuseArray (m.diffuse.a / 255.0)
				p = GetStringFromArray diffuseArray
				AddParameter matNode "color" "color" p 0.1 0 1
				AddParameter matNode "roughness" "float" 1.0 0.05 0.0 1.0
				AddParameter matNode "mipLevels" "float" 7.0 1.0 1.0 11.0
				AddParameter matNode "metallic" "float" 0.0 0.05 0.0 1.0
				AddParameter matNode "occlusion" "float" 0.0 0.05 0.0 1.0
				AddParameter matNode "bump" "float" 8.0 0.2 0.0 20.0
			)
		)
		
		xmlDoc.SaveXmlDocument()
	)
)

function AddParameter parent pname ptype pvalue vstep vmin vmax =
(
	local pNode = XmlNode tag:"parameter"
	parent.AddChild pNode
	pNode.AddAttribute "type" ptype
	pNode.AddAttribute "name" pname
	pNode.AddAttribute "value" pvalue
	pNode.AddAttribute "step" vstep
	pNode.AddAttribute "min" vmin
	pNode.AddAttribute "max" vmax
)

struct texStruct
(
	mType = undefined,
	mFilename = undefined
)


function CopyTexture tex dir copyTextures =
(
	if tex != undefined then
	(
		local mTexturesDirectory = dir + "textures\\"
		local lTextureName = filenameFromPath (tex .filename)
		local lCopyTexturePath = mTexturesDirectory + lTextureName
		if copyTextures == true then
		(
			makedir mTexturesDirectory
			copyFile (openBitMap tex .filename).fileName lCopyTexturePath
			logText = "Textura copiada en " + lCopyTexturePath
			LogInfo logText
		)
	)
)

--DoMain()