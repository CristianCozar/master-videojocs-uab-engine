function DoMainCinematics filePath isLoop isReverse nFrames =
(
	local totalTime = (((animationRange.end as float) / ticksPerFrame) / framerate)
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"cinematics"
	xmlDoc.rootNode = rootNode
	
	--local f = getSaveFilename "Save XML File" filename:( (getdir #userscripts) + "\\" + (getFilenameFile maxFilename) + "CINEMATICS") \
	--															 types:"XML File(.xml)|*.xml|All|*.*|"
	
	local f = filePath + "\\Cinematics.xml"
	if f != undefined then
	(
		xmlDoc.filename = f
		local cinematicNodes = dotnetobject "System.Collections.Generic.Dictionary`2[System.String,System.Int16]"
		local cinematicNodesArray = #()
		select $*
		local objs = $
		if classOf $ != objectSet then
		(
			objs = #($)
		)
		for i = 1 to objs.count do
		(
			local obj = objs[i]
			local animated = GetAnimated obj
			if animated == true then
			(
				local objectName = obj.name
				local objectScene = GetScene obj
				local objectLayer = GetLayer obj
				local objectNode
				if (superclassof obj == camera) then
							(
								objectNode = XmlNode tag:"cinematic_camera_player"
							) else if (superclassof obj == light) then
							(
								-- TODO
							) else (
								objectNode = XmlNode tag:"cinematic_object_player"
								objectNode.AddAttribute "scene_node" objectName
								objectNode.AddAttribute "layer" objectLayer
								objectNode.AddAttribute "scene" objectScene
							)
				local cinematicName = GetAnimationName obj
				
				for t = animationRange.start to animationRange.end by nFrames do
				(
					animate on
					(
						at time t
						(
							TimeInSeconds = (((t as float) / ticksPerFrame) / framerate)
							if (superclassof obj == camera) then
							(
								GetCameraData obj TimeInSeconds objectNode
							) else if (superclassof obj == light) then
							(
								-- TODO
							) else (
								GetObjectData obj TimeInSeconds objectNode
							)
						)
					)
				)
				AddToCinematic cinematicNodes cinematicNodesArray cinematicName objectNode rootNode totalTime isLoop isReverse
			)
		)
		xmlDoc.SaveXmlDocument()
	)
)

function GetObjectData obj t objectNode =
(
	local keyNode = XmlNode tag:"key"
	local tr = obj.transform
	local position = #(-tr.translation.x, tr.translation.z, tr.translation.y)
	local scal = #(tr.scale.x, tr.scale.z, tr.scale.y)
	local yaw = -(tr as eulerangles).z
	local pitch = -(tr as eulerangles).y
	local roll = (tr as eulerangles).x
	local rotatio = #(yaw, pitch, roll)
	local strPosition = GetStringFromArray position
	local strScale = GetStringFromArray scal
	local strRotation = GetStringFromArray rotatio
	keyNode.AddAttribute "time" t
	keyNode.AddAttribute "position" strPosition
	keyNode.AddAttribute "rotation" strRotation
	keyNode.AddAttribute "scale" strScale
	objectNode.AddChild keyNode
)

function GetCameraData obj t objectNode =
(
	local keyNode = XmlNode tag:"key"
	local tr = obj.transform
	local lookatobj = obj.target
	local position = #(-tr.translation.x, tr.translation.z, tr.translation.y)
	local lookatpos = #(-lookatobj.position.x, lookatobj.position.z, lookatobj.position.y)
	--local up = #(lookatobj[2].x, -lookatobj[2].z, -lookatobj[2].y)
	local up = #(0.0, 1.0, 0.0);
	local fov = obj.fov as float
	local nearclip = obj.nearclip
	local farclip = obj.farclip
	local strPosition = GetStringFromArray position
	local strLookatpos = GetStringFromArray lookatpos
	local strUp = GetStringFromArray up
	keyNode.AddAttribute "time" t
	keyNode.AddAttribute "position" strPosition
	keyNode.AddAttribute "look_at" strLookatpos
	keyNode.AddAttribute "up" strUp
	keyNode.AddAttribute "fov" fov
	keyNode.AddAttribute "near_plane" nearclip
	keyNode.AddAttribute "far_plane" farclip
	objectNode.AddChild keyNode
)

function AddToCinematic cinematicsMap cinematicsArray cinematicName xmlObj root duration loop revers =
(
	local index = 0
	if (cinematicsMap.ContainsKey(cinematicName) == false) then
	(
		local cinematicNode = XmlNode tag:"cinematic"
		cinematicNode.AddAttribute "name" cinematicName
		cinematicNode.AddAttribute "duration" duration
		cinematicNode.AddAttribute "loop" loop
		cinematicNode.AddAttribute "reverse" revers
		root.AddChild cinematicNode
		append cinematicsArray cinematicNode
		index = cinematicsArray.Count
		cinematicsMap.Add cinematicName index
	)
	else
	(
		index = cinematicsMap.Item[cinematicName]
	)
	cinematicsArray[index].AddChild xmlObj
	return cinematicsArray[index]
)

function GetAnimated obj =
(
	animated = getUserProp obj "animated"
	if animated == undefined then
	(
		animated = false
	)
	return animated
)

function GetAnimationName obj =
(
	animationName = getUserProp obj "animationName"
	if animationName == undefined then
	(
		animationName = "DefaultAnim"
	)
	return animationName
)

function GetPos obj =
(
	local pos = ConvertToRH obj.pos
	return pos
)

function GetOrderedKeys obj =
(
	local keys = #()
	local posKeys = GetKeys obj.position.controller
	local rotationKeys = GetKeys obj.rotation.controller
	local scaleKeys = GetKeys obj.scale.controller
	keys = AppendArray keys posKeys
	keys = AppendArray keys rotationKeys
	keys = AppendArray keys scaleKeys
	return keys
)

function GetKeys ctrl =
(
	return ctrl.keys
)

function GetKeysCount controller =
(
	return numKeys controller
	/*local keys = GetKeys controller
	return keys.count*/
)

function GetKeyByIndex controller index =
(
	return GetKey controller index
	/*local keys = GetKeys controller
	return keys[index]*/
)

function GetKeyTimeByIndex controller index =
(
	return GetKeyTime controller index as integer
	/*local key = GetKeyByIndex controller index
	return key.time as integer*/
)

--DoMainCinematics false false 1