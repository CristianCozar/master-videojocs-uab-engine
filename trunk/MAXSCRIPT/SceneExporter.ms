filein "xml.ms"
filein "Debug.ms"
filein "MeshExporter.ms"
filein "Utils.ms"

/*
- Seleccionar todos los objetos
- Determinar los cores
- De cada core exportar malla
- Scene_Mesh por cada instancia

*/

function DoMainScene filePath exportMeshes exportConvex =
(
	select $*
	local cores = GetCores()
	DoSceneXml filePath exportMeshes exportConvex
)

function GetCores =
(
	local uniqueInstance = #()
	for obj in selection do
	(
		if (InstanceMgr.GetInstances obj &instances) > 1 then
		(
			if findItem uniqueInstance instances[1] == 0 do
				append uniqueInstance instances[1]
		)
		else
		(
			append uniqueInstance obj
		)
	)
	return uniqueInstance
)

function GetLayer obj =
(
	if (SuperClassOf obj == light) then
	(
		return "Light"
	)
	return "Opaque"
)

function DoSceneXml filePath exportMeshes exportConvex =
(
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"scene"
	xmlDoc.rootNode = rootNode
	
	--local f = getSaveFilename "Save XML File" filename:( (getdir #userscripts) + "\\" + (getFilenameFile maxFilename) + "-SCENE") \
	--															 types:"XML File(.xml)|*.xml|All|*.*|"
	local f = filePath + "\\Scene.xml"
	select $*
	local sel = #()
	if classof $ == objectset then
	(
		for k = 1 to $.count do
		(
			append sel $[k]
		)
	) else (
		append sel $
	)
	if f != undefined then
	(
		xmlDoc.filename = f
		local layerNodes = dotnetobject "System.Collections.Generic.Dictionary`2[System.String,System.Int16]"
		local layerNodesArray = #()
		for i = 1 to sel.count do
		(
			local layerName = GetLayer sel[i]
			local tagNode
			if layerName == "Light" then
			(
				LogInfo "Light"
				tagNode = XmlNode tag:"scene_light"
				tagNode.AddAttribute "name" sel[i].name
				local core = GetCore sel[i]
				tagNode.AddAttribute "light" core.name
			)
			else
			(
				LogInfo "Mesh"
				tagNode = XmlNode tag:"scene_mesh"
				tagNode.AddAttribute "name" sel[i].name
				local core = GetCore sel[i]
				tagNode.AddAttribute "mesh" core.name
				GetTransform tagNode sel[i]
			)
			tagNode.AddAttribute "active" true
			tagNode.AddAttribute "visible" true
			AddToLayer layerNodes layerNodesArray layerName tagNode rootNode
			select sel[i]
			if (exportMeshes == true) then
			(
				DoMainMesh f
			)
			if (exportConvex == true) then
			(
				ConvexMesh f
				TriangleMesh f
			)
		)
		xmlDoc.SaveXmlDocument()
	)
)

function GetTransform meshNode obj =
(
	local transformNode = XmlNode tag:"transform"
	local tr = obj.transform
	local position = #(-tr.translation.x, tr.translation.z, tr.translation.y)
	local scal = #(tr.scale.x, tr.scale.z, tr.scale.y)
	local yaw = -(tr as eulerangles).z
	local pitch = -(tr as eulerangles).y
	local roll = (tr as eulerangles).x
	local rotatio = #(yaw, pitch, roll)
	local strPosition = GetStringFromArray position
	local strScale = GetStringFromArray scal
	local strRotation = GetStringFromArray rotatio
	transformNode.AddAttribute "position" strPosition
	transformNode.AddAttribute "rotation" strRotation
	transformNode.AddAttribute "scale" strScale
	meshNode.AddChild transformNode
)

function AddToLayer layersMap layersArray layerName xmlObj root =
(
	local index = 0
	if (layersMap.ContainsKey(layerName) == false) then
	(
		local layerNode = XmlNode tag:"layer"
		layerNode.AddAttribute "name" layerName
		root.AddChild layerNode
		append layersArray layerNode
		index = layersArray.Count
		layersMap.Add layerName index
	)
	else
	(
		index = layersMap.Item[layerName]
	)
	layersArray[index].AddChild xmlObj
	return layersArray[index]
)