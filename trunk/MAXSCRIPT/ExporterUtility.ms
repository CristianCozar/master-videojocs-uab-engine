fileIn "GetSetProperties.ms"
fileIn "Loader.ms"

utility Prim3ExporterUtility "PRIM3 Exporter" width:192 height:376
(
	button BExportPath "Export Path" pos:[8,216] width:150 height:21
	GroupBox GBExportSettings "Export Settings" pos:[8,16] width:150 height:192
	checkbox CHMaterials "Materials" pos:[16,40] width:140 height:15
	checkbox CHCopyTextures "Copy Textures" pos:[30,64] width:140 height:15
	checkbox CHScene "Scene" pos:[16,88] width:140 height:15
	checkbox CHMeshes "Meshes" pos:[30,112] width:140 height:15
	checkbox CHConvex "Collision Meshes" pos:[30,136] width:140 height:15
	checkbox CHCinematics "Cinematics" pos:[16,160] width:140 height:15
	checkbox CHLights "Lights" pos:[16,184] width:140 height:15
	label LBExportPath "C:/" pos:[8,248] width:150 height:21
	button BExport "Export" pos:[8,323] width:150 height:21
	--combobox CBScene "Scene" pos:[105,199] width:150 height:1 enabled:true items:#("Scene01", "Scene02", "Scene03", "TestScene") selection:1
	button BGet_SetProperties "Get/Set Properties" pos:[8,362] width:150 height:21
	editText edt_scene "" pos:[8,296] width:152 height:16 text:"SceneName"
	label lbl12 "Scene" pos:[16,272] width:152 height:16
	on BExportPath pressed do
	(
		LBExportPath.text = getSavePath "Export folder" "C:"
	)
	on BExport pressed do
	(
		local filesPath = (LBExportPath.text + "\\" + edt_scene.text)
		makedir LBExportPath.text
		makedir filesPath
		
		if (CHCinematics.checked == true) then
		(
			DoMainCinematics filesPath false false 1 --TODO: Solo se exporta con estos parámetros
		)
		
		if (CHMaterials.checked == true) then
		(
			DoMainMaterials filesPath CHCopyTextures.checked edt_scene.text
		)
		
		if (CHScene.checked == true) then
		(
			DoMainScene filesPath CHMeshes.checked CHConvex.checked
		)
		
		if (CHLights.checked == true) then
		(
			DoMainLights filesPath
		)
	)
	on BGet_SetProperties pressed do
	(
		createDialog GetSetProperties
	)
)
