function AppendArray array1 array2 =
(
	for i = 1 to array2.count do
	(
		append array1 array2[i]
	)
	return array1
)

function IsInArray arr obj =
(
	for i = 1 to arr.count do
	(
		if (arr[i] == obj) then
		(
			return true
		)
	)
	return false
)

function AppendArrayNoRepeat array1 array2 =
(
	for i = 1 to array2.count do
	(
		inArray = IsInArray array1 array2[i]
		if (inArray == false) then
		(
			append array1 array2[i]
		)
	)
	return array1
)

function GetStringFromArray arr =
(
	local l_Text = stringstream ""
	local l_Value=""
	for i=1 to arr.count do
	(
		format "% " arr[i] to:l_Text
	)
	-- Quitamos la coma que queda al final
	local finaltext = (l_Text as string)
	local count = finaltext.count - 1
	local finaltext = substring finaltext 1 count
	return finaltext
)

function GetInstances obj &instances =
(
	return InstanceMgr.GetInstances obj &instances
)

function GetCore obj =
(
	local instances
	GetInstances obj &instances
	return instances[1]
)

fn ConvertToRH vect3 =
(
	return point3 -vect3.x vect3.z vect3.y
)

fn BaseChange vect2 =
(
	return point2 vect2.x 1-vect2.y
)

fn Deg2Rad deg =
(
	return deg * (pi/180)
)

fn Rad2Deg rad =
(
	return rad * (180/pi)
)

fn GetLayer obj =
(
	layer = getUserProp obj "layer"
	if layer == undefined then
	(
		layer = "Opaque"
	)
	return layer
)

fn GetScene obj =
(
	scen = getUserProp obj "scene"
	if scen == undefined then
	(
		scen = "DefaultScene"
	)
	return scen
)