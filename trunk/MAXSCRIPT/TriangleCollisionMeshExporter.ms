filein "Debug.ms"
filein "UABFuncs.ms"
filein "Utils.ms"
filein "BinFileWritter.ms"

function DoMainCollisionSingle =
(
	TriangleMesh "C:\\Users\\Mesiah\\Desktop\\PRIM3_Level1\\mesh.mesh"
)

function TriangleMesh filename =
(
	local dir = getFilenamePath filename
	dir = dir + "\\collisionMeshes\\"
	oldobj = $
	newobj = copy $ -- Copiamos nuestro objeto
	select newobj
	resetxform $
	convertToMesh $ -- Y lo convertimos a mesh
	try
	(
	--ConvexMesh(filename)
	local numVertexs = getNumVerts $
	local numFaces = getNumFaces $
	local totalVertexs = numFaces * 3
	local lInverseTrsf = inverse $.transform
		
	file = BinFileWritter()
	file.ChangeVerbose true
	file.Open oldobj.name dir ".indices"
	file.PrintUnsignedShort numFaces

	for i = 1 to numFaces do
	(
		local face = getFace $ i
		file.PrintUnsignedShort face[1]
		file.PrintUnsignedShort face[2]
		file.PrintUnsignedShort face[3]
	)
		
	LogInfo "Indices guardados"
	file.Close()
	)
	catch
	(
	)
	delete $ -- Para terminar, borramos el objeto exportado (que era una copia)
	select oldobj
)