filein "Debug.ms"
filein "UABFuncs.ms"
filein "Utils.ms"
filein "BinFileWritter.ms"

global POSITION = 0x0001 -- Posicion
global GCOLOR = 0x0002 -- Color. No guardar, lo tiene en cuenta el material!
global NORMAL = 0x0004 -- Normal
global TANGENT = 0x0008 -- Bump
global BINORMAL = 0x0010 -- Bump
global UV = 0x0020 -- Diffuse
global UV2 = 0x0040 -- Self illum
global POSITION4 = 0x0080

global STRINGSIZE = 256

global AA = point3 0 0 0
global BB = point3 0 0 0
global SPHERECENTER = point3 0 0 0
global RADIUS = 0

function ConvertVtx Vtx =
(
	local l_Text = stringstream ""
	local l_Value=""
	for i=1 to Vtx.count do
	(
		format "%," Vtx[i] to:l_Text
	)
	-- Quitamos la coma que queda al final
	local finaltext = (l_Text as string)
	local count = finaltext.count - 1
	local finaltext = substring finaltext 1 count
	return finaltext
)



function GetBoundingSphere =
(
	SPHERECENTER = (AA+BB) / 2
	len1 = length(AA-SPHERECENTER)
	len2 = length(BB-SPHERECENTER)
	RADIUS = amax len1 len2
)

function UpdateAABB vertexs =
(
	local p = point3 vertexs[1] vertexs[2] vertexs[3]
	minx = amin p.x AA.x
	miny = amin p.y AA.y
	minz = amin p.z AA.z
	maxx = amax p.x BB.x
	maxy = amax p.y BB.y
	maxz = amax p.z BB.z
	
	AA = point3 minx miny minz
	BB = point3 maxx maxy maxz
)

function AddVertex VBIds VB Vertexs Key =
	-- Param1: Vertex Buffer Map (de la estructura SMaterial)
	-- Param2: Vertex Buffer (de la estructura SMaterial)
	-- Param3: Vertices en formato array de floats
	-- Param4: Clave en formato string "POSITION NORMAL UV1"
(
	local index = 1
	if (VBIds.ContainsKey(Key)) == false then
	(
		UpdateAABB Vertexs
		append VB Vertexs
		index = VB.count
		VBIds.Add Key index
	) else (
		index = VBIds.Item[Key]
	)
	return index
)

struct SMaterial
(
	m_MaterialName = undefined,
	m_MaterialFlags = undefined,
	m_VB = #(), -- VertexBuffer
	m_VBIds = undefined, -- VertexBufferIDs. Hacer un map!
	m_IB = #() -- IndexBuffer
)

struct SVertexBuffer -- NOT USED
(
	m_Position = undefined,
	m_Normal = undefined,
	m_UV = undefined,
	m_UV2 = undefined,
	m_Bump = undefined -- ... etc
)

function DebugResults matsArray =
(
	Format "Showing results...\n\n"
	for i = 1 to matsArray.count do
	(
		local matStruct = matsArray[i]
		Format "MATERIAL %\n" matStruct.m_MaterialName
		local VBString = ConvertVtx matStruct.m_VB
		local IBString = ConvertVtx matStruct.m_IB
		Format "VERTEX BUFFER: %\n" VBString
		Format "VB SIZE: %\n" matStruct.m_VB.count
		Format "INDEX BUFFER: %\n" IBString
		Format "IB SIZE: %\n" matStruct.m_IB.count
		Format "AA=% BB=%\n" AA BB
		Format "SPHERE CENTER=% RADIUS=%\n\n\n" SPHERECENTER RADIUS
	)
)

function DoMainMeshSingle =
(
	DoMainMesh "C:\\Users\\Mesiah\\Desktop\\PRIM3_Level1\\mesh.mesh"
)

function DoMainMesh filename =
(
	local dir = getFilenamePath filename
	dir = dir + "\\meshes\\"
	local oldobj = $
	local obj = copy $ -- Copiamos nuestro objeto
	select obj
	$.name = oldobj.name
	--resetxform $
	convertToMesh $ -- Y lo convertimos a mesh
	try
	(
	local numFaces = getNumFaces $
	local numVertexs = getNumVerts $
	local isLong = false
	if (numVertexs > 65535) then
	(
		isLong = true
	)
	local matsArray = GetMaterialStructures()
	LoopFaces numFaces matsArray
	GetBoundingSphere()
	matsArray = EraseUselessMaterials matsArray
	DebugResults matsArray
	WriteFile matsArray dir isLong
	)
	catch
	(
	)
	delete $ -- Para terminar, borramos el objeto exportado (que era una copia)
	select oldobj
)

function EraseUselessMaterials mats =
(
	local deleted = 0
	local deletedMats = #()
	local newMats = #()
	for i = 1 to mats.count do
	(
		local mat = mats[i]
		if (mat.m_VB.count == 0) then
		(
			local matName = mat.m_MaterialName
			LogWarning "Un material eliminado: "
			LogWarning m_MaterialName
			append deletedMats m_MaterialName
			deleted = deleted + 1
		) else (
			append newMats mat
		)
	)
	LogInfo "Materiales eliminados: "
	LogInfo deleted
	return newMats
)

function GetMaterialStructures =
(
	local structs = #()
	--if $.material == undefined then
	--(
		local matname = $.name + "_defaultMaterial"
		AddMaterialByName matname structs
	--)
	if classOf $.material == Standardmaterial then
	(
		AddMaterialStructure $.material structs
	)
	else if classOf $.material == Multimaterial then
	(
		for i = 1 to $.material.count do
		(
			local mat = $.material[i]
			if mat != undefined then
			(
				if classOf mat == Multimaterial then
				(
					for j = 1 to mat.count do
					(
						local submat = mat[j]
						if submat != undefined then (
							LogInfo submat.name
							AddMaterialStructure submat structs
						)
					)
				) else (
					LogInfo mat.name
					AddMaterialStructure mat structs
				)
			)
		)
	)
	return structs
)

function AddMaterialStructure mat structs =
(
	local struc = SMaterial()
	struc.m_MaterialName = mat.name
	--Creamos un objeto de tipo dictionary de .net, d�nde la clave es de tipo string y el valor es de tipo int16
	struc.m_VBIds = dotnetobject "System.Collections.Generic.Dictionary`2[System.String,System.Int16]"
	struc.m_MaterialFlags = 0x0000
	struc.m_MaterialFlags = bit.or struc.m_MaterialFlags POSITION
	--struc.m_MaterialFlags = bit.or struc.m_MaterialFlags GCOLOR
	struc.m_MaterialFlags = bit.or struc.m_MaterialFlags NORMAL
	--struc.m_MaterialFlags = bit.or struc.m_MaterialFlags UV
	try
	(
		if mat.bumpMapEnable then
		(
			LogInfo "A�adida flag TANGENT"
			LogInfo "A�adida flag BINORMAL"
			struc.m_MaterialFlags = bit.or struc.m_MaterialFlags TANGENT
			struc.m_MaterialFlags = bit.or struc.m_MaterialFlags BINORMAL
		)
	) catch ()
	try
	(
		if mat.diffuseMapEnable then
		(
			LogInfo "A�adida flag UV"
			struc.m_MaterialFlags = bit.or struc.m_MaterialFlags UV
		)
	) catch ()
	try
	(
		if mat.selfIllumMapEnable then
		(
			LogInfo "A�adida flag UV2"
			struc.m_MaterialFlags = bit.or struc.m_MaterialFlags UV2
		)
	) catch ()
	append structs struc
)

function AddMaterialByName mat structs =
(
	local struc = SMaterial()
	struc.m_MaterialName = mat
	--Creamos un objeto de tipo dictionary de .net, d�nde la clave es de tipo string y el valor es de tipo int16
	struc.m_VBIds = dotnetobject "System.Collections.Generic.Dictionary`2[System.String,System.Int16]"
	struc.m_MaterialFlags = 0x0000
	struc.m_MaterialFlags = bit.or struc.m_MaterialFlags POSITION
	--struc.m_MaterialFlags = bit.or struc.m_MaterialFlags GCOLOR
	struc.m_MaterialFlags = bit.or struc.m_MaterialFlags NORMAL
	struc.m_MaterialFlags = bit.or struc.m_MaterialFlags UV
	append structs struc
)

function LoopFaces numFaces mats =
(
	for i = 1 to numFaces do
	(
		local face = getFace $ i
		DoFace face i mats
	)
)

function AddToAuxBuffer buffer list size =
(
	for i = 1 to size do
	(
		append buffer list[i]
	)
)

function DoFace face index mats =
(
	try (
		if mats.count >= 2 then
		(
			local MaterialID=getFaceMatID $ index
			local MatStruct = mats[MaterialID+1]
			if (MatStruct == undefined) then
			(
				LogWarning "No existe el material. Se aplicar� material por defecto."
				local MatStruct = mats[2]
			)
		) else (
			local MatStruct = mats[2]
		)
		
		-- Normal init
		UABCalcVertexsNormals $
		local lFaceNormal = getFaceNormal $ index
		local lSmoothValue = getFaceSmoothGroup $ index
		
		local lInverseTrsf = inverse $.transform
		--lInverseTrsf.position = -$.center
		
		for i = 1 to 3 do -- 1 x, 2 y, 3 z
		(
			local lAuxBuffer = #()
			local lAuxKeyBuffer = #()
			local Key = ""
			if (bit.and MatStruct.m_MaterialFlags POSITION) == POSITION and
			(bit.and MatStruct.m_MaterialFlags NORMAL) == NORMAL --and
			--(bit.and MatStruct.m_MaterialFlags UV) == UV
			then
			(
				-- Position
				local Vtx = ( getVert $ face[i]) * lInverseTrsf
				Vtx = ConvertToRH Vtx
				AddToAuxBuffer lAuxBuffer Vtx 3
				AddToAuxBuffer lAuxKeyBuffer Vtx 3
				
				-- Normals
				local Normal1 = UABGetVertexNormal $ face[i] lSmoothValue lFaceNormal
				Normal1 = ConvertToRH Normal1
				AddToAuxBuffer lAuxBuffer Normal1 3
				AddToAuxBuffer lAuxKeyBuffer Normal1 3
				
				-- BUMP
				if (bit.and MatStruct.m_MaterialFlags TANGENT) == TANGENT and
				(bit.and MatStruct.m_MaterialFlags BINORMAL) == BINORMAL then
				(
					bump = #(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
					AddToAuxBuffer lAuxBuffer bump 8
				)
				
				-- UV1
				local IdxsMap = meshop.getMapFace $ 1 index
				local UV11 = meshop.getMapVert $ 1 IdxsMap[i]
				--UV11 = BaseChange UV11
				if (bit.and MatStruct.m_MaterialFlags UV) == UV then
				(
					AddToAuxBuffer lAuxBuffer UV11 2
				)
				AddToAuxBuffer lAuxKeyBuffer UV11 2
				
				-- String a partir de vertices
				Key = ConvertVtx lAuxBuffer
			) else (
				STOP
			)
			
			-- UV2
			if (bit.and MatStruct.m_MaterialFlags UV2) == UV2 then
			(
				local IdxsMap2 = meshop.getMapFace $ 2 index
				local UV21 = meshop.getMapVert $ 2 IdxsMap2[i]
				--UV21 = BaseChange UV21
				AddToAuxBuffer lAuxBuffer UV21 2
			)
			
			-- A�adimos, en caso de ser necesario, el vertex buffer al material
			local VBIndex = AddVertex MatStruct.m_VBIds MatStruct.m_VB lAuxBuffer Key
			append MatStruct.m_IB VBIndex
			lAuxBuffer = #()
		)
		-- Normals end
		UABClearNormalsArray
	) catch (
		LogInfo "No se ha podido exportar una cara"
	)
)

fn ChangeIndexs arr =
(
	local count = arr.count
	local newArray = #()
	for i = 1 to count by 3 do
	(
		append newArray arr[i]
		append newArray arr[i+2]
		append newArray arr[i+1]
	)
	return newArray
)

function WriteFile matlist fnamepath isLong =
(
	LogInfo "WriteFile1"
	file = BinFileWritter()
	file.ChangeVerbose false
	--local f = getSaveFilename "Save Object File" filename:( (getdir #userscripts) + "\\" + ($.name)) \
	--															 types:"Mesh(.mesh)|*.mesh|All|*.*|"
	--file.Open $.name "/meshes" ".mesh"
	file.Open $.name fnamepath ".mesh"
	--file.Open2 f
	if (isLong == false) then
	(
		file.PrintUnsignedShort 0xfe55 --Header
	) else (
		LogInfo "Utilizando Longs para cantidad de vertices..."
		file.PrintUnsignedShort 0xfe56 --Header Long
	)
	
		file.PrintUnsignedShort matlist.count -- #mats
			for i = 1 to matlist.count do -- Por cada material...
			(
				local mat = matlist[i]
				file.PrintString mat.m_MaterialName -- materialname
				LogInfo "Material flags: "
				LogInfo mat.m_MaterialFlags
				file.PrintUnsignedShort mat.m_MaterialFlags -- flags
				if (isLong == false) then
				(
					file.PrintUnsignedShort mat.m_VB.count -- VB.size
				) else (
					file.PrintUnsignedLong mat.m_VB.count -- VB.size, caso LONG
				)
				
				for y = 1 to mat.m_VB.count do -- Por cada entrada del vertex buffer...
				(
					--format "-------\n"
					local vertex = mat.m_VB[y]
					for z = 1 to vertex.count do -- Por cada float del v�rtice...
					(
						local num = vertex[z]
						file.PrintFloat num -- lista floats
					)
				)
				if (isLong == false) then
				(
					file.PrintUnsignedShort mat.m_IB.count -- IB.size
				) else (
					file.PrintUnsignedLong mat.m_IB.count -- IB.size Long
				)
				
				--mat.m_IB = ChangeIndexs mat.m_IB
				if (isLong == false) then
				(
					for k = 1 to mat.m_IB.count do -- Por cada entrada el index buffer...
					(
						local index = mat.m_IB[k]-1
						file.PrintUnsignedShort index -- lista unsigned shorts
					)
				) else (
					for k = 1 to mat.m_IB.count do -- Por cada entrada el index buffer...
					(
						local index = mat.m_IB[k]-1
						file.PrintUnsignedLong index -- lista unsigned shorts
					)
				)
				
				for j1 = 1 to 3 do -- AA
				(
					file.PrintFloat AA[j1]
				)
				for j2 = 1 to 3 do -- BB
				(
					file.PrintFloat BB[j2]
				)
				for j3 = 1 to 3 do -- Sphere center
				(
					file.PrintFloat SPHERECENTER[j3]
				)
				file.PrintFloat RADIUS -- Radio
				
			)
	if (isLong == false) then
	(
		file.PrintUnsignedShort 0x55fe --Footer
	) else (
		file.PrintUnsignedShort 0x56fe --Footer Long
	)
	LogInfo "Mesh guardada"
	file.Close()
)

/*
FILE FORMAT

Header 0xfe55 -- unsigned short
#mats -- unsigned short
materialname -- char[256]
flags -- unsigned short
VB.size -- unsigned short
VB -- lista floats
IB.size -- unsigned short
IB -- lista unsigned shorts
BOUNDINGBOX AABB -- 6 float (min + max)
BOUNDINGSPHERE -- 4 float (vect3 + radio)
Footer 0x55fe -- unsigned short
*/