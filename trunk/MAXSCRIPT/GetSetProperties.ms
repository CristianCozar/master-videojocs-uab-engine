rollout GetSetProperties "User Properties Modifier" width:392 height:456
(
	GroupBox cnm_grp1 "Cinematics" pos:[24,20] width:347 height:172
	checkbox cnm_chk1 "Is Cinematic" pos:[40,48] width:87 height:15
	spinner cnm_spn1 "Frame Step" pos:[184,48] width:176 height:16 type:#integer
	checkbox cnm_chk2 "Loop" pos:[40,72] width:80 height:16
	checkbox cnm_chk3 "Reverse" pos:[176,72] width:80 height:16
	edittext cnm_edt1 "" pos:[40,128] width:144 height:24
	edittext cnm_edt2 "" pos:[208,128] width:144 height:24
	editText cnm_edt3 "" pos:[208,160] width:144 height:24
	label lbl34 "Cinematic Name" pos:[96,168] width:88 height:16
	label lbl1 "Scene" pos:[48,104] width:112 height:16
	label lbl2 "Layer" pos:[216,104] width:112 height:16
	GroupBox lgh_grp "Lights" pos:[23,205] width:344 height:80
	spinner lgh_spn1 "" pos:[199,229] width:139 height:16
	label lbl5 "Intensity (0..10)" pos:[31,229] width:160 height:16
	spinner lgh_spn2 "" pos:[199,253] width:139 height:16
	label lbl6 "Specular Exp. (1..80)" pos:[31,253] width:160 height:16
	GroupBox scn_grp "Scene" pos:[23,302] width:344 height:72
	label lbl7 "Layer" pos:[39,333] width:56 height:16
	edittext scn_edt1 "" pos:[135,333] width:216 height:16
	button btn1 "Apply" pos:[104,400] width:176 height:40
	
	
	on GetSetProperties open do
	(
		local obj = $
		-- Cinematics
		cnm_chk1.checked = GetAnimated obj
		cnm_spn1.value = 1 --TODO
		cnm_chk2.checked = false -- TODO
		cnm_chk3.checked = false -- TODO
		cnm_edt1.text = GetScene obj
		cnm_edt2.text = GetLayer obj
		cnm_edt3.text = GetAnimationName obj
		-- Lights
		lgh_spn1.value = GetIntensity obj
		lgh_spn2.value = GetExponent obj
		-- Scene
		scn_edt1.text = GetLayer obj
	)
	on btn1 pressed do
	(
		local obj = $
		-- Cinematics
		setUserProp obj "animated" cnm_chk1.checked
		cnm_spn1.value = 1 --TODO
		cnm_chk2.checked = false -- TODO
		cnm_chk3.checked = false -- TODO
		setUserProp obj "scene" cnm_edt1.text
		setUserProp obj "layer" cnm_edt2.text
		setUserProp obj "animationName" cnm_edt3.text
		-- Lights
		setUserProp obj "intensity" lgh_spn1.value
		setUserProp obj "exponent" lgh_spn2.value
		-- Scene
		setUserProp obj "layer" scn_edt1.text
		WindowLogInfo "Se han aplicado los cambios."
	)
)
