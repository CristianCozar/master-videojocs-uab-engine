struct BinFileWritter
(
	mFile=undefined,
	mPath= undefined,
	mVerbose = false,
	fn ChangeVerbose aVerbose =
	(
		mVerbose = aVerbose
		if mVerbose == true then
		( 
			format "Verbose enabled\n"
		)
		else
		(
			format "Verbose disabled\n"
		)
	),
	fn Open2 pathString =
	(
		mPath = pathString
		if mVerbose == true then
		(
			format "New file %\n" mPath
		)
		if doesFileExist mPath  then
		(
			if mVerbose == true then
			(
				format "Old % deleted\n" mPath
			)
			deleteFile mPath
		)
	
		mFile = fopen mPath "wb"
		if mVerbose == true then
		(
			format "% opened in wb\n" mPath
		)
		return true
	),
	fn Open aName aDirectory aExtension = 
	(
		if aName != undefined and aDirectory != undefined then
		(
			if not doesFileExist aDirectory then
			(
				if mVerbose == true then
				(
					format "New directory %\n" aDirectory
				)
				makeDir aDirectory
			)
			
			mPath = aDirectory + aName + aExtension
			if mVerbose == true then
			(
				format "New file %\n" mPath
			)
			if doesFileExist mPath  then
			(
				if mVerbose == true then
				(
					format "Old % deleted\n" mPath
				)
				deleteFile mPath
			)
		
			mFile = fopen mPath "wb"
			if mVerbose == true then
			(
				format "% opened in wb\n" mPath
			)
			return true
		)
		
		if mVerbose == true then
		(
			format "% not opened\n" mPath
		)
		
		return false
	),
	fn Close =
	(
		closedOk = FClose mFile
		if mVerbose == true  and closedOk == true then
		(
			format "% closed\n" mPath
		) else if mVerbose == true and closedOk == false then
		(
			format "% got error closing\n" mPath
		)
	),
	fn  PrintShort aShort =
	(
		if mVerbose == true then
		(
			format "PrintShort %\n" aShort
		)
		WriteShort  mFile aShort
	),
	fn  PrintUnsignedShort aShort =
	(
		if mVerbose == true then
		(
			format "PrintUnsignedShort %\n" aShort
		)
		WriteShort  mFile aShort #unsigned
	),
	fn  PrintLong aLong =
	(
		if mVerbose == true then
		(
			format "PrintLong %\n" aLong
		)
		WriteLong  mFile aLong
	),
	fn  PrintUnsignedLong aLong =
	(
		if mVerbose == true then
		(
			format "PrintUnsignedLong %\n" aLong
		)
		WriteLong  mFile aLong #unsigned
	),
	fn  PrintFloat aFloat =
	(
		if mVerbose == true then
		(
			format "PrintFloat %\n" aFloat
		)
		WriteFloat mFile aFloat
	),
	fn PrintString aString =
	(
		if mVerbose == true then
		(
			format "PrintString % count % \n" aString aString.count
		)
		WriteShort mFile aString.count #unsigned
		WriteString mFile aString
	),
	fn PrintString2 aString aSize =
	(
		if mVerbose == true then
		(
			format "PrintString % size % \n" aString aSize
		)
		WriteString mFile aString
	)
)