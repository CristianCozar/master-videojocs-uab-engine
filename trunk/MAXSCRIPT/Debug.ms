function LogInfo text =
(
	format "[Info]: %\n" text
)

function LogWarning text =
(
	format "[Warning]: %\n" text
)

function LogError text =
(
	format "[Error]: %\n" text
)

rollout DebugInfoRollout "DebugWindow" width:228 height:102
	(
		label lbl1 "[Info]" pos:[10,6] width:215 height:26
		label lbl2 "asd" pos:[10,33] width:151 height:29
		--button btn1 "Ok" pos:[88,56] width:50 height:32
		fn changeText  message =
		(
			lbl2.text =  message
		)
		fn changeLevel level =
		(
			lbl1.text = level
		)
		/*on btn1 pressed do
		(
			DebugInfoRollout.open = false
		)*/
	)
	
function WindowLogInfo message =
(
	createDialog DebugInfoRollout
	DebugInfoRollout.changeText(message)
	DebugInfoRollout.changeLevel("[Info]")
)

function WindowLogWarning message =
(
	createDialog DebugInfoRollout
	DebugInfoRollout.changeText(message)
	DebugInfoRollout.changeLevel("[Warning]")
)

function WindowLogError message =
(
	createDialog DebugInfoRollout
	DebugInfoRollout.changeText(message)
	DebugInfoRollout.changeLevel("[Error]")
)