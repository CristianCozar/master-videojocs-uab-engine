filein "Debug.ms"
filein "UABFuncs.ms"
filein "Utils.ms"
filein "BinFileWritter.ms"

function DoMainConvexSingle =
(
	ConvexMesh "C:\\Users\\Mesiah\\Desktop\\PRIM3_Level1\\mesh.mesh"
)

function ConvexMesh filename =
(
	local dir = getFilenamePath filename
	dir = dir + "\\collisionMeshes\\"
	oldobj = $
	newobj = copy $ -- Copiamos nuestro objeto
	select newobj
	resetxform $
	convertToMesh $ -- Y lo convertimos a mesh
	try
	(
	local numVertexs = getNumVerts $
	local lInverseTrsf = inverse $.transform
		
	file = BinFileWritter()
	file.ChangeVerbose true
	file.Open oldobj.name dir ".cmesh"
	file.PrintUnsignedLong numVertexs
	for i = 1 to numVertexs do
	(
		local vtx = getVert $ i * lInverseTrsf
		Vtx = ConvertToRH Vtx
		for j = 1 to 3 do -- x, y, z
		(
			file.PrintFloat vtx[j]
		)
	)
		
	LogInfo "Convex mesh guardada"
	file.Close()
	)
	catch
	(
	)
	delete $ -- Para terminar, borramos el objeto exportado (que era una copia)
	select oldobj
)