filein "Utils.ms"
filein "xml.ms"
filein "Debug.ms"

function DoMainLights filePath =
(
	local lightsArray = GetLights()
	WriteXML lightsArray filePath
)

function GetLights =
(
	local l = lights
	local lightsArray = #()
	for i = 1 to lights.count do
	(
		if superClassOf lights[i] == light then
		(
			append lightsArray lights[i]
		)
	)
	return lightsArray
)

function WriteXML lightsArray filePath =
(
	-- create the doc
	local xmlDoc = XmlDocument()
	-- create the root node, assign to the document
	local rootNode = XmlNode tag:"lights"
	xmlDoc.rootNode = rootNode
	
	--local f = getSaveFilename "Save XML File" filename:( (getdir #userscripts) + "\\" + (getFilenameFile maxFilename) + "-LIGHTS") \
	--															 types:"XML File(.xml)|*.xml|All|*.*|"
	local f = filePath + "\\Lights.xml"
	if f != undefined then
	(
		xmlDoc.filename = f
		for i = 1 to lightsArray.count do
		(
			local l = lightsArray[i]
			local lightNode = XmlNode tag:"light"
			rootNode.AddChild lightNode
			lightNode.AddAttribute "name" l.name
			local type = GetLightType l
			lightNode.AddAttribute "type" type
			GetAttributes lightNode type l
			GetTransformWithForward lightNode l
		)
		xmlDoc.SaveXmlDocument()
	)
)

function GetIntensity obj =
(
	local intensity = getUserProp obj "intensity"
	if intensity == undefined then
	(
		intensity = 1
	)
	return intensity as float
)

function GetExponent obj =
(
	local exponent = getUserProp obj "exponent"
	if exponent == undefined then
	(
		exponent = 1
	)
	return exponent as float
)

function GetAttributes lightNode type l =
(
	local col = #(l.rgb.r / 255.0, l.rgb.g / 255.0, l.rgb.b / 255.0, l.rgb.a / 255.0)
	local colStr = GetStringFromArray col
	lightNode.AddAttribute "color" colStr
	lightNode.AddAttribute "enabled" l.on
	local l_Intensity = GetIntensity l
	lightNode.AddAttribute "intensity" l_Intensity
	local l_Exponent = GetExponent l
	lightNode.AddAttribute "specular_exponent" l_Exponent
	local atenuationRange = #(l.farAttenStart, l.farAttenEnd)
	local atenuationRangeStr = GetStringFromArray atenuationRange
	lightNode.AddAttribute "attenuation_range" atenuationRangeStr
	
	if type == "point" then
	(
		
	) else if type == "spot" then
	(
		local ang = Deg2Rad l.hotspot
		local fallof = Deg2Rad l.falloff
		lightNode.AddAttribute "angle" ang
		lightNode.AddAttribute "fall_off" fallof
	) else if type == "directional" then
	(
		
	)
)

function GetLightType l =
(
	local type
	if classOf l == Omnilight then
	(
		type = "point"
	) else if classOf l == TargetSpot or classOf l == FreeSpot then
	(
		type = "spot"
	) else if classOf l == DirectionalLight or classOf == TargetDirectionalLight then
	(
		type = "directional"
	)
	return type
)

function GetTransformWithForward lightNode obj =
(
	local transformNode = XmlNode tag:"transform"
	local tr = obj.transform
	local position = #(-tr.translation.x, tr.translation.z, tr.translation.y)
	local strPosition = GetStringFromArray position
	transformNode.AddAttribute "position" strPosition
	local forward = #(obj.dir.x, -obj.dir.z, -obj.dir.y)
	local strForward = GetStringFromArray forward
	transformNode.AddAttribute "forward" strForward
	lightNode.AddChild transformNode
)