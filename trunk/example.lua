-- example.lua
var1 = 12
var2 = 25
result = 150

bar = {}
bar[3] = "hi"
bar["key"] = "there"

function add(a, b)
  return a + b
end

function get_float2()
  return float2(10, 10)
end

function sum_float2(a, b)
  return a + b
end