class 'simple_movement' (BaseComponent)

function simple_movement:__init(name)
	BaseComponent.__init(self, name)
	
	self.speed = 10.0
	
	self.movement_x = 0.0
	self.movement_y = 0.0
	self.movement_z = 0.0
end

function simple_movement:update(dt)
	self.movement_x = 0.0
	self.movement_z = 0.0
	local moved = false
	if get_action("MOVEMENTX") then
		self.movement_x = -get_action_amount("MOVEMENTX") * dt * self.speed
		moved = true
	end
	if get_action("MOVEMENTY") then
		self.movement_z = get_action_amount("MOVEMENTY") * dt * self.speed
		moved = true
	end
	self.movement_y = -1.0
	--[[if (moved == true) then
		local pos = gameObject.transform.position
		pos.x = self.movement_x + pos.x
		pos.z = self.movement_z + pos.z
		gameObject.transform.position = pos
	end]]--
end

simpleMovement = simple_movement('SimpleMovement')

function get_movement_offset()
	return float3(simpleMovement.movement_x, simpleMovement.movement_y, simpleMovement.movement_z)
end