-- /////////// Gameobject Engine CLASS /////////// --
class 'GameObjectEngine'

function GameObjectEngine:__init()
	self.gameObjects = {}
	self._components = {}
end

function GameObjectEngine:register_component_class(constructor, name)
	self._components[name] = constructor
end

function GameObjectEngine:register_component(objectName, componentName)
	self.gameObjects[objectName]:register_component(self._components[componentName](objectName))
end

function GameObjectEngine:register_object(objectName)
	self.gameObjects[objectName] = GameObject(objectName)
end

function GameObjectEngine:update(dt)
	for key,value in pairs(self.gameObjects) do
		value:update(dt)
	end
end

function GameObjectEngine:start()
	add_new_info_log("Ejecutando start global")
	for key,value in pairs(self.gameObjects) do
		value:start()
	end
end

function GameObjectEngine:destroy()
	for key,value in pairs(self.gameObjects) do
		value:destroy()
	end
end

engine = GameObjectEngine()

-- ////////////////////////////////////////// --

function register_component_class(constructor, name)
	engine:register_component_class(constructor, name)
end

function register_component(objectName, componentName)
	engine:register_component(objectName, componentName)
end

function register_object(objectName)
	engine:register_object(objectName)
end

function start()
	engine:start()
end

function update(dt)
	engine:update(dt)
end

function ondestroy()
	engine:destroy()
end
