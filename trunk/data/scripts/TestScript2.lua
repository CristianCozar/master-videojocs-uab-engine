class 'lua_testclass2' (BaseComponent)

function lua_testclass2:__init(name)
	BaseComponent.__init(self, name)
end

function lua_testclass2:print()
    add_new_info_log(self.name)
end

function lua_testclass2:printother()
	a:print()
end

function lua_testclass2:update(dt)
	if get_action("DEBUGTEST2") then
		self:printother()
	end
end

b = lua_testclass2('example2')