
-- /////////// GAME OBJECT CLASS /////////// --
class 'GameObject'

function GameObject:__init(name)
    self.name = name
	self.components = {}
	self.transform = nil
end

function GameObject:update(dt)
	for key,value in pairs(self.components) do
		value:update(dt)
	end
end

function GameObject:start()
	add_new_info_log("Ejecutando start")
	for key,value in pairs(self.components) do
		value:start()
	end
end

function GameObject:destroy()
	for key,value in pairs(self.components) do
		value:destroy()
	end
end

function GameObject:register_component(c)
	add_new_info_log("Registrando " .. c.name)
	self.components[c.name] = c
end

-- ////////////////////////////////////////// --

function set_name(name)
	gameObject.name = name
end

function get_name()
	return gameObject.name
end

function set_transform(transform)
	gameObject.transform = transform
end

function get_transform()
	return gameObject.transform
end

function get_object()
	return gameObject
end

function get_object_name(object)
	add_new_info_log("[NAME] " .. object.name)
end

function start()
	gameObject:start()
end

function update(dt)
	gameObject:update(dt)
end

function ondestroy()
	gameObject:destroy()
end





gameObject = GameObject('gameObject')

-- /////////// BASE COMPONENT CLASS /////////// --
class 'BaseComponent'

function BaseComponent:__init(name)
	add_new_info_log("Creando " .. name)
	self.name = name
	gameObject:register_component(self)
end

function BaseComponent:update(dt)
end

function BaseComponent:start()
end

function BaseComponent:destroy()
end

-- ////////////////////////////////////////// --