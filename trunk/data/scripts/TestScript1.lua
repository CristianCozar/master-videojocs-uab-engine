class 'lua_testclass1' (BaseComponent)

function lua_testclass1:__init(name)
	BaseComponent.__init(self, name)
end

function lua_testclass1:print()
    add_new_info_log(self.name)
end

function lua_testclass1:printother()
	b:print()
end

function lua_testclass1:update(dt)
	if get_action("DEBUGTEST") then
		self:printother()
	end
end

a = lua_testclass1('example1')