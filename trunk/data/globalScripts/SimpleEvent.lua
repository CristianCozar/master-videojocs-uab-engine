-- SimpleEvent component

class 'SimpleEvent' (BaseComponent)


function SimpleEvent:__init(name)
	BaseComponent.__init(self, name)
	
	self.solved_count = 0
	
	self.TRIGGER_PLAYER_IN_AREA = "PLAYER_IN_AREA"
	self.TRIGGER_RUNES = "RUNES"
	self.TRIGGER_ENEMIES = "ENEMIES"
	self.TRIGGER_EXTERNAL = "EXTERNAL"
	
	-- Parametros de xml
	self.trigger_type = "PLAYER_IN_AREA"
	self.quantity_to_solve = 1
	self.event_name = "event"
	self.only_once = true
	self.destroy_object = false
	self.function_to_execute = nil
	self.function_parameter = nil
	self.function_gameobject_as_parameter = false
	-- Fin parametros xml
end

function SimpleEvent:start()
	EventList[self.event_name] = self
	add_new_info_log("SimpleEvent: " ..self.event_name)
end

function SimpleEvent:update(dt)

end

function SimpleEvent:on_trigger_enter(other_name)
	if self.trigger_type == self.TRIGGER_PLAYER_IN_AREA then
		self:trigger_event()
	end
end

function SimpleEvent:rune_activated()
	if self.trigger_type == self.TRIGGER_RUNES then
		self.solved_count = self.solved_count + 1
		self:check_solved_quantity()
	end
end

function SimpleEvent:enemy_killed()
	if self.trigger_type == self.TRIGGER_ENEMIES then
		self.solved_count = self.solved_count + 1
		self:check_solved_quantity()
	end
end

function SimpleEvent:check_solved_quantity()
	--[[add_new_info_log("To_solve: " ..self.quantity_to_solve)
	add_new_info_log("Solved: " ..self.solved_count)--]]
	if self.quantity_to_solve <= self.solved_count then
		self:trigger_event()
	end
end

function SimpleEvent:external_trigger()
	if self.trigger_type == self.TRIGGER_EXTERNAL then
		self:trigger_event()
	end
end

function SimpleEvent:trigger_event()
	self:execute_function()
	if self.only_once then
	end
	if self.destroy_object then
		delete_named_node(self.gameObject.name)
	end
end

function SimpleEvent:execute_function()
	if self.function_to_execute ~= nil then
		local func = _G[self.function_to_execute]
		if self.function_parameter == nil then
			if self.function_gameobject_as_parameter then
				func(self.gameObject)
			else
				func()
			end
		else
			if self.function_gameobject_as_parameter then
				func(self.function_parameter, self.gameObject)
			else
				func(self.function_parameter)
			end
		end
	end
end

EventList = {}