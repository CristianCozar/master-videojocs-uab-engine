-- InstanceTest component

class 'InstanceTest' (BaseComponent)

function InstanceTest:__init(name)
	BaseComponent.__init(self, name)
	
	self.last = 0
end

function InstanceTest:start()
	
end

function InstanceTest:update(dt)
	if get_action("NUMBER") then
		--[[local num = math.floor(get_action_amount("NUMBER"))
		local scene = "Level" .. num
		add_new_info_log("Loading scene " .. scene)
		engine:change_scene(scene)]]--
		
		local num = math.floor(get_action_amount("NUMBER"))
		if (num < 9) then
			self:play_loop(num)
		else
			--clear_cycle("Salt", self.last, 0.0)
			--self.last = nil
			queue_action_by_name("Salt", "death", 0.0, 0.0, 1.0)
			queue_action_by_name("Salt", "resurrection", 0.0, 0.0, 1.0)
			queue_action_by_name("Salt", "attack1", 0.0, 0.0, 1.0)
			queue_action_by_name("Salt", "attack2", 0.0, 0.0, 1.0)
			queue_action_by_name("Salt", "attack3", 0.0, 0.0, 1.0)
			queue_action_by_name("Salt", "death", 0.0, 0.0, 1.0)
		end
	end
	if get_action("DEBUGTEST") then
		self:play_animation("death")
		--chapelObject = self.gameObject:instance_return("Chapel", "Chapel")
		--chapelObject.transform.position.x = 30.0
		--add_new_info_log("Modificando material")
		--change_material_vec4("01 - Default", "color", float4(1.0,0.0,1.0,1.0))
		--add_new_info_log("Parametro name " .. self.testname)
		--add_new_info_log("Parametro position " .. self.position.x)
		--engine:change_scene("Level1")
		--remove_current_scene()
		--engine:request_scene_load("Level1")
	end
end

function InstanceTest:play_loop(anim)
	if (self.last ~= nil) then
		clear_cycle("Salt", self.last, 0.0)
	end
	blend_cycle("Salt", anim, 1.0, 0.0)
	self.last = anim
end

function InstanceTest:play_animation(anim)
	execute_action_by_name("Salt", anim, 0.0, 0.0, 1.0)
end