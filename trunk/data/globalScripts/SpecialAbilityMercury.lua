-- SpecialAbilityMercury component: Controls Mercury's ability'

class 'SpecialAbilityMercury' (SpecialAbility)

function SpecialAbilityMercury:__init(name)
	SpecialAbility.__init(self, name)

	self.mercury_copy_initial_distance = 1.0
	self.mercury_copy_max_distance = 4.0
	self.mercury_copy_speed = 0.2
	self.mercury_copy = nil
	self.button_hold = false
	self.movement_script = nil
	self.camera = nil
	self.max_distance = 17.5
	self.animator = nil

	self.gui_pos = gui_position(0.02, 0.02, 0.06, 0.11, "TOP_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
end

function SpecialAbilityMercury:start()
	self.movement_script = self.gameObject:get_component("MovementController")
	self.camera = get_prim3_camera()	
	self.animator = self.gameObject:get_component("AnimatorController")
end

function SpecialAbilityMercury:update(dt)
	self.current_cooldown = float_clamp(0, self.cooldown, self.current_cooldown - dt)
	if get_active(self.gameObject.name) then
		self:use()
		gui_image("player_portrait", "mercury_portrait", self.gui_pos)
	elseif self.mercury_copy ~= nil then
		self.mercury_copy:destroy()
		self.mercury_copy = nil
		self.button_hold = false
	end
end


function SpecialAbilityMercury:use()
	local go = self.gameObject
	
	local mc = self.gameObject:get_component("MovementController")
	if mc ~= nil then
		if mc.grounded then		
			if get_action("SPECIAL_ABILITY_PRESSED") and (self.current_cooldown == 0) then
				if (go:get_component("Energy"):reserve_energy(self.energy_cost)) then
					self.button_hold = true
					self.movement_script:SetActive(false)
					self.animator:play_animation("ability_begin")
					self.animator:play_loop_clear("ability_pose")
					self.mercury_copy = get_object_by_name(go:instance("Mercury_Copy", "SpecialAbilityMercuryMarker"))
					self.mercury_copy.transform.position = float3(go.transform.position.x, go.transform.position.y, go.transform.position.z) +  (go.transform.forward * self.mercury_copy_initial_distance)
				end
			elseif get_action("SPECIAL_ABILITY_HOLD") and (self.current_cooldown == 0) and 	(self.button_hold) then
				local movementX_amount = get_action_amount("MOVEMENTX")
				local movementY_amount = get_action_amount("MOVEMENTY")	
				local camera_forward = normalize(scale(self.camera.m_Front, float3(1.0, 0.0, 1.0)))
				local camera_right = cross_product(self.camera.m_Front, self.camera.m_Up)
				local direction = normalize((camera_forward * movementY_amount) + (camera_right * movementX_amount))
				local temp_pos = self.mercury_copy.transform.position + float3(direction.x * self.mercury_copy_speed, direction.y * self.mercury_copy_speed, direction.z * self.mercury_copy_speed)
				local distance = temp_pos:distance(self.gameObject.transform.position)
				if distance <= self.max_distance then
					--add_new_info_log("Distance: " ..distance)
					self.mercury_copy.transform.position = temp_pos
				end
			elseif get_action("SPECIAL_ABILITY_RELEASED") and (self.button_hold) then
				go:get_component("Energy"):consume_reserved_energy(self.energy_cost)
			--	self:createAreaDamage()
				local mov = self.mercury_copy.transform.position - self.transform.position
				move_node(go.name, mov)
				mov.y = 0.0
				mov = mov:normalized(1.0)
				--mov = mov * self.movement_speed * dt	
				--set_position(go.name, self.mercury_copy.transform.position)
				self.mercury_copy:destroy()
				self.mercury_copy = nil
				self.button_hold = false
				self.current_cooldown = self.cooldown
				self.movement_script:SetActive(true)
				self.animator:play_loop_clear("idle")
				self.movement_script.is_moving = false
				self.movement_script.is_idle = false
				self.animator:play_animation("mercury_ability_fin")

			end
		end
	end
end

function SpecialAbilityMercury:createAreaDamage()

	local distance = vector3_lenght(self.mercury_copy.transform.position - self.gameObject.transform.position)
	local direction = normalize(self.mercury_copy.transform.position - self.gameObject.transform.position)
	local num_areas = math.ceil(distance)
	local area_distance = distance / num_areas
	local go = self.gameObject
	
	add_new_info_log("Direction: " .. vect3_to_string(direction) .. " Area distance: " .. area_distance .. " Num Areas: " .. num_areas)
	for i=1, num_areas do
		local damage_area = get_object_by_name(go:instance("SpecialAbilityMercury", "DamageArea"))
		set_position(damage_area.name, self.gameObject.transform.position + float3(direction.x * area_distance * i, direction.y * area_distance * i, direction.z * area_distance * i))
		damage_area:add_component("SpecialAbilityMercuryArea")
		add_new_info_log("A�adida area de da�o")
	end
end

