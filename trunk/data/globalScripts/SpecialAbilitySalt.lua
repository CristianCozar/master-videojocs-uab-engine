-- SpecialAbilitySalt component: Controls Salt's ability'

class 'SpecialAbilitySalt' (SpecialAbility)

function SpecialAbilitySalt:__init(name)
	SpecialAbility.__init(self, name)
	self.shield_radius = 2.0
	self.shield = nil
	self.energy_cost_per_sec = 10.0
	self.button_hold = false

	-- MovementController
	self.movement_script = nil
	self.health_script = nil
	self.speed_multiplier = 0.5
	self.initial_movement_speed = 0.0
	self.animator = nil

	
	self.gui_pos = gui_position(0.02, 0.02, 0.06, 0.11, "TOP_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
end

function SpecialAbilitySalt:start()
	self.movement_script = self.gameObject:get_component("MovementController")
	self.health_script = self.gameObject:get_component("Health")
	self.animator = self.gameObject:get_component("AnimatorController")
end

function SpecialAbilitySalt:update(dt)
	self.current_cooldown = float_clamp(0, self.cooldown, self.current_cooldown - dt)
	if get_active(self.gameObject.name) then
		self:use(dt)
		gui_image("player_portrait", "salt_portrait", self.gui_pos)
	elseif self.shield ~= nil then
		self:DestroyShield()
	end
end


function SpecialAbilitySalt:use(dt)
	local go = self.gameObject
	local mc = self.gameObject:get_component("MovementController")
	if mc ~= nil then
		if mc.grounded then
			if get_action("SPECIAL_ABILITY_PRESSED") and (self.current_cooldown == 0) then
				if (self.gameObject:get_component("Energy"):consume_energy(self.energy_cost)) then 
					self.shield = get_object_by_name(go:instance_in_layer("SpecialAbilitySalt", "SpecialAbilitySalt", "Transparent"))
					self.animator:queue_action("ability_begin")
					self.movement_script.has_shield = true
					self.movement_script.is_moving = false
					self.movement_script.is_idle = false
					local sassc = self.shield:add_component("SpecialAbilitySaltShield")
					sassc:set_salt_reference(self.gameObject)
					set_position(self.shield.name ,  self.gameObject.transform.position)
					self.button_hold = true

					self.initial_movement_speed = self.movement_script.speed
					self.movement_script.speed = self.initial_movement_speed * self.speed_multiplier

					self.health_script.invincible = true

				end
			elseif get_action("SPECIAL_ABILITY_HOLD") and (self.button_hold) then
				if (self.gameObject:get_component("Energy"):consume_energy(self.energy_cost_per_sec * dt)) then 
					if self.shield then
					--	if not self.movement_script.is_moving then
					--		self.animator:play_loop_clear("ability_pose")
					--	end
						set_position(self.shield.name ,  self.gameObject.transform.position)
					end
				else
					self:DestroyShield()
				end
			elseif get_action("SPECIAL_ABILITY_RELEASED") and (self.button_hold) then
				self:DestroyShield()
			end
		end
	end
end


function SpecialAbilitySalt:DestroyShield()
		self.button_hold = false
		self.movement_script.has_shield = false

		if (self.shield) then
			self.shield:destroy()
			self.shield = nil
		end

		self.health_script.invincible = false
		self.current_cooldown = self.cooldown
		self.gameObject:get_component("MovementController").speed = self.initial_movement_speed
		if self.movement_script.is_moving then
			self.animator:play_loop_clear("run")
		else 
			self.animator:play_loop_clear("idle")
		end
		self.animator:queue_action("ability_end")

end





