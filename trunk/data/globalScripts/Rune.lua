-- Rune component

class 'Rune' (BaseComponent)

function Rune:__init(name)
	BaseComponent.__init(self, name)
	self.needStay = false
	self.runeActivationTime = 4.0
	self.currentTimer = 0.0
	self.activated = false
    self.partycles = nil
    self.m_numPlayersStay = 0
	self.finished = false
	self.event_to_register = ""
	self.activationSpeed = 5
	
	self.particles = nil
end

function Rune:start()
	create_physx_trigger_box(self.gameObject.name, 1.0, 2.0, 1.0, self.position, quat(0.0,0.0,1.0,0.0), "Default")
end
	


function Rune:update(dt)
end

function Rune:on_trigger_enter(name)
	self.currentTimer = 0.0
	--add_new_info_tag("NameEnter: " ..name)
	local c = get_object_by_name(name)
	if c:has_tag("Player") then
		self.m_numPlayersStay = self.m_numPlayersStay + 1
		if not self.activated then
			self.particles = engine:get_object_by_tag("slash_spawner"):get_component("SlashSpawner"):spawn("RuneParticle", self.transform.position)
		end
	end
	table.insert(self.gameObject.gameObjectsOnTrigger, name)
end


function Rune:on_trigger_exit(name)	
	--add_new_info_tag("NameExit: " ..name)
	local c = get_object_by_name(name)
	if c:has_tag("Player") then
		self.m_numPlayersStay = self.m_numPlayersStay - 1
		if not self.finished and self.needStay then
			--[[if self.activated and self.needStay then
				self.activated = false
				self:on_deactivate()
				self.currentTimer = 0.0
			end--]]			
			if not self.activated or self.needStay then
				self.currentTimer = 0.0
			end
		end
		if self.particles ~= nil then
			engine:request_destroy(self.particles)
			self.particles = nil
		end
	end
	local i = index_of_value(self.gameObject.gameObjectsOnTrigger, name)
	table.remove(self.gameObject.gameObjectsOnTrigger, i)
end

function Rune:on_trigger_stay(name, dt)	
	--add_new_info_tag("NameStay: " ..name)
	local c = get_object_by_name(name)
	if c:has_tag("Player") then
		if self.currentTimer >= self.runeActivationTime then
			if not self.activated then
				self.activated = true
				self:on_activate()
			end
		else
			self.currentTimer = self.currentTimer + dt
		end
	end
end

function Rune:on_activate()
	if self.particles ~= nil then
		engine:request_destroy(self.particles)
		self.particles = nil
	end
	local ev = EventList[self.event_to_register]
	if ev ~= nil then
		EventList[self.event_to_register]:rune_activated()
	end
end