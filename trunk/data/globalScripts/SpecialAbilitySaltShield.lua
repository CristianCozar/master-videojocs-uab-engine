-- SpecialAbilitySaltShield component: Controls Salt's shield lifetime

class 'SpecialAbilitySaltShield' (BaseComponent)

function SpecialAbilitySaltShield:__init(name)
	BaseComponent.__init(self, name)
	self.shield_radius = 2.0
	
	self.energy = nil
	self.salt = nil
end

function SpecialAbilitySaltShield:start()
	
end

function SpecialAbilitySaltShield:set_salt_reference(salt)
	self.salt = salt
	if self.salt ~= nil then
		self.energy = self.salt:get_component("Energy")
	end
end

function SpecialAbilitySaltShield:update(dt)
	self:manage_collisions()
end


function SpecialAbilitySaltShield:manage_collisions()
	local projectiles = engine:get_objects_by_tag("Projectile")
	if projectiles then
		for i = 1, #projectiles do	
			local distance = vector3_lenght(projectiles[i].transform.position - self.gameObject.transform.position)
			if (distance <= self.shield_radius) then
				add_new_info_log("Choque")
				
				self:damage_shield_projectile(projectiles[i])
				
				projectiles[i]:destroy()
			end
		end
	end
end

function SpecialAbilitySaltShield:damage_shield_projectile(projectile)
	local gpc = projectile:get_component("GolemProjectile")
	if gpc ~= nil and self.energy ~= nil then
		self.energy:consume_energy(gpc.damage)
	end
end

function SpecialAbilitySaltShield:damage_shield(damage)
	if self.energy ~= nil then
		self.energy:consume_energy(damage)
		return damage
	end
	return 0
end