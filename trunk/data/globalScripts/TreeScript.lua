-- TreeScript component

class 'TreeScript' (Health)

function TreeScript:__init(name)
	Health.__init(self, name)
	self.has_event = false
	self.cinematic_name = "TrunkAnim"
	self.activate_on_damage = true
	self.activated = false
	
end

function TreeScript:start()
		--create_char_controller(self.gameObject.name, 5.0, 1.0, self.gameObject.transform.position, quat(0.0,0.0,1.0,0.0), 1.0, "Default")
end

function TreeScript:update(dt)
end


function TreeScript:damage(dmg)
	if self.activate_on_damage then
		self:activate()
	end
	
	if has_event then
		local ev = EventList[self.event_to_register]
		if ev ~= nil then
			EventList[self.event_to_register]:external_trigger()
		end
	end
end

function TreeScript:activate()
	if not self.activated then
		play_cinematic(self.cinematic_name)
		self.activated = true
	end
end

