-- AnimatorController component

class 'AnimatorController' (BaseComponent)

function AnimatorController:__init(name)
	BaseComponent.__init(self, name)
	self.last_animation = "idle"	
end

function AnimatorController:start()	
end

function AnimatorController:update(dt)	
end

function AnimatorController:play_loop_clear(anim)
	--add_new_info_log("AnimationLoopClear: " ..anim)
	clear_cycle_by_name(self.gameObject.name, self.last_animation, 0.0)
	blend_cycle_by_name(self.gameObject.name, anim, 1.0, 0.0)
	self.last_animation = anim
end

function AnimatorController:play_loop(anim)
--	add_new_info_log("AnimationLoop: " ..anim)
	--clear_cycle_by_name(self.gameObject.name, self.last_animation, 0.0)
	blend_cycle_by_name(self.gameObject.name, anim, 1.0, 0.0)
	self.last_animation = anim
end

function AnimatorController:play_animation(anim)
	--add_new_info_log("Animation: " ..anim)
	execute_action_by_name(self.gameObject.name, anim, 0.0, 0.0, 1.0)
end

function AnimatorController:play_animation_clear(anim)
--	add_new_info_log("Animation: " ..anim)
	clear_cycle_by_name(self.gameObject.name, anim, 0.0)
	execute_action_by_name(self.gameObject.name, anim, 0.0, 0.0, 1.0)
end


function AnimatorController:animation_active(anim)
	return is_action_animation_active_by_name(self.gameObject.name, anim)
end

function AnimatorController:any_animation_active()
	return is_any_action_animation_active(self.gameObject.name)
end

function AnimatorController:clear_cycle()
	clear_cycle_by_name(self.gameObject.name, self.last_animation, 0.0)
end

function AnimatorController:queue_action(anim)
--	add_new_info_log("actionQueued: " ..anim)
	queue_action_by_name(self.gameObject.name, anim, 0.0, 0.0, 1.0)
end

function AnimatorController:queue_clear()
	clear_queue(self.gameObject.name)
end