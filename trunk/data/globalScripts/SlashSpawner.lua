-- SlashSpawner component

class 'SlashSpawner' (BaseComponent)

function SlashSpawner:__init(name)
	BaseComponent.__init(self, name)
end

function SlashSpawner:start()
	
end

function SlashSpawner:update(dt)
	if get_action("DEBUGTEST") then
		particlesObject = self.gameObject:instance_return("Hit", "Hit")
		--particlesObject = self.gameObject:instance_return("Slash", "Slash")
		--chapelObject = self.gameObject:instance_return("Chapel", "Chapel")
		--chapelObject.transform.position.x = 30.0
	end
end

function SlashSpawner:spawn(object, position)
	particlesObject = self.gameObject:instance_return(object, object)
	particlesObject.transform.position = position
	return particlesObject
end