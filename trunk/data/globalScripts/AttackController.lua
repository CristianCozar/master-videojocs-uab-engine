-- movementController component: Allows the Character to Move, Rotate, Jump and Dash

class 'AttackController' (BaseComponent)

function AttackController:__init(name)
	BaseComponent.__init(self, name)

	self.attack_damage = 20.0
	self.attack_speed = 2.0
	self.attack_range = 20.0
	self.attack_angle = 90.0
	self.attack_height = 100.0
	self.current_attack = 0.0
	
	self.movement = nil
	self.animator = nil
	self.attacking = false
	self.attacking2 = false
	self.timer_between_attacks = 1.0
	self.curret_timer = 1.0
	
end

function AttackController:start()
	self.movement = self.gameObject:get_component("MovementController")
	self.animator = self.gameObject:get_component("AnimatorController")

end


function AttackController:update(dt)
	self:manage_attack()
	self:end_attack()
	self.curret_timer = self.curret_timer + dt
end

function AttackController:manage_attack()
	local grounded = true
	if self.movement ~= nil then
		grounded = self.movement.grounded
	end
	if get_action("ATTACK") and (not self:is_attacking()) and grounded and not self.animator:any_animation_active() 
		and not self.attacking and self.curret_timer >= self.timer_between_attacks then
		--self.animator:play_animation("attack1")
		self.animator:queue_action("attack1")
		self.animator:queue_action("attack1_return")
		self.curret_timer = 0.0
		local enemies = engine:get_objects_by_tag("Enemy")
		self.attacking = true
		
		for key,value in pairs(enemies) do
			local enemy = enemies[key]
			local result = static_attack_check(self.gameObject.transform, enemy.transform, self.attack_range, self.attack_angle, self.attack_height, self.attack_damage, 0.0, 0.0)
			if (result >= 0) then
				local health = enemy:get_component("Health")
				if health then
					add_new_info_log("Enemy " .. enemy.name .. " damaged by " .. result .. " damage")
					health:damage(result)
				end
			end		
		end
	elseif get_action("ATTACK") and (not self:is_attacking()) and grounded and self.attacking  and not self.attacking2
	and self.curret_timer >= self.timer_between_attacks	then
		self.animator:queue_clear()
		self.animator:queue_action("attack2")
		self.animator:queue_action("attack2_return")
		self.curret_timer = 0.0
		local enemies = engine:get_objects_by_tag("Enemy")
		self.attacking = true
		self.attacking2 = true
		
		for key,value in pairs(enemies) do
			local enemy = enemies[key]
			local result = static_attack_check(self.gameObject.transform, enemy.transform, self.attack_range, self.attack_angle, self.attack_height, self.attack_damage, 0.0, 0.0)
			if (result >= 0) then
				local health = enemy:get_component("Health")
				if health then
					add_new_info_log("Enemy " .. enemy.name .. " damaged by " .. result .. " damage")
					health:damage(result)
				end
			end		
		end	
--[[	elseif get_action("ATTACK") and (not self:is_attacking()) and grounded and self.attacking  and self.attacking2
	and self.curret_timer >= self.timer_between_attacks	then
		self.animator:queue_clear()
		self.animator:queue_action("attack3")
		self.animator:queue_action("attack3_return")
		local enemies = engine:get_objects_by_tag("Enemy")
		self.attacking = false
		self.attacking2 = true
		self.curret_timer = 0.0

		for key,value in pairs(enemies) do
			local enemy = enemies[key]
			local result = static_attack_check(self.gameObject.transform, enemy.transform, self.attack_range, self.attack_angle, self.attack_height, self.attack_damage, 0.0, 0.0)
			if (result >= 0) then
				local health = enemy:get_component("Health")
				if health then
					add_new_info_log("Enemy " .. enemy.name .. " damaged by " .. result .. " damage")
					health:damage(result)
				end
			end		
		end--]]
	end
end

function AttackController:end_attack()
	if self.attacking and not self.animator:any_animation_active() then
		--self.animator:play_animation("attack1_return")
		self.attacking = false		
		self.attacking2 = false
	end
end


function AttackController:is_attacking()
	return false
end
