-- Portal2 component

class 'Portal2' (BaseComponent)

function Portal2:__init(name)
	BaseComponent.__init(self, name)
	self.position = float3(0.0, 0.0, 0.0)
	self.rotation = nil
	self.size = float3(1.0,22.5,10.0)
	self.quat = quat(0.0, 0.0, 1.0, 0.0)
	self.current_scene = ""
	self.next_scene = ""
end

function Portal2:start()
	if self.rotation ~= nil then
		self.quat:quat_from_yaw_pitch_roll(deg_2_rad(self.rotation.x), deg_2_rad(self.rotation.y), deg_2_rad(self.rotation.z))	
	end
	create_physx_trigger_box(self.gameObject.name, self.size.x, self.size.y, self.size.z, self.position, self.quat, "Default")
end
	


function Portal2:update(dt)
end

function Portal2:on_trigger_enter(name)
	local c = get_object_by_name(name)
	if c:has_tag("Player") then
		engine:change_scene(self.next_scene)
	end
end

function Portal2:activate()	
end