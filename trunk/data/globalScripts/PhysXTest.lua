-- PhysXTest component

class 'PhysXTest' (BaseComponent)

function PhysXTest:__init(name)
	BaseComponent.__init(self, name)
	self.corrutina = nil
end

function PhysXTest:start()
	--register_physx_material("MaterialTest", 0.1, 0.1, 0.5)
	--create_physx_static_plane("Plane", 0.0, 1.0, 0.0, 1.0,"Default")
	--create_physx_static_box("BoxTest", 1.0, 1.0, 1.0, float3(1.0,0.0,0.0), quat(0.0, 0.0, 1.0, 0.0), "Default")
	--create_physx_dynamic_box("BoxTest2", 1.0, 1.0, 1.0, float3(1.0,0.5,0.0), quat(0.0, 0.0, 1.0, 0.0), 1.0, "Default")
	--create_physx_dynamic_sphere("DynamicSphere", 1.0, float3(0.0,5.0,0.0), quat(0.0,0.0,1.0,0.0), 1.0, "MaterialTest")
	--create_physx_static_sphere("StaticSphere", 1.0, float3(0.0,5.0,0.0), quat(0.0,0.0,1.0,0.0), "MaterialTest")
	--create_physx_trigger_box(self.gameObject.name, 1.0, 1.0, 1.0, float3(1.0,0.0,0.0), quat(0.0,0.0,1.0,0.0), "Default")	
end
	


function PhysXTest:update(dt)
	--if self.corrutina ~= nil then
	--	coroutine.resume(self.corrutina)
	--end		
end

function PhysXTest:on_trigger_enter(name)	
	--add_new_info_log("onTriggerEnter:" ..name)
	transform = get_transform(name)
	transform.position.y = transform.position.y + 0.5
	table.insert(self.gameObject.gameObjectsOnTrigger, name)

	--[[self.corrutina = coroutine.create(function(name)
											while true do
											--add_new_info_log("onTriggerStay:" .. name)
											transform = get_transform(name)
											transform.position.x = transform.position.x + 0.5
											coroutine.yield()
											end
										end)
	coroutine.resume(self.corrutina, name)	--]]
end


function PhysXTest:on_trigger_exit(name)	
	--add_new_info_log("onTriggerExit: " ..name)
	transform = get_transform(name)
	transform.position.y = transform.position.y + 0.5
	
	i = index_of_value(self.gameObject.gameObjectsOnTrigger, name)
	table.remove(self.gameObject.gameObjectsOnTrigger, i)
	--self.corrutina = nil
end

function PhysXTest:on_trigger_stay(name)	
--	add_new_info_log("onTriggerStay: " ..name)
	transform = get_transform(name)
	transform.position.x = transform.position.x + 0.5
end

function PhysXTest:on_collision_enter(name)	
		add_new_info_log("on_collision_enter: " ..name)
end

function PhysXTest:on_collision_stay(name)	
		add_new_info_log("on_collision_stay: " ..name)
end

function PhysXTest:on_collision_exit(name)	
		add_new_info_log("on_collision_exit: " ..name)
end

