-- Health component

class 'Health' (BaseComponent)

function Health:__init(name)
	BaseComponent.__init(self, name)
	self.health = 0
	self.invincible = false
	self.is_dead = false
	
	self.gui_pos = gui_position(0.1, 0.02, 150, 35, "TOP_LEFT", "GUI_RELATIVE", "GUI_ABSOLUTE")
	self.animator = nil
	
	-- Parametros de xml
	self.max_health = 100
	self.defense = 5
	self.damage_barrier = 0
	self.event_to_register = ""
	self.destroy_gameobject = true
	-- Fin parametros xml
end

function Health:start()
	self.health = self.max_health
	if self.gameObject:has_tag("Player") then
		self.animator = self.gameObject:get_component("AnimatorController")
	end
end

function Health:update(dt)
	if self.gameObject:has_tag("Player") and get_active(self.gameObject.name) then
		gui_bar("life_bar", "life_bar", self.gui_pos, 0.0, self.max_health, self.health)
	end
end

function Health:damage(dmg)
	local received_damage = 0
	if not self.invincible then
		if self.damage_barrier <= dmg then
			dmg = dmg - self.defense
			if dmg < 0 then 
				dmg = 0 
			end
			received_damage = dmg
			self.health = self.health - dmg
			self:damage_feedback()
			if self.health <= 0 then
				self:on_die()
			end
		end
	end

	add_new_info_log("Damage taken: " .. received_damage .. " Name: " .. self.gameObject.name)
	return received_damage
end

function Health:damage_feedback()
	local particle_position = float3(self.transform.position.x, self.transform.position.y, self.transform.position.z)
	if self.gameObject:has_tag("Player") then
		particle_position.y = particle_position.y + 1.0
		engine:get_object_by_tag("slash_spawner"):get_component("SlashSpawner"):spawn("Hit", particle_position)
	else
		particle_position.y = particle_position.y + 1.0
		engine:get_object_by_tag("slash_spawner"):get_component("SlashSpawner"):spawn("Slash", particle_position)
	end
end

function Health:on_die()
	if self.gameObject:has_tag("Player") then
		-- Incapacitar
		self.animator:play_animation_clear("death")
	else
		if not self.is_dead then
			local ev = EventList[self.event_to_register]
			if ev ~= nil then
				EventList[self.event_to_register]:enemy_killed()
			end
			add_new_info_log("NameOnDie: " ..self.gameObject.name)

			if self.destroy_gameobject then
				table.insert(engine.gameObjectsToDelete, self.gameObject)
			end
			--self.gameObject:destroy()
			--delete_named_node(self.gameObject.name)
		end
	end

	self.is_dead = true
end