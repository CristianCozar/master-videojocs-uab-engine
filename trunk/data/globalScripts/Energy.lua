-- Energy component

class 'Energy' (BaseComponent)

function Energy:__init(name)
	BaseComponent.__init(self, name)
	self.energy = 100.0
	self.current_energy = self.energy
	self.recovery = 5.0
	self.reserved_energy = 0.0
	self.interval = 5.0
	self.timer = 0.0
	
	self.gui_pos = gui_position(0.1, 0.07, 150, 35, "TOP_LEFT", "GUI_RELATIVE", "GUI_ABSOLUTE")
end

function Energy:start()	
end

function Energy:update(dt)
	--add_new_info_log("Energy: " .. self.current_energy .. " Reserved energy: " .. self.reserved_energy)
	if (self.timer > 0.0) then
		self.timer = float_clamp(0.0, self.interval, self.timer - dt)
	elseif (self.timer == 0.0) then
		self:recover_energy(self.recovery * dt)
	end
	if self.gameObject:has_tag("Player") and get_active(self.gameObject.name) then
		gui_bar("energy_bar", "energy_bar", self.gui_pos, 0.0, self.energy, self.current_energy)
	end
end


function Energy:consume_energy(consumed_energy)
	if (self.current_energy - self.reserved_energy >= consumed_energy) and (consumed_energy >= 0.0) then
		self.current_energy = self.current_energy - consumed_energy
		self.timer = self.interval 
		return true
	end
	return false
end

function Energy:consume_all_energy(consumed_energy)
	self.current_energy = 0.0
end

function Energy:recover_energy(recovered_energy)
	if (recovered_energy > 0.0) then
		self.current_energy = float_clamp(0.0 , self.energy , self.current_energy + recovered_energy)
	end
end

function Energy:recover_all_energy()
	self.current_energy = self.energy
end

function Energy:reserve_energy(reserved_energy)
	local new_reserved_energy = self.reserved_energy + reserved_energy
	if (new_reserved_energy <= self.energy) and (reserved_energy > 0.0) and (self.current_energy >= reserved_energy) then
		self.reserved_energy = new_reserved_energy
		return true
	end
	return false
end

function Energy:consume_reserved_energy(consumed_reserved_energy)
	if (consumed_reserved_energy <= self.reserved_energy) then
		self.reserved_energy = self.reserved_energy - consumed_reserved_energy
		self.current_energy = self.current_energy - consumed_reserved_energy
	end
end

function Energy:release_reserved_energy(released_reserved_energy)
	local new_reserved_energy = self.reserved_energy - released_reserved_energy
	if (new_reserved_energy >= 0.0) and (released_reserved_energy >= 0.0) then
		 self.reserved_energy = new_reserved_energy
	end
end

function Energy:consume_all_reserved_energy()
	self.current_energy = self.current_energy - self.reserved_energy 
	self.reserved_energy = 0.0
end

function Energy:release_all_reserved_energy()
	self.reserved_energy = 0.0
end
