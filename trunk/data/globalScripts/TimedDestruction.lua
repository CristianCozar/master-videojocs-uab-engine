-- TimedDestruction component

class 'TimedDestruction' (BaseComponent)

function TimedDestruction:__init(name)
	BaseComponent.__init(self, name)
	self.time = 5.0
	self.timer = 0.0
end

function TimedDestruction:start()
	
end

function TimedDestruction:update(dt)
	self.timer = self.timer + dt
	if (self.timer >= self.time) then
		engine:request_destroy(self.gameObject)
	end
end