-- GolemAI component

class 'GolemAI' (BaseComponent)

function GolemAI:__init(name)
	BaseComponent.__init(self, name)
	self.last_animation = "idle"
	
	self.target = nil
	
	self.last_action_time = 0
	self.last_action_duration = 0
	
	self.last_time_attack = 0
	self.last_time_throw = 0
	self.doing_action = -1
	
	
	-- Parametros de XML
	self.max_y_distance = 2.5
	self.movement_speed = 5.0
	self.rotation_speed = 10.0
	self.rotation_threshold = 10.0
	self.stopping_distance = 1.5
	self.min_target_distance = 20.0
	self.pre_attack_delay = 2.0
	self.attack_delay = 5.0
	self.attack_distance = 3.0
	self.attack_height = 2.0
	self.attack_angle = 30.0
	self.attack_duration = 1.5
	self.attack_force = 10.0
	self.pre_throw_delay = 1.0
	self.throw_delay = 7.0
	self.throw_distance = 15.0
	self.throw_angle = 20.0
	self.throw_duration = 1.5
	self.rock_offset = 7.0
	self.rock_objective_offset = 3.0
	self.throw_prefab = "GolemProjectile"
	self.raycast_offset = 1.0
	self.up_offset = 1.5
	self.avoid_offset = 2.0
	-- Fin parametros XML
end

function GolemAI:start()
	
end

function GolemAI:update(dt)
	if get_action("DEBUGTEST") then
		is_cycle_animation_active(self.gameObject.name, 0)
	end
	if get_action("NUMBER") then
		val = get_action_amount("NUMBER")
		--add_new_info_log("Result is: " .. val)
		if (val == 1.0) then
			execute_action_by_name(self.gameObject.name, "hit", 0.0, 0.0, 1.0)
		elseif (val == 2.0) then
			self:play_loop("walk")
		elseif (val == 3.0) then
			self:play_loop("idle")
		end
	end
	self:play_loop("idle")
	self:apply_gravity(dt)
	if (self.target == nil or not get_active(self.target.name)) then
		self.target = self:get_target()
	else

		if self:check_action_finished() then
			local pos2D = float3(0.0,0.0,0.0)
			pos2D.x = self.transform.position.x
			pos2D.z = self.transform.position.z
			
			local target2D = float3(0.0,0.0,0.0)
			target2D.x = self.target.transform.position.x
			target2D.z = self.target.transform.position.z
			
			local forward2D = float3(0.0,0.0,0.0)
			forward2D.x = self.transform.forward.x
			forward2D.z = self.transform.forward.z
--			local pos2D = self.transform.position
			--pos2D.y = 0.0
	--		local target2D = self.target.transform.position
			--target2D.y = 0.0
--			local forward2D = self.transform.forward
			--forward2D.y = 0.0
			local distance = pos2D:distance(target2D)
			local angle = forward2D:angle(target2D - pos2D)
			angle = rad_2_deg(angle)
			if (self:check_attack(distance, angle)) then
				self:do_attack()
			elseif (self:check_throw(distance, angle)) then
				self:do_throw()
			elseif (self:check_stop(distance) == false) and self:check_y_distance() then
				self:do_move(dt)
			else
				self:do_rotate(angle, dt)
			end
		else
			if self.doing_action == 1 then -- Attack
				local passed_time = os.clock() - self.last_time_attack
				if passed_time >= self.pre_attack_delay then
					self:finish_attack()
				end
			elseif self.doing_action == 2 then -- Throw
			
				local pos2D = float3(0.0,0.0,0.0)
				pos2D.x = self.transform.position.x
				pos2D.z = self.transform.position.z
				
				local target2D = float3(0.0,0.0,0.0)
				target2D.x = self.target.transform.position.x
				target2D.z = self.target.transform.position.z
				
				local forward2D = float3(0.0,0.0,0.0)
				forward2D.x = self.transform.forward.x
				forward2D.z = self.transform.forward.z
				
					
				local distance = pos2D:distance(target2D)
				local angle = forward2D:angle(target2D - pos2D)
				angle = rad_2_deg(angle)
				
				self:action_rotate(dt)
				local passed_time = os.clock() - self.last_time_throw
				if passed_time >= self.pre_throw_delay then
					self:finish_throw()
				end
			end
		end
	end
end

function GolemAI:check_y_distance()
	if self.gameObject.name == "Golem4" then
		add_new_info_log("PosY: " ..self.transform.position.y)
		add_new_info_log("TarPosY: " ..self.target.transform.position.y)
	end
	if self.max_y_distance >= math.abs(self.transform.position.y - self.target.transform.position.y) then
		return true
	else
		return false		
	end	
end


function GolemAI:do_attack()
	self:play_animation("hit")
	self.last_action_time = os.clock()
	self.last_time_attack = self.last_action_time
	self.last_action_duration = self.attack_duration
	self.doing_action = 1
end

function GolemAI:action_rotate(dt)
		local dir = (self.target.transform.position - self.transform.position)
		dir.y = 0.0
		
	--	local current_rot = float3(0.0, 0.0, 0.0)
	--	current_rot.x = self.transform.yaw
	--	current_rot.y = self.transform.pitch
	--	current_rot.z = self.transform.roll
		
		--current_rot = vector3_move_towards(current_rot, dir, 10*dt)
		
		--self.transform.yaw = current_rot.x 
		self.transform.forward = dir
		--self.transform.pitch = current_rot.y
		--self.transform.roll = current_rot.z
	
	--[[ Quaternion lookRotation;
        Vector3 direction;        
        if (m_Target.target)
        {
            direction = (m_Target.target.transform.position + m_Target.target.transform.forward * l_TargetSpeed + Vector3.up * aimOffset) - transform.position;

            direction.y = transform.forward.y;
            lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        }--]]
end

function GolemAI:finish_attack()
	local players = engine:get_objects_by_tag("Player")
	for i = 1, #players do
		static_attack_check(self.transform, players[i].transform, self.attack_distance, self.attack_angle, self.attack_height, self.attack_force, 0, 0)
	end
	local salt_shield = engine:get_object_by_tag("Shield")
	if salt_shield ~= nil then
		static_attack_check(self.transform, salt_shield.transform, self.attack_distance, self.attack_angle, self.attack_height, self.attack_force, 0, 0)
	end
	
	
	self.doing_action = -1
end

function GolemAI:do_throw()
	self:play_animation("throw")
	self.last_action_time = os.clock()
	self.last_time_throw = self.last_action_time
	self.last_action_duration = self.throw_duration
	self.doing_action = 2
end

function GolemAI:finish_throw()
	local rock = self.gameObject:instance_return(self.throw_prefab, self.throw_prefab)
	set_position(rock.name, self.gameObject.transform.position + self.gameObject.transform.up * self.rock_offset)
	local aim = self.target.transform.position + self.target.transform.up * self.rock_objective_offset
	local ProjeFroward = aim - rock.transform.position
	ProjeFroward = ProjeFroward:normalized(1.0)
	rock.transform.forward = ProjeFroward
	rock:add_component("GolemProjectile")
	self.doing_action = -1
end

function GolemAI:do_move(dt)
	local done = false
	self:play_loop("walk")	
	local mov = self.target.transform.position - self.transform.position
	mov.y = 0.0
	mov = mov:normalized(1.0)
	mov = mov * self.movement_speed * dt	
	self.transform.forward = mov

	local positionStart = self.transform.position + self.transform.up * self.up_offset + self.transform.forward

	while not done do
		
		local raycastData = raycast_data()
		local raycastHit = raycast(positionStart, positionStart + self.transform.forward * self.raycast_offset,raycastData)
		if raycastHit then
		--	add_new_info_log("RaycastHit: " ..raycastData.actor)
			local go = engine:get_object_by_name(raycastData.actor)
			if not go:has_tag("Player") then
		--	add_new_info_log("IsNotPlayer")
		--	add_new_info_log("RaycastNormalX: " ..raycastData.normal.x)
		--	add_new_info_log("RaycastNormalY: " ..raycastData.normal.y)
		--	add_new_info_log("RaycastNormalZ: " ..raycastData.normal.z)
		--	add_new_info_log("_----------------------------_")
		--	add_new_info_log("MovBeforeX: " ..mov.x)
		--	add_new_info_log("MovBeforeY: " ..mov.y)
		--	add_new_info_log("MovBeforeZ: " ..mov.z)
		--	add_new_info_log("-------------------")
			if math.abs(raycastData.normal.x) > math.abs(raycastData.normal.z) then
				if raycastData.normal.x < 0 then
					mov.z = mov.z - self.avoid_offset
				else				
					mov.z = mov.z + self.avoid_offset
				end
			else
				if raycastData.normal.z < 0 then
					mov.x = mov.x + self.avoid_offset
				else
					mov.x = mov.x - self.avoid_offset
				end
			end
			
			done = true
			--	mov = mov + raycastData.normal * self.avoid_offset
			--	mov = mov - raycastData.position + raycastData.normal:normalized(1.0) * self.avoid_offset
		--		mov = (self.transform.position - raycastData.position + raycastData.normal)
				mov.y = 0.0
				mov = mov:normalized(1.0)
				mov = mov * self.movement_speed * dt	
				self.transform.forward = mov
				--move_node(self.gameObject.name, mov)

		--	add_new_info_log("MovAfterX: " ..mov.x)
		--	add_new_info_log("MovAfterY: " ..mov.y)
		--	add_new_info_log("MovAfterZ: " ..mov.z)
			else
		--		add_new_info_log("IsPlayer")
				done = true
			end
		else
			done = true
		end
	end

	local raycastData2 = raycast_data()
	local raycastHit2 = raycast(positionStart, positionStart + self.transform.forward * self.raycast_offset - self.transform.up * self.up_offset ,raycastData2)

	if not raycastHit2 then
		mov = float3(0.0, 0.0, 0.0)
	end

	move_node(self.gameObject.name, mov)
	-- TODO
end

function GolemAI:apply_gravity(dt)
	move_node(self.gameObject.name, float3(0.0, -5 * dt, 0.0))
end

function GolemAI:do_rotate(angle, dt)
	self:play_loop("idle")
	if (angle > self.rotation_threshold) then
		local rotation = float3(0.0, self.rotation_speed * dt, 0.0)
		self.transform.rotation = rotation		
	end
	-- TODO
end

function GolemAI:check_stop(distance)
	return distance < self.stopping_distance
end

function GolemAI:check_throw(distance, angle)
	local passed_time = os.clock() - self.last_time_throw
	return distance <= self.throw_distance and angle <= self.throw_angle and passed_time >= self.throw_delay
end

function GolemAI:check_attack(distance, angle)
	local passed_time = os.clock() - self.last_time_attack
	return distance <= self.attack_distance and angle <= self.attack_angle and passed_time >= self.attack_delay
end

function GolemAI:play_loop(anim)
	clear_cycle_by_name(self.gameObject.name, self.last_animation, 0.0)
	blend_cycle_by_name(self.gameObject.name, anim, 1.0, 0.0)
	self.last_animation = anim
end

function GolemAI:play_animation(anim)
	execute_action_by_name(self.gameObject.name, anim, 0.0, 0.0, 1.0)
end

function GolemAI:check_action_finished()
	local passed_time = os.clock() - self.last_action_time
	return passed_time > self.last_action_duration and is_any_action_animation_active(self.gameObject.name) == false
end

function GolemAI:get_target()
	local players = engine:get_objects_by_tag("Player")
	local min_distance = self.min_target_distance
	local player = nil
	for i = 1, #players do
		local ppos = players[i].transform.position
		local dis = ppos:distance(self.transform.position)
		if dis < min_distance then
			min_distance = dis
			player = players[i]
		end
	end
	return player
end