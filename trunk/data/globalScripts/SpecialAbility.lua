-- movementController component: Allows the Character to Move, Rotate, Jump and Dash

class 'SpecialAbility' (BaseComponent)

function SpecialAbility:__init(name)
	BaseComponent.__init(self, name)

	self.cooldown = 5.0
	self.current_cooldown = 0.0
	self.energy_cost = 20.0
end


function SpecialAbility:use()
	add_new_info_log("SpecialAbility's function 'use' is not implemented")
end


