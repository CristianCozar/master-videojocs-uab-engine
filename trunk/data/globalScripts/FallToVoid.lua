-- FallToVoid component

class 'FallToVoid' (BaseComponent)

function FallToVoid:__init(name)
	BaseComponent.__init(self, name)
	self.respawn_point = nil
	self.fall_damage = 10.0
end

function FallToVoid:start()
	--Create Trigger
	create_physx_trigger_box(self.gameObject.name, 300.0, 10.0, 300.0, float3(0.0,-5.0,0.0), quat(0.0,0.0,1.0,0.0), "Default")	
end
	


function FallToVoid:update(dt)
end

function FallToVoid:on_trigger_enter(name)	
	--check if player or Enemy
	a = engine:get_object_by_name(name)
	if a ~= nil then
		if a:has_tag("Player") then
			local mov = a:get_component("MovementController")
			if mov ~= nil then
				set_actor_position(name, mov.last_grounded_position)
				player_transform = get_transform(name)
				player_transform.position = mov.last_grounded_position
			end		
			local health = a:get_component("Health")
			if health ~= nil then
				health:damage(self.fall_damage)
			end
		elseif a:has_tag("Enemy") then
			local health = a:get_component("Health")
			if health ~= nil then
				health:damage(9999.0)
			end
		end
	end
end