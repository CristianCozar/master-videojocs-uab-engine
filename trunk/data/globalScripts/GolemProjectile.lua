-- GolemProjectile component: Controls Sulphur's projectile behaviour

class 'GolemProjectile' (BaseComponent)

function GolemProjectile:__init(name)
	BaseComponent.__init(self, name)
	self.speed = 20.0
	self.damage = 20.0
	self.time_to_live = 10.0
	self.current_timer = 0.0
end

function GolemProjectile:start()
end

function GolemProjectile:update(dt)
	self.current_timer = self.current_timer + dt
	if self.current_timer < self.time_to_live then
		move_node(self.gameObject.name, self.gameObject.transform.forward * self.speed * dt)
	else
		self.gameObject:destroy()
	end
end

function GolemProjectile:on_trigger_enter(otherName)
	local go = get_object_by_name(otherName)
	if go ~= nil then
		local health = go:get_component("Health")
		if health and go:has_tag("Player") then
			health:damage(self.damage)
		end
	end
	self.gameObject:destroy()
end

