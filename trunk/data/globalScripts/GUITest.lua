-- GUITest component

class 'GUITest' (BaseComponent)

function GUITest:__init(name)
	BaseComponent.__init(self, name)
	self.pos1 = gui_position(0.0, 0.0, 0.3, 0.1, "TOP_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.pos2 = gui_position(0.0, 1.0, 0.4, 0.1, "BOTTOM_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.pos3 = gui_position(1.0, 1.0, 0.4, 0.1, "BOTTOM_RIGHT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.pos4 = gui_position(0.5, 0.0, 0.2, 0.08, "TOP_CENTER", "GUI_RELATIVE", "GUI_RELATIVE")
	self.pos5 = gui_position(0.03, 0.90, 0.3, 0.3, "BOTTOM_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.pos6 = gui_position(0.7, 0.5, 0.3, 0.3, "CENTER_RIGHT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.sizeLimits = gui_sizeLimits(0.2, 0.2, 1.0, 1.0, "GUI_RELATIVE");
	self.button_value = false
	self.slider_result = nil
	self.value = 100.0
	self.bar_value = 100.0
	self.show_button = true
	self.new_thread = coroutine.wrap(self.coroutine_test)
	self.zelda_lives = 3
	self.zelda_max = 6
end

function GUITest:coroutine_test(dt)
	while (true) do
		if (self.show_button == true) then
			self.button_value = gui_button("button_test", "start_button", self.pos1)
		end
		self.slider_result = gui_slider("slider_test", "test_slider", self.pos2, 0.0, 100.0, self.value)
		self.value = self.slider_result.temp
		gui_bar("bar_test", "test_bar", self.pos3, 0.0, 100.0, self.bar_value)
		
		gui_zelda("zelda_test", "test_zelda", self.pos4, self.zelda_max, self.zelda_lives)	
		gui_console("console_t", "console", self.pos5, self.sizeLimits);
		gui_textBox("test_textbox", "test_textbox", self.pos6, self.sizeLimits, true)
		coroutine.yield()
	end
end

function GUITest:start()
end

function GUITest:update(dt)
	self:new_thread()
	if get_action("MOVEMENTX") then
		self.bar_value = self.bar_value + get_action_amount("MOVEMENTX") * dt * 20.0
	end
	if get_action("DEBUGTEST") then
		self.show_button = false;
		--instance("Chapel", "Chapel2", "TestApi", "Deferred")
	end
end

function debug_print(text)
	add_new_info_log(text)
end

function zelda(num)
	local gui = engine.gameObjects["Skybox"].components.GUITest
	gui.zelda_lives = num
end

function health(num)
	local gui = engine.gameObjects["Skybox"].components.GUITest
	gui.bar_value = num
end