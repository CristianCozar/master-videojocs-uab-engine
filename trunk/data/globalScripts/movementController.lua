-- movementController component: Allows the Character to Move, Rotate, Jump and Dash

class 'MovementController' (BaseComponent)

function MovementController:__init(name)
	BaseComponent.__init(self, name)
	self.speed = 7.0
	self.jump_speed = 20.0
	self.dash_speed = 20.0
	self.dash_current_speed = 10.0
	self.dash_duration = 0.5
	self.current_y_speed = 0.0
	self.gravity = 1.0
	self.can_jump = true
	
	self.movement_this_frame = false
	self.shape_hit_this_frame = false

	self.jumping = false
	self.falling = false
	self.is_flying = false
	self.grounded = true
	self.dashing = false
	self.is_moving = false
	self.is_idle = true
	
	
	self.has_target_fixed = false
	self.fixed_target = nil
	self.fixed_target_index = 0
	self.target_cooldown = 1.0
	self.max_target_distance = 35.0
	self.current_target_cooldown = 0.0

	self.grounded_angle = 135.0
	self.current_direction = float3(0.0, 0.0, 0.0)
	self.last_grounded_position = float3(0.0, 0.0, 0.0)
	
	self.active = true
	self.animator = nil
	self.attack_controller = nil
	self.has_shield = false
	
end

function MovementController:start()
	self.fixed_target = engine:get_object_by_tag("Enemy")
	self.animator = self.gameObject:get_component("AnimatorController")
	self.attack_controller = self.gameObject:get_component("AttackController")
	self.animator:play_loop_clear("idle")
end


function MovementController:SetActive(active)
	self.active = active
end

function MovementController:update(dt)
	if (self.active) then
		self:manage_xz_axis(dt) 
		self:manage_target(dt)
		self:manage_y_axis(dt)
		self:manage_rotation()
	end

	self.shape_hit_this_frame = false
end

--Modifica la direccion x,y en la que se dirige el personaje si apunta a un enemigo
function MovementController:manage_target(dt)
	self.current_target_cooldown = float_clamp(0, self.target_cooldown, self.current_target_cooldown - dt)
	if get_action("FIX_TARGET") then	
		if self.has_target_fixed then
			self.has_target_fixed = false
		else
			if self:select_target() then
				self.has_target_fixed = true
			end
		end
	end
	
--	if self.has_target_fixed then
	--	add_new_info_log("Taget: " ..self.fixed_target.name)
	--end
	
	if self.has_target_fixed and #self.fixed_target.tags == 0 then
		if not self:select_target() then
			self.has_target_fixed = false
		end
	end

	
	if get_action("CHANGE_TARGET") and (self.current_target_cooldown == 0.0) then		
		self.current_target_cooldown = self.target_cooldown
		local enemies = engine:get_objects_by_tag("Enemy")
		
		if #enemies > 0 then
			if get_action_amount("CHANGE_TARGET") > 0.0 then
				self:next_target()
				--self.fixed_target_index = math.fmod(self.fixed_target_index + 1, #enemies)
			else
				self:next_target()
				--self.fixed_target_index = math.fmod(self.fixed_target_index + 1, #enemies) --TODO: Iterar al contrario
			end
			
			
			--add_new_info_log("Indice: " .. self.fixed_target_index)
	
			self.fixed_target = enemies[self.fixed_target_index + 1]
		else
			self.fixed_target = nil
			self.has_target_fixed = false
		end
	end

	if self.has_target_fixed then
		local enemies = engine:get_objects_by_tag("Enemy")
		if #enemies > 0 then
			self.current_direction = self.fixed_target.transform.position - self.gameObject.transform.position
			self.current_direction = normalize(float3(self.current_direction.x, 0 , self.current_direction.z))
		else			
			self.fixed_target = nil
			self.has_target_fixed = false
		end
	end
end

function MovementController:select_target()
	local enemies = engine:get_objects_by_tag("Enemy")

	local pos2D = float3(0.0,0.0,0.0)
	pos2D.x = self.transform.position.x
	pos2D.z = self.transform.position.z
	local minDistance = 99999
	local minIndex = -1
	
	local target2D = float3(0.0,0.0,0.0)
	local distance = 999999

	for i = 1, #enemies do	
		target2D.x = enemies[i].transform.position.x
		target2D.z = enemies[i].transform.position.z
		distance = pos2D:distance(target2D)
		if distance <= minDistance and distance <= self.max_target_distance then
			minDistance = distance
			minIndex = i
		end
	end		
	
	if minIndex ~= -1 then
		self.fixed_target = enemies[minIndex]
		self.fixed_target_index = minIndex - 1
		return true		
	else
		return false
	end
end

function MovementController:next_target()
	local enemies = engine:get_objects_by_tag("Enemy")

	local pos2D = float3(0.0,0.0,0.0)
	pos2D.x = self.transform.position.x
	pos2D.z = self.transform.position.z
	local minDistance = 99999
	local minIndex = -1
	
	local target2D = float3(0.0,0.0,0.0)
	local distance = 999999

	for i = 1, #enemies do	
		target2D.x = enemies[i].transform.position.x
		target2D.z = enemies[i].transform.position.z
		distance = pos2D:distance(target2D)
		if distance <= minDistance and distance <= self.max_target_distance and i ~= self.fixed_target_index + 1 then
			minDistance = distance
			minIndex = i
		end
	end		
	
	if minIndex ~= -1 then
		self.fixed_target = enemies[minIndex]
		self.fixed_target_index = minIndex - 1
	end
end

--Rota al personaje en la direcci�n que le llega como parametro
function MovementController:manage_rotation()
	local go = self.gameObject

	if not is_zero(self.current_direction) then
		go.transform.forward = self.current_direction
	end
end

--Devuelve la direccion x,y en la que se dirige el personaje
function MovementController:manage_xz_axis(dt)
	local go = self.gameObject
	if is_cycle_animation_active_by_name(self.gameObject.name, "salt_ability_move") then
		add_new_info_log("SaltMovingWithSHIELD")
	end
	local character_forward = go.transform.forward
	local character_right = go.transform.right

	local world_forward = float3(0.0, 0.0, 1.0)
	local world_right = float3(1.0, 0.0, 0.0)

	local camera = get_prim3_camera()

	local hasMovementX = get_action("MOVEMENTX")
	local hasMovementY = get_action("MOVEMENTY")

	local movementX_amount = get_action_amount("MOVEMENTX")
	local movementY_amount = get_action_amount("MOVEMENTY")
	
	if (hasMovementX or hasMovementY) and not self.is_moving and self.grounded and not self.animator:any_animation_active() then
		self.is_moving = true
		self.is_idle = false

		if not self.has_shield then
			self.animator:play_loop_clear("run")
		else
			self.animator:play_loop_clear("salt_ability_move")
		end
	end
	
	if not hasMovementX and not hasMovementY and not self.is_idle and self.grounded and not self.animator:any_animation_active() then	
		self.is_moving = false
		self.is_idle = true
		if not self.has_shield then
			self.animator:play_loop_clear("idle")
		else
			self.animator:play_loop_clear("ability_pose")
		end
	end
	
	local camera_forward = normalize(scale(camera.m_Front, float3(1.0, 0.0, 1.0)))
	local camera_right = cross_product(camera.m_Front, camera.m_Up)


	self.current_direction = normalize((camera_forward * movementY_amount) + (camera_right * movementX_amount))
	local mov = self.current_direction * self.speed * dt

	
	if (get_action("DASH")) and (not self.dashing) and (self.grounded) and not self.attack_controller.attacking then
		self.dashing = true
		self.dash_current_speed = self.dash_speed
		self.animator:queue_action("dash")
	end

	if self.dashing then
		self.dash_current_speed = self.dash_current_speed - (self.dash_speed / self.dash_duration * dt)
		if (self.dash_current_speed < 0.0) then
			self.dash_current_speed = 0.0
			self.dashing = false
		else 
			mov = self.transform.forward * self.dash_current_speed * dt
		end
	end

	move_node(go.name, mov)
end

function MovementController:manage_y_axis(dt) 
	--Si no ha habido colision contra el suelo este frame no esta Grounded
	if not self.shape_hit_this_frame then
		self.grounded = false
	end

	local go = self.gameObject

	--Si el personaje salta se contraresta la velocidad de caida
	if self.gameObject.name == "Salt" then 
		local SAS = self.gameObject:get_component("SpecialAbilitySalt")
		if SAS ~= nil then	
			if SAS.shield ~= nil then
				self.can_jump = false
			else
				self.can_jump = true
			end
		end
	end
	
	if get_action("JUMP") and (not self.jumping) and (self.grounded) and self.can_jump and not self.attack_controller.attacking then
			self.grounded = false
			self.jumping = true
			self.current_y_speed = self.current_y_speed + self.jump_speed
			--self.animator:play_animation_clear("despegue")
			self.animator:queue_action("despegue")
			self.animator:queue_action("fly")

	end
	
	if self.jumping and not self.falling and not self.animator:any_animation_active() and not self.is_flying
	then
		--self.animator:queue_action("fly")
		--self.animator:play_animation_clear("fly")
		self.is_flying = true
	end

	--Siempre se aplica la gravedad
	self.current_y_speed = self.current_y_speed - self.gravity * dt
	

	if (self.current_y_speed < 0) then
		if not self.grounded and not self.falling and self.jumping then		
			self.animator:queue_action("caida")
			self.animator:queue_action("aterrizaje")
			--self.animator:play_animation_clear("caida")
		end
		self.falling = true		
	else
		self.falling = false
	end
	


	local up = go.transform.up
	local mov = up * self.current_y_speed * dt
	move_node(go.name, mov)
end

function MovementController:on_shape_hit(otherName, hitWorldPosition, hitWorldNormal)
	self.shape_hit_this_frame = true
	local character_ground_angle = dot_product(float3(0, -1, 0), hitWorldNormal)
	-- Si esta tocando el suelo se resetea la velocidad de caida
	if (character_ground_angle > self.grounded_angle) then
		
		self.last_grounded_position.x = self.transform.position.x
		self.last_grounded_position.y = self.transform.position.y
		self.last_grounded_position.z = self.transform.position.z

		self.grounded = true
		if self.jumping then
			--self.animator:play_animation_clear("aterrizaje")
			--self.animator:play_animation_clear("aterrizaje")
			self.is_moving = false
			self.animator:queue_clear()
			self.is_idle = false
			self.is_flying = false
		end
		self.jumping = false
		self.current_y_speed = -self.gravity
	end
end
