-- SpecialAbilitySulphur component: Controls Sulphur's ability'

class 'SpecialAbilitySulphur' (SpecialAbility)

function SpecialAbilitySulphur:__init(name)
	SpecialAbility.__init(self, name)
	self.offset = float3(0.0, 1.2, 2.5)
	self.gui_pos = gui_position(0.02, 0.02, 0.06, 0.11, "TOP_LEFT", "GUI_RELATIVE", "GUI_RELATIVE")
	self.animator = nil
	self.shot_fired = false
	self.fire_timer = 1.0
	self.current_timer = 0.0
end

function SpecialAbilitySulphur:start()
	self.animator = self.gameObject:get_component("AnimatorController")
end

function SpecialAbilitySulphur:update(dt)
	self.current_cooldown = float_clamp(0, self.cooldown, self.current_cooldown - dt)
	if get_active(self.gameObject.name) then
		if get_action("SPECIAL_ABILITY_PRESSED") and (self.current_cooldown == 0) and not self.shot_fired and not self.animator:any_animation_active() then
			self:use()
		end
		if self.shot_fired then
			self.current_timer = self.current_timer + dt
			if self.current_timer >= self.fire_timer then
				self:fire()
				self.current_timer = 0.0
				self.shot_fired = false
			end
		end
		gui_image("player_portrait", "sulphur_portrait", self.gui_pos)
		
	end
end


function SpecialAbilitySulphur:use()
	if (self.gameObject:get_component("Energy"):consume_energy(self.energy_cost)) then
		self.animator:queue_action("ability_begin")
		self.animator:queue_action("ability_end")
		self.shot_fired = true
	end
end

function SpecialAbilitySulphur:fire()
		self.current_cooldown = self.cooldown
		local go = self.gameObject
		add_new_info_log("RigtX: " ..go.transform.right.x)
				add_new_info_log("RigtY: " ..go.transform.right.y)
						add_new_info_log("RigtZ: " ..go.transform.right.z)
		local projectile =  get_object_by_name(go:instance("SpecialAbilitySulphur", "SpecialAbilitySulphur"))
		set_position(projectile.name, go.transform.position + ((go.transform.forward) *  self.offset.z) + ((go.transform.up) *  self.offset.y) + ((go.transform.right) *  self.offset.x))
		projectile.transform.forward = go.transform.forward
		projectile:add_component("SpecialAbilitySulphurProjectile") -- Estaria bien poder a�adir componentes en el instantiables.xml
end


