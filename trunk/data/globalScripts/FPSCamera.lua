g_YawSpeed = 2.0
g_PitchSpeed = 1.0
g_ZoomSpeed = 1.0
g_FrontSpeed = 500.0
g_SideSpeed = 500.0
g_VerticalSpeed = 100.0

function start()
	add_new_info_log("Script de camara FPS cargado correctamente!")
end

function ondestroy()

end

function CameraUpdate(ElapsedTime)
		c = get_camera()
		if get_action("ROTATE") then
			if get_action("YAW") then
				c.m_YawSpeed = get_action_amount("YAW")
			end
		end
		
		if get_action("ROTATE") then
			if get_action("PITCH") then
				c.m_PitchSpeed = get_action_amount("PITCH")
			end
		end
		
		if get_action("FRONT") then
			c.m_FrontSpeed = get_action_amount("FRONT")
		end
		
		if get_action("SIDE") then
			c.m_SideSpeed = get_action_amount("SIDE")
		end
		
		if get_action("VERTICAL") then
			c.m_VerticalSpeed = get_action_amount("VERTICAL")
		end

		c.m_Yaw = c.m_Yaw + c.m_YawSpeed * ElapsedTime
		c.m_Pitch = c.m_Pitch + c.m_PitchSpeed * ElapsedTime
		c.m_Zoom = c.m_Zoom - c.m_ZoomSpeed * ElapsedTime		
	
		if c.m_Pitch > c.m_MaxPitch then
			c.m_Pitch = c.m_MaxPitch end
		if c.m_Pitch < c.m_MinPitch then
			c.m_Pitch = c.m_MinPitch end

		if c.m_Zoom > c.m_MaxZoom then
			c.m_Zoom = c.m_MaxZoom end
		if c.m_Zoom < c.m_NoZoom then
			c.m_Zoom = c.m_NoZoom end

		if c.m_Yaw > math.pi then
			c.m_Yaw = c.m_Yaw - math.pi* 2.0 end
		if c.m_Yaw < -math.pi then
			c.m_Yaw = c.m_Yaw +math.pi* 2.0 end

		deltaX = (math.cos(c.m_Yaw) * math.cos(c.m_Pitch) * c.m_FrontSpeed * g_FrontSpeed) + (math.sin(c.m_Yaw) * c.m_SideSpeed * g_SideSpeed)
		deltaY = math.sin(-c.m_Pitch) * c.m_FrontSpeed * g_FrontSpeed
		deltaZ = (math.sin(c.m_Yaw) * math.cos(c.m_Pitch) * c.m_FrontSpeed * g_FrontSpeed) + (-math.cos(c.m_Yaw) * c.m_SideSpeed * g_SideSpeed)
		
		deltaX = deltaX + (math.cos(c.m_Yaw) * math.sin(c.m_Pitch) * c.m_VerticalSpeed * g_VerticalSpeed)
		deltaZ = deltaZ + (math.sin(c.m_Yaw) * math.sin(c.m_Pitch) * c.m_VerticalSpeed * g_VerticalSpeed)
		deltaY = deltaY + (math.cos(c.m_Pitch) * c.m_VerticalSpeed * g_VerticalSpeed)
		
		deltaX = deltaX * ElapsedTime
		deltaY = deltaY * ElapsedTime
		deltaZ = deltaZ * ElapsedTime
	
		front = c.m_Front
		
		front.z = math.sin(c.m_Yaw) * math.cos(c.m_Pitch)
		front.y = -math.sin(c.m_Pitch)
		front.x = math.cos(c.m_Yaw) * math.cos(c.m_Pitch)
		
		c.m_Front = front
		
		pos = c.m_Position
			
		pos.x = pos.x + deltaX
		pos.y = pos.y + deltaY
		pos.z = pos.z + deltaZ
		
		c.m_Position = pos
		
		
		c.m_YawSpeed = 0.0
		c.m_PitchSpeed = 0.0
		c.m_ZoomSpeed = 0.0
		c.m_FrontSpeed = 0.0	
		c.m_SideSpeed = 0.0
		c.m_VerticalSpeed = 0.0
		
		return true
end