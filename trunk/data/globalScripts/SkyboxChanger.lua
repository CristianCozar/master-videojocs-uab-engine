-- SkyboxChanger component

class 'SkyboxChanger' (BaseComponent)

function SkyboxChanger:__init(name)
	BaseComponent.__init(self, name)
	
	
	self.color = float4(1,1,1,1)
end

function SkyboxChanger:start()
	change_material_vec4("SkyBox_defaultMaterial", "diffuseColor", self.color)
end

function SkyboxChanger:update(dt)

end