-- Player component

class 'Player' (BaseComponent)

function Player:__init(name)
	BaseComponent.__init(self, name)
	self.player = ""
	self.input = 0
	self.character_controller_script = nil
	self.next_frame_next = false
	self.prev_frame_next = false
	self.movement_script = nil

end

function Player:start()
	add_new_info_log("StartPlayer: " ..self.player)
	self.movement_script = self.gameObject:get_component("MovementController")
	self.character_controller_script = engine:get_object_by_name("CharacterController"):get_component("CharacterController")

	if not get_active(self.gameObject.name) then
		add_new_info_log("NOTACTIVE: " ..self.gameObject.name)
		self.movement_script:SetActive(false)
	else
		self.character_controller_script:initialize_character(self.player)	
	end
	
end

function Player:update()
	if self.next_frame_next then
		self.character_controller_script:next_character(self.player, self.input)
		self.next_frame_next = false
	elseif self.prev_frame_next then
		self.character_controller_script:previous_character(self.player, self.input)
		self.prev_frame_next = false
	end	
	self:change_character()
	
end


function Player:change_character()
	if get_active(self.gameObject.name) then
		local mc = self.gameObject:get_component("MovementController")
		if mc ~= nil then
			if mc.grounded then		
				if get_action("NEXT_CHARACTER") then
						self.next_frame_next = true
						--self.character_controller_script:next_character(self.player, self.input)
				elseif get_action("PREVIOUS_CHARACTER") then
					self.prev_frame_next = true
						--	self.character_controller_script:next_character(self.player, self.input)					
				end
				
				
				--[[if math.abs(get_action_amount("NEXT_CHARACTER")) > 0.0 then
					self.character_controller_script:next_character(self.player, self.input)
				end

				if math.abs(get_action_amount("PREVIOUS_CHARACTER")) > 0.0 then
					self.character_controller_script:previous_character(self.player, self.input)
				end--]]
			end
		end
	end
end
