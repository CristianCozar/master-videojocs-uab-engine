-- CameraEvent component

class 'CameraEvent' (BaseComponent)

function CameraEvent:__init(name)
	BaseComponent.__init(self, name)
	self.camera_event_done = false
	self.teleportPlayers = false
	self.alreadyTeleported = false
	self.teleportLocation = float3(-2.0, 1.5, 13.0)
	self.players = nil
	self.trigger = false
	self.Prim3Camera = nil
	self.Prim3FollowStoppedPlayers = false
	self.Prim3CustomFocusPoint = nil
	self.Prim3Zoom = 10.0
	self.Prim3Pitch = 20.0
	self.Prim3Yaw = 120.0
	self.Prim3ObjectiveYaw = 120.0
	self.Prim3Roll = 0.0
	self.Prim3FocusOffset = float3(0.0, 0.0, 0.0)
	self.Prim3smoothing = true
	self.Prim3Speed = 180.0
	self.Prim3ZoomSpeed = 80.0
	self.Prim3PitchSpeed = 80.0
	self.Prim3YawSpeed = 80.0
	self.Prim3RollSpeed = 80.0
	self.Prim3Accelerated = true
	self.Prim3TimeToDeccel = 0.08
	self.Prim3TimePassed = 0
	self.Prim3Acceleration = 5.0
	self.Prim3MinSpeed = 5.0
	self.Prim3DistanceToDeccel = 3.0
	self.Prim3ZoomAcceleration = 20.0
	self.Prim3MinZoomSpeed = 1.0
	self.Prim3PitchAcceleration = 20.0
	self.Prim3MinPitchSpeed = 1.0
	self.Prim3YawAcceleration = 20.0
	self.Prim3MinYawSpeed = 1.0
	self.Prim3MinDistance = 0.0
	self.Prim3MaxDistance = 40.0
	self.Prim3MinZoom = 10.0
	self.Prim3MaxZoom = 30.0
	self.Prim3MinPitch = 10.0
	self.Prim3MaxPitch = 20.0
	self.Prim3RotationYaw = true
	self.Prim3BeginYaw = 45.0
	self.Prim3EndYaw = 45.0
	self.Prim3BeginYawPos = float3(0.0, 0.0, 0.0)
	self.Prim3EndYawPos = float3(0.0, 0.0, 0.0)
	self.Prim3RotationPitch = true
	self.Prim3BeginMinPitch = 30.0
	self.Prim3EndMinPitch = 20.0
	self.Prim3BeginMaxPitch = 20.0
	self.Prim3EndMaxPitch = 100.0
	self.Prim3BeginPitchPos = float3(0.0, 0.0, 0.0)
	self.Prim3EndPitchPos = float3(0.0, 0.0, 0.0)
	self.Prim3MovementConstraint = false
	self.Prim3RotateInstead = false
	self.Prim3ZoomConstraint = false
	self.Prim3YawConstraint = false
	self.Prim3PitchConstraint = false
	self.Prim3RollConstraint = false
	self.Prim3OnePlayerLerp = 0.5
	self.Prim3Players = nil;
	self.Prim3Follow = float3(0.0, 0.0, 0.0)
	self.Prim3LookAtPosition = float3(0.0, 0.0, 0.0)
	self.Prim3LastPosition = nil
	self.Prim3LastObjectiveYaw = self.Prim3Yaw
	self.Prim3CurrentSpeed = self.Prim3Speed
	self.Prim3CurrentZoomSpeed = self.Prim3ZoomSpeed
	self.Prim3CurrentPitchSpeed = self.Prim3PitchSpeed
	self.Prim3CurrentYawSpeed = self.Prim3YawSpeed
	self.Prim3TransformToCopy = nil
	self.copySpeeds = false	
	self.copyYaw = false
	self.copyValues  = false
	self.copyDistance  = false
	self.copyZoom  = false
	self.copyPitch  = false
	self.copyYawRotation  = false
	self.copyBeginEndPitchRotation  = false
	self.copyYawConstraint  = false
	self.copyPitchConstraint  = false
	self.copyRollConstraint  = false
	self.copyMovementConstraint  = false
	self.copyZoomConstraint  = false
	self.copyOnePlayerLerp  = false
	self.copyAcceleration  = false
	self.cinematicForPlayers  = false
	self.copyTransform  = false
	self.copyFocus  = false
    self.copyOffset  = false
	self.event_to_register = ""

	
	--Restore camera Parameters
	self.RPrim3PitchSpeed = nil
	self.RPrim3RollSpeed = nil
	self.RPrim3Speed = nil
	self.RPrim3YawSpeed = nil
	self.RPrim3ZoomSpeed = nil
		
	self.RPrim3ObjectiveYaw  = nil
	
	self.RPrim3Yaw = nil
	self.RPrim3Pitch = nil
	self.RPrim3Zoom = nil
	self.RPrim3Roll = nil
	
	
	self.RPrim3MaxDistance = nil
	self.RPrim3MinDistance = nil
	self.RPrim3MinZoom = nil
	self.RPrim3MaxZoom = nil
	self.RPrim3MaxPitch = nil
	self.RPrim3MinPitch = nil

	self.RPrim3BeginYaw = nil
	self.RPrim3EndYaw = nil
	self.RPrim3BeginYawPos = nil
	self.RPrim3EndYawPos = nil
	self.RPrim3RotationYaw = nil
	
	
	self.RPrim3RotationPitch = nil
	self.RPrim3BeginMinPitch = nil
	self.RPrim3EndMinPitch = nil
	self.RPrim3BeginMaxPitch = nil
	self.RPrim3EndMaxPitch = nil
	self.RPrim3BeginPitchPos = nil
	self.RPrim3EndPitchPos = nil
	
	self.RPrim3YawConstraint = nil
	self.RPrim3RotateInstead = nil
	
	self.RPrim3PitchConstraint = nil
	
	self.RPrim3RollConstraint = nil
	self.RPrim3MovementConstraint = nil
	
	self.RPrim3ZoomConstraint = nil

	self.RPrim3OnePlayerLerp = nil

	self.RPrim3Accelerated = nil
	self.RPrim3Acceleration = nil
	self.RPrim3MinSpeed = nil
	self.RPrim3DistanceToDeccel = nil
	self.RPrim3ZoomAcceleration = nil
	self.RPrim3ZoomSpeed = nil
	self.RPrim3PitchAcceleration = nil
	self.RPrim3MinPitchSpeed = nil
	self.RPrim3YawAcceleration = nil
	self.RPrim3MinYawSpeed = nil
	self.RPrim3TimeToDeccel = nil
	
	self.RPrim3FollowStoppedPlayers = nil
	
	self.RPrim3TransformToSave = nil
	
	self.RPrim3CustomFocusPoint = nil
	
	self.RPrim3FocusOffset = nil	

	self.quat = quat(0.0, 0.0, 1.0, 0.0)
	self.rotation = nil
	self.size = float3(1.0, 1.0, 1.0)
end

function CameraEvent:start()
	self.players = engine:get_objects_by_tag("Player")	
	if self.rotation ~= nil then
		self.quat:quat_from_yaw_pitch_roll(deg_2_rad(self.rotation.x), deg_2_rad(self.rotation.y), deg_2_rad(self.rotation.z))	
	end
	create_physx_trigger_box(self.gameObject.name, self.size.x, self.size.y, self.size.z, self.position, self.quat, "Default")	
end
	
function CameraEvent:update(dt)
end

function CameraEvent:on_trigger_enter(name)	
	self.players = engine:get_objects_by_tag("Player")	
	c = get_object_by_name(name)
	if c:has_tag("Player") then
		self:check_stopped_players(c)
		self:add_player_to_list(c)
		self:teleport()
		if self:check_condition() then
			self.camera_event_done = true
			self:do_camera_event()
			local ev = EventList[self.event_to_register]
			if ev ~= nil then
				EventList[self.event_to_register]:external_trigger()
			end			
			if self.trigger then
				delete_actor(self.gameObject.name)
				self.gameObject:destroy()		
			end
		end
	end
end

function CameraEvent:check_condition()
	if not self.trigger then
		if not self.camera_event_done then
			return true
		else
			return false
		end
	else
		return true
	end
end

function CameraEvent:do_camera_event()
	if self.trigger then
		self:copy_camera()
	else
		self:save_camera()--while
		self:copy_camera()
	end
	self:check_cinematic()
	self:do_events()
	self:check_destroy()
end

function CameraEvent:save_camera()

	if self.copySpeeds then
		self.RPrim3PitchSpeed = g_Prim3PitchSpeed
		self.RPrim3RollSpeed = g_Prim3RollSpeed
		self.RPrim3Speed = g_Prim3Speed
		self.RPrim3YawSpeed = g_Prim3YawSpeed
		self.RPrim3ZoomSpeed = g_Prim3ZoomSpeed
	end
		
	if self.copyYaw then
		self.RPrim3ObjectiveYaw = g_Prim3ObjectiveYaw 
	end
	
	if self.copyValues then
		self.RPrim3Yaw = g_Prim3Yaw
		self.RPrim3Pitch = g_Prim3Pitch
		self.RPrim3Zoom = g_Prim3Zoom
		self.RPrim3Roll = g_Prim3Roll
	end
	
	if self.copyDistance then
		self.RPrim3MaxDistance = g_Prim3MaxDistance
		self.RPrim3MinDistance = g_Prim3MinDistance
	end
	
	if self.copyZoom then
		self.RPrim3MinZoom = g_Prim3MinZoom
		self.RPrim3MaxZoom = g_Prim3MaxZoom
	end
	
	if self.copyPitch then
		self.RPrim3MaxPitch = g_Prim3MaxPitch
		self.RPrim3MinPitch = g_Prim3MinPitch
	end
	
	if self.copyYawRotation then
		self.RPrim3BeginYaw = g_Prim3BeginYaw
		self.RPrim3EndYaw = g_Prim3EndYaw
		self.RPrim3BeginYawPos = g_Prim3BeginYawPos
		self.RPrim3EndYawPos = g_Prim3EndYawPos
		self.RPrim3RotationYaw = g_Prim3RotationYaw
	end
	
	if self.copyBeginEndPitchRotation then
		self.RPrim3RotationPitch = g_Prim3RotationPitch
		self.RPrim3BeginMinPitch = g_Prim3BeginMinPitch
		self.RPrim3EndMinPitch = g_Prim3EndMinPitch
		self.RPrim3BeginMaxPitch = g_Prim3BeginMaxPitch
		self.RPrim3EndMaxPitch = g_Prim3EndMaxPitch
		self.RPrim3BeginPitchPos = g_Prim3BeginPitchPos
		self.RPrim3EndPitchPos = g_Prim3EndPitchPos
	end
	
	if self.copyYawConstraint then
	 self.RPrim3YawConstraint = g_Prim3YawConstraint
	 self.RPrim3RotateInstead = g_Prim3RotateInstead
	end
	
	if self.copyPitchConstraint then
		self.RPrim3PitchConstraint = g_Prim3PitchConstraint
	end
	
	if self.copyRollConstraint then
		self.RPrim3RollConstraint = g_Prim3RollConstraint
	end
	
	if self.copyMovementConstraint then
		self.RPrim3MovementConstraint = g_Prim3MovementConstraint
	end
	
	if self.copyZoomConstraint then
		self.RPrim3ZoomConstraint = g_Prim3ZoomConstraint
	end

	if self.copyOnePlayerLerp then
		self.RPrim3OnePlayerLerp = g_Prim3OnePlayerLerp
	end

	if self.copyAcceleration then
		self.RPrim3Accelerated = g_Prim3Accelerated
		self.RPrim3Acceleration = g_Prim3Acceleration
		self.RPrim3MinSpeed = g_Prim3MinSpeed
		self.RPrim3DistanceToDeccel = g_Prim3DistanceToDeccel
		self.RPrim3ZoomAcceleration = g_Prim3ZoomAcceleration
		self.RPrim3ZoomSpeed = g_Prim3ZoomSpeed
		self.RPrim3PitchAcceleration = g_Prim3PitchAcceleration
		self.RPrim3MinPitchSpeed = g_Prim3MinPitchSpeed
		self.RPrim3YawAcceleration = g_Prim3YawAcceleration
		self.RPrim3MinYawSpeed = g_Prim3MinYawSpeed
		self.RPrim3TimeToDeccel = g_Prim3TimeToDeccel
	end
	
	self.RPrim3FollowStoppedPlayers = g_Prim3FollowStoppedPlayers

	if self.copyTransform then
		self.RPrim3TransformToSave = g_Prim3Camera
	end
	
	if self.copyFocus then
		self.RPrim3CustomFocusPoint = g_Prim3CustomFocusPoint
	end
		
	if self.copyOffset then
		self.RPrim3FocusOffset = g_Prim3FocusOffset
	end
end

function CameraEvent:copy_camera()

	if self.copySpeeds then
		g_Prim3PitchSpeed = self.Prim3PitchSpeed
		g_Prim3RollSpeed = self.Prim3RollSpeed
		g_Prim3Speed = self.Prim3Speed
		g_Prim3YawSpeed = self.Prim3YawSpeed
		g_Prim3ZoomSpeed = self.Prim3ZoomSpeed		
	end
		
	if self.copyYaw then
		g_Prim3ObjectiveYaw = self.Prim3ObjectiveYaw 
	end
	
	if self.copyValues then
		g_Prim3Yaw = self.Prim3Yaw
		g_Prim3Pitch = self.Prim3Pitch
		g_Prim3Zoom = self.Prim3Zoom
		g_Prim3Roll = self.Prim3Roll
	end
	
	if self.copyDistance then
		g_Prim3MaxDistance = self.Prim3MaxDistance
		g_Prim3MinDistance = self.Prim3MinDistance
	end
	
	if self.copyZoom then
		g_Prim3MinZoom = self.Prim3MinZoom
		g_Prim3MaxZoom = self.Prim3MaxZoom
	end
	
	if self.copyPitch then
		g_Prim3MaxPitch = self.Prim3MaxPitch
		g_Prim3MinPitch = self.Prim3MinPitch
	end
	
	if self.copyYawRotation then
		g_Prim3BeginYaw = self.Prim3BeginYaw
		g_Prim3EndYaw = self.Prim3EndYaw
		g_Prim3BeginYawPos = self.Prim3BeginYawPos
		g_Prim3EndYawPos = self.Prim3EndYawPos
		g_Prim3RotationYaw = self.Prim3RotationYaw
	end
	
	if self.copyBeginEndPitchRotation then
		g_Prim3RotationPitch = self.Prim3RotationPitch
		g_Prim3BeginMinPitch = self.Prim3BeginMinPitch
		g_Prim3EndMinPitch = self.Prim3EndMinPitch
		g_Prim3BeginMaxPitch = self.Prim3BeginMaxPitch
		g_Prim3EndMaxPitch = self.Prim3EndMaxPitch
		g_Prim3BeginPitchPos = self.Prim3BeginPitchPos
		g_Prim3EndPitchPos = self.Prim3EndPitchPos
	end
	
	if self.copyYawConstraint then
	 g_Prim3YawConstraint = self.Prim3YawConstraint
	 g_Prim3RotateInstead = self.Prim3RotateInstead
	end
	
	if self.copyPitchConstraint then
		g_Prim3PitchConstraint = self.Prim3PitchConstraint
	end
	
	if self.copyRollConstraint then
		g_Prim3RollConstraint = self.Prim3RollConstraint
	end
	
	if self.copyMovementConstraint then
		g_Prim3MovementConstraint = self.Prim3MovementConstraint
	end
	
	if self.copyZoomConstraint then
		g_Prim3ZoomConstraint = self.Prim3ZoomConstraint
	end

	if self.copyOnePlayerLerp then
		g_Prim3OnePlayerLerp = self.Prim3OnePlayerLerp
	end

	if self.copyAcceleration then
		g_Prim3Accelerated = self.Prim3Accelerated
		g_Prim3Acceleration = self.Prim3Acceleration
		g_Prim3MinSpeed = self.Prim3MinSpeed
		g_Prim3DistanceToDeccel = self.Prim3DistanceToDeccel
		g_Prim3ZoomAcceleration = self.Prim3ZoomAcceleration
		g_Prim3ZoomSpeed = self.Prim3ZoomSpeed
		g_Prim3PitchAcceleration = self.Prim3PitchAcceleration
		g_Prim3MinPitchSpeed = self.Prim3MinPitchSpeed
		g_Prim3YawAcceleration = self.Prim3YawAcceleration
		g_Prim3MinYawSpeed = self.Prim3MinYawSpeed
		g_Prim3TimeToDeccel = self.Prim3TimeToDeccel
	end
	
	g_Prim3FollowStoppedPlayers = self.Prim3FollowStoppedPlayers
	
	if self.cinematicForPlayers then
		g_Prim3FollowStoppedPlayers = true
	end
	
	if self.copyTransform then
		g_Prim3Camera.position = self.Prim3TransformToCopy.position
		g_Prim3Camera.m_Yaw = self.Prim3TransformToCopy.m_Yaw
		g_Prim3Camera.m_Pitch = self.Prim3TransformToCopy.m_Pitch
		
		x = math.cos(math.rad(g_Prim3Yaw)) * math.cos(math.rad(g_Prim3Pitch))
		y = -math.sin(math.rad(g_Prim3Pitch))
		z =  math.sin(math.rad(g_Prim3Yaw)) * math.cos(math.rad(g_Prim3Pitch))
		
		g_Prim3Camera.m_Front = float3(x,y,z)
	end
	
	if self.copyFocus then
		g_Prim3CustomFocusPoint = self.Prim3CustomFocusPoint
	else
		g_Prim3CustomFocusPoint = nil
	end
	
	if self.copyOffset then
		g_Prim3FocusOffset = self.Prim3FocusOffset
	end
end

function CameraEvent:check_cinematic()
end

function CameraEvent:do_events()
end

function CameraEvent:check_destroy()
end



function CameraEvent:teleport()
	if self.teleportPlayers then
		offset = 0.0
		plusoffset = 2.0
		for i = 1, #self.players do
			player_transform = get_transform(self.players[i].name)
			player_transform.position = self.teleportLocation + player_transform.forward * offset + player_transform.right * offset
			set_actor_position(self.players[i].name, player_transform.position)
			offset = offset + plusoffset
		end
		self.alreadyTeleported = true
	end
end

function CameraEvent:add_player_to_list(c)	
end

function CameraEvent:remove_player_from_list(c)	
end

function CameraEvent:check_stopped_players(c)
	 
end

function CameraEvent:any_player_in_area()
	return false
end

function CameraEvent:on_trigger_exit(name)	
	self.camera_event_done = false
	self.players = engine:get_objects_by_tag("Player")	
	c = get_object_by_name(name)
	if c:has_tag("Player") then
		self:remove_player_from_list(c)
		if not self.trigger and not self:any_player_in_area() then
			self:restore_camera()
		end		
	end	
	
end


function CameraEvent:restore_camera()
	
	if self.copySpeeds then
		g_Prim3PitchSpeed = self.RPrim3PitchSpeed
		g_Prim3RollSpeed = self.RPrim3RollSpeed
		g_Prim3Speed = self.RPrim3Speed
		g_Prim3YawSpeed = self.RPrim3YawSpeed
		g_Prim3ZoomSpeed = self.RPrim3ZoomSpeed
	end
		
	if self.copyYaw then
		g_Prim3ObjectiveYaw = self.RPrim3ObjectiveYaw
	end
	
	if self.copyValues then
		g_Prim3Yaw = self.RPrim3Yaw
		g_Prim3Pitch = self.RPrim3Pitch
		g_Prim3Zoom = self.RPrim3Zoom
		g_Prim3Roll = self.RPrim3Roll
	end
	
	if self.copyDistance then
		g_Prim3MaxDistance = self.RPrim3MaxDistance
		g_Prim3MinDistance = self.RPrim3MinDistance
	end
	
	if self.copyZoom then
		g_Prim3MinZoom = self.RPrim3MinZoom
		g_Prim3MaxZoom = self.RPrim3MaxZoom
	end
	
	if self.copyPitch then
		g_Prim3MaxPitch = self.RPrim3MaxPitch
		g_Prim3MinPitch = self.RPrim3MinPitch
	end
	
	if self.copyYawRotation then
		g_Prim3BeginYaw = self.RPrim3BeginYaw
		g_Prim3EndYaw = self.RPrim3EndYaw
		g_Prim3BeginYawPos = self.RPrim3BeginYawPos
		g_Prim3EndYawPos = self.RPrim3EndYawPos
		g_Prim3RotationYaw = self.RPrim3RotationYaw
	end
	
	if self.copyBeginEndPitchRotation then
		g_Prim3RotationPitch = self.RPrim3RotationPitch
		g_Prim3BeginMinPitch = self.RPrim3BeginMinPitch
		g_Prim3EndMinPitch = self.RPrim3EndMinPitch
		g_Prim3BeginMaxPitch = self.RPrim3BeginMaxPitch
		g_Prim3EndMaxPitch = self.RPrim3EndMaxPitch
		g_Prim3BeginPitchPos = self.RPrim3BeginPitchPos
		g_Prim3EndPitchPos = self.RPrim3EndPitchPos
	end
	
	if self.copyYawConstraint then
	 g_Prim3YawConstraint = self.RPrim3YawConstraint
	 g_Prim3RotateInstead = self.RPrim3RotateInstead
	end
	
	if self.copyPitchConstraint then
		g_Prim3PitchConstraint = self.RPrim3PitchConstraint
	end
	
	if self.copyRollConstraint then
		g_Prim3RollConstraint = self.RPrim3RollConstraint
	end
	
	if self.copyMovementConstraint then
		g_Prim3MovementConstraint = self.RPrim3MovementConstraint
	end
	
	if self.copyZoomConstraint then
		g_Prim3ZoomConstraint = self.RPrim3ZoomConstraint
	end

	if self.copyOnePlayerLerp then
		g_Prim3OnePlayerLerp = self.RPrim3OnePlayerLerp
	end

	if self.copyAcceleration then
		g_Prim3Accelerated = self.RPrim3Accelerated
		g_Prim3Acceleration = self.RPrim3Acceleration
		g_Prim3MinSpeed = self.RPrim3MinSpeed
		g_Prim3DistanceToDeccel = self.RPrim3DistanceToDeccel
		g_Prim3ZoomAcceleration = self.RPrim3ZoomAcceleration
		g_Prim3ZoomSpeed = self.RPrim3ZoomSpeed
		g_Prim3PitchAcceleration = self.RPrim3PitchAcceleration
		g_Prim3MinPitchSpeed = self.RPrim3MinPitchSpeed
		g_Prim3YawAcceleration = self.RPrim3YawAcceleration
		g_Prim3MinYawSpeed = self.RPrim3MinYawSpeed
		g_Prim3TimeToDeccel = self.RPrim3TimeToDeccel
	end
	
	g_Prim3FollowStoppedPlayers = self.RPrim3FollowStoppedPlayers

	if self.copyTransform then
		g_Prim3Camera = self.RPrim3TransformToSave
	end
	
	if self.copyFocus then
		g_Prim3CustomFocusPoint = self.RPrim3CustomFocusPoint
	end
		
	if self.copyOffset then
		g_Prim3FocusOffset = self.RPrim3FocusOffset
	end
end