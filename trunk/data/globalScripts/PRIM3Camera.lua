-- PRIM3Camera script

g_Prim3Camera = nil
g_Prim3FollowStoppedPlayers = false
g_Prim3CustomFocusPoint = nil
g_Prim3Zoom = 10.0
g_Prim3Pitch = 20.0
g_Prim3Yaw = 35.0
g_Prim3ObjectiveYaw = g_Prim3Yaw
g_Prim3Roll = 0.0
g_Prim3FocusOffset = float3(0.0, 0.0, 0.0)
g_Prim3smoothing = true
g_Prim3Speed = 180.0
g_Prim3ZoomSpeed = 80.0
g_Prim3PitchSpeed = 80.0
g_Prim3YawSpeed = 80.0
g_Prim3RollSpeed = 80.0
g_Prim3Accelerated = true
g_Prim3TimeToDeccel = 0.08
g_Prim3TimePassed = 0
g_Prim3Acceleration = 5.0
g_Prim3MinSpeed = 5.0
g_Prim3DistanceToDeccel = 3.0
g_Prim3ZoomAcceleration = 20.0
g_Prim3MinZoomSpeed = 1.0
g_Prim3PitchAcceleration = 20.0
g_Prim3MinPitchSpeed = 1.0
g_Prim3YawAcceleration = 20.0
g_Prim3MinYawSpeed = 1.0
g_Prim3MinDistance = 0.0
g_Prim3MaxDistance = 40.0
g_Prim3MinZoom = 10.0
g_Prim3MaxZoom = 30.0
g_Prim3MinPitch = 20.0
g_Prim3MaxPitch = 30.0
g_Prim3RotationYaw = false
g_Prim3BeginYaw = 20.0
g_Prim3EndYaw = 200.0
g_Prim3BeginYawPos = float3(0.0, 0.0, 0.0)
g_Prim3EndYawPos = float3(0.0, 0.0, 0.0)
g_Prim3RotationPitch = false
g_Prim3BeginMinPitch = 30.0
g_Prim3EndMinPitch = 20.0
g_Prim3BeginMaxPitch = 20.0
g_Prim3EndMaxPitch = 100.0
g_Prim3BeginPitchPos = float3(0.0, 0.0, 0.0)
g_Prim3EndPitchPos = float3(0.0, 0.0, 0.0)
g_Prim3MovementConstraint = false
g_Prim3RotateInstead = false
g_Prim3ZoomConstraint = false
g_Prim3YawConstraint = false
g_Prim3PitchConstraint = false
g_Prim3RollConstraint = false
g_Prim3OnePlayerLerp = 0.5
g_Prim3Players = nil;
g_Prim3Follow = float3(0.0, 0.0, 0.0)
g_Prim3LookAtPosition = float3(0.0, 0.0, 0.0)
g_Prim3LastPosition = nil
g_Prim3LastObjectiveYaw = g_Prim3Yaw
g_Prim3CurrentSpeed = g_Prim3Speed
g_Prim3CurrentZoomSpeed = g_Prim3ZoomSpeed
g_Prim3CurrentPitchSpeed = g_Prim3PitchSpeed
g_Prim3CurrentYawSpeed = g_Prim3YawSpeed

function prim3_camera_start()
	g_Prim3Camera = get_prim3_camera()
	g_Prim3LastPosition = g_Prim3Camera.m_Position	
end

function prim3_get_players()
	local index_not_active = {}
	local Prim3Players = engine:get_objects_by_tag("Player")
	g_Prim3Players = {}
--	add_new_info_log("NumPlayers: " ..#Prim3Players)

	for i = 1, #Prim3Players do
		if get_active(Prim3Players[i].name) then	
		--	add_new_info_log("Active: " ..Prim3Players[i].name)
			table.insert(g_Prim3Players, Prim3Players[i])
		end		
	end 
	--[[
	for i = 1, #g_Prim3Players do
		add_new_info_log("Active: " ..g_Prim3Players[i].name)
	end 
	
--	for i = 1, #index_not_active do
--		table.remove(g_Prim3Players, index_not_active[i])
--	end 
	
		add_new_info_log("------------")

--	g_Prim3Players = engine:get_objects_by_tag("Player")
	add_new_info_log("NumPlayersAfter: " ..#g_Prim3Players)--]]
end
	
function prim3_set_lookat()
	g_Prim3Camera.m_Front = float3(1.0, 1.0, 1.0)
end	
	
function prim3_camera_update(ElapsedTime)
	--c = get_prim3_camera()
	prim3_get_players()
	if g_Prim3Players ~= nil then
		if #g_Prim3Players ~= 0 then
				--add_new_info_log("ObjYaw" ..g_Prim3ObjectiveYaw)
			if g_Prim3CustomFocusPoint ~= nil then
					g_Prim3Follow = g_Prim3CustomFocusPoint + g_Prim3FocusOffset
					elseif  not g_Prim3MovementConstraint then
						prim3_calculate_follow ()
						g_Prim3Follow = g_Prim3Follow + g_Prim3FocusOffset
					elseif g_Prim3RotateInstead then  
						prim3_calculate_lookat()
			end
				prim3_calculate_zoom (ElapsedTime)

				if not g_Prim3YawConstraint then
					prim3_calculate_yaw (ElapsedTime)
				end

				if not pitchConstraint then
					prime3_calculate_begin_end_pitch ()
				end

				prim3_update_transform (ElapsedTime)

				prim3_update_speed (ElapsedTime)
		end
	end
		
	return true
end

function prim3_update_speed(dt)
	
	if g_Prim3Accelerated then	
		distance = g_Prim3Follow:distance(g_Prim3LookAtPosition)
		if distance >= g_Prim3DistanceToDeccel then
			g_Prim3CurrentSpeed = g_Prim3CurrentSpeed + g_Prim3Acceleration * dt
			g_Prim3TimePassed = 0.0
		else
			g_Prim3TimePassed = g_Prim3TimePassed + dt
			if g_Prim3TimePassed >= g_Prim3TimeToDeccel then
				g_Prim3CurrentSpeed = g_Prim3CurrentSpeed - g_Prim3Acceleration * dt
			else
				g_Prim3CurrentSpeed = g_Prim3CurrentSpeed + g_Prim3Acceleration * dt
			end
		end
	end
	
	g_Prim3CurrentSpeed = float_clamp(g_Prim3MinSpeed,g_Prim3Speed,g_Prim3CurrentSpeed)
end

function prim3_update_transform(dt)

	if g_Prim3smoothing then
		lastOkPosition = g_Prim3LastPosition
		g_Prim3LastPosition = vector3_move_towards(g_Prim3LastPosition, g_Prim3Follow, g_Prim3CurrentSpeed * dt) 
		
		--if not float.IsNaN(g_Prim3LastPosition.x) then
			g_Prim3Camera.m_Position = g_Prim3LastPosition
			
		--else 
			--g_Prim3LastPosition = lastOkPosition
			--return
		--end
	else
		--if not float.IsNaN(g_Prim3Follow.x) then
			g_Prim3Camera.m_Position = g_Prim3Follow
			
		--else 
			--return
		--end
	end
	g_Prim3LookAtPosition = g_Prim3Camera.m_Position
	
	--if not float.IsNaN(g_Prim3Pitch) and not float.IsNaN(g_Prim3Yaw) and not float.IsNaN(g_Prim3Roll) and not (g_Prim3MovementConstraint and g_Prim3RotateInstead) then
		g_Prim3Camera.m_Yaw = g_Prim3Yaw
		g_Prim3Camera.m_Pitch = g_Prim3Pitch
		
		x = math.cos(math.rad(g_Prim3Yaw)) * math.cos(math.rad(g_Prim3Pitch))
		y = -math.sin(math.rad(g_Prim3Pitch))
		z =  math.sin(math.rad(g_Prim3Yaw)) * math.cos(math.rad(g_Prim3Pitch))

		g_Prim3Camera.m_Front = float3(x,y,z)
		g_Prim3Camera.m_Zoom = g_Prim3Zoom
		--g_Prim3Camera.m_Roll = g_Prim3Roll
--	end
--if not float.IsNaN(g_Prim3Zoom) then 
	g_Prim3Camera.m_Position = g_Prim3Camera.m_Position - g_Prim3Camera.m_Front * g_Prim3Zoom
--end
end

function prim3_calculate_follow()
		g_Prim3Follow = float3(0.0, 0.0, 0.0)
		playersFollowed = 0
		for i = 1, #g_Prim3Players do
		--	if g_Prim3FollowStoppedPlayers or not g_Prim3Players[i].insideBoundingBox then 
				g_Prim3Follow = g_Prim3Follow + g_Prim3Players[i].transform.position 
				playersFollowed = playersFollowed + 1
		--	end
		end
		g_Prim3Follow = g_Prim3Follow / playersFollowed;
end

function prim3_calculate_lookat()
	local target = float3(0.0, 0.0, 0.0)
	for i = 1, #g_Prim3Players do
		target = target + g_Prim3Players[i].transform.position 
	end
	target = target / #g_Prim3Players
	g_Prim3Camera.m_Front = target - g_Prim3Camera.m_Position
	g_Prim3Camera.m_Front = g_Prim3Camera.m_Front:normalized(1.0)
end

function prime3_calculate_begin_end_pitch()
	if g_Prim3RotationPitch then
		beginEndDistance = g_Prim3BeginPitchPos:distance(g_Prim3EndPitchPos)
		followEndDistance = g_Prim3Follow:distance(g_Prim3EndPitchPos)
		lerp = float_inverse_lerp(0.0, beginEndDistance, followEndDistance)
		g_Prim3MinPitch = float_lerp_angle(g_Prim3BeginMinPitch, g_Prim3EndMinPitch, lerp)
		g_Prim3MaxPitch = float_lerp_angle(g_Prim3BeginMaxPitch, g_Prim3BeginMaxPitch, lerp)		
	end
end

function prim3_calculate_yaw(dt)

	g_Prim3LastObjectiveYaw = g_Prim3ObjectiveYaw
	if g_Prim3RotationYaw then
		beginEndDistance = g_Prim3BeginYawPos:distance(g_Prim3EndYawPos)
		followEndDistance = g_Prim3Follow:distance(g_Prim3EndYawPos)

		lerp = float_inverse_lerp(0.0, beginEndDistance, followEndDistance)
		g_Prim3ObjectiveYaw = float_lerp_angle(g_Prim3EndYaw, g_Prim3BeginYaw, lerp)
	end

	if g_Prim3smoothing then
		if g_Prim3Yaw ~= g_Prim3ObjectiveYaw then
	--		add_new_info_log("Yaw1: " ..g_Prim3Yaw)
			g_Prim3CurrentYawSpeed = g_Prim3CurrentYawSpeed + g_Prim3YawAcceleration * dt
		--	add_new_info_log("YawNotEqual: " ..g_Prim3Yaw)
			--add_new_info_log("ObjectiveYawNotEqual: " ..g_Prim3ObjectiveYaw)

		else
			g_Prim3CurrentYawSpeed = g_Prim3CurrentYawSpeed - g_Prim3YawAcceleration * dt
		end	
		--if !float.IsNaN(objectiveYaw)	
			g_Prim3CurrentYawSpeed = float_clamp(g_Prim3MinYawSpeed, g_Prim3YawSpeed, g_Prim3CurrentYawSpeed)
			g_Prim3Yaw = float_move_towards(g_Prim3Yaw, g_Prim3ObjectiveYaw, g_Prim3CurrentYawSpeed * dt)
		--	add_new_info_log("YawEqual: " ..g_Prim3Yaw)

		--end

	else
		--add_new_info_log("Yaw2: " ..g_Prim3Yaw)
		g_Prim3Yaw = g_Prim3ObjectiveYaw
		--if !float.IsNaN(objectiveYaw)	
			g_Prim3ObjectiveYaw = g_Prim3LastObjectiveYaw
		--end
	end
--	add_new_info_log("ObjYaw4: " ..g_Prim3ObjectiveYaw)
--	add_new_info_log("Yaw: " ..g_Prim3Yaw)

end

function prim3_calculate_zoom(dt)
	
	distance = 0
	for i = 1, #g_Prim3Players do

		distance = distance + g_Prim3Follow:distance(g_Prim3Players[i].transform.position)
	end
	
	lerp = float_inverse_lerp(g_Prim3MinDistance, g_Prim3MaxDistance, distance)

	if #g_Prim3Players == 1 then
		lerp = g_Prim3OnePlayerLerp
	end

	if g_Prim3smoothing then
		if not g_Prim3ZoomConstraint then

			newzoom = float_lerp(g_Prim3MinZoom, g_Prim3MaxZoom, lerp)
			if g_Prim3Zoom ~= newzoom then

				g_Prim3CurrentZoomSpeed = g_Prim3CurrentZoomSpeed + g_Prim3ZoomAcceleration * dt
			else

				g_Prim3CurrentZoomSpeed = g_Prim3CurrentZoomSpeed - g_Prim3ZoomAcceleration * dt
			end
				g_Prim3CurrentZoomSpeed = float_clamp(g_Prim3MinZoomSpeed, g_Prim3ZoomSpeed, g_Prim3CurrentZoomSpeed)
				g_Prim3Zoom = float_move_towards(g_Prim3Zoom, newzoom, g_Prim3CurrentZoomSpeed * dt)			
		end

		if not g_Prim3PitchConstraint then

			newpitch = float_lerp(g_Prim3MinPitch, g_Prim3MaxPitch, lerp)
			if g_Prim3Pitch ~= newpitch then

				g_Prim3CurrentPitchSpeed = g_Prim3CurrentPitchSpeed + g_Prim3PitchAcceleration * dt
			else
				g_Prim3CurrentPitchSpeed = g_Prim3CurrentPitchSpeed - g_Prim3PitchAcceleration * dt
			end
				g_Prim3CurrentPitchSpeed = float_clamp(g_Prim3MinPitchSpeed, g_Prim3PitchSpeed, g_Prim3CurrentPitchSpeed)
				g_Prim3Pitch = float_move_towards(g_Prim3Pitch, newpitch, g_Prim3CurrentPitchSpeed * dt)
		end

	else

		if not g_Prim3ZoomConstraint then
			g_Prim3Zoom = float_lerp(g_Prim3MinZoom, g_Prim3MaxZoom, lerp)
		end
		
		if not g_Prim3PitchConstraint then
			g_Prim3Pitch = float_lerp(g_Prim3MinPitch, g_Prim3MaxPitch, lerp)
		end
	end
end



