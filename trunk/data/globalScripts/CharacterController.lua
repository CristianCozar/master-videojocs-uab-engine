-- CharacterController component: Manages players data such as Input, Visibility, etc

class 'CharacterController' (BaseComponent)

function CharacterController:__init(name)
	BaseComponent.__init(self, name)
	
	self.number_of_players = 1
	self.available = {"Sulphur", "Salt", "Mercury"}
end

function CharacterController:start()
end

function CharacterController:next_character(player, input)
	if (self.number_of_players <= 2) then
--		for i = 1, #self.available do
--		add_new_info_log("First: " ..self.available[i]) 
	--end
	
		local new_character = table.remove(self.available, 1)
		table.insert(self.available, player)
	--	add_new_info_log("Cambio de jugador " .. player .. " por " .. new_character)
		
--	for i = 1, #self.available do
--		add_new_info_log("Second: " ..self.available[i]) 
--	end
		--engine:get_object_by_name(player):destroy()
		local playergo = engine:get_object_by_name(player)
		local pos = float3(0.0, 0.0, 0.0)
		pos.x = playergo.transform.position.x
		pos.y = playergo.transform.position.y
		pos.z = playergo.transform.position.z

	--	set_actor_position(playergo.name, float3(playergo.transform.position.x, playergo.transform.position.y - 200, playergo.transform.position.z))
		set_position(playergo.name, float3(playergo.transform.position.x, playergo.transform.position.y - 200, playergo.transform.position.z))
		set_active(playergo.name, false)
		set_visible(playergo.name, false)
		local current_health =  playergo:get_component("Health")
		local current_energy =  playergo:get_component("Energy")		
		local current_movement =  playergo:get_component("MovementController")
		current_movement:SetActive(false)
		
		local nextgo = engine:get_object_by_name(new_character)
		local nextAnimator = nextgo:get_component("AnimatorController")
		nextAnimator:queue_clear()
		set_active(nextgo.name, true)
		set_visible(nextgo.name, true)
		local next_health =  nextgo:get_component("Health")
		local next_energy =  nextgo:get_component("Energy")		
		local next_movement =  nextgo:get_component("MovementController")
		next_movement:SetActive(true)
		if next_health ~= nil and current_health ~= nil then
			next_health.health = current_health.health		
		end		
		if next_energy ~= nil and current_energy ~= nil then
			next_energy.current_energy = current_energy.current_energy		
		end

		current_movement:SetActive(true)

	--	set_actor_position(nextgo.name, pos)
		set_position(nextgo.name, pos)
		
		set_actor_rotation(nextgo.name, get_actor_rotation(playergo.name))

		local player_script = nextgo:get_component("Player")

		player_script.input = input

		player_script.available = self.available

		--engine:request_destroy(playergo)
		
		

		--[[local new_player = instance_return(new_character, new_character,get_scene(self.gameObject.name),get_layer(self.gameObject.name))
		set_position(new_player.name, pos)
		new_player.transform.forward.x = playergo.transform.forward.x
		new_player.transform.forward.y = playergo.transform.forward.y
		new_player.transform.forward.z = playergo.transform.forward.z

		set_actor_rotation(new_player.name, get_actor_rotation(playergo.name))
		local player_script = new_player:get_component("Player")
		player_script.input = input
		player_script.available = self.available
		local new_health = new_player:get_component("Health")
		if new_health ~= nil and current_health ~= nil then
			new_health.health = current_health.health		
		end
		
		local new_energy = new_player:get_component("Energy")
		
		if new_energy ~= nil and current_energy ~= nil then
			new_energy.current_energy = current_energy.current_energy		
		end--]]
		
	--	add_new_info_log("Name: "  ..nextgo.name)

		
		
		--[[
		local new_character_prefab = get_object_by_name(self.gameObject:instance(new_character, new_character))
		local player_script = new_character_prefab:get_component("Player")
		player_script.input = input
		]]--
	end
	--	add_new_info_log("next_character1")

end

function CharacterController:previous_character(player, input)
	if (self.number_of_players <= 2) then
	--		for i = 1, #self.available do
	--		add_new_info_log("First: " ..self.available[i]) 
	--	end
		
			local new_character = table.remove(self.available, 3 -  self.number_of_players)
			table.insert(self.available, 1, player)
		--	add_new_info_log("Cambio de jugador " .. player .. " por " .. new_character)
			
--		for i = 1, #self.available do
--			add_new_info_log("Second: " ..self.available[i]) 
--		end
			--engine:get_object_by_name(player):destroy()
			local playergo = engine:get_object_by_name(player)
			local pos = float3(0.0, 0.0, 0.0)
			pos.x = playergo.transform.position.x
			pos.y = playergo.transform.position.y
			pos.z = playergo.transform.position.z

		--	set_actor_position(playergo.name, float3(playergo.transform.position.x, playergo.transform.position.y - 200, playergo.transform.position.z))
			set_position(playergo.name, float3(playergo.transform.position.x, playergo.transform.position.y - 200, playergo.transform.position.z))
			set_active(playergo.name, false)
			set_visible(playergo.name, false)
			local current_health =  playergo:get_component("Health")
			local current_energy =  playergo:get_component("Energy")		
			local current_movement =  playergo:get_component("MovementController")
			current_movement:SetActive(false)
			
			local nextgo = engine:get_object_by_name(new_character)
			
			set_active(nextgo.name, true)
			set_visible(nextgo.name, true)
			local next_health =  nextgo:get_component("Health")
			local next_energy =  nextgo:get_component("Energy")		
			local next_movement =  nextgo:get_component("MovementController")
			next_movement:SetActive(true)
			if next_health ~= nil and current_health ~= nil then
				next_health.health = current_health.health		
			end		
			if next_energy ~= nil and current_energy ~= nil then
				next_energy.current_energy = current_energy.current_energy		
			end

			current_movement:SetActive(true)

		--	set_actor_position(nextgo.name, pos)
			set_position(nextgo.name, pos)
			
			set_actor_rotation(nextgo.name, get_actor_rotation(playergo.name))

			local player_script = nextgo:get_component("Player")

			player_script.input = input

			player_script.available = self.available

			--engine:request_destroy(playergo)
			
			

			--[[local new_player = instance_return(new_character, new_character,get_scene(self.gameObject.name),get_layer(self.gameObject.name))
			set_position(new_player.name, pos)
			new_player.transform.forward.x = playergo.transform.forward.x
			new_player.transform.forward.y = playergo.transform.forward.y
			new_player.transform.forward.z = playergo.transform.forward.z

			set_actor_rotation(new_player.name, get_actor_rotation(playergo.name))
			local player_script = new_player:get_component("Player")
			player_script.input = input
			player_script.available = self.available
			local new_health = new_player:get_component("Health")
			if new_health ~= nil and current_health ~= nil then
				new_health.health = current_health.health		
			end
			
			local new_energy = new_player:get_component("Energy")
			
			if new_energy ~= nil and current_energy ~= nil then
				new_energy.current_energy = current_energy.current_energy		
			end--]]
			
		--	add_new_info_log("Name: "  ..nextgo.name)

			
			
			--[[
			local new_character_prefab = get_object_by_name(self.gameObject:instance(new_character, new_character))
			local player_script = new_character_prefab:get_component("Player")
			player_script.input = input
			]]--
		end
	--		add_new_info_log("previous_character1")

end

function CharacterController:initialize_character(player)
	local index = nil

	
	for k, v in pairs(self.available) do
		if (v == player) then
			index = k
			add_new_info_log("reservado")
			break
		end
	end

	if index then
		if get_active(self.available[index]) then
			table.remove(self.available, index)
			add_new_info_log("Personaje " .. player .. " eliminado de la lista de disponibles")
		end
	else
		add_new_info_log("Player: " ..player)
		add_new_info_log("No hay mas personajes disponibles")
	end
	
	for i = 1, #self.available do
		add_new_info_log("NameCharControllerAvail: " ..self.available[i]) 
	end
	
	
end