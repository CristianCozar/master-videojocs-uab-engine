-- SpecialAbilitySulphurProjectile component: Controls Sulphur's projectile behaviour

class 'SpecialAbilitySulphurProjectile' (BaseComponent)

function SpecialAbilitySulphurProjectile:__init(name)
	BaseComponent.__init(self, name)
	self.speed = 40.0
	self.damage = 40.0
	self.time_to_live = 10.0
	self.current_timer = 0.0
end

function SpecialAbilitySulphurProjectile:start()
	--add_new_info_log("IN")
end

function SpecialAbilitySulphurProjectile:update(dt)
	local go = self.gameObject
	self.current_timer = self.current_timer + dt
	if self.current_timer < self.time_to_live then
		move_node(go.name, go.transform.forward * self.speed * dt)
	else
		self.gameObject:destroy()
	end
end

function SpecialAbilitySulphurProjectile:on_trigger_enter(otherName)
	local go = self.gameObject
	local othergo = get_object_by_name(otherName)
	local health = othergo:get_component("Health")
	if health then
		health:damage(self.damage)
	end
	if not othergo:has_tag("Trigger") then
		if otherName == "A2_arbol_puzle_tronco" then
			add_new_info_log("1")
			self.gameObject:set_visible(false)
			add_new_info_log("2")
		else
			self.gameObject:destroy()
		end
	end
end

