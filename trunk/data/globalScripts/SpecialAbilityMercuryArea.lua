-- SpecialAbilityMercuryArea component: The area that Mercury leaves behind when using his ability

class 'SpecialAbilityMercuryArea' (BaseComponent)

function SpecialAbilityMercuryArea:__init(name)
	BaseComponent.__init(self, name)

    self.tick_interval = 10.0
    self.tick_interval = 1.0
    self.current_tick_interval = 0.0
    self.damage = 10.0
    self.tick_interval_frame = false
    self.damage = 3.0

end

function SpecialAbilityMercuryArea:start()
end

function SpecialAbilityMercuryArea:update(dt)
    self.current_tick_interval = float_clamp(0.0, self.tick_interval, self.current_tick_interval - dt)

    if (self.current_tick_interval == 0.0) then
        self.tick_interval_frame = true
        self.current_tick_interval = self.tick_interval
    else 
        self.tick_interval_frame = false
    end
end


function SpecialAbilityMercuryArea:on_trigger_enter(otherName)
    table.insert(self.gameObject.gameObjectsOnTrigger, otherName)
end

function SpecialAbilityMercuryArea:on_trigger_exit(otherName)
	for i, v in pairs(self.gameObject.gameObjectsOnTrigger) do
        if v == otherName then
			table.remove(self.gameObject.gameObjectsOnTrigger, i)
        end
    end
end


function SpecialAbilityMercuryArea:on_trigger_stay(otherName, dt)
    if (self.tick_interval_frame) then
        local health = get_object_by_name(otherName):get_component("Health")
        if health then
            health:damage(self.damage)
        end
        self.current_tick_interval = self.tick_interval
    end
end
