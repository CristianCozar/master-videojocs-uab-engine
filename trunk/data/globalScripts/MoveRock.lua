-- MoveRock component

class 'MoveRock' (BaseComponent)

function MoveRock:__init(name)
	BaseComponent.__init(self, name)
	self.targetPos = nil
	self.activated = false
	self.movSpeed = 1.0
	self.finishSpawn = false
    self.initPosition = float3(0.0, 0.0, 0.0)
    self.returning = false
	self.finishReturn = false
	self.instant = false
	self.change_rotation = false
	self.targetRotation = nil
end

function MoveRock:start()
	self.initPosition.x = self.transform.position.x
	self.initPosition.y = self.transform.position.y
	self.initPosition.z = self.transform.position.z
end
	


function MoveRock:update(dt)

	if self.activated and not self.finishSpawn then
		if not self.instant then
			local pos = vector3_move_towards(self.transform.position, self.targetPos, self.movSpeed * dt)
			self.transform.position = pos
			set_actor_position(self.gameObject.name, pos)
			if pos == self.targetPos then
				self.finishSpawn = true
				self.activated = false
			end
		else		
			self.transform.position = self.targetPos
			set_actor_position(self.gameObject.name, self.transform.position)
			if self.change_rotation and self.targetRotation ~= nil then
				self.transform.yaw = deg_2_rad(self.targetRotation.x)
				self.transform.pitch = deg_2_rad(self.targetRotation.y)
				self.transform.roll = deg_2_rad(self.targetRotation.z)
				set_actor_rotation(self.gameObject.name, self.targetRotation)
			end
			self.finishSpawn = true
			self.activated = false
		end
	end
	
	if self.returning and not self.finishReturn then
		if not self.instant then
			local pos = vector3_move_towards(self.transform.position, self.initPosition, self.movSpeed * dt)
			self.transform.position = pos
			set_actor_position(self.gameObject.name, self.transform.position)
			if pos == self.initPosition then
				self.finishReturn = true
				self.returning = false
			end
		else
			self.transform.position = self.targetPos
			set_actor_position(self.gameObject.name, self.transform.position)
			self.finishReturn = true	
			self.returning = false
		end
	end
end

function MoveRock:activate()
	self.returning = false
	self.activated = true
end
	
function MoveRock:return_to_position()
	self.activated = false
    self.returning = true
end
	
