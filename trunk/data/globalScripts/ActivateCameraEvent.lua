-- ActivateCameraEvent component

class 'ActivateCameraEvent' (BaseComponent)

function ActivateCameraEvent:__init(name)
	BaseComponent.__init(self, name)
	self.position = float3(0.0, 0.0, 0.0)
end

function ActivateCameraEvent:start()
end
	


function ActivateCameraEvent:update(dt)
end

function ActivateCameraEvent:activate()
	set_actor_position(self.gameObject.name, self.position)
end