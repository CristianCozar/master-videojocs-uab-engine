function register_physx_material(name, static_friction, dynamic_friction, restitution)
	register_material(name, static_friction, dynamic_friction, restitution)
end

function create_physx_static_box(name, sizeX, sizeY, sizeZ, position, orientation, materialName)
	create_static_box(name, sizeX, sizeY, sizeZ, position, orientation, materialName)
end

function create_physx_static_plane(name, x, y, z, dist, materialName)
	create_static_plane(name, x, y, z, dist, materialName)
end

function create_physx_dynamic_box(name, sizeX, sizeY, sizeZ, position, orientation, density, materialName)
	create_dynamic_box(name, sizeX, sizeY, sizeZ, position, orientation, density, materialName)
end

function create_physx_dynamic_sphere(name, radius, position, orientation, density, materialName)
	create_dynamic_sphere(name, radius, position, orientation, density, materialName)
end

function create_physx_static_sphere(name, radius, position, orientation, materialName)
	create_static_sphere(name, radius, position, orientation, materialName)
end

function create_physx_dynamic_convex_mesh(name, vertices, position, orientation, density, materialName)
	create_dynamic_convex_mesh(name, vertices, position, orientation, density, materialName)
end

function create_physx_static_convex_mesh(name, vertices, position, orientation, materialName)
	create_static_convex_mesh(name, vertices, position, orientation, materialName)
end

function create_physx_static_triangle_mesh(name, vertices, position, orientation, materialName)
	create_static_triangle_mesh(name, vertices, position, orientation, materialName)
end

function create_physx_trigger_box(name, sizeX, sizeY, sizeZ, position, orientation, materialName)
	create_trigger_box(name, sizeX, sizeY, sizeZ, position, orientation, materialName)
end