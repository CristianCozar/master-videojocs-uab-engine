// UBER SHADER
// Este shader incluye todos los shaders Forward. Cual se ejecuta depende de los DEFINE de preprocessor.
/*
NORMAL_DEBUG: Muestra el valor de la normal en el pixel.
ALBEDO: Aplica una textura albedo usando las UV.
LIGHTS: Aplica luces
LIGHTMAP: Aplica un Lightmap usando las UV2.
BUMP: Modifica las normales para aplicar un efecto BUMP.
ANIMATED_MODEL: Un modelo animado de Cal3D
RNM: Radiosity Normal Map. Solo funcionará si están definidos LIGHTMAP y BUMP.


Orden de samplers: Albedo -> Bump -> Lightmap1 -> Lightmap2 -> Lightmap3
*/

#include "Globals.fxh"

PS_INPUT_COMPLETE mainVS(VS_INPUT_COMPLETE IN)
{
	PS_INPUT_COMPLETE l_Out = (PS_INPUT_COMPLETE)0;
	float4 l_Position;
	float3 l_Normal;
#ifdef ANIMATED_MODEL
	float4 l_TempPos=float4(IN.Pos.xyz, 1.0);
	float4 l_Indices=IN.Indices;
	l_Normal=float3(0,0,0);
	l_Position=mul(l_TempPos, m_Bones[l_Indices.x]) * IN.Weight.x;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]) * IN.Weight.y;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]) * IN.Weight.z;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]) * IN.Weight.w;
	float3x3 m;
	m[0].xyz = m_Bones[l_Indices.x][0].xyz;
	m[1].xyz = m_Bones[l_Indices.x][1].xyz;
	m[2].xyz = m_Bones[l_Indices.x][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.x;
	m[0].xyz = m_Bones[l_Indices.y][0].xyz;
	m[1].xyz = m_Bones[l_Indices.y][1].xyz;
	m[2].xyz = m_Bones[l_Indices.y][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.y;
	l_Normal=normalize(l_Normal);
#else
	l_Position = float4(IN.Pos, 1.0);
	l_Normal = IN.Normal;
#endif
	l_Position = mul(l_Position, m_World);
	l_Position = mul(l_Position, m_ViewProjection);
	l_Out.Pos = l_Position;
	l_Out.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	l_Out.WorldNormal = normalize(mul(normalize(IN.Normal).xyz, (float3x3)m_World));
	l_Out.Normal = mul(l_Normal, m_World);
#ifdef ALBEDO
	l_Out.UV=IN.UV;
#endif
#ifdef LIGHTMAP
	l_Out.UV2=IN.UV2;
#endif
#ifdef BUMP
	l_Out.WorldTangent=normalize(mul(IN.Tangent.xyz, (float3x3)m_World));
	l_Out.WorldBinormal=normalize(mul(cross(IN.Tangent.xyz, IN.Normal.xyz), (float3x3)m_World));
#endif
	return l_Out;
}

float4 mainPS(PS_INPUT_COMPLETE IN) : SV_Target
{
	float3 normals = normalize(IN.WorldNormal);
#ifdef NORMAL_DEBUG
	return float4(IN.WorldNormal.xyz, 1.0);
#endif
	float4 l_BaseColor = float4(1.0, 1.0, 1.0, 1.0);
	l_BaseColor = l_BaseColor * m_MaterialColor;
#ifdef ALBEDO
#ifndef ANIMATED_MODEL
	IN.UV[1] = -IN.UV[1];
#endif
	l_BaseColor = l_BaseColor * T0Texture.Sample(S0Sampler, IN.UV);
#endif
	float3 l_FinalColor = l_BaseColor.xyz;
#ifdef BUMP
	CalculateBumpNormals(IN.WorldTangent, IN.WorldBinormal, IN.UV, normals);
#endif
#ifdef LIGHTMAP
#ifdef RNM
#ifdef BUMP
	// Solo aplicamos RNM si están activos LIGHTMAP, BUMP y RNM.
	float3 l_AmbientContrib = GetRadiosityNormalMap(normals, IN.UV2, T2Texture, S2Sampler, T3Texture, S3Sampler, T4Texture, S4Sampler);
	l_FinalColor = l_FinalColor * l_AmbientContrib;
#endif
#else
#ifdef BUMP
	l_FinalColor = l_FinalColor * T2Texture.Sample(S2Sampler, IN.UV2).xyz;
#else
	l_FinalColor = l_FinalColor * T1Texture.Sample(S1Sampler, IN.UV2).xyz;
#endif
#endif
#endif
#ifdef LIGHTS
	l_FinalColor = l_FinalColor * m_LightAmbient.xyz * m_MaterialAmbient;
	float3 l_LDiffuse = float3(0.0, 0.0, 0.0);
	float3 l_LSpecular = float3(0.0, 0.0, 0.0);
	for (int i = 0; i < MAX_LIGHTS_BY_SHADER; ++i)
	{
		CalculateSingleLight(i, normals, IN.WorldPosition, l_BaseColor.xyz, m_MaterialSpecular, m_MaterialSpecularLevel, l_LDiffuse, l_LSpecular);
		l_FinalColor += l_LDiffuse + l_LSpecular;		
	}
#endif
	return float4(l_FinalColor,1.0);
}