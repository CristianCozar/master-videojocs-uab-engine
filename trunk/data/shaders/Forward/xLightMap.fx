/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/


	  texture TexTest  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "TexTest Texture";
	  string ResourceType = "2D";
	  >;
	  
	  	  sampler2D TexTestSampler = sampler_state {
	  Texture = <TexTest>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  	  texture lightmap  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Lightmap Texture";
	  string ResourceType = "2D";
	  >;
	  
	  	  sampler2D lightmapSampler = sampler_state {
	  Texture = <lightmap>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  	  		  float Multiplier
		  	  <
		  	  string UIWidget = "slider";
		  	  float UIMin = 0.1;
		  	  float UIMax = 1.0;
		  	  float UIStep = 0.01;
		  	  string UIName =  "Multiplier";
		  	  > = 0.5;
	  
	  
	   	  float Offset
	  	  <
	  	  string UIWidget = "slider";
	  	  float UIMin = 0.0;
	  	  float UIMax = 1.0;
	  	  float UIStep = 0.01;
	  	  string UIName =  "Offset";
	  	  > = 0.5;


	

float4x4 WorldViewProj : WorldViewProjection;
float4x4 View : View; 
float4x4 World : World; 
float4x4 Projection : Projection; 

struct TVertexVS 
{ 
	float3 Pos : POSITION; 
	float3 Normal: NORMAL;
	float2 UV: TEXCOORD0;
}; 

struct TVertexPS 
{ 
	float4 Pos : POSITION; 
	float3 Normal: NORMAL;
	float2 UV: TEXCOORD0;
}; 

TVertexPS mainVS(TVertexVS IN) 
{ 
	TVertexPS l_Out=(TVertexPS)0; 
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World); 
	l_Out.Pos=mul(l_Out.Pos, View); 
	l_Out.Pos=mul(l_Out.Pos, Projection); 
	l_Out.Normal = normalize(mul(IN.Normal,(float3x3)World));
	l_Out.UV = IN.UV;
	return l_Out; 
} 
float4 mainPS(TVertexPS IN) : COLOR 
{ 
//	return tex2D(TexTestSampler,IN.UV)*tex2D(lightmapSampler,mul(IN.UV,Multiplier)+Offset); 
	IN.UV[1] = -IN.UV[1];
	return tex2D(TexTestSampler,IN.UV)*tex2D(lightmapSampler,IN.UV); 
} 

technique technique0 
{ 
	pass p0 
	{ 
		CullMode = None; 
		VertexShader = compile vs_3_0 mainVS(); 
		PixelShader = compile ps_3_0 mainPS(); 
	} 
} 