/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/
struct TVertexVS {  
	float3 Pos : POSITION;  
	float3 Normal : NORMAL;  
	float2 UV : TEXCOORD0; 
}; 
 
struct TVertexPS {  
	float4 Pos : POSITION;  
	float3 Normal : NORMAL;  
	float2 UV : TEXCOORD1; 
	float4 WorldPosition : TEXCOORD2;
};


texture S0  < 
	string ResourceName = "";//Optional default file name 
	string UIName =  "S0 Texture";
	string ResourceType = "2D"; 
>; 
 
sampler2D S0Sampler = sampler_state { 
	Texture = <S0>; 
	MinFilter = Linear; 
	MagFilter = Linear; 
	MipFilter = Linear; 
	AddressU = Wrap; 
	AddressV = Wrap; 
}; 

texture S1  < 
	string ResourceName = "";//Optional default file name 
	string UIName =  "S1 Texture";
	string ResourceType = "2D"; 
>; 
 
sampler2D S1Sampler = sampler_state { 
	Texture = <S1>; 
	MinFilter = Linear; 
	MagFilter = Linear; 
	MipFilter = Linear; 
	AddressU = Clamp; 
	AddressV = Clamp; 
}; 
	
	  //Iluminacion ambiente
	  float3 AmbientColor : COLOR
	  <
	  string UIWidget = "Color";
	  string UIName =  "AmbientColor";
	  >  = {0.8f, 0.8f, 0.8f};
	  
	  
	  	  float AmbientIntensity
	  	  <
	  	  string UIWidget = "slider";
	  	  float UIMin = 0.0;
	  	  float UIMax = 20.0;
	  	  float UIStep = 0.01;
	  	  string UIName =  "AmbientIntensity";
	  	  > = 0.5;
		 
		  //Iluminacion difusa
		  float DiffuseIntensity
	  	  <
	  	  string UIWidget = "slider";
	  	  float UIMin = 0.0;
	  	  float UIMax = 1.0;
	  	  float UIStep = 0.01;
	  	  string UIName =  "DiffuseIntensity";
	  	  > = 0.5;
		  
			//Luz direccional
		  float3 Lamp0Direction : DIRECTION
		  <
		  string Object = "Directional Light 0";
		  string UIName =  "Lamp 0 Direction";
		  string Space = "World";
		  > = {-0.5f,2.0f,1.25f};
	
		  float3 Lamp_0_color : COLOR <
		  string Object = "Directional Light 0";
		  string UIName =  "Lamp 0 Color";
		  string UIWidget = "Color";
		  > = {1.0f,1.0f,1.0f};
		
	  	
	

float4x4 WorldViewProj : WorldViewProjection;
float4x4 View : View;
float4x4 World : World;
float4x4 Projection : Projection;

TVertexPS mainVS(TVertexVS IN){
	TVertexPS l_Out = (TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);  
	l_Out.Pos=mul(l_Out.Pos, View);  
	l_Out.Pos=mul(l_Out.Pos, Projection);
	l_Out.Normal=normalize(mul(IN.Normal, (float3x3)World));  
	l_Out.UV=IN.UV;  
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR {
	float4 l_Out = float4(0.0f, 0.0f , 0.0f, 0.0f);
	float3 lightDir = -Lamp0Direction;
	
	//Textura
	float4 l_texture1 = tex2D(S0Sampler, IN.UV);
	l_Out = l_texture1 * float4(AmbientColor * AmbientIntensity, 1.0f);
	
	float2 colorUV = IN.UV.xy;
	colorUV[0] = 1 - (saturate(dot(IN.Normal, lightDir)));
	float4 l_texture2 = tex2D(S1Sampler, colorUV);
	l_Out += l_texture2;
	return l_Out;
}

technique technique0 {
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
