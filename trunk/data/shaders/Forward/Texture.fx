
	  texture S0  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "S0 Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D S0Sampler = sampler_state {
	  Texture = <S0>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  	  float S0Offset
	  	  <
	  	  string UIWidget = "slider";
	  	  float UIMin = 0.0;
	  	  float UIMax = 1.0;
	  	  float UIStep = 0.01;
	  	  string UIName =  "S0Offset";
	  	  > = 0.5;
		  
		  
		  	  float S0Multiplier
		  	  <
		  	  string UIWidget = "slider";
		  	  float UIMin = 0.0;
		  	  float UIMax = 10.0;
		  	  float UIStep = 0.01;
		  	  string UIName =  "S0Multiplier";
		  	  > = 0.5;
		  	
	  	
	

float4x4 WorldViewProj : WorldViewProjection;
float4x4 World : World;
float4x4 ViewProjection : ViewProjection;

struct TVertexVS
{
 float3 Pos : POSITION;
 float4 Normals : NORMAL;
 float2 UV : TEXCOORD0;
};

struct TVertexPS
{
 float4 Pos : POSITION;
 float4 Normals: TEXCOORD0;
 float2 UV : TEXCOORD1;
}; 


TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out = (TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
 	l_Out.Pos=mul(l_Out.Pos, ViewProjection);
	l_Out.Normals=mul(IN.Normals, World);
	l_Out.UV=mul(IN.UV, S0Multiplier) + S0Offset;
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR
{
	IN.UV[1] = -IN.UV[1];
	return tex2D(S0Sampler, IN.UV);
}

technique technique0
{
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
