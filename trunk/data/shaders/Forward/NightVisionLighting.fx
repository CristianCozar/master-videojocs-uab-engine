
	  texture S0  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "S0 Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D S0Sampler = sampler_state {
	  Texture = <S0>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  		  
		  float4 g_AmbientColor : COLOR
		  <
		  string UIWidget = "Color";
		  string UIName =  "g_AmbientColor";
		  >  = {1.0f, 1.0f, 1.0f, 1.0f};
		  
		  float g_Intensity
		  <
		  string UIWidget = "slider";
		  float UIMin = 0.0;
		  float UIMax = 10.0;
		  float UIStep = 0.01;
		  string UIName =  "g_Intensity";
		  > = 1.0;
		  
		  float g_SpecularContrib
		  <
		  string UIWidget = "slider";
		  float UIMin = 0.0;
		  float UIMax = 1.0;
		  float UIStep = 0.01;
		  string UIName =  "g_SpecularContrib";
		  > = 1.0;
		  
		  float g_SpecularComponent
		  <
		  string UIWidget = "slider";
		  float UIMin = 1.0;
		  float UIMax = 80.0;
		  float UIStep = 0.01;
		  string UIName =  "g_SpecularComponent";
		  > = 20.0;

		  
		  float g_SpotAngle
		  <
		  string UIWidget = "slider";
		  float UIMin = 5.0;
		  float UIMax = 180.0;
		  float UIStep = 0.01;
		  string UIName =  "g_SpotAngle";
		  > = 45.0;
		  
		  float g_SpotFallOff
		  <
		  string UIWidget = "slider";
		  float UIMin = 5.0;
		  float UIMax = 180.0;
		  float UIStep = 0.01;
		  string UIName =  "g_SpotFallOff";
		  > = 90.0;
		  
		  float3 Spot0Point : POSITION
		  <
		  string Object = "SpotLight0";
		  string UIName =  "Spot 0 Position";
		  string Space = "World";
		  > = {0.0f,0.0f,0.0f};
	  
		  float3 Spot_0_color : COLOR <
		  string Object = "SpotLight0";
		  string UIName =  "Spot 0 Color";
		  string UIWidget = "Color";
		  > = {1.0f,1.0f,1.0f};
		  
		  float3 Spot0Direction : DIRECTION
		  <
		  string Object = "SpotLight0";
		  string UIName =  "SpotLight0 Direction";
		  string Space = "World";
		  > = {0.0f,0.0f,1.0f};
	
	  
		  float g_AttStart
		  <
		  string UIWidget = "slider";
		  float UIMin = 0.0;
		  float UIMax = 10.0;
		  float UIStep = 0.01;
		  string UIName =  "g_AttStart";
		  > = 0.1;
		  
		  
		  float g_AttEnd
		  <
		  string UIWidget = "slider";
		  float UIMin = 0.0;
		  float UIMax = 10.0;
		  float UIStep = 0.01;
		  string UIName =  "g_AttEnd";
		  > = 0.5;
		  
float4x4 World : World;
float4x4 ViewProjection : ViewProjection;
float4x4 ViewInverse : ViewInverse;


struct TVertexVS
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct TVertexPS
{
	float4 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float3 wPosition: TEXCOORD1;
}; 

TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out = (TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
 	l_Out.Pos=mul(l_Out.Pos, ViewProjection);
	l_Out.wPosition = mul(float4(IN.Pos.xyz, 1.0), World).xyz;
	l_Out.Normal=mul(IN.Normal, World);
	l_Out.UV=IN.UV;
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR
{
	// Base
	IN.UV[1] = -IN.UV[1];
	float4 l_BaseColor = tex2D(S0Sampler, IN.UV);
	
	float3 l_Eye=ViewInverse[3].xyz;
	float3 l_ViewDir = normalize(l_Eye - IN.wPosition);
	float3 l_Normal = normalize(IN.Normal);
	
	float3 l_LightDirection = IN.wPosition - Spot0Point;
	
	float l_DistanceToPixel = length(l_LightDirection);
	l_LightDirection = l_LightDirection/l_DistanceToPixel;
	
	//Ambient
	
	float3 l_LAmbient = g_AmbientColor * l_BaseColor.xyz;
	
	//Diffuse
	
	float l_DiffuseContrib = saturate(dot(-l_LightDirection,l_Normal));
	
	//Specular
	
	float3 Hn = normalize(l_ViewDir-l_LightDirection);
	float l_SpecularContrib = pow(saturate(dot(Hn,l_Normal)), g_SpecularComponent);
	
	//Distance Attenuation
	
	float l_DistanceAtten = 1.0 - saturate((l_DistanceToPixel - g_AttStart)/(g_AttEnd-g_AttStart));

	//Spot cone & Attenuation
	
	float l_SpotAngle=cos(g_SpotAngle*0.5*(3.1416/180.0));
	float l_SpotFallOff=cos(g_SpotFallOff*0.5*(3.1416/180.0));
	float l_DotAngle=dot(l_LightDirection, Spot0Direction);
	float l_AngleAtenuation =  saturate((l_DotAngle-l_SpotFallOff)/(l_SpotAngle-l_SpotFallOff));
	
	// final Diffuse and Specular contributions
	float3 l_LDiffuse = l_DiffuseContrib * g_Intensity * Spot_0_color * l_BaseColor.xyz * l_DistanceAtten * l_AngleAtenuation;
	float3 l_LSpecular = l_SpecularContrib* g_Intensity *	Spot_0_color *g_SpecularContrib * l_DistanceAtten * l_AngleAtenuation;
	
	float4 col = float4(l_LAmbient+l_LDiffuse+l_LSpecular,1.0);	
	
	float x = ( col.x +  col.y +  col.z)/3.0;
	
	return float4(0.0,x,0.0,1.0);
}

technique technique0
{
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}