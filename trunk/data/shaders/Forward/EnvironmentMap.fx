#include "Globals.fxh"

struct PS_ENVIRONMENTMAP_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Normal : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float2 UV: TEXCOORD2;
};
//static float m_EnvironmentFactor = 0.4;
static float m_EnvironmentFactor = m_RawData[0].r;
//static float4 m_EnvironmentColor = float4(m_RawDatArray[1], m_RawDatArray[2], m_RawDatArray[3], m_RawDatArray[4]);

PS_ENVIRONMENTMAP_INPUT mainVS(VS_INPUT IN)
{
	PS_ENVIRONMENTMAP_INPUT l_Output = (PS_ENVIRONMENTMAP_INPUT)0;
	l_Output.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	l_Output.Pos = mul(l_Output.Pos, m_ViewProjection);
	
	l_Output.WorldPos = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	

	l_Output.Normal = normalize(mul(IN.Normal, (float3x3)m_World));
	l_Output.UV = IN.UV; 
	
	return l_Output;
}

float4 mainPS(PS_ENVIRONMENTMAP_INPUT IN) : SV_Target
{
	float3 Nn = normalize(IN.Normal);
	float3 l_EyeToWorldPosition = normalize(IN.WorldPos - m_ViewInverse[3].xyz);
	float3 l_ReflectVector = normalize(reflect(l_EyeToWorldPosition, Nn));

	float3 l_Albedo = T0Texture.Sample(S0Sampler, IN.UV).xyz + (T1TextureCUBE.Sample(S1Sampler, l_ReflectVector).xyz) * m_EnvironmentFactor;
	return float4(l_Albedo, 1);
	//return float4(T1TextureCUBE.Sample(S2Sampler, l_ReflectVector).xyz* 1 , 1); //* m_EnvironmentFactor
}


