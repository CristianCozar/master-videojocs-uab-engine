/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/

//Luz Direccional
float3 Lamp0Direction : DIRECTION
<
	string Object = "Directional Light 0";
	string UIName =  "Lamp 0 Direction";
	string Space = "World";
> = {-1.0f,0.0f,0.0f};

float3 Lamp_0_color : COLOR <
	string Object = "Directional Light 0";
	string UIName =  "Lamp 0 Color";
	string UIWidget = "Color";
> = {0.0f,1.0f,0.0f};
	  
//Propiedades luz difusa

	float DiffuseIntensity <
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 1.0;
		float UIStep = 0.01;
		string UIName =  "DIffuseIntensity";
	> = 0.5;
	
//Propiedades luz Especular

	float SpecularIntensity <
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 1.0;
		float UIStep = 0.01;
		string UIName =  "SpecularIntensity";
	> = 0.5;
	
	float SpecularReflectionPower <
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 10.0;
		float UIStep = 0.01;
		string UIName =  "SpecularReflectionPower";
	> = 5.0;

		  
//Propiedades luz ambiente
	float3 AmbientColor <
		string UIName =  "AmbientColor";
	> = {0.3f, 0.3f, 0.3f};
	
	float AmbientIntensity <
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 1.0;
		float UIStep = 0.01;
		string UIName =  "AmbientIntensity";
	> = 0.5;


texture S0  < 
	string ResourceName = "";//Optional default file name 
	string UIName =  "S0 Texture";
	string ResourceType = "2D"; 
>; 
 
sampler2D S0Sampler = sampler_state { 
	Texture = <S0>; 
	MinFilter = Linear; 
	MagFilter = Linear; 
	MipFilter = Linear; 
	AddressU = Wrap; 
	AddressV = Wrap; 
}; 
float4x4 View : View; 
float4x4 World : World; 
float4x4 Projection : Projection;
float4x4 ViewInverse : ViewInverse;

struct TVertexVS {  
	float3 Pos : POSITION;  
	float3 Normal : NORMAL;  
	float2 UV : TEXCOORD0; 
}; 
 
struct TVertexPS {  
	float4 Pos : POSITION;  
	float3 Normal : NORMAL;  
	float2 UV : TEXCOORD1; 
	float4 WorldPosition : TEXCOORD2;
};

TVertexPS  mainVS(TVertexVS IN){
	TVertexPS l_Out=(TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);  
	l_Out.Pos=mul(l_Out.Pos, View);  
	l_Out.Pos=mul(l_Out.Pos, Projection);  
	l_Out.Normal=normalize(mul(IN.Normal, (float3x3)World));  
	l_Out.UV=IN.UV;  
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR {
	float4 l_Out;
	
	//Textura
	float4 l_texture = tex2D(S0Sampler, IN.UV);
	
	l_Out = l_texture * float4(AmbientColor * AmbientIntensity, 1.0f);
	
	//Difusa
	float3 lightDir = -Lamp0Direction;
	l_Out +=  l_texture * float4(Lamp_0_color * DiffuseIntensity * saturate(dot(IN.Normal, lightDir)), 1.0f);
	
	//Especular
	float3 HalfWayVector;
	float3 ViewPosition = ViewInverse[3].xyz;

	HalfWayVector = normalize( normalize(ViewInverse[3].xyz - IN.WorldPosition) + (lightDir));
	l_Out +=  l_texture * float4(Lamp_0_color * SpecularIntensity * pow(saturate(dot(IN.Normal, normalize(HalfWayVector))), SpecularReflectionPower ), 1.0f);
	
	
	return l_Out;
}

technique technique0 {
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
