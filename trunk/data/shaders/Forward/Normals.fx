float4x4 WorldViewProj : WorldViewProjection;
float4x4 World : World;
float4x4 ViewProjection : ViewProjection;

struct TVertexVS
{
 float3 Pos : POSITION;
 float4 Normals : NORMAL;
};

struct TVertexPS
{
 float4 Pos : POSITION;
 float4 Normals: TEXCOORD0;
}; 


TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out = (TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
 	l_Out.Pos=mul(l_Out.Pos, ViewProjection);
	l_Out.Normals=mul(IN.Normals, World);
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR
{
	return IN.Normals;
}

technique technique0
{
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
