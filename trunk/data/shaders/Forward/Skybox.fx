#include "Globals.fxh"

struct PS_SKYBOX_INPUT
{
	float4 Pos : SV_POSITION;
	float3 UV : TEXCOORD0;
};

PS_SKYBOX_INPUT mainVS(VS_INPUT IN)
{
	PS_SKYBOX_INPUT l_Output = (PS_SKYBOX_INPUT)0;

	l_Output.UV=IN.Pos;
	float l_Near;
	float l_Far;
	GetNearFarFromProjectionMatrix(l_Near, l_Far, m_Projection);

	l_Output.Pos = float4(m_ViewInverse[3].xyz + l_Output.UV*l_Far*0.80, 1.0);
	l_Output.Pos = mul(l_Output.Pos, m_ViewProjection);

	return l_Output;
}

float4 mainPS(PS_SKYBOX_INPUT IN) : SV_Target
{
	//return float4(0, 1, 0, 1);
	return T0TextureCUBE.Sample(S0Sampler, IN.UV) * m_SkyboxColor;
}
