
	  texture Texture  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Texture Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D TextureSampler = sampler_state {
	  Texture = <Texture>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  float4 g_AmbientColor : COLOR
	  <
	  string UIWidget = "Color";
	  string UIName =  "g_AmbientColor";
	  >  = {0.8f, 0.8f, 0.8f, 1.0f};
	  
	  
	  float3 Lamp0Point : POSITION
	  <
	  string Object = "Point Light 0";
	  string UIName =  "Lamp 0 Position";
	  string Space = "World";
	  > = {-0.5f,2.0f,1.25f};
  
	  float3 Lamp_0_color : COLOR <
	  string Object = "Point Light 0";
	  string UIName =  "Lamp 0 Color";
	  string UIWidget = "Color";
	  > = {1.0f,1.0f,1.0f};
	  
	  
	  float g_Intensity
	  <
	  string UIWidget = "slider";
	  float UIMin = 0.0;
	  float UIMax = 10.0;
	  float UIStep = 0.01;
	  string UIName =  "g_Intensity";
	  > = 0.5;
	  
	  float g_AttStart
	  <
	  string UIWidget = "slider";
	  float UIMin = 0.0;
	  float UIMax = 1000.0;
	  float UIStep = 0.01;
	  string UIName =  "g_AttStart";
	  > = 1;
	  
	  float g_AttEnd
	  <
	  string UIWidget = "slider";
	  float UIMin = 0.0;
	  float UIMax = 1000.0;
	  float UIStep = 0.01;
	  string UIName =  "g_AttEnd";
	  > = 2;
	  
	  float g_SpecularComponent
	  <
	  string UIWidget = "slider";
	  float UIMin = 1.0;
	  float UIMax = 80.0;
	  float UIStep = 0.01;
	  string UIName =  "g_SpecularComponent";
	  > = 20;
	  	
	  	
	  	
	  	
	  	
	

float4x4 World : World;
float4x4 ViewProjection : ViewProjection;
float4x4 ViewInverse : ViewInverse;

struct TVertexVS
{
 float3 Pos : POSITION;
 float3 Normals : NORMAL;
 float2 UV : TEXCOORD0;
};

struct TVertexPS
{
 float4 Pos : POSITION;
 float3 Normals: NORMAL;
 float2 UV : TEXCOORD0;
 float4 WorldPosition : TEXCOORD1;
}; 



TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out = (TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
 	l_Out.Pos=mul(l_Out.Pos, ViewProjection);
	l_Out.WorldPosition = mul(float4(IN.Pos, 1.0), World);
	//l_Out.WorldPosition = mul(IN.Pos, (float3x3) World); // Por qué no va si es float3?
	l_Out.Normals=mul(IN.Normals, World).xyz;
	l_Out.UV= IN.UV;
	return l_Out;
}

float4 mainPS(TVertexPS IN) : COLOR {
	IN.UV[1] = -IN.UV[1];
	float3 l_Normals = normalize(IN.Normals);
	float3 l_BaseColor = tex2D(TextureSampler, IN.UV);
	float3 l_Eye = ViewInverse[3].xyz;
	float3 l_LightDirection = IN.WorldPosition.xyz - Lamp0Point;
	float l_Distance = length(l_LightDirection);
	l_LightDirection /= l_Distance;
	
	// Attenuation
	float l_Att;
	if (g_AttEnd == g_AttStart) {
		l_Att = 1.0;
	} else {
		l_Att = 1 - saturate((l_Distance - g_AttStart) / (g_AttEnd - g_AttStart));
	}
	// Ambient
	float3 l_AmbientColor = g_AmbientColor.xyz * l_BaseColor;
	// Diffuse
	float l_DiffuseContrib = saturate(dot(-l_LightDirection, l_Normals));
	float3 l_DiffuseColor = l_BaseColor * Lamp_0_color * l_DiffuseContrib * g_Intensity * l_Att;
	// Specular
	float3 l_H = normalize(-l_LightDirection + normalize(l_Eye - IN.WorldPosition.xyz));
	float l_SpecularContrib = pow(saturate(dot(l_H, l_Normals)), g_SpecularComponent);
	float3 l_SpecularColor = l_BaseColor * Lamp_0_color * l_SpecularContrib * g_Intensity * l_Att;
	
	float4 l_FinalColor = float4(l_AmbientColor + l_DiffuseColor + l_SpecularColor, 1.0);
	// Sum
	return l_FinalColor;
	return float4(l_SpecularColor, 1.0);
	return float4(l_DiffuseColor, 1.0);
	return float4(l_DiffuseContrib, l_DiffuseContrib, l_DiffuseContrib, 1.0);
	return float4(l_AmbientColor, 1.0);
	return float4(Lamp0Point, 1.0);
	return float4(Lamp_0_color, 1.0);
	return tex2D(TextureSampler, IN.UV);
	return float4(g_AmbientColor.xyz, 1.0);
	return float4(1.0, 0.0, 0.0, 1.0);
}

technique technique0 {
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}