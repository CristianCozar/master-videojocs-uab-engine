//Lightning.FX
#include "Globals.fxh"
		  	 
PS_INPUT2 mainVS(VS_INPUT IN)
{
	PS_INPUT2 l_Out = (PS_INPUT2)0;
	l_Out.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	l_Out.Pos = mul(l_Out.Pos, m_ViewProjection);
	l_Out.WorldNormal = normalize(mul(normalize(IN.Normal).xyz, (float3x3)m_World));
	l_Out.Normal = mul(IN.Normal, m_World);
	l_Out.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	l_Out.UV=IN.UV;
	return l_Out;
}

float4 mainPS(PS_INPUT2 IN) : SV_Target
{
	// Base
	IN.UV[1] = -IN.UV[1];
	float4 l_BaseColor = T0Texture.Sample(S0Sampler, IN.UV);

	float3 l_FinalColor = m_LightAmbient.xyz * l_BaseColor.xyz * m_MaterialAmbient;
	float3 l_LDiffuse = float3(0.0, 0.0, 0.0);
	float3 l_LSpecular = float3(0.0, 0.0, 0.0);

	for (int i = 0; i < MAX_LIGHTS_BY_SHADER; ++i)
	{
		CalculateSingleLight(i, IN.Normal, IN.WorldPosition, l_BaseColor.xyz, l_LDiffuse, l_LSpecular);
		l_FinalColor += l_LDiffuse + l_LSpecular;		
	}

	return float4(l_FinalColor,1.0);
}