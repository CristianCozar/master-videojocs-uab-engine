#include "Globals.fxh"

struct App2Vertex
{
	float3 pos    : POSITION;
	float3 normal : NORMAL;
};

struct Vertex2Pixel
{
	float4 pos         : SV_POSITION;
	float3 worldNormal : TEXCOORD1;
};

PS_INPUT2 mainVertexShader( App2Vertex IN )
{
	PS_INPUT2 l_Output = (PS_INPUT2)0;
	float4 lPos = float4( IN.pos.xyz, 1.0 );
	l_Output.Pos = mul( lPos, m_World );
	l_Output.Pos = mul( l_Output.Pos, m_ViewProjection );
	l_Output.WorldNormal = normalize(mul(normalize(IN.normal).xyz, (float3x3)m_World));
	l_Output.Normal = mul(IN.normal, m_World);
	l_Output.WorldPosition = mul(float4(IN.pos.xyz, 1.0), m_World).xyz;
	return l_Output;
}