#include "Globals.fxh"

PS_INPUT_SPLAT mainVS(VS_INPUT_SPLAT IN) {
	PS_INPUT_SPLAT OUT = (PS_INPUT_SPLAT)0;
	//OUT.Pos = mul(float4(IN.Pos.xyz, 1.0), WorldViewProj);
	OUT.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	OUT.Pos = mul(OUT.Pos, m_ViewProjection);
	OUT.UV = IN.UV;
	OUT.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	OUT.WorldNormal = normalize(mul(normalize(IN.Normal).xyz, (float3x3)m_World));
	OUT.Normal = mul(IN.Normal, m_World);

	return OUT;
}

float4 mainPS(PS_INPUT_SPLAT OUT) : SV_Target{

	OUT.UV.y = -OUT.UV.y;
	//float4 splat = tex2D(SplatSampler, OUT.UV);
	float4 splat = T0Texture.Sample(S0Sampler, OUT.UV);

	//float4 r = tex2D(R_ChannelSampler, OUT.UV) * splat.r;
	float4 r = T1Texture.Sample(S1Sampler, OUT.UV * 20) * splat.r;
	//float4 g = tex2D(G_ChannelSampler, OUT.UV) * splat.g;
	float4 g = T2Texture.Sample(S2Sampler, OUT.UV * 20)  * splat.g;

	//float4 b = tex2D(B_ChannelSampler, OUT.UV) * splat.b;
	float4 b = T3Texture.Sample(S3Sampler, OUT.UV * 20) * splat.b;

	return r + g + b;
}
