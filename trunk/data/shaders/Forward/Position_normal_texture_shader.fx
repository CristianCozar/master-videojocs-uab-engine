//POSITION_NORMAL_TEXTURE_SHADER.FX
#include "Globals.fxh"

Texture2D DiffuseTexture : register( t0 );
SamplerState LinearSampler : register( s0 );

//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT mainVertexShader( VS_INPUT IN )
{
	PS_INPUT l_Output = (PS_INPUT)0;
	l_Output.Pos = mul( float4(IN.Pos, 1.0), m_World );
	l_Output.Pos = mul( l_Output.Pos, m_ViewProjection );
	l_Output.UV = IN.UV; 
	l_Output.WorldNormal=normalize(mul(normalize(IN.Normal).xyz, (float3x3)m_World));
	return l_Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 mainPixelShader( PS_INPUT IN) : SV_Target
{
	IN.UV = -IN.UV;
	return DiffuseTexture.Sample(LinearSampler, IN.UV);
} 