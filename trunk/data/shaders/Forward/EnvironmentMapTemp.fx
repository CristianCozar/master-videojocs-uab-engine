/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/

	  TextureCube T0TextureCUBE  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "NAME Texture";
	  string ResourceType = "CUBE";
	  >;

	  sampler T0TextureCUBESampler = sampler_state {
	  Texture = <T0TextureCUBE>;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	 


float4x4 WorldViewProj : WorldViewProjection;
float4x4 World : World;
float4x4 ViewInverse : ViewInverse;

	  float g_EnvironmentFactor
	  <
	  string UIWidget = "slider";
	  float UIMin = 0.0;
	  float UIMax = 1.0;
	  float UIStep = 0.01;
	  string UIName =  "g_EnvironmentFactor";
	  > = 0.5;
	
struct VS_ENVIRONMENTMAP_INPUT
{
	float4 Pos : POSITION;
	float4 Normal : NORMAL;
	float2 UV: TEXCOORD0;
};

struct PS_ENVIRONMENTMAP_INPUT
{
	float4 Pos : POSITION;
	float3 Normal : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float2 UV: TEXCOORD2;
};



PS_ENVIRONMENTMAP_INPUT mainVS(VS_ENVIRONMENTMAP_INPUT IN)
{
	PS_ENVIRONMENTMAP_INPUT l_Output = (PS_ENVIRONMENTMAP_INPUT)0;
	l_Output.Pos = mul(float4(IN.Pos.xyz, 1.0), WorldViewProj);
	l_Output.WorldPos = mul(float4(IN.Pos.xyz, 1.0), World).xyz;
	

	l_Output.Normal = normalize(mul(IN.Normal, (float3x3)World));
	l_Output.UV = IN.UV;  
	
	
	return l_Output;
}

float4 mainPS(PS_ENVIRONMENTMAP_INPUT IN) : SV_Target
{
	/*l_Output.Pos=float4(m_InverseView[3].xyz+l_Output.UV*l_Far*0.95, 1.0);
 l_Output.Pos=mul(l_Output.Pos, m_ViewProjection);
 return l_Output; */
 //return texCUBE(T0TextureCUBESampler, IN.WorldPos); 

	
	
	float3 Nn=normalize(IN.Normal);	
	float3 l_EyeToWorldPosition = normalize(IN.WorldPos - ViewInverse[3].xyz);
	float3 l_ReflectVector = normalize(reflect(l_EyeToWorldPosition, Nn));
	float3 l_ReflectColor = T0TextureCUBE.Sample(T0TextureCUBESampler,l_ReflectVector).xyz*g_EnvironmentFactor;
	return texCUBE(T0TextureCUBESampler, l_ReflectVector);

	return float4(l_ReflectColor, 1.0);
	
}

technique technique0 {
	pass p0 {
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}


