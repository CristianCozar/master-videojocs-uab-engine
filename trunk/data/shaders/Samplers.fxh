#ifndef SAMPLERS_FXH
#define SAMPLERS_FXH

Texture2D T0Texture: register( t0 );	// Diffuse
Texture2D T1Texture: register( t1 );	// Bump
Texture2D T2Texture: register( t2 );	// LightMap1
Texture2D T3Texture: register( t3 );	// LightMap2
Texture2D T4Texture: register( t4 );	// LightMap3
	
Texture2D T5Texture: register( t5 );	
Texture2D T6Texture: register( t6 );	
Texture2D T7Texture: register( t7 );	

Texture2D T8Texture: register( t8 );	// ShadowMap Light1
Texture2D T9Texture: register( t9 );	// ShadowMask Light1
Texture2D T10Texture: register( t10 );	// ShadowMap Light2
Texture2D T11Texture: register( t11 );	// ShadowMask Light2
Texture2D T12Texture: register( t12 );	// ShadowMap Light3
Texture2D T13Texture: register( t13 );	// ShadowMask Light3
Texture2D T14Texture: register( t14 );	// ShadowMap Light4
Texture2D T15Texture: register( t15 );	// ShadowMask Light4

SamplerState S0Sampler: register( s0 );
SamplerState S1Sampler: register( s1 );
SamplerState S2Sampler: register( s2 );
SamplerState S3Sampler: register( s3 );
SamplerState S4Sampler: register( s4 );

SamplerState S5Sampler: register( s5 );
SamplerState S6Sampler: register( s6 );
SamplerState S7Sampler: register( s7 );

SamplerState S8Sampler: register( s8 ); //ShadowMap Light1
SamplerState S9Sampler: register( s9 ); //ShadowMask Light1
SamplerState S10Sampler: register( s10 ); //ShadowMap Light2
SamplerState S11Sampler: register( s11 ); //ShadowMask Light2
SamplerState S12Sampler: register( s12 ); //ShadowMap Light3
SamplerState S13Sampler: register( s13 ); //ShadowMask Light3
SamplerState S14Sampler: register( s14 ); //ShadowMap Light4
SamplerState S15Sampler: register( s15 ); //ShadowMask Light4

TextureCube T0TextureCUBE: register( t0 );	// Diffuse
TextureCube T1TextureCUBE: register( t1 );	// Bump
TextureCube T2TextureCUBE: register( t2 );	// LightMap
TextureCube T3TextureCUBE: register( t3 );	// Specular

#endif //SAMPLERS_FXH