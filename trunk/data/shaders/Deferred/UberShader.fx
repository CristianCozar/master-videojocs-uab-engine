// UBER SHADER
// Este shader incluye todos los shaders Deferred. Cual se ejecuta depende de los DEFINE de preprocessor.
/*
NORMAL_DEBUG: Muestra el valor de la normal en el pixel.
ALBEDO: Aplica una textura albedo usando las UV.
LIGHTS: Aplica luces
LIGHTMAP: Aplica un Lightmap usando las UV2.
BUMP: Modifica las normales para aplicar un efecto BUMP.
ANIMATED_MODEL: Un modelo animado de Cal3D
RNM: Radiosity Normal Map. Solo funcionará si están definidos LIGHTMAP y BUMP.

Orden de samplers: Albedo -> Bump -> Lightmap1 -> Lightmap2 -> Lightmap3
*/

#include "Globals.fxh"

PS_INPUT_COMPLETE mainVS(VS_INPUT_COMPLETE IN)
{
	PS_INPUT_COMPLETE l_Out = (PS_INPUT_COMPLETE)0;
	float4 l_Position;
	float3 l_Normal;
#ifdef ANIMATED_MODEL
	float4 l_TempPos=float4(IN.Pos.xyz, 1.0);
	float4 l_Indices=IN.Indices;
	l_Normal=float3(0,0,0);
	l_Position=mul(l_TempPos, m_Bones[l_Indices.x]) * IN.Weight.x;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]) * IN.Weight.y;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]) * IN.Weight.z;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]) * IN.Weight.w;
	float3x3 m;
	m[0].xyz = m_Bones[l_Indices.x][0].xyz;
	m[1].xyz = m_Bones[l_Indices.x][1].xyz;
	m[2].xyz = m_Bones[l_Indices.x][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.x;
	m[0].xyz = m_Bones[l_Indices.y][0].xyz;
	m[1].xyz = m_Bones[l_Indices.y][1].xyz;
	m[2].xyz = m_Bones[l_Indices.y][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.y;
	m[0].xyz = m_Bones[l_Indices.z][0].xyz;
	m[1].xyz = m_Bones[l_Indices.z][1].xyz;
	m[2].xyz = m_Bones[l_Indices.z][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.z;
	m[0].xyz = m_Bones[l_Indices.w][0].xyz;
	m[1].xyz = m_Bones[l_Indices.w][1].xyz;
	m[2].xyz = m_Bones[l_Indices.w][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.w;
	l_Normal=normalize(l_Normal);
#else
	l_Position = float4(IN.Pos, 1.0);
	l_Normal = IN.Normal;
#endif
	l_Position = mul(l_Position, m_World);
	l_Position = mul(l_Position, m_ViewProjection);
	l_Out.Pos = l_Position;
	l_Out.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	l_Out.WorldNormal = normalize(mul(normalize(l_Normal).xyz, (float3x3)m_World));
	l_Out.Normal = mul(l_Normal, m_World);
#ifdef ALBEDO
	l_Out.UV=IN.UV;
#endif
#ifdef LIGHTMAP
	l_Out.UV2=IN.UV2;
#endif
#ifdef BUMP
	l_Out.WorldTangent=normalize(mul(IN.Tangent.xyz, (float3x3)m_World));
	l_Out.WorldBinormal=normalize(mul(cross(IN.Tangent.xyz, IN.Normal.xyz), (float3x3)m_World));
#endif
	l_Out.Depth=l_Out.Pos;
	return l_Out;
}

PixelOutputTypePBR mainPS(PS_INPUT_COMPLETE IN) : SV_Target
{
	PixelOutputTypePBR l_Out = (PixelOutputTypePBR)0;
	// 0 ALBEDO + METALLIC
	// 1 CUBEMAPAMBIENT + OCCLUSION
	// 2 NORMAL_FACE(2) + NORMAL_PIXEL(2)
	// 3 DEPTH
	// 4 CUBEMAPSPECULAR + ROUGHNESS
	
	float3 l_Albedo = float3(1.0, 1.0, 1.0);
	l_Albedo = l_Albedo * m_MaterialColorPBR.xyz;
#ifdef ALBEDO
#ifndef ANIMATED_MODEL
	IN.UV[1] = -IN.UV[1];
#endif

#ifdef SPLAT
	//IN.UV.y = -IN.UV.y;
	float4 splat = T1Texture.Sample(S1Sampler, IN.UV);
	float4 r = T2Texture.Sample(S2Sampler, IN.UV * m_Splat_R_Channel_Tile) * splat.r;
	float4 g = T3Texture.Sample(S3Sampler, IN.UV * m_Splat_G_Channel_Tile) * splat.g;
	float4 b = T4Texture.Sample(S4Sampler, IN.UV * m_Splat_B_Channel_Tile) * splat.b;
	if (m_HasAlpha == 1.0) {
		float4 a = T5Texture.Sample(S5Sampler, IN.UV * m_Splat_A_Channel_Tile) * splat.a;
		l_Albedo = l_Albedo * (r + g + b + a);
	} else {
		l_Albedo = l_Albedo * (r + g + b);
	}
	
#else
	float4 l_Sample = T1Texture.Sample(S1Sampler, IN.UV);
	if (l_Sample.a <= 0.1)
	{
		clip(-1);
	}
	l_Albedo = l_Albedo * l_Sample.xyz;
#endif
#endif
	float3 l_Normal = normalize(IN.WorldNormal);
#ifdef BUMP
	CalculateBumpNormals(IN.WorldTangent, IN.WorldBinormal, IN.UV, m_MaterialBumpPBR, l_Normal);
#endif



#ifdef LIGHTMAP
#ifdef RNM
#ifdef BUMP
	// Solo aplicamos RNM si están activos LIGHTMAP, BUMP y RNM.
	float3 l_AmbientContrib = GetRadiosityNormalMap(l_Normal, IN.UV2, T2Texture, S2Sampler, T3Texture, S3Sampler, T4Texture, S4Sampler);
	//l_Ambient = l_Ambient * l_AmbientContrib;
#endif
#else
#ifdef BUMP
	//l_Ambient = l_Ambient * T2Texture.Sample(S2Sampler, IN.UV2).xyz;
#else
	//l_Ambient = l_Ambient * T1Texture.Sample(S1Sampler, IN.UV2).xyz;
#endif
#endif
#endif
	float l_Depth = IN.Depth.z / IN.Depth.w;
	
	// Cubemap Sample Ambient
	float4 cubemapSampleAmbient = m_CubeMapAmbient;
	cubemapSampleAmbient = pow(abs(cubemapSampleAmbient), 2.2);
	float occlusion = 1 - (1 - m_MaterialOcclusion)*0.75f;
	cubemapSampleAmbient *= occlusion;
	
	// Specular color
	float3 l_EyeToWorldPosition = normalize(IN.WorldPosition - m_ViewInverse[3].xyz);
	float3 l_ReflectVector = normalize(reflect(l_EyeToWorldPosition, l_Normal));
	float4 l_SpecularColor = T0TextureCUBE.SampleLevel(S0Sampler, l_ReflectVector, GetSpecPowToMip(m_MaterialRoughness, m_MaterialMipLevels));
	
	float3 l_PlaneNormal = normalize(IN.WorldNormal);
	l_PlaneNormal.xy = Encode(l_PlaneNormal);
	l_Normal.xy = Encode(l_Normal);
	
	l_Out.Target0 = float4(l_Albedo.rgb, m_MaterialMetallic);
	l_Out.Target1 = float4(cubemapSampleAmbient.rgb, m_MaterialOcclusion);
	l_Out.Target2 = float4(l_PlaneNormal.x, l_PlaneNormal.y, l_Normal.x, l_Normal.y);
	l_Out.Target3 = float4(l_Depth, l_Depth, l_Depth, l_Depth);
	l_Out.Target4 = float4(l_SpecularColor.rgb, m_MaterialRoughness);
	
	return l_Out;
}