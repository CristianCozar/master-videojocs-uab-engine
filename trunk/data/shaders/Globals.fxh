//Globals.fxh
#include "Samplers.fxh"

#define MAXBONES 40
#define MAX_LIGHTS_BY_SHADER 4
#define MAX_OBJECT_RAW_PARAMETER 16


cbuffer PerFrame : register( b0 )
{
    float4x4 m_View;
	float4x4 m_Projection;
	float4x4 m_ViewProjection;
	float4x4 m_ViewInverse;
	float4x4 m_InverseProjection;
	float m_Time;
}

cbuffer PerObject : register( b1 )
{
    float4x4 m_World;
}

cbuffer PerAnimatedInstance : register (b2)
{
	float4x4 m_Bones[MAXBONES];
}

cbuffer PerLights : register (b3)
{
	float4 m_LightAmbient;
	float4 m_LightEnabled;
	float4 m_LightType; //0 : OMNI, 1 : DIRECTIONAL, 2 : SPOT
	float4 m_LightPosition[MAX_LIGHTS_BY_SHADER];
	float4 m_LightDirection[MAX_LIGHTS_BY_SHADER];
	float4 m_LightAngle;
	float4 m_LightFallOffAngle;
	float4 m_LightAttenuationStartRange;
	float4 m_LightAttenuationEndRange;
	float4 m_LightIntensity;
	float4 m_LightColor[MAX_LIGHTS_BY_SHADER];
	float4 m_UseShadowMap;
	float4 m_UseShadowMask;
	float4 m_ShadowMapBias;
	float4 m_ShadowMapStrength;
	float4x4 m_LightView[MAX_LIGHTS_BY_SHADER];
	float4x4 m_LightProjection[MAX_LIGHTS_BY_SHADER];
}

cbuffer PerMaterial : register (b4)
{
	float4 m_RawData[MAX_OBJECT_RAW_PARAMETER];
	// Abajo del archivo los "shortcuts" para cada parámetro
}


struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct VS_INPUT_PARTICLES
{
	float3 Pos : POSITION;
	float4 Color : COLOR0;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;
};

struct GS_INPUT_PARTICLES
{
	float4 Pos : POSITION;
	float4 Color : COLOR0;
	float Size : TEXCOORD0;
	float Angle : TEXCOORD1;
	float SpriteIndex : TEXCOORD2;
};

struct PS_INPUT_PARTICLES
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR0;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;
	float TextureBlendFactor : TEXCOORD2;
};

struct VS_INPUT_GUI
{
	float3 Pos : POSITION;
	float4 Color : COLOR0;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT_GUI
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR0;
	float2 UV : TEXCOORD0;
};



struct VS_INPUT2
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;
};

struct VS_INPUT3
{
	float4 Pos : POSITION;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;	
	float2 UV : TEXCOORD0;
	float3 WorldNormal : TEXCOORD1;
};

struct PS_INPUT2
{
	float4 Pos : SV_POSITION;		
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float3 WorldNormal : TEXCOORD1;	
	float3 WorldPosition: TEXCOORD2;
};

struct PS_INPUT3
{
	float4 Pos : SV_POSITION;		
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float2 UV2 : TEXCOORD1;	
	float3 WorldPosition: TEXCOORD2;
};

struct PS_INPUT4
{
	float4 Pos : SV_POSITION;
	float2 UV : TEXCOORD0;
};

struct VS_INPUT_SPLAT {
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT_SPLAT {
	float4 Pos : SV_POSITION;		
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float3 WorldNormal : TEXCOORD1;	
	float3 WorldPosition: TEXCOORD2;
};



struct VS_FOG
{
	float4 Pos : POSITION;
	float2 UV : TEXCOORD0;
};

struct PS_FOG
{
	float4 Pos : SV_POSITION;
	float3 WorldPosition : TEXCOORD1;
	float2 UV : TEXCOORD0;
};

struct VS_ShadowMap
{
	float3 Pos : POSITION;
};

struct PS_ShadowMap
{
	float4 Pos : SV_POSITION;
	float4 Depth : TEXCOORD0;
};

struct VS_INPUT_COMPLETE
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
#ifdef ALBEDO
	float2 UV : TEXCOORD0;
#endif
#ifdef LIGHTMAP
	float2 UV2 : TEXCOORD1;
#endif
#ifdef BUMP
	float3 Tangent : TANGENT;
#endif
#ifdef ANIMATED_MODEL
	float4 Weight : BLENDWEIGHT;
	float4 Indices : BLENDINDICES;
#endif
};

struct PS_INPUT_COMPLETE
{
	float4 Pos : SV_POSITION;		
	float3 Normal : NORMAL;
#ifdef ALBEDO
	float2 UV : TEXCOORD0;
#endif
#ifdef LIGHTMAP
	float2 UV2 : TEXCOORD1;	
#endif
	float3 WorldPosition: TEXCOORD2;
	float3 WorldNormal : TEXCOORD3;
#ifdef BUMP
	float3 WorldTangent : TEXCOORD4;
	float3 WorldBinormal : TEXCOORD5;
#endif
	float4 Depth : TEXCOORD6;
	float3 CamToPixel: TEXCOORD7;
};

struct VS_SSR
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
};

struct PS_SSR
{
	float4 Pos : SV_POSITION;
	float3 Normal : NORMAL;
};

struct SSR_OUTPUT
{
	float4 Target0 : SV_Target0; // Reflection Mask
};

// Valores del material
static float m_MaterialAmbient = m_RawData[0].r;
static float m_MaterialBump = m_RawData[0].g;
static float m_MaterialDiffuse = m_RawData[0].b;
static float m_MaterialDisplacement = m_RawData[0].a;
static float m_MaterialFilter = m_RawData[1].r;
static float m_MaterialGlossiness = m_RawData[1].g;
static float m_MaterialOpacity = m_RawData[1].b;
static float m_MaterialReflection = m_RawData[1].a;
static float m_MaterialRefraction = m_RawData[2].r;
static float m_MaterialSelfIllum = m_RawData[2].g;
static float m_MaterialSpecularLevel = m_RawData[2].b;
static float m_MaterialSpecular = m_RawData[2].a;
static float4 m_MaterialColor = m_RawData[3];

// Valores del Skybox
static float4 m_SkyboxColor = m_RawData[0];

// Valores del material PBR
static float4 m_CubeMapAmbient = m_RawData[0];
static float4 m_MaterialColorPBR = m_RawData[1];
static float m_MaterialRoughness = m_RawData[2].r;
static float m_MaterialMipLevels = m_RawData[2].g;
static float m_MaterialMetallic = m_RawData[2].b;
static float m_MaterialOcclusion = m_RawData[2].a;
static float m_MaterialBumpPBR = m_RawData[3].r;

//Valores del Splat
static float m_Splat_R_Channel_Tile = m_RawData[3].g;
static float m_Splat_G_Channel_Tile = m_RawData[3].b;
static float m_Splat_B_Channel_Tile = m_RawData[3].a;
static float m_Splat_A_Channel_Tile = m_RawData[4].r;
static float m_HasAlpha = m_RawData[4].g;

// Valores de noise and vignetting
static float m_VignettingPct = m_RawData[0].r;
static float m_NoisePct = m_RawData[0].g;
static float m_NoiseAmount = m_RawData[0].b;
static float m_NoiseAndVignettingActive = m_RawData[0].a;

// Valores de fog
static float4 m_FogColor = m_RawData[0];
static float m_StartLinearFog = m_RawData[1].r;
static float m_EndLinearFog = m_RawData[1].g;
static float m_ExpDensityFog = m_RawData[1].b;
static float m_ExcludeFarPixels = m_RawData[1].a;
static float m_FogActive = m_RawData[2].r;

//Valores de Blur
static float2 m_TextureSize = m_RawData[0].rg; 
static float m_BlurScale = m_RawData[0].b;

//Valores de ZBlur
static float m_ZBlurActive=m_RawData[0].r; 
static float m_ZBlurShowDepths=m_RawData[0].g; 
static float m_ZBlurConstantBlur=m_RawData[0].b; 
static float m_ZBlurFocalStart=m_RawData[0].a; 
static float m_ZBlurFocalEnd=m_RawData[1].r; 
static float m_ZBlurEnd=m_RawData[1].g;
//Valores de Bloom
static float m_Treshold = m_RawData[0].r;

//Valores de Bloom Combinado
static float m_BloomActive = m_RawData[0].r;
static float m_BloomIntensity = m_RawData[0].g;
static float m_OriginalIntensity = m_RawData[0].b;
static float m_BloomSaturation = m_RawData[0].a;
static float m_OriginalSaturation = m_RawData[1].r;

//Valores de Color Grading
static float m_BrownSepiaColorGrading = m_RawData[0].r;
static float m_GreenSepiaColorGrading = m_RawData[0].g;
static float m_BlueSepiaColorGrading = m_RawData[0].b;
static float m_MonochromeColorGrading = m_RawData[0].a;
static float m_ColorColorGrading = m_RawData[1].r;
static float m_ContrastColorGrading = m_RawData[1].g;
static float m_BrightnessColorGrading = m_RawData[1].b;
static float m_ActiveColorGrading = m_RawData[1].a;

// Valores de AntiAliasing
static float m_AntiAliasingPower = m_RawData[0].r;
static float m_AntiAliasingActive = m_RawData[0].g;

// Valores de HDAO
static float m_HDAOActive = m_RawData[0].r;
static float m_HDAOShowNormals = m_RawData[0].g;
static float m_HDAOShowAO = m_RawData[0].b;
static float m_HDAOUseNormals = m_RawData[0].a;
static float2 m_RTSize=float2(m_RawData[1].r, m_RawData[1].g); 
static float m_AcceptAngle = m_RawData[1].b;
static float m_HDAOIntensity = m_RawData[1].a;
//static float g_fQTimesZNearKK=m_RawData[2].r;
//static float g_fQKK=m_RawData[2].g;
static float m_NormalScale = m_RawData[2].r;
static float m_HDAORejectRadius = m_RawData[2].g;
static float m_HDAOAcceptRadius = m_RawData[2].b;

// Valores de SSR
static float2 m_ScreenResolution = m_RawData[0].rg;
static float m_SSREnable = m_RawData[0].b;
static float m_SSROpacity = m_RawData[0].a;
static float m_OffsetScreen = m_RawData[1].r;
static float m_SSRStep = m_RawData[1].g;
static float m_SSRDepthThreshold = m_RawData[1].b;
static float m_CorrectJitter = m_RawData[1].a;

// Valores de Light Scattering
static float2 m_LightScatteringPosition = m_RawData[0].rg;
static bool m_LSActive = m_RawData[0].b;
static float m_LSExposure = m_RawData[0].a;
static float m_LSDecay = m_RawData[1].r;
static float m_LSDensity = m_RawData[1].g;
static float m_LSWeight = m_RawData[1].b;
static int m_LSNumSamples = (int)m_RawData[1].a;

//Valores de Particulas

static float sprite_sheet_width = m_RawData[0].r;
static float sprite_sheet_height = m_RawData[0].g;
static float lerp_sprite = m_RawData[0].b;
static float ratio_y = m_RawData[0].b;
static float du = 1.0 / sprite_sheet_width;
static float dv = 1.0 / sprite_sheet_height;





struct PixelOutputType
{
	float4 Target0 : SV_Target0; //Albedo (float3) + (float) SpecularFactor
	float4 Target1 : SV_Target1; //AmbientLight (float3) + (float) SpecularPow
	float4 Target2 : SV_Target2; //Normal (float3) + (float) Not used
	float4 Target3 : SV_Target3; //Depth (float4)
};

struct PixelOutputTypePBR
{
	float4 Target0 : SV_Target0; //Albedo (float3) + (float) Metallic
	float4 Target1 : SV_Target1; //CubemapSampleAmbient (float3) + (float) Occlusion
	float4 Target2 : SV_Target2; //Face normal (float2) + (float2) Pixel normal
	float4 Target3 : SV_Target3; //Depth (float4)
	float4 Target4 : SV_Target4; //CubemapSampleSpec (float3) + (float) Roughness
};

struct PixelOutputTypePBRLight
{
	float4 Target0 : SV_Target0; // Diffuse
	float4 Target1 : SV_Target1; // Specular
};

void CalculateBumpNormals(float3 worldTangent, float3 worldBinormal, float2 uv, float bumpFactor, inout float3 normal)
{
	float3 Tn = normalize(worldTangent);
	float3 Bn = normalize(worldBinormal);
	float3 bump = bumpFactor * (T2Texture.Sample(S2Sampler, uv).rgb - float3(0.5,0.5,0.5));
	normal = normal + bump.x * Tn + bump.y * Bn;
	normal = normalize(normal); 
}



static float m_LightEnabledArray[4]=(float[4])m_LightEnabled;
static float m_LightTypeArray[4]=(float[4])m_LightType;
static float m_LightAngleArray[4]=(float[4])m_LightAngle;
static float m_LightFallOffAngleArray[4]=(float[4])m_LightFallOffAngle;
static float m_LightAttenuationStartRangeArray[4]=(float[4])m_LightAttenuationStartRange;
static float m_LightAttenuationEndRangeArray[4]=(float[4])m_LightAttenuationEndRange;
static float m_LightIntensityArray[4]=(float[4])m_LightIntensity;
static float m_UseShadowMapArray[4]=(float[4])m_UseShadowMap;
static float m_ShadowMapBiasArray[4] = (float[4])m_ShadowMapBias;
static float m_ShadowMapStrengthArray[4]=(float[4])m_ShadowMapStrength;
static float4 m_BaseColor;

void CalculateSingleLight(int idLight, float3 NormalPixel, float3 WorldPixel, float3 ColorPixel, float MatSpecular, float MatSpecularLevel, out float3 DiffuseColor, out float3 SpecularColor)
{

		float3 l_Eye = m_ViewInverse[3].xyz;
		float3 l_ViewDir = normalize(l_Eye - WorldPixel);
		float3 l_Normal = normalize(NormalPixel);
		float3 Hn = float3(0.0,0.0,0.0);
		float l_DiffuseContrib = 0.0;
		float l_SpecularContrib = 0.0;
		float l_DistanceAtten = 0.0;
		float3 l_LightDirection = float3(0.0, 0.0, 0.0);
		float l_DistanceToPixel = 0.0;
		
		DiffuseColor = float3(0.0, 0.0, 0.0);
		SpecularColor = float3(0.0, 0.0, 0.0);
		float l_ShadowContrib = 1.0;
		
		if (m_LightEnabled[idLight] == 1.0)
		{
			if(m_UseShadowMapArray[idLight]) 
			{ 
				float4 l_LightViewPosition=mul(float4(WorldPixel, 1), m_LightView[idLight]); 
				l_LightViewPosition=mul(l_LightViewPosition, m_LightProjection[idLight]); 
				float2 l_ProjectedLightCoords=float2(((l_LightViewPosition.x/l_LightViewPosition.w)/2.0f)+0.5f, ((-l_LightViewPosition.y/l_LightViewPosition.w)/2.0f)+0.5f); 
				float l_DepthShadowMap; //=T8Texture.Sample(S8Sampler,l_ProjectedLightCoords).r; 
				switch(idLight)
				{
				case 0:
					l_DepthShadowMap = T8Texture.Sample(S8Sampler,l_ProjectedLightCoords).r;
					break;
				case 1:
					l_DepthShadowMap = T10Texture.Sample(S10Sampler,l_ProjectedLightCoords).r;
					break;
				case 2:
					l_DepthShadowMap = T12Texture.Sample(S12Sampler,l_ProjectedLightCoords).r;
					break;
				case 3:
					l_DepthShadowMap = T14Texture.Sample(S14Sampler,l_ProjectedLightCoords).r;
					break;
				}
				float l_LightDepth=l_LightViewPosition.z/l_LightViewPosition.w; 
				l_DepthShadowMap=l_DepthShadowMap + m_ShadowMapBias; //Bias de donde se saca?
				if((saturate(l_ProjectedLightCoords.x)==l_ProjectedLightCoords.x) 	&& 	(saturate(l_ProjectedLightCoords.y)==l_ProjectedLightCoords.y)) 
				{ 
					if(l_LightDepth>l_DepthShadowMap) 
						l_ShadowContrib=saturate(1.0f - m_ShadowMapStrength[idLight]);//0.0; 
				} 
			} 
			
			switch (m_LightType[idLight])
			{
			case 0://Omni
				l_LightDirection = WorldPixel - m_LightPosition[idLight].xyz;
				l_DistanceToPixel = length(l_LightDirection);
				l_LightDirection = l_LightDirection / l_DistanceToPixel;

				l_DiffuseContrib = saturate(dot(-l_LightDirection, l_Normal));

				Hn = normalize(l_ViewDir - l_LightDirection);
				l_SpecularContrib = pow(saturate(dot(Hn, l_Normal)), MatSpecular) * MatSpecularLevel;
				
				l_DistanceAtten = 1.0 - saturate((l_DistanceToPixel - m_LightAttenuationStartRange[idLight]) / (m_LightAttenuationEndRange[idLight] - m_LightAttenuationStartRange[idLight]));

				DiffuseColor = l_DiffuseContrib*m_LightIntensity[idLight] * m_LightColor[idLight].xyz * ColorPixel * l_DistanceAtten;
				SpecularColor = l_SpecularContrib*m_LightIntensity[idLight] * m_LightColor[idLight].xyz * l_DistanceAtten;

				break;
			case 1://Directional
				Hn = normalize(l_ViewDir - m_LightDirection[idLight].xyz);
				l_DiffuseContrib = saturate(dot(-m_LightDirection[idLight].xyz, l_Normal));
				l_SpecularContrib = pow(saturate(dot(Hn, l_Normal)), MatSpecular) * MatSpecularLevel;
				DiffuseColor = l_DiffuseContrib * m_LightIntensity[idLight] * m_LightColor[idLight].xyz * ColorPixel;
				SpecularColor = l_SpecularContrib* m_LightIntensity[idLight] * m_LightColor[idLight].xyz;

				break;
			case 2://Spot
				l_LightDirection = WorldPixel - m_LightPosition[idLight].xyz;
				l_DistanceToPixel = length(l_LightDirection);

				l_LightDirection = l_LightDirection / l_DistanceToPixel;

				l_DiffuseContrib = saturate(dot(-l_LightDirection, l_Normal));

				Hn = normalize(l_ViewDir - l_LightDirection);
				l_SpecularContrib = pow(saturate(dot(Hn, l_Normal)), MatSpecular) * MatSpecularLevel;

				l_DistanceAtten = 1.0 - saturate((l_DistanceToPixel - m_LightAttenuationStartRange[idLight]) / (m_LightAttenuationEndRange[idLight] - m_LightAttenuationStartRange[idLight]));
				float l_SpotAngle = cos(m_LightAngle[idLight]*0.5);
				float l_SpotFallOff = cos(m_LightFallOffAngle[idLight]*0.5);
				float l_DotAngle = dot(l_LightDirection, m_LightDirection[idLight].xyz);

				float l_AngleAtenuation = saturate((l_DotAngle - l_SpotFallOff) / (l_SpotAngle - l_SpotFallOff));


				DiffuseColor = l_DiffuseContrib * m_LightIntensity[idLight] * m_LightColor[idLight].xyz * ColorPixel * l_DistanceAtten * l_AngleAtenuation;
				SpecularColor = l_SpecularContrib* m_LightIntensity[idLight] * m_LightColor[idLight].xyz * l_DistanceAtten * l_AngleAtenuation;
				
				break;
			}
			DiffuseColor *= l_ShadowContrib;
			SpecularColor *= l_ShadowContrib;
			
		}	
}

static const float Pi = 3.14159265359;
// Lys constants
static const float k0 = 0.00098, k1 = 0.9921, fUserMaxSPow = 0.2425;
static const float g_fMaxT = ( exp2(-10.0/sqrt( fUserMaxSPow )) - k0)/k1;
static const int nMipOffset = 0;

// Lys function, copy/pasted from https://www.knaldtech.com/docs/doku.php?id=specular_lys
float GetSpecPowToMip(float fSpecPow, int nMips)
{
	// This line was added because ShaderTool destroys mipmaps.
	fSpecPow = 1 - pow(1 - fSpecPow, 8);
	// Default curve - Inverse of Toolbag 2 curve with adjusted constants.
	float fSmulMaxT = ( exp2(-10.0 / sqrt( fSpecPow )) - k0) / k1;
	return float(nMips - 1 - nMipOffset) * (1.0 - clamp( fSmulMaxT / g_fMaxT, 0.0, 1.0 ));
}

//https://knarkowicz.wordpress.com/2014/04/16/octahedron-normal-vector-encoding/
float2 OctWrap( float2 v )
{
	return ( 1.0 - abs( v.yx ) ) * ( v.xy >= 0.0 ? 1.0 : -1.0 );
}

float2 Encode( float3 n )
{
	n /= ( abs( n.x ) + abs( n.y ) + abs( n.z ) );
	n.xy = n.z >= 0.0 ? n.xy : OctWrap( n.xy );
	n.xy = n.xy * 0.5 + 0.5;
	return n.xy;
}

float3 Decode( float2 encN )
{
	encN = encN * 2.0 - 1.0;
	float3 n;
	n.z = 1.0 - abs( encN.x ) - abs( encN.y );
	n.xy = n.z >= 0.0 ? encN.xy : OctWrap( encN.xy );
	n = normalize( n );
	return n;
}

float3 Normal2Texture(float3 Normal)
{
	return Normal*0.5+0.5;
}

float3 Texture2Normal(float3 Color)
{
	return (Color-0.5)*2;
}

float3 GetPositionFromZDepthViewInViewCoordinates(float ZDepthView, float2 UV, float4x4 InverseProjection)
{
	// Get the depth value for this pixel
	// Get x/w and y/w from the viewport position
	float x = UV.x * 2 - 1;
	float y = (1 - UV.y) * 2 - 1;
	float4 l_ProjectedPos = float4(x, y, ZDepthView, 1.0);
	// Transform by the inverse projection matrix
	float4 l_PositionVS = mul(l_ProjectedPos, InverseProjection);
	// Divide by w to get the view-space position
	return l_PositionVS.xyz / l_PositionVS.w;
}

float3 GetPositionFromZDepthView(float ZDepthView, float2 UV, float4x4 ViewInverse, float4x4 InverseProjection)
{
	float3 l_PositionView = GetPositionFromZDepthViewInViewCoordinates(ZDepthView, UV, InverseProjection);
	return mul(float4(l_PositionView,1.0), ViewInverse).xyz;
}


float4 GetGaussianBlurFromSampler(Texture2D _Texture2D, SamplerState _SamplerState, float2 
UV, float2 TextureSize, float TextureScale) 
{ 
    float2 l_PixelKernel[7] = 
    { 
        { -1, 1 }, 
        { -1, -1 }, 
        { -1, 0 }, 
        { 0, 0 }, 
        { 1, 0 }, 
        { 1, -1 }, 
        { 1, 1 }, 
    }; 
    float l_BlurWeights[7] =  
    { 
        0.120985, 
        0.120985, 
        0.176033, 
        0.199471, 
        0.176033, 
        0.120985, 
        0.120985, 
    }; 
    float4 l_GaussianColor = 0; 
    float2 l_TextureSize=TextureSize*TextureScale;
    float l_TotalWeights=0.0; 
    for(int i = 0; i<7; ++i) 
    { 
        float4    l_Color=_Texture2D.Sample(_SamplerState,    UV+(l_PixelKernel[i] 
*l_TextureSize))*l_BlurWeights[i]; 
        l_TotalWeights+=l_BlurWeights[i]; 
        l_GaussianColor += l_Color; 
    } 
	
    return l_GaussianColor/l_TotalWeights; 
} 

float4 GetZBlurColor(float Distance, float4 SourceColor, float4 BlurColor) 
{ 
    float l_Blur=1.0; 
    if(Distance<m_ZBlurFocalStart) 
        l_Blur=max(Distance/m_ZBlurFocalStart, m_ZBlurConstantBlur); 
    else if(Distance>m_ZBlurFocalEnd) 
        l_Blur=max(1.0-(Distance-m_ZBlurFocalEnd)/m_ZBlurEnd, m_ZBlurConstantBlur); 
	
    if(l_Blur<1.0) 
        return BlurColor*(1-l_Blur)+l_Blur*SourceColor; 
    else 
        return SourceColor; 
} 

float4 GetZBlurColorDistance(float Distance, float4 SourceColor) 
{ 
    if(Distance<m_ZBlurFocalStart)
	{
		return SourceColor * float4(1,0,0,1);
	}else if(Distance < m_ZBlurFocalEnd)
	{
		return SourceColor * float4(0,1,0,1);
	}else if(Distance < m_ZBlurEnd)
	{
		return SourceColor * float4(0,0,1,1);
	}
	return SourceColor * float4(1,1,0,1);
} 

float3 GetRadiosityNormalMap(float3 Nn, float2 UV, Texture2D LightmapXTexture, SamplerState LightmapXSampler, Texture2D LightmapYTexture, SamplerState LightmapYSampler, Texture2D LightmapZTexture, SamplerState LightmapZSampler)
{
	float3 l_LightmapX = LightmapXTexture.Sample(LightmapXSampler, UV) * 2;
	float3 l_LightmapY = LightmapYTexture.Sample(LightmapYSampler, UV) * 2;
	float3 l_LightmapZ = LightmapZTexture.Sample(LightmapZSampler, UV) * 2;
	float3 l_BumpBasisX = normalize(float3(0.816496580927726, 0.5773502691896258, 0 ));
	float3 l_BumpBasisY = normalize(float3(-0.408248290463863, 0.5773502691896258, 0.7071067811865475 ));
	float3 l_BumpBasisZ = normalize(float3(-0.408248290463863, 0.5773502691896258, -0.7071067811865475));
	float3 l_RNMLighting = saturate(dot(Nn, l_BumpBasisX)) * l_LightmapX + saturate(dot(Nn, l_BumpBasisY)) * l_LightmapY + saturate(dot(Nn, l_BumpBasisZ)) * l_LightmapZ;
	return l_RNMLighting;
}
void GetNearFarFromProjectionMatrix(out float Near, out float Far, float4x4 ProjectionMatrix)
{
	Near=ProjectionMatrix[3].z/ProjectionMatrix[2].z;
	Far=(Near*ProjectionMatrix[2].z/ProjectionMatrix[2].w)/((ProjectionMatrix[2].z/ProjectionMatrix[2].w)-1.0);
}

