// UBER SHADER
// Este shader incluye todos los shaders Deferred. Cual se ejecuta depende de los DEFINE de preprocessor.
/*
NORMAL_DEBUG: Muestra el valor de la normal en el pixel.
ALBEDO: Aplica una textura albedo usando las UV.
LIGHTS: Aplica luces
LIGHTMAP: Aplica un Lightmap usando las UV2.
BUMP: Modifica las normales para aplicar un efecto BUMP.
ANIMATED_MODEL: Un modelo animado de Cal3D
RNM: Radiosity Normal Map. Solo funcionará si están definidos LIGHTMAP y BUMP.

Orden de samplers: Albedo -> Bump -> Lightmap1 -> Lightmap2 -> Lightmap3
*/

#include "Globals.fxh"

PS_INPUT_COMPLETE mainVS(VS_INPUT_COMPLETE IN)
{
	PS_INPUT_COMPLETE l_Out = (PS_INPUT_COMPLETE)0;
	float4 l_Position;
	float3 l_Normal;
	l_Position = float4(IN.Pos, 1.0);
	l_Normal = IN.Normal;
	l_Position = mul(l_Position, m_World);
	l_Position = mul(l_Position, m_ViewProjection);
	l_Out.Pos = l_Position;
	l_Out.Normal = mul(l_Normal, m_World);
	return l_Out;
}

float4 mainPS(PS_INPUT_COMPLETE IN) : SV_Target
{	
	return float4(1,1,1,1);
}