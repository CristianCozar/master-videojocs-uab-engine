#include "Globals.fxh"
#include "Samplers.fxh"

PS_INPUT4 mainVS(VS_INPUT3 IN)
{
	PS_INPUT4 l_Output = (PS_INPUT4)0;
	l_Output.UV = IN.UV;
	l_Output.Pos = IN.Pos;

	return l_Output;
}


float4 mainPS(PS_INPUT4 IN) : SV_Target
{

	float4 l_Color = T0Texture.Sample(S0Sampler, IN.UV);
	return saturate((l_Color - m_Treshold)/(1 - m_Treshold));
}