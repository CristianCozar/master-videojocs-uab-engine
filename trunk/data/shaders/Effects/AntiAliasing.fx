//AntiAliasing.FX
#include "Globals.fxh"

float4 mainPS(PS_INPUT4 IN) : SV_Target
{
	if (m_AntiAliasingActive == 0.0)
	{
		clip(-1);
	}
	float4 l_CurrentColor1 = T0Texture.Sample(S0Sampler, IN.UV + float2(0, -m_AntiAliasingPower));
	float4 l_CurrentColor2 = T0Texture.Sample(S0Sampler, IN.UV + float2(0, m_AntiAliasingPower));
	float4 l_CurrentColor3 = T0Texture.Sample(S0Sampler, IN.UV + float2(-m_AntiAliasingPower, 0));
	float4 l_CurrentColor4 = T0Texture.Sample(S0Sampler, IN.UV + float2(m_AntiAliasingPower, 0));
	#ifdef AAX8
	float4 l_CurrentColor5 = T0Texture.Sample(S0Sampler, IN.UV + float2(m_AntiAliasingPower, m_AntiAliasingPower));
	float4 l_CurrentColor6 = T0Texture.Sample(S0Sampler, IN.UV + float2(m_AntiAliasingPower, -m_AntiAliasingPower));
	float4 l_CurrentColor7 = T0Texture.Sample(S0Sampler, IN.UV + float2(-m_AntiAliasingPower, m_AntiAliasingPower));
	float4 l_CurrentColor8 = T0Texture.Sample(S0Sampler, IN.UV + float2(-m_AntiAliasingPower, -m_AntiAliasingPower));
	float4 l_CurrentColor = 0.125 * (l_CurrentColor1 + l_CurrentColor2 + l_CurrentColor3 + l_CurrentColor4 + l_CurrentColor5 + l_CurrentColor6 + l_CurrentColor7 + l_CurrentColor8);
	#else
	float4 l_CurrentColor = 0.25 * (l_CurrentColor1 + l_CurrentColor2 + l_CurrentColor3 + l_CurrentColor4);
	#endif
	
	return l_CurrentColor;
}