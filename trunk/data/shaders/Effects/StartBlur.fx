//Blur.FX
#include "Globals.fxh"
#include "Samplers.fxh"

PS_INPUT4 mainVS(VS_INPUT3 IN)
{
	PS_INPUT4 l_Output = (PS_INPUT4)0;
	l_Output.Pos = IN.Pos;
	l_Output.UV = IN.UV; 
	
	return l_Output; 
}

float4 mainPS(PS_INPUT4 IN) : SV_Target 
{	
	return T0Texture.Sample(S0Sampler, IN.UV);	
} 
