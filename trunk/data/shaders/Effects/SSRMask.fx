// Shader de máscara para SSR
// Pinta de blanco los objetos en que se debe aplicar SSR

#include "Globals.fxh"

PS_SSR mainVS(VS_SSR IN)
{
	PS_SSR l_Out = (PS_SSR)0;
	float4 l_Position;
	float3 l_Normal;
	l_Position = float4(IN.Pos, 1.0);
	l_Normal = IN.Normal;
	l_Position = mul(l_Position, m_World);
	l_Position = mul(l_Position, m_ViewProjection);
	l_Out.Pos = l_Position;
	l_Out.Normal = mul(l_Normal, m_World);
	return l_Out;
}

SSR_OUTPUT mainPS(PS_SSR IN) : SV_Target
{
	SSR_OUTPUT l_Out = (SSR_OUTPUT)0;
	l_Out.Target0 = float4(1,1,1,1);
	return l_Out;
}