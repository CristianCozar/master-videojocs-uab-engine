//Blur.FX
#include "Globals.fxh"
#include "Samplers.fxh"

PS_INPUT4 mainVS(VS_INPUT3 IN)
{
	PS_INPUT4 l_Output = (PS_INPUT4)0;
	l_Output.Pos = IN.Pos;
	l_Output.UV = IN.UV; 
	
	return l_Output; 
}

float4 mainPS(PS_INPUT4 IN) : SV_Target 
{
	//return float4(IN.UV.x, 0.0, 0.0, 1.0);
	//return T0Texture.Sample(S0Sampler, IN.UV);
	return GetGaussianBlurFromSampler(T0Texture,  S0Sampler,  IN.UV,  1/m_TextureSize, m_BlurScale); 
} 
