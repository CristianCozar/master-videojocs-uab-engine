//DrawQuad.FX
#include "Globals.fxh"

float CalcAttenuation(float Depth, float StartFog, float EndFog)
{
	return saturate((Depth-StartFog) / (EndFog-StartFog));
}

float4 CalcLinearFog(float Depth, float StartFog, float EndFog, float3 FogColor)
{
	return float4(FogColor, /*1.0-*/CalcAttenuation(Depth, StartFog, EndFog));
}

float4 CalcExp2Fog(float Depth, float ExpDensityFog, float3 FogColor)
{
	const float LOG2E = 1.442695; // = 1 / log(2)
	float l_Fog = exp2(-ExpDensityFog * ExpDensityFog * Depth * Depth * LOG2E);
	return float4(FogColor, 1.0-l_Fog);
}

float4 CalcExpFog(float Depth, float ExpDensityFog, float3 FogColor)
{
	const float LOG2E = 1.442695;
	float l_Fog = exp2(-ExpDensityFog * Depth * LOG2E);
	return float4(FogColor, 1.0-l_Fog);
}

float3 GetFogColor(float Depth, float3 CurrentColor)
{
#ifdef EXP
	float4 l_FogColor = CalcExpFog(Depth, m_ExpDensityFog, m_FogColor);
#else
#ifdef EXP2
	float4 l_FogColor = CalcExp2Fog(Depth, m_ExpDensityFog, m_FogColor);
#else
	float4 l_FogColor = CalcLinearFog(Depth, m_StartLinearFog, m_EndLinearFog, m_FogColor);
#endif
#endif
	return float3(CurrentColor * ( 1.0 - l_FogColor.a) + l_FogColor.xyz * l_FogColor.a);
}

		  	 
PS_FOG mainVS(VS_FOG IN)
{
	PS_FOG l_Output = (PS_FOG)0;
	l_Output.Pos = IN.Pos;
	l_Output.UV = IN.UV; 
	
	return l_Output; 
}

float4 mainPS(PS_FOG IN) : SV_Target
{
	if (m_FogActive == 0.0)
	{
		clip(-1);
	}
	float4 l_CurrentColor = T0Texture.Sample(S0Sampler, IN.UV);
	float l_Depth = T1Texture.Sample(S1Sampler, IN.UV)[0];
	if(l_Depth <= 0.1)
	{
		if (m_ExcludeFarPixels > 0.0) {
			clip(-1);
		}
		return float4(GetFogColor(1000.0, l_CurrentColor.xyz), l_CurrentColor.a);
	}
	float3 l_WorldPosition = GetPositionFromZDepthView(l_Depth, IN.UV, m_ViewInverse, m_InverseProjection);
	float l_DistanceEyeToWorldPosition = length(l_WorldPosition - m_ViewInverse[3].xyz);
	return float4(GetFogColor(l_DistanceEyeToWorldPosition, l_CurrentColor.xyz), l_CurrentColor.a);
	
}