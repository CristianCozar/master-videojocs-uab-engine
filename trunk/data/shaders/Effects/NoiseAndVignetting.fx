//DrawQuad.FX
#include "Globals.fxh"

		  	 
PS_INPUT4 mainVS(VS_INPUT3 IN)
{
	PS_INPUT4 l_Output = (PS_INPUT4)0;
	l_Output.Pos = IN.Pos;
	l_Output.UV = IN.UV; 
	
	return l_Output; 
}

float4 mainPS(PS_INPUT4 IN) : SV_Target
{
	if (m_NoiseAndVignettingActive == 0.0)
	{
		clip(-1);
	}
	float l_NoiseX = m_Time * sin(IN.UV.x * IN.UV.y + m_Time);
	l_NoiseX = fmod(l_NoiseX, 8) * fmod(l_NoiseX, 4);
	float l_DistortX = fmod(l_NoiseX, m_NoiseAmount);
	float l_DistortY = fmod(l_NoiseX, m_NoiseAmount+0.002); 
	float2 l_DistortUV = float2(l_DistortX, l_DistortY);
	float4 l_Vignetting = T0Texture.Sample(S0Sampler, IN.UV) * m_VignettingPct;
	float4 l_Noise = T1Texture.Sample(S1Sampler, IN.UV + l_DistortUV) * m_NoisePct;
	return l_Noise + l_Vignetting;
	return T0Texture.Sample(S0Sampler, IN.UV);
	return T1Texture.Sample(S1Sampler, IN.UV);
	
}