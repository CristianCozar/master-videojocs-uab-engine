//ZBlur.FX
#include "Globals.fxh"
#include "Samplers.fxh"

PS_INPUT4 mainVS(VS_INPUT3 IN)
{
	PS_INPUT4 l_Output = (PS_INPUT4)0;
	l_Output.Pos = IN.Pos;
	l_Output.UV = IN.UV; 
	
	return l_Output; 
}

float4 mainPS(PS_INPUT4 IN) : SV_Target 
{
	if (m_ZBlurActive == 0.0)
	{
		clip(-1);
	}
    float4 l_SourceColor=T0Texture.Sample(S0Sampler, IN.UV);  
	
	float l_Depth=T2Texture.Sample(S2Sampler, IN.UV).r; 
	
    float3  l_WorldPosition=GetPositionFromZDepthView(l_Depth, IN.UV, m_ViewInverse, m_InverseProjection); 
    float3 l_CameraPosition=m_ViewInverse[3].xyz; 
    float l_Distance=length(l_WorldPosition-l_CameraPosition); 
	if(l_Depth == 0.0)
	{
		float l_Near;
		GetNearFarFromProjectionMatrix(l_Near, l_Distance, m_Projection);
	}
	if(m_ZBlurShowDepths == 1.0)
	{
		return GetZBlurColorDistance(l_Distance, l_SourceColor);
	}else{
		return GetZBlurColor(l_Distance, l_SourceColor, T1Texture.Sample(S1Sampler, IN.UV));
	}
} 
