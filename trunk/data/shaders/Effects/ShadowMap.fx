//ShadowMap.FX
/*#include "Globals.fxh"
#include "Samplers.fxh"

PS_ShadowMap mainVS(VS_ShadowMap IN)
{
	PS_ShadowMap l_Out = (PS_ShadowMap)0;
	l_Out.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	l_Out.Pos = mul(l_Out.Pos, m_View); 
    l_Out.Pos = mul(l_Out.Pos, m_Projection); 
    l_Out.Depth = l_Out.Pos; 
    return l_Out; 
}

float4 mainPS(PS_ShadowMap IN) : SV_Target
{
	return float4(1,0,0,1);
	float l_Depth = IN.Depth.z / IN.Depth.w;
	return float4(l_Depth, l_Depth, l_Depth, 1);
}*/




#include "Globals.fxh"

PS_INPUT_COMPLETE mainVS(VS_INPUT_COMPLETE IN)
{
	PS_INPUT_COMPLETE l_Out = (PS_INPUT_COMPLETE)0;	
	float4 l_Position;
#ifdef ANIMATED_MODEL
	float4 l_TempPos=float4(IN.Pos.xyz, 1.0);
	float4 l_Indices=IN.Indices;
	l_Position=mul(l_TempPos, m_Bones[l_Indices.x]) * IN.Weight.x;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]) * IN.Weight.y;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]) * IN.Weight.z;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]) * IN.Weight.w;
	IN.Pos = l_Position;
	l_Out.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	l_Out.Pos = mul(l_Out.Pos, m_View); 
    l_Out.Pos = mul(l_Out.Pos, m_Projection); 
    l_Out.Depth = l_Out.Pos; 
	return l_Out;
#endif
	l_Out.Pos = mul(float4(IN.Pos.xyz, 1.0), m_World);
	l_Out.Pos = mul(l_Out.Pos, m_View); 
    l_Out.Pos = mul(l_Out.Pos, m_Projection); 
    l_Out.Depth = l_Out.Pos; 
    return l_Out; 
}

float4 mainPS(PS_INPUT_COMPLETE IN) : SV_Target
{	
	
	float l_Depth = IN.Depth.z / IN.Depth.w;
	
	return float4(l_Depth, l_Depth, l_Depth, 1);
	
}