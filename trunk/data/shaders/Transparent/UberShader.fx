// UBER SHADER
// Este shader incluye todos los shaders Forward. Cual se ejecuta depende de los DEFINE de preprocessor.
/*
NORMAL_DEBUG: Muestra el valor de la normal en el pixel.
ALBEDO: Aplica una textura albedo usando las UV.
LIGHTS: Aplica luces
LIGHTMAP: Aplica un Lightmap usando las UV2.
BUMP: Modifica las normales para aplicar un efecto BUMP.
ANIMATED_MODEL: Un modelo animado de Cal3D
RNM: Radiosity Normal Map. Solo funcionará si están definidos LIGHTMAP y BUMP.


Orden de samplers: Skybox -> Albedo -> Bump -> Lightmap1 -> Lightmap2 -> Lightmap3
*/

#include "Globals.fxh"



PS_INPUT_COMPLETE mainVS(VS_INPUT_COMPLETE IN)
{
	PS_INPUT_COMPLETE l_Out = (PS_INPUT_COMPLETE)0;
	float4 l_Position;
	float3 l_Normal;
#ifdef ANIMATED_MODEL
	float4 l_TempPos=float4(IN.Pos.xyz, 1.0);
	float4 l_Indices=IN.Indices;
	l_Normal=float3(0,0,0);
	l_Position=mul(l_TempPos, m_Bones[l_Indices.x]) * IN.Weight.x;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.y]) * IN.Weight.y;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.z]) * IN.Weight.z;
	l_Position+=mul(l_TempPos, m_Bones[l_Indices.w]) * IN.Weight.w;
	float3x3 m;
	m[0].xyz = m_Bones[l_Indices.x][0].xyz;
	m[1].xyz = m_Bones[l_Indices.x][1].xyz;
	m[2].xyz = m_Bones[l_Indices.x][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.x;
	m[0].xyz = m_Bones[l_Indices.y][0].xyz;
	m[1].xyz = m_Bones[l_Indices.y][1].xyz;
	m[2].xyz = m_Bones[l_Indices.y][2].xyz;
	l_Normal+=mul(IN.Normal.xyz, m)* IN.Weight.y;
	l_Normal=normalize(l_Normal);
#else
	l_Position = float4(IN.Pos, 1.0);
	l_Normal = IN.Normal;
#endif
	l_Position = mul(l_Position, m_World);
	l_Position = mul(l_Position, m_ViewProjection);
	l_Out.Pos = l_Position;
	l_Out.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), m_World).xyz;
	l_Out.WorldNormal = normalize(mul(normalize(IN.Normal).xyz, (float3x3)m_World));
	l_Out.Normal = mul(l_Normal, m_World);
	float3 camPos = float3(m_ViewInverse._41, m_ViewInverse._42, m_ViewInverse._43);
	l_Out.CamToPixel = l_Out.WorldPosition - camPos;
#ifdef ALBEDO
	l_Out.UV=IN.UV;
#endif
#ifdef LIGHTMAP
	l_Out.UV2=IN.UV2;
#endif
#ifdef BUMP
	l_Out.WorldTangent=normalize(mul(IN.Tangent.xyz, (float3x3)m_World));
	l_Out.WorldBinormal=normalize(mul(cross(IN.Tangent.xyz, IN.Normal.xyz), (float3x3)m_World));
#endif
	return l_Out;
}

float4 mainPS(PS_INPUT_COMPLETE IN) : SV_Target
{
	float3 normals = normalize(IN.WorldNormal);
#ifdef NORMAL_DEBUG
	return float4(IN.WorldNormal.xyz, 1.0);
#endif
	float4 l_BaseColor = float4(1.0, 1.0, 1.0, 1.0);
	l_BaseColor = l_BaseColor * m_MaterialColorPBR;
#ifdef ALBEDO
#ifndef ANIMATED_MODEL
	IN.UV[1] = -IN.UV[1];
#endif
	l_BaseColor = l_BaseColor * T1Texture.Sample(S1Sampler, IN.UV);
#endif
	float3 l_FinalColor = l_BaseColor.xyz;
#ifdef BUMP
	CalculateBumpNormals(IN.WorldTangent, IN.WorldBinormal, IN.UV, m_MaterialBumpPBR, normals);
#endif
#ifdef LIGHTMAP
#ifdef RNM
#ifdef BUMP
	// Solo aplicamos RNM si están activos LIGHTMAP, BUMP y RNM.
	float3 l_AmbientContrib = GetRadiosityNormalMap(normals, IN.UV2, T3Texture, S3Sampler, T4Texture, S4Sampler, T5Texture, S5Sampler);
	l_FinalColor = l_FinalColor * l_AmbientContrib;
#endif
#else
#ifdef BUMP
	l_FinalColor = l_FinalColor * T3Texture.Sample(S3Sampler, IN.UV2).xyz;
#else
	l_FinalColor = l_FinalColor * T2Texture.Sample(S2Sampler, IN.UV2).xyz;
#endif
#endif
#endif
	
	

#ifdef LIGHTS
	float3 camToPixelRaw = IN.CamToPixel;
	float3 camToPixel = normalize(camToPixelRaw);
	float3 camNormalReflect = reflect(camToPixel, normals);
	float roughnessSample = abs(m_MaterialRoughness);
	float4 cubemapSampleAmbient = m_CubeMapAmbient;//texCUBE(SkyboxSampler, normal);
	float4 l_camNormalReflectLod = float4(camNormalReflect.xyz, GetSpecPowToMip(roughnessSample, m_MaterialMipLevels));
	float4 cubemapSampleSpec = T0TextureCUBE.SampleLevel(S0Sampler, l_camNormalReflectLod.xyz, l_camNormalReflectLod.a);
	roughnessSample = pow(roughnessSample, 0.4);
	// Creating specular color and intensity, this needs to be done before gamma correction
	float4 specularColor = float4(lerp(0.04f.rrr, l_BaseColor.rgb, m_MaterialMetallic), 1.0f);
	l_BaseColor.rgb = lerp(l_BaseColor.rgb, 0.0f.rrr, m_MaterialMetallic);
	// Gamma correct textures
	l_BaseColor = pow(abs(l_BaseColor), 2.2);
	roughnessSample = pow(abs(roughnessSample), 2.2);
	cubemapSampleAmbient = pow(abs(cubemapSampleAmbient), 2.2);
	cubemapSampleSpec = pow(abs(cubemapSampleSpec), 2.2);
	// Add in occlusion
	float occlusion = 1 - (1 - m_MaterialOcclusion)*0.75f;
	cubemapSampleAmbient *= occlusion;
	cubemapSampleSpec *= occlusion;
	// Material settings
	float roughPow2 = roughnessSample*roughnessSample;
	float roughPow22 = roughPow2*roughPow2;
	float roughPow2Half = roughPow2*0.5;
	float4 l_LDiffuse = float4(0.0, 0.0, 0.0, 0.0);
	float4 l_LSpecular = float4(0.0, 0.0, 0.0, 0.0);
	for (int i = 0; i < MAX_LIGHTS_BY_SHADER; ++i)
	{
		int idLight = i;
		
		if (m_LightEnabled[idLight] == 1.0)
		{
			float3 l_LightDirection = float3(0.0, 0.0, 0.0);
			float3 lightColor = m_LightColor[idLight].xyz;
			float l_DistanceToPixel = 0.0;
			switch (m_LightType[idLight])
			{
			case 0: //Omni
				l_LightDirection = IN.WorldPosition - m_LightPosition[idLight].xyz;
				l_DistanceToPixel = length(l_LightDirection);
				l_LightDirection = l_LightDirection / l_DistanceToPixel;
				break;
			case 1: // Directional
				l_LightDirection = m_LightDirection[idLight].xyz;
				break;
			case 2: // Spot
				l_LightDirection = IN.WorldPosition - m_LightPosition[idLight].xyz;
				l_DistanceToPixel = length(l_LightDirection);
				l_LightDirection = l_LightDirection / l_DistanceToPixel;
				break;
			}
			
			float lightIntensity = m_LightIntensity[idLight];
			l_LDiffuse += float4((saturate(dot(-l_LightDirection, normals)))* lightColor * lightIntensity * l_BaseColor.rgb, 1.0f);
			// SPECULAR
			float3 halfVector = normalize(camToPixel + l_LightDirection);
			float halfVecDotNorm = dot(halfVector, normals);
			float normDotLight = max(dot(normals, -l_LightDirection), 0);
			float normDotCam = max(dot(normals, -camToPixel), 0);
			// Fresnel term
			float4 schlickFresnel = specularColor + (1 - specularColor) * (pow(1 - dot(camToPixel, halfVector), 5) / (6 - 5*(1-roughnessSample)));
			// Distribution term
			float denominator = halfVecDotNorm*halfVecDotNorm*(roughPow22 - 1) + 1;
			float ggxDistribution = roughPow22/(Pi*denominator*denominator);
			// Geometry term
			float schlickGgxGeometry = (normDotCam / (normDotCam*(1 - roughPow2Half) + roughPow2Half));
			//float ggxGeometry = (2*normDotCam) / (normDotCam + sqrt(roughPow2 +
			// (1 - roughPow2)*pow(normDotCam, 2))); // ggxG without schlick approximation
			// Add the spec from this light
			l_LSpecular += float4(((schlickFresnel*ggxDistribution*schlickGgxGeometry) / 4*normDotLight*normDotCam).rrr * lightColor*specularColor.rgb*lightIntensity, 1.0f);
		}
	}
	//end per light
	// Ambient cubemap light
	l_LDiffuse.rgb += cubemapSampleAmbient.rgb*l_BaseColor.rgb;
	// Specular cubemap light
	float normDotCam = max(dot(lerp(IN.Normal, normals, max(dot(IN.Normal, -camToPixel), 0)), -camToPixel), 0);
	float4 schlickFresnel = saturate(specularColor + (1 - specularColor)*pow(1 - normDotCam, 5));
	l_FinalColor = lerp(l_LDiffuse, cubemapSampleSpec, schlickFresnel);
	l_FinalColor += l_LSpecular;
#endif
	return float4(l_FinalColor,l_BaseColor.a);
}