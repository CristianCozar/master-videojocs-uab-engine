
	  float g_SpecularExponent
	  <
	  string UIWidget = "slider";
	  float UIMin = 1.0;
	  float UIMax = 80.0;
	  float UIStep = 0.01;
	  string UIName =  "g_SpecularExponent";
	  > = 20.0;
	
	  texture Diffuse  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Diffuse Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D DiffuseSampler = sampler_state {
	  Texture = <Diffuse>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	
		  float3 Lamp0Direction : DIRECTION
		  <
		  string Object = "Directional Light 0";
		  string UIName =  "Lamp 0 Direction";
		  string Space = "World";
		  > = {-0.5f,2.0f,1.25f};
	
		  float3 Lamp_0_color : COLOR <
		  string Object = "Directional Light 0";
		  string UIName =  "Lamp 0 Color";
		  string UIWidget = "Color";
		  > = {1.0f,1.0f,1.0f};
		

float4x4 View : View;
float4x4 ViewInverse : ViewInverse;
float4x4 World : World;
float4x4 Projection : Projection;

struct TVertexVS
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct TVertexPS
{
	float4 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
	float3 WorldPosition: TEXCOORD1;
};

TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out=(TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
	l_Out.Pos=mul(l_Out.Pos, View);
	l_Out.Pos=mul(l_Out.Pos, Projection);
	l_Out.Normal=normalize(mul(IN.Normal, (float3x3)World));
	l_Out.WorldPosition = mul(float4(IN.Pos.xyz, 1.0), World).xyz;
	l_Out.UV = IN.UV;
	return l_Out;
}
float4 mainPS(TVertexPS IN) : COLOR
{
	float3 l_Eye=ViewInverse[3].xyz;
	float3 l_ViewDir = normalize(l_Eye - IN.WorldPosition);
	float3 Hn = normalize(l_ViewDir-Lamp0Direction);
	float3 l_Normal = normalize(IN.Normal);
	float l_DiffuseContrib = saturate(dot(-Lamp0Direction,l_Normal));
	float l_SpecularContrib = pow(saturate(dot(Hn,l_Normal)), g_SpecularExponent ) ;
	return (l_SpecularContrib ,l_SpecularContrib ,l_SpecularContrib ,1.0);
	return float4 (l_DiffuseContrib,l_DiffuseContrib,l_DiffuseContrib,1.0);
	return float4(l_Normal,1.0);
	return float4 (Lamp0Direction,1.0);
	return float4 (Lamp_0_color,1.0);
	return float4(1.0,0.0,0.0,1.0);
	return tex2D(DiffuseSampler, IN.UV);
}
technique technique0
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
} 