
	  texture Diffuse  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Diffuse Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D DiffuseSampler = sampler_state {
	  Texture = <Diffuse>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	

	  texture Noise  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Noise Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D NoiseSampler = sampler_state {
	  Texture = <Noise>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  	  float DisolvePCT
	  	  <
	  	  string UIWidget = "slider";
	  	  float UIMin = 0.0;
	  	  float UIMax = 1.0;
	  	  float UIStep = 0.01;
	  	  string UIName =  "DisolvePCT";
	  	  > = 0.0;
		  
	  	
	
	  
float4x4 View : View;
float4x4 World : World;
float4x4 Projection : Projection;

struct TVertexVS
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct TVertexPS
{
	float4 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out=(TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
	l_Out.Pos=mul(l_Out.Pos, View);
	l_Out.Pos=mul(l_Out.Pos, Projection);
	l_Out.Normal=normalize(mul(IN.Normal, (float3x3)World));
	l_Out.UV = IN.UV;
	return l_Out;
}
float4 mainPS(TVertexPS IN) : COLOR
{
#if DEBUG_COLOR
	return float4(1,0,0,1);
#else
	float l_PCT = tex2D(NoiseSampler, IN.UV).x;
	
	
	if (DisolvePCT<0.5)
		return tex2D(DiffuseSampler, IN.UV) * (1- (DisolvePCT*2));
	else 
	{
		if (l_PCT<((DisolvePCT-0.5)*2))
			clip(-1); 	
		return float4(0.0,0.0,0.0,1.0);
	}
	
	
	//return float4(1.0,0.0,0.0,1.0);
	//return tex2D(NoiseSampler, IN.UV);
	return tex2D(DiffuseSampler, IN.UV);
#endif
}
technique technique0
{
	pass p0
	{
		CullMode = CW;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}