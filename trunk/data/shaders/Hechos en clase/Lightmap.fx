
	  texture Diffuse  <
	  string ResourceName = "";//Optional default file name
	  string UIName =  "Diffuse Texture";
	  string ResourceType = "2D";
	  >;

	  sampler2D DiffuseSampler = sampler_state {
	  Texture = <Diffuse>;
	  MinFilter = Linear;
	  MagFilter = Linear;
	  MipFilter = Linear;
	  AddressU = Wrap;
	  AddressV = Wrap;
	  };
	  
	  
	  	  texture LightMap  <
	  	  string ResourceName = "";//Optional default file name
	  	  string UIName =  "LightMap Texture";
	  	  string ResourceType = "2D";
	  	  >;
	  
	  	  sampler2D LightMapSampler = sampler_state {
	  	  Texture = <LightMap>;
	  	  MinFilter = Linear;
	  	  MagFilter = Linear;
	  	  MipFilter = Linear;
	  	  AddressU = Wrap;
	  	  AddressV = Wrap;
	  	  };
	  	
	

float4x4 View : View;
float4x4 World : World;
float4x4 Projection : Projection;

struct TVertexVS
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

struct TVertexPS
{
	float4 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD0;
};

TVertexPS mainVS(TVertexVS IN)
{
	TVertexPS l_Out=(TVertexPS)0;
	l_Out.Pos=mul(float4(IN.Pos.xyz, 1.0), World);
	l_Out.Pos=mul(l_Out.Pos, View);
	l_Out.Pos=mul(l_Out.Pos, Projection);
	l_Out.Normal=normalize(mul(IN.Normal, (float3x3)World));
	l_Out.UV = IN.UV;
	return l_Out;
}
float4 mainPS(TVertexPS IN) : COLOR
{
	//return float4 (1.0,0.0,0.0,1.0);
	return tex2D(DiffuseSampler, IN.UV)*tex2D(LightMapSampler, IN.UV);
}
technique technique0
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}