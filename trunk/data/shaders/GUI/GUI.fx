//GUI.FX
#include "Globals.fxh"

		  	 
PS_INPUT_GUI mainVS(VS_INPUT_GUI IN)
{
	PS_INPUT_GUI l_Output = (PS_INPUT_GUI)0;
	l_Output.Pos = float4(IN.Pos, 1.0);
	l_Output.UV = IN.UV;
	l_Output.Color = IN.Color;
	
	return l_Output; 
}

float4 mainPS(PS_INPUT_GUI IN) : SV_Target
{
	return T0Texture.Sample(S0Sampler, IN.UV) * IN.Color;
}