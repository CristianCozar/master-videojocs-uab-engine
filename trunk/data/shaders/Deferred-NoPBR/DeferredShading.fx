#include "Globals.fxh"

float4 mainPS(PS_INPUT4 IN) : SV_Target
{
	// Obtenemos la profundidad, y a partir de ella, la posici�n en mundo del pixel
	float l_Depth = T3Texture.Sample(S3Sampler, IN.UV).r;
	float3 l_WorldPosition = GetPositionFromZDepthView(l_Depth, IN.UV, m_ViewInverse, m_InverseProjection);
	
	// Obtenemos la normal a partir de la textura
	float3 l_Normal = T2Texture.Sample(S2Sampler, IN.UV).xyz;
	l_Normal = normalize(Texture2Normal(l_Normal));
	
	// Obtenemos el factor especular y el nivel especular del material
	float l_SpecularFactor = T1Texture.Sample(S1Sampler, IN.UV).a;
	float l_SpecularLevel = T0Texture.Sample(S0Sampler, IN.UV).a;
	if (l_Depth == 0) // Para los p�xeles en los que la luz no deber�a incidir, no se aplica especular.
	{
		l_SpecularFactor = 1.0;
		l_SpecularLevel = 0.0;
	}
	
	// Obtenemos el color de la textura de albedo + ambient + lightmap
	float3 l_LightmapColor = T1Texture.Sample(S1Sampler, IN.UV).xyz;
	
	// Obtenemos el color de la textura albedo
	float3 l_AlbedoColor = T0Texture.Sample(S0Sampler, IN.UV).xyz;
	
	// Calculamos las luces
	float3 l_LDiffuse = float3(0.0, 0.0, 0.0);
	float3 l_LSpecular = float3(0.0, 0.0, 0.0);
	CalculateSingleLight(0, l_Normal, l_WorldPosition, l_AlbedoColor, l_SpecularFactor, l_SpecularLevel, l_LDiffuse, l_LSpecular);
	float3 l_FinalColor = l_LDiffuse + l_LSpecular + l_LightmapColor;
	return float4(l_FinalColor, 1.0);
}