-- /////////// Gameobject Engine CLASS /////////// --
class 'GameObjectEngine'

function GameObjectEngine:__init()
	self.gameObjects = {}
	self.gameObjectsToDelete = {}
	self.tags = {}
	self.load_scene_indicator = false
end

function GameObjectEngine:register_component(objectName, componentName)
	add_new_info_log("[GameObjectEngine] Registrando componente " .. componentName .. " en objeto " .. objectName)
	self.gameObjects[objectName]:register_component(_G[componentName](componentName))
end

function GameObjectEngine:unregister_component(objectName, componentName)
	self.gameObjects[objectName]:unregister_component(componentName)
end

function GameObjectEngine:register_object(objectName)
	self.gameObjects[objectName] = GameObject(objectName)
end

function GameObjectEngine:update(dt)
	if (self.load_scene_indicator == true) then
		self:load_scene()
	end
	
	for key,value in pairs(self.gameObjects) do
		if value ~= nil then
		--	add_new_info_log("Updating: " .. value.name)
			value:update(dt)
		--	add_new_info_log("DoneUpdating: " .. value.name)

		end
	end
	
	for i=1,#self.gameObjectsToDelete do
		self.gameObjectsToDelete[i]:destroy()
	end
	--for key,value in pairs(self.gameObjectsToDelete) do
	--	if value ~= nil then			
	--		value:destroy()
	--	end
	--end
	self.gameObjectsToDelete = {}
	
end


function GameObjectEngine:destroy_object(name)
	self:remove_all_tags(name)
	self.gameObjects[name] = nil
end

function GameObjectEngine:start()
	add_new_info_log("Ejecutando start global")
	for key,value in pairs(self.gameObjects) do
		add_new_info_log("GameObject: " .. key)
		value:start()
	end
end

function GameObjectEngine:destroy()
	for key,value in pairs(self.gameObjects) do
		value:destroy()
	end
	add_new_info_log("Destroy GOE")
	self.tags = {}
end

function GameObjectEngine:add_tag(name, tag)
	if type(self.tags[tag]) ~= "table" then
		self.tags[tag] = {}
	end
	go = self:get_object_by_name(name)
	table.insert(self.tags[tag], go)
	go:add_tag(tag)
end

function GameObjectEngine:remove_tag(name, tag)

	go = self:get_object_by_name(name)
	add_new_info_log("Looking for " .. name .. " in tag list " .. tag)
	for i, v in pairs(self.tags[tag]) do
        if v.name == name then
			--add_new_info_log("Removed gameobject " .. name .. " in tag list " .. tag .. " in position " .. i)
			table.remove(self.tags[tag], i)
			break
        end
    end
	go:remove_tag(tag)
end

function GameObjectEngine:remove_all_tags(name)
	go = self:get_object_by_name(name)
	--add_new_info_log("Removing all tags from " .. name)
	for i, v in pairs(go.tags) do
        self:remove_tag(name, v)
    end
end

function GameObjectEngine:get_object_by_name(name)
	return self.gameObjects[name]
end

function GameObjectEngine:get_object_by_tag(tag)
	objects = self:get_objects_by_tag(tag)
	if objects ~= nil then
		return objects[1]
	else
		return nil
	end
end

function GameObjectEngine:get_objects_by_tag(tag)
	if (type(self.tags[tag]) == "table") then
		return self.tags[tag]
	end
	return {}
end

function GameObjectEngine:request_scene_load(name)
	self.scene_to_load = name
	self.load_scene_indicator = true
end

function GameObjectEngine:load_scene()
	load_scene(self.scene_to_load)
	self:start()
	self.load_scene_indicator = false
end

function GameObjectEngine:change_scene(name)
	remove_current_scene()
	self:request_scene_load(name)
end

function GameObjectEngine:request_destroy(go)
	table.insert(self.gameObjectsToDelete, go)
end

add_new_info_log("Ejecutando script GameObjectEngine")
engine = GameObjectEngine()

-- ////////////////////////////////////////// --

-- ////////////// GENERAL API /////////////// --

function remove_scene(name)
	engine:destroy()
	unload_scene(name)
end

function remove_current_scene()
	engine:destroy()
	unload_current_scene()
end

function register_component_class(name)
	add_new_info_log("Registrando componente " .. name)
	engine:register_component_class(name)
end

function register_component(objectName, componentName)
	add_new_info_log("Registrando componente " .. componentName .. " en objeto " .. objectName)
	engine:register_component(objectName, componentName)
end

function register_object(objectName)
	engine:register_object(objectName)
end

function unregister_component(objectName, componentName)
	engine:unregister_component(objectName, componentName)
end

function start_global()
	engine:start()
end

function start(name)
	engine.gameObjects[name]:start()
end

function update(dt)
	engine:update(dt)
end

function ondestroy(name)
	engine.gameObjects[name]:ondestroy()
end

function add_tag(name, tag)
	engine:add_tag(name, tag)
end

function remove_tag(name, tag)
	engine:remove_tag(name, tag)
end

function set_transform(name, transform)
	engine.gameObjects[name].transform = transform
	engine.gameObjects[name].transform.gameObject = engine.gameObjects[name]
end

function get_transform(name)
	return engine.gameObjects[name].transform
end

function get_object_by_name(name)
	return engine.gameObjects[name]
end

function instance_return(instantiable, name, scene, layer)
	add_new_info_log("Instance Start0")
	new_object_name = instance(instantiable, name, scene, layer)
	add_new_info_log("Instance Start1")
	engine:get_object_by_name(new_object_name):start()
	add_new_info_log("Instance Start2")
	return engine:get_object_by_name(new_object_name)
end

function delete_named_node(name)
	add_new_info_log("Eliminando gameobject " .. name)
	delete_node(name)
end

function delete_nodes_in_layer(scene, layer)
	delete_all_nodes(scene, layer)
end

function set_parameter(object_name, component_name, parameter_name, value)
	engine:get_object_by_name(object_name):set_parameter(component_name, parameter_name, value)
end

function on_trigger_enter(name, otherName)
	if engine.gameObjects[name] ~= nil then
		engine.gameObjects[name]:on_trigger_enter(otherName)
	end
end

function on_trigger_exit(name, otherName)
	if engine.gameObjects[name] ~= nil then
		engine.gameObjects[name]:on_trigger_exit(otherName)
	end
end

function on_collision_enter(name, otherName)
	engine.gameObjects[name]:on_collision_enter(otherName)
end

function on_collision_stay(name, otherName)
	engine.gameObjects[name]:on_collision_stay(otherName)
end

function on_collision_exit(name, otherName)
	engine.gameObjects[name]:on_collision_exit(otherName)
end

function on_shape_hit(name, otherName, hitWorldPosition, hitWorldNormal)
	engine.gameObjects[name]:on_shape_hit(otherName, hitWorldPosition, hitWorldNormal)
end

function RockSpawn(b) --b = gameObject a mover
--	add_new_info_log("Move Rock")	
	c = b:get_component("MoveRock")
	c:activate()
end

function RockSpawnReturn() --b = gameObject a mover
	--add_new_info_log("Move Rock")	
	a = engine:get_object_by_tag("blockPath")
	b = a:get_component("MoveRock")
	b:return_to_position()
end

function block_path(b) --b = gameObject a mover
	--add_new_info_log("Block Path")	
	c = b:get_component("MoveRock")
	c:return_to_position()
end

function RockSpawn2(b) --b = gameObject a mover
	c = b:get_component("MoveRock")
	c:activate()
	d = engine:get_object_by_tag("A2Emergente")
	e = d:get_component("MoveRock")
	e:activate()	
end

function TreeFall(b) 
	add_new_info_log("FALLING")	
	a = engine:get_object_by_tag("TreeLeaves")
	a:destroy()
	delete_actor(b.name)
	add_actor(b.name, b.transform.position, float3(3.0, 50.0, 3.5))
	--create_static_box(b.name, 3.0,50.0,3.5, b.transform.position,quat(0.0, 0.0, 1.0, 0.0), "Default")
	d = b:get_component("MoveRock")
	d:activate()
end

function TreeFall2(b)
	a = engine:get_object_by_tag("TreeLeaves")
	a:destroy()
	c = b:get_component("MoveRock")
	c:activate()
	d = engine:get_object_by_tag("ActivateCameraEvent")
	e = d:get_component("ActivateCameraEvent")
	e:activate()
end

function activate_portal(go)
		--Activat Portal
		a = go:get_component("Portal")
		a:activate()
		set_visible("PortalParticles", true)
end


-- //////////////// UTILS /////////////// --
require "math"

function index_of_value(t, val)
	local index={}
	for k,v in pairs(t) do
	   index[v]=k
	end
	return index[val]
end

function vect3_to_string(vect3)
	return vect3.x .. "," .. vect3.y .. "," .. vect3.z
end

function scale(vect3_1, vect3_2)
	return float3(vect3_1.x * vect3_2.x, vect3_1.y * vect3_2.y, vect3_1.z * vect3_2.z)
end

function float_inverse_lerp(minVal, maxVal, val)
	
	if minVal == maxVal then
		return minVal
	end
	
	if val < minVal then
		val = minVal
	elseif val > maxVal then
		val = maxVal
	end
	
	result = (val - minVal) / (maxVal - minVal)
	
	return result
end

function float_lerp(minVal, maxVal, t)
	
	if t < 0.0 then
		t = 0.0
	elseif t > 1.0 then
		t = 1.0
	end
	
	result = (maxVal - minVal) * t + minVal
	
	return result
end

function deg_2_rad(angle)
	return angle * (2.0 * math.pi) / 360.0
end

function rad_2_deg(angle)
	return angle * 360.0/(2.0 * math.pi)
end

function float_lerp_angle(minVal, maxVal, val)
	result = float_repeat(maxVal - minVal, 360.0)
	if result > 180.0 then
		result = result - 360.0
	end
	
	return minVal + result * float_clamp(0.0, 1.0, val)
end

function float_repeat(a, b)
	return a - math.floor(a / b) * b
end

function float_clamp(minVal, maxVal, val)
	if val > maxVal then
		val = maxVal
	elseif val < minVal then
		val = minVal
	end
	
	return val
end

function float_move_towards(current, target, maxDelta)
		if math.abs(target - current) <= maxDelta then
			return target
		end
		
		return current + float_sign(target - current) * maxDelta
end

function vector3_move_towards(current, target, maxDelta)

		a = target - current
		magnitude = a:length()
		if magnitude <= maxDelta or magnitude == 0.0 then
			return target
		end
		return current + a / magnitude * maxDelta
end

function float_sign(val)
	if val < 0 then
		return -1.0
	else 
		return 1.0
	end
end

function cross_product( a, b )
        local x, y, z
        x = a.y * (b.z or 0) - (a.z or 0) * b.y
        y = (a.z or 0) * b.x - a.x * (b.z or 0)
        z = a.x * b.y - a.y * b.x
        return float3(x, y, z)
end

function vector3_lenght(vect3_1)
	return math.sqrt((vect3_1.x * vect3_1.x) + (vect3_1.y * vect3_1.y) + (vect3_1.z * vect3_1.z)) 
end

function dot_product(vect3_1, vect3_2)
	vect3_1 = normalize(vect3_1)
	vect3_2 = normalize(vect3_2)
	local angle = (vect3_1.x * vect3_2.x) + (vect3_1.y * vect3_2.y) + (vect3_1.z * vect3_2.z)
	return math.deg(math.acos(angle))
end

function normalize(vect3_1)
	local lenght = math.sqrt((vect3_1.x * vect3_1.x) + (vect3_1.y * vect3_1.y) + (vect3_1.z * vect3_1.z)) 

	if lenght == 0.0 then
		return float3(0.0, 0.0, 0.0)
	else
		return float3(vect3_1.x / lenght, vect3_1.y / lenght, vect3_1.z / lenght)
	end
end

function is_zero(vect3_1)
	return vect3_1.x == 0.0 and vect3_1.y == 0.0 and vect3_1.z == 0.0 
end

function static_attack_check(attacker, target, attack_length, attack_angle, attack_height, attack_force, damage_bonus, current_phase)
	--[[
		attacker: transform
		target: transform
		attack_length: float
		attack_angle: float, grados
		attack_height: float
		attack_force: int
		damage_bonus: int, usar 0 de default
		current_phase: int, usar 0 de default
		
		return: cantidad de daño causada, -1 si no se ha impactado, 0 si no se ha causado daño pero sí impactado
	]]--
	
	local damage_done = -1.0
	local height_dif = math.abs(target.position.y - attacker.position.y);
	
	if height_dif <= attack_height then
		local distance = attacker.position:distance(target.position)
		if distance <= attack_length then
			local angle = math.deg(attacker.forward:angle(target.position - attacker.position))
			if angle <= attack_angle then
				local health = target.gameObject:get_component("Health")
				if health ~= nil then
					damage_done = health:damage(attack_force + attack_force * (current_phase * damage_bonus / 100.0))
				else
					local shield = target.gameObject:get_component("SpecialAbilitySaltShield")
					if shield ~= nil then
						damage_done = shield:damage_shield(attack_force + attack_force * (current_phase * damage_bonus / 100.0))
					end
				end
			end
		end
	end
	
	return damage_done
end

-- ////////////////////////////////////////// --
