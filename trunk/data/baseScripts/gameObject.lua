
-- /////////// GAME OBJECT CLASS /////////// --
class 'GameObject'

function GameObject:__init(name)
    self.name = name
	self.components = {}
	self.tags = {}
	self.transform = nil	
	self.gameObjectsOnTrigger = {}
	self.timerBetweenTriggerStay = 0.5
	self.currentTimer = 0.0
end

function GameObject:update(dt)
	self:trigger_controller(dt)
	for key,value in pairs(self.components) do
	--add_new_info_log("Updating: " .. value.name.. " from " ..self.name)
		value:update(dt)		
		--add_new_info_log("DoneUpdating: " .. value.name.. " from " ..self.name)		
	end
end

function GameObject:start()
	add_new_info_log("Ejecutando start")
	for key,value in pairs(self.components) do
		value:start()
	end
end

function GameObject:destroy()
	self:unregister_all_components()
	delete_named_node(self.name)
	engine:destroy_object(self.name)
end

--[[
function GameObject:trigger_controller(dt)
	self.currentTimer = self.currentTimer + dt
	--add_new_info_log("Update:" ..self.currentTimer)
	--if(self.timerBetweenTriggerStay < self.currentTimer) then	
		for k,v in pairs(self.gameObjectsOnTrigger) do
			for key,value in pairs(self.components) do
				value:on_trigger_stay(v,dt)
			end
		end
	--	self.currentTimer = 0.0
--	end
end
]]--

function GameObject:trigger_controller(dt)
    for key,value in pairs(self.components) do
        --add_new_info_log("Componente " .. value.name)
        for k,v in pairs(self.gameObjectsOnTrigger) do
			--add_new_info_log("1")
			if( engine:get_object_by_name(v) ~= nil) then
			--	add_new_info_log("Name: " ..v)
				value:on_trigger_stay(v,dt)
			--	add_new_info_log("2")
			else
			--	add_new_info_log("3")
				local i = index_of_value(self.gameObjectsOnTrigger, v)
				table.remove(self.gameObjectsOnTrigger, i)
			end
            --add_new_info_log("Elemento de la lista " .. v)
        end
        --add_new_info_log("Fin Componente")
    end
end


function GameObject:ondestroy()
	add_new_info_log("OnDestroy: " ..self.name)
	for key,value in pairs(self.components) do
		value:destroy()
	end
end

function GameObject:register_component(c)
	add_new_info_log("[" .. self.name .. "][GameObject] Registrando " .. c.name)
	c.gameObject = self
	c.transform = self.transform
	self.components[c.name] = c
end

function GameObject:add_component(cname)
	self:register_component(_G[cname](cname))
	self.components[cname]:start()
	return self.components[cname]
end

function GameObject:unregister_component(cname)
	self.components[cname]:ondestroy()
	self.components[cname].gameObject = nil
	self.components[cname].transform = nil
	i = index_of_value(self.components, cname)
	table.remove(self.components, i)
end

function GameObject:unregister_all_components()
	for k,v in pairs(self.components) do
		v:ondestroy()
		v.gameObject = nil
		v.transform = nil
	end
	self.components = {}
end

function GameObject:get_component(cname)
	return self.components[cname]
end

function GameObject:set_parent(name)
	set_parent(name, self.name)
end

function GameObject:detach_from_parent()
	detach_from_parent(self.name)
end

function GameObject:detach_children(name)
	detach_children(self.name, name)
end

function GameObject:add_children(name)
	add_children(self.name, name)
end

function GameObject:add_tag(tag)
	table.insert(self.tags, tag)
end

function GameObject:remove_tag(tag)
	--add_new_info_log("TemoveTagGO")
	i = index_of_value(self.tags, tag)
	table.remove(self.tags, i)
end

function GameObject:remove_all_tags()
	self.tags = {}
end

function GameObject:has_tag(tag)
	for k,v in pairs(self.tags) do
		if v == tag then
			return true
		end
	end
	return false
end

function GameObject:set_active(active)
	set_active(self.name, active)
end

function GameObject:set_visible(visible)
	set_visible(self.name, visible)
end

function GameObject:instance(instantiable, name)
	return instance_in_same(instantiable, name, self.name)
end

function GameObject:instance_return(instantiable, name)
	new_object_name = self:instance(instantiable, name)
	return engine:get_object_by_name(new_object_name)
end

function GameObject:instance_in_layer(instantiable, name, layer)
	return instance_in_layer(instantiable, name, self.name, layer)
end

function GameObject:instance_in_layer_return(instantiable, name, layer)
	new_object_name = self:instance_in_layer(instantiable, name, layer)
	return engine:get_object_by_name(new_object_name)
end

function GameObject:set_parameter(component_name, parameter_name, value)
	self.components[component_name]:set_parameter(parameter_name, value)
end

function GameObject:on_trigger_enter(name)
	for key,value in pairs(self.components) do
		value:on_trigger_enter(name)
	end
end

function GameObject:on_trigger_exit(name)
	for key,value in pairs(self.components) do
		value:on_trigger_exit(name)
	end
end

function GameObject:on_collision_enter(name)
	for key,value in pairs(self.components) do
		value:on_collision_enter(name)
	end
end

function GameObject:on_collision_stay(name)
	for key,value in pairs(self.components) do
		value:on_collision_stay(name)
	end
end

function GameObject:on_collision_exit(name)
	for key,value in pairs(self.components) do
		value:on_collision_exit(name)
	end
end

function GameObject:on_shape_hit(name, hitWorldPosition, hitWorldNormal)
	for key,value in pairs(self.components) do
		value:on_shape_hit(name, hitWorldPosition, hitWorldNormal)
	end
end


-- ////////////////////////////////////////// --



-- /////////// BASE COMPONENT CLASS /////////// --
class 'BaseComponent'

function BaseComponent:__init(name)
	add_new_info_log("Creando " .. name)
	self.name = name
	self.gameObject = nil
	self.transform = nil
end

function BaseComponent:update(dt)
end

function BaseComponent:start()
end

function BaseComponent:ondestroy()
end

function BaseComponent:set_parameter(parameter_name, value)
	self[parameter_name] = value
end

function BaseComponent:on_trigger_enter(name)	
end

function BaseComponent:on_trigger_stay(name, dt)	
end

function BaseComponent:on_trigger_exit(name)	
end

function BaseComponent:on_collision_enter(name)	
end

function BaseComponent:on_collision_stay(name)	
end

function BaseComponent:on_collision_exit(name)	
end

function BaseComponent:on_shape_hit(name, hitWorldPosition, hitWorldNormal)	
end

-- ////////////////////////////////////////// --


add_new_info_log("Ejecutando script gameObject")